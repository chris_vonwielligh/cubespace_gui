import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QWidget, QVBoxLayout
from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot, QThread
from PyQt5 import QtCore
from PyQt5 import QtGui

from logger import Logger
import pyqt_gui
import pyqt_gui_exitpromt
from tctlm_wrapper import TCTLM_SignalWrapper
from file_handlers import *
import time
from datetime import datetime, timedelta
import os
import numpy as np
# from PIL import Image, ImageQt
import csv
import json
from struct import *
import traceback
import matplotlib as mpl
from matplotlib import dates
import math
from warnings import warn as real_warn
from standa_wrapper import standa_SignalWrapper
import json
from cubedock import CubeDock

class AppThreaded(QMainWindow):
    '''This class handles the entire program, the GUI and input things'''

    auto_update = False
    current_image = None
    loggers = None
    rotation_active = False
    rotation_paused = False
    active_axis = 1

    serialNumber = "SN"

    sig_init_axis = QtCore.pyqtSignal(object)
    sig_step_axis = QtCore.pyqtSignal(object, object)
    sig_capture_action = QtCore.pyqtSignal(object, object, int)

    progressbar_total_ticks = 100
    progressbar_tick_count = 0

    active_directory = 'none'

    samples_per_step = 1


    #_sig_send_generic_tctlm = QtCore.pyqtSignal(object)
    _sig_echo = QtCore.pyqtSignal(object)

    class GenericRotation(object):
        def __init__(self, start, stop, step, number):
            self.start = start
            self.stop = stop
            self.step = step
            self.currrentPos = start
            self.state = 'idle'
            self.direction = 'forward'
            self.number = number


    def __init__(self, loggers=None):
        super(AppThreaded, self).__init__()

       # if loggers: self.loggers = loggers
     #   self.data_to_log = None

        self.standa = standa_SignalWrapper(False, loggers=None)
        self.standa_thread = QThread()
        self.standa.moveToThread(self.standa_thread)
        self.standa_thread.start()

        self.tctlm = TCTLM_SignalWrapper(False, loggers=None)
        self.tctlm_thread = QThread()
        self.tctlm.moveToThread(self.tctlm_thread)
        self.tctlm_thread.start()

        self.axis1 = self.GenericRotation(-100, 100, 10, 1)
        self.axis2 = self.GenericRotation(-100, 100, 10, 2)

        '''Initialise UI'''
        self.ui = pyqt_gui.Ui_MainWindow()
        self.ui.setupUi(self)
        self._setup_my_ui()

        '''Start the recurrent timer'''
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.timer_handler)
        self.timer.start(1000)

        self.setup_standa_sigs()

    def _setup_my_ui(self):
        '''Setup manual button controls (Some are straight connected to signals'''

        self.ui.btn_start.clicked.connect(self.butt_fcn_start)
        self.ui.btn_pause.clicked.connect(self.btn_fcn_pause)
        self.ui.btn_stop.clicked.connect(self.btn_fcn_stop)
        self.ui.btn_restart.clicked.connect(self.btn_fcn_restart)

        self.ui.btn_pause.setEnabled(False)
        self.ui.btn_stop.setEnabled(False)
        self.ui.btn_restart.setEnabled(False)

        self.ui.spinBox_axis1_start.setMinimum(-180)
        self.ui.spinBox_axis1_stop.setMinimum(-180)
        self.ui.spinBox_axis2_start.setMinimum(-180)
        self.ui.spinBox_axis2_stop.setMaximum(180)

        self.ui.spinBox_axis2_step.setValue(10)
        self.ui.spinBox_axis1_step.setValue(10)
        self.ui.spinBox_axis1_start.setValue(-100)
        self.ui.spinBox_axis1_stop.setValue(100)
        self.ui.spinBox_axis2_start.setValue(-100)
        self.ui.spinBox_axis2_stop.setValue(100)

        self.ui.btn_exit.clicked.connect(self.btn_fcn_exit)

        self.ui.btn_browse_directory.clicked.connect(self.btn_fcn_browse)
        self.ui.lineEdit_directory.setText("Enter Directory")

        self.ui.btn_load_preset.clicked.connect(self.btn_fcn_loadpreset)
        self.ui.btn_save_new_preset.clicked.connect(self.btn_fcn_savepreset)

        self.ui.display_serialNumber.setText(self.serialNumber)
        self.ui.label_active_sn.setText(self.serialNumber)
        self.ui.btn_new_serialNumber.clicked.connect(self.btn_fcn_update_sn)

        self.ui.progressBar_calibration.setMinimum(0)
        self.ui.progressBar_calibration.setMaximum(self.progressbar_total_ticks)
        self.ui.progressBar_calibration.setValue(0)

        self.ui.spinBox_number_samples.setValue(1)

#        self.ui.butt_comm_refresh.clicked.connect(self.butt_fcn_refresh_comm_list)
        self.ui.btn_refresh_comms2.clicked.connect(self.butt_fcn_refresh_comm_list_cubeIR)
        self.ui.btn_connect_cubeIR.clicked.connect(self.btn_fcn_comm_connect_cubeIR)


    def setup_standa_sigs(self):
        self.standa.sig_action_complete.connect(self.slot_standa_ready_response)
        self.standa.sig_axis_init_complete.connect(self.slot_axis_init_response)

        self.sig_init_axis.connect(self.standa.slot_init_axis)
        self.sig_step_axis.connect(self.standa.slot_rotate_step)
        self._sig_echo.connect(self.tctlm.slot_echo)
        self.tctlm.sig_echo.connect(self.slot_echo)

        self.tctlm.sig_capture_complete.connect(self.standa.slot_rotate_step)
        self.sig_capture_action.connect(self.tctlm.slot_capture_img)


    def update_gui_fields(self, tlm):
        self.ui.display_firmware.setText("NOT_REAL_TEST")

        '''Populates the GIU and data to log'''


        # # Get some data from the CubeDock if we are connected to it
        # if self.cubedock.uart.is_open() and len(self.cubedock.tlms) > 0:
        #     self.ui.check_cubedock_pwr3v3_1state.setChecked(self.cubedock.tlms['Pwr_3V3_1'])
        #     self.ui.text_cubedock_pwr3v3_1curr.setText("%.2fmA" % self.cubedock.tlms['Curr_3V3_1'])
        #     data_to_log[self.loggers['data'].key_index("CurrentCubeDock3V3_1")] = str(self.cubedock.tlms['Curr_3V3_1'])
        #     # If want to see afterwards when switch was on/off, go see the event log



    def update_progressbar(self):
        self.ui.progressBar_calibration.setValue(self.progressbar_tick_count)


    def reset_rotation(self):
        self.axis1.start = self.ui.spinBox_axis1_start.value()
        self.axis1.stop = self.ui.spinBox_axis1_stop.value()
        self.axis1.step = self.ui.spinBox_axis1_step.value()
        self.axis2.start = self.ui.spinBox_axis2_start.value()
        self.axis2.stop = self.ui.spinBox_axis2_stop.value()
        self.axis2.step = self.ui.spinBox_axis2_step.value()
        self.samples_per_step = self.ui.spinBox_number_samples.value()
        self.rotation_active = False
        axis1ticks = (self.axis1.stop - self.axis1.start)/self.axis1.step
        axis2ticks = (self.axis2.stop - self.axis2.start)/self.axis2.step
        self.progressbar_total_ticks = int(axis1ticks*axis2ticks)
        self.progressbar_tick_count = 0
        self.ui.progressBar_calibration.setMaximum(self.progressbar_total_ticks)
        self.update_progressbar()
        self.ui.btn_start.setEnabled(False)
        self.active_axis = 1
        dt_string = datetime.now().strftime("%y%m%d_%H%M")
        directory = self.ui.lineEdit_directory.text() + '/' + self.serialNumber + "/run_" + dt_string
        print(directory)
        self.active_directory = directory
        if not os.path.exists(directory):
            os.makedirs(directory)
        if not os.path.exists(directory + '/images'):
            os.makedirs(directory + '/images')
        csv_filename = directory + '/datalog.csv'
        headers = 'time, axis1 position, axis2 position, img name'
        with open(csv_filename, 'w') as fp:
            fp.write(headers + '\n')


    def butt_fcn_start(self):
        if(self.rotation_active == False):
            self.event_log_update('Start Button Pressed.')
            self.reset_rotation()
            self.move_standa()
        elif(self.rotation_paused):
            self.event_log_update('Rotation Resumed.')
            self.ui.btn_start.setText('START')
            self.ui.btn_start.setEnabled(False)
            self.resume_standa_rotation()

    def btn_fcn_pause(self):
        if(self.rotation_active):
            self.event_log_update('Rotation Paused')
            self.ui.btn_start.setText('RESUME')
            self.ui.btn_start.setEnabled(True)
            self.rotation_paused = True
            self.ui.btn_pause.setEnabled(False)

    def btn_fcn_stop(self):
        return

    def btn_fcn_restart(self):
        return

    def btn_fcn_exit(self):
        sys.exit(app.exec_())

    def btn_fcn_update_sn(self):
        self.serialNumber = self.ui.display_serialNumber.text()
        self.ui.label_active_sn.setText(self.serialNumber)

    def btn_fcn_browse(self):
        file = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
        self.ui.lineEdit_directory.setText(file)

    def btn_fcn_loadpreset(self):
        preset_file_name = QFileDialog.getOpenFileName(self, "Open Preset", "", "JSON files (*.json)")
        print(preset_file_name[0])
        with open(preset_file_name[0]) as json_file:
            data = json.load(json_file)
            self.ui.spinBox_axis2_step.setValue(data['axis2_step'])
            self.ui.spinBox_axis1_step.setValue(data['axis1_step'])
            self.ui.spinBox_axis1_start.setValue(data['axis1_start'])
            self.ui.spinBox_axis1_stop.setValue(data['axis1_stop'])
            self.ui.spinBox_axis2_start.setValue(data['axis2_start'])
            self.ui.spinBox_axis2_stop.setValue(data['axis2_stop'])
            self.ui.lineEdit_directory.setText(data['logDirectory'])
            self.ui.spinBox_number_samples.setValue(data['samples_per_iteration'])
        self.btn_fcn_update_sn()

    def btn_fcn_savepreset(self):
        data = {}
        data['axis1_start'] = self.ui.spinBox_axis1_start.value()
        data['axis1_stop'] = self.ui.spinBox_axis1_stop.value()
        data['axis1_step'] = self.ui.spinBox_axis1_step.value()
        data['axis2_start'] = self.ui.spinBox_axis2_start.value()
        data['axis2_stop'] = self.ui.spinBox_axis2_stop.value()
        data['axis2_step'] = self.ui.spinBox_axis2_step.value()
        data['logDirectory'] = self.ui.lineEdit_directory.text()
        data['samples_per_iteration'] = self.ui.spinBox_number_samples.value()
        pathName = QFileDialog.getSaveFileName(self, 'Save File', '', "JSON files (*.json)")
        print(pathName[0])
        with open(pathName[0], 'w') as outfile:
            json.dump(data, outfile)

    # def butt_fcn_refresh_comm_list(self):
    #     '''Refreshes the comms list'''
    #     if not self.tctlm.is_open():
    #         print('btnpres')
    #         self.ui.combo_comm_ports.clear()
    #         ports = self.tctlm.comms.uart.return_list_of_ports(all=True)
    #         for port in ports:
    #             self.ui.combo_comm_ports.addItem(port.description, port.device)

    def butt_fcn_refresh_comm_list_cubeIR(self):
        '''Refreshes the comms list'''
        if not self.tctlm.is_open():
            print('btnpres')
            self.ui.comboBox_cubeIR_comms.clear()
            ports = self.tctlm.comms.uart.return_list_of_ports(all=True)
            for port in ports:
                self.ui.comboBox_cubeIR_comms.addItem(port.description, port.device)

    def btn_fcn_comm_connect_cubeIR(self):
        port = str(self.ui.comboBox_cubeIR_comms.currentData())
        print(port)
        if self.tctlm.comms.uart.connect_to_port(port):
            self.tctlm.comms.channel = 'UART'
            self.event_log_update('Connected to TCTLM')
            self.updateTelemetries()
        else:
            print('Error connecting to port')

    def updateTelemetries(self):
        identification = self.tctlm.get_identification()
        print(identification)
        hardware_state = self.tctlm.get_hardware_state()
        print(hardware_state)
        htpa_calibration = self.tctlm.get_HTPA_Config_Info()
        print(htpa_calibration)
        self.ui.display_interface.setText(str(identification.InterfaceVersion))
        self.ui.display_firmware.setText(str(identification.FirmwareVersionMajor) + '.' + str(identification.FirmwareVersionMinor))
        m, s = divmod(identification.RunTimeSeconds, 60)
        h, m = divmod(m, 60)
        self.ui.display_runtime.setText('{:d}:{:02d}:{:02d}'.format(h, m, s))
        self.ui.display_current3v3.setText('{0:.4f}'.format(hardware_state.current3V3))
        self.ui.display_current_htpa.setText('{0:.4f}'.format(hardware_state.currentHTPA))
        self.ui.display_temp_mcu.setText('{0:.4f}'.format(hardware_state.temperatureMCU))
        self.ui.display_temp_htpa.setText('{0:.4f}'.format(hardware_state.temperatureHTPA))
        self.ui.display_voltage_htpa.setText('{0:.4f}'.format(hardware_state.voltageHTPA))
        self.ui.display_voltage_mcu.setText('{0:.4f}'.format(hardware_state.voltageMCU))
        self.ui.display_htpa_deviceID.setText(str(htpa_calibration.DeviceID))
        self.ui.display_htpa_tableNumber.setText(str(htpa_calibration.TableNumber))

    def move_standa(self):
        self.ui.btn_pause.setEnabled(True)
        self.ui.btn_stop.setEnabled(True)
        self.ui.btn_restart.setEnabled(True)
        self.rotation_active = True
        self.rotation_paused = False
        self.active_axis = 1
        self.sig_init_axis.emit(self.axis1)

    def resume_standa_rotation(self):
        self.ui.btn_pause.setEnabled(True)
        self.ui.btn_stop.setEnabled(True)
        self.ui.btn_restart.setEnabled(True)
        self.rotation_active = True
        self.rotation_paused = False
        self.event_log_update('Rotation resumed...')
        new_img_name = self.active_directory + '/images/img_' + str(self.axis1.currrentPos) + '_' + str(self.axis2.currrentPos)
        if(self.active_axis == 1):
            self.slot_standa_ready_response(self.axis1, new_img_name)
        if(self.active_axis == 2):
            self.slot_standa_ready_response(self.axis2, new_img_name)


    def event_log_update(self, log_string):
        print(log_string)
        self.ui.textBrowser_eventlog.insertPlainText(log_string + '\n')
        if(self.active_directory != 'none'):
            fileName = self.active_directory + '/eventlog.txt'
            with open(fileName, 'a') as outfile:
                outfile.write(log_string + '\n')


    def timer_handler(self):
        return
        #self.ui.display_runtime.setText(time.strftime('%H:%M:%S'))

    @pyqtSlot(object, object)
    def slot_standa_ready_response(self, axis, img_name):
        if(self.rotation_paused):
            return
        else:
            self.log_angle_csv(img_name)
            if(axis.number == 2):
                self.active_axis = 2
                if(self.axis2.direction == 'forward'):
                    self.axis2.currrentPos += self.axis2.step
                    new_img_name = self.active_directory + '/images/img_' + str(self.axis1.currrentPos) + '_' + str(self.axis2.currrentPos)
                    if(self.axis2.currrentPos >= self.axis2.stop):
                        self.axis2.direction = 'backwards'
                        self.sig_capture_action.emit(self.axis1, new_img_name, self.samples_per_step)
                    else:
                        self.sig_capture_action.emit(self.axis2, new_img_name, self.samples_per_step)
                elif(self.axis2.direction == 'backwards'):
                    self.axis2.currrentPos -= self.axis2.step
                    new_img_name = self.active_directory + '/images/img_' + str(self.axis1.currrentPos) + '_' + str(self.axis2.currrentPos)
                    if(self.axis2.currrentPos <= self.axis2.start):
                        self.axis2.direction = 'forward'
                        self.sig_capture_action.emit(self.axis1, new_img_name, self.samples_per_step)
                    else:
                        self.sig_capture_action.emit(self.axis2, new_img_name, self.samples_per_step)
                self.progressbar_tick_count += 1
                self.update_progressbar()
            elif(axis.number == 1):
                self.active_axis = 1
                self.axis1.currrentPos += self.axis1.step
                new_img_name = self.active_directory + '/images/img_' + str(self.axis1.currrentPos) + '_' + str(self.axis2.currrentPos)
                if(self.axis1.currrentPos >= self.axis1.stop):
                    print("Rotations Complete")
                    self.event_log_update("Rotations Complete")
                    self.progressbar_tick_count = self.progressbar_total_ticks
                    self.update_progressbar()
                    self.ui.btn_pause.setEnabled(False)
                    self.ui.btn_stop.setEnabled(False)
                    self.ui.btn_restart.setEnabled(False)
                    self.ui.btn_start.setText('START')
                else:
                    self.sig_capture_action.emit(self.axis2, new_img_name, self.samples_per_step)


    @pyqtSlot(object)
    def slot_axis_init_response(self, axis):
        if(axis.number == 1):
            print("axis1 setup done")
            self.event_log_update("Axis 1 moved to start position: " + str(self.axis1.start) + " degrees.")
            self.axis1.currrentPos = self.ui.spinBox_axis1_start.value()
            self.sig_init_axis.emit(self.axis2)
        elif(axis.number == 2):
            print("axis2 setup done")
            self.event_log_update("Axis 2 moved to start position: " + str(self.axis1.start) + " degrees.")
            self.axis2.currrentPos = self.ui.spinBox_axis2_start.value()
            self.axis1.state = 'active'
            self.axis2.state = 'active'
            self.axis1.direction = 'forward'
            self.axis2.direction = 'forward'
            img_name = self.active_directory + '/images/img_' + str(self.axis1.currrentPos) + '_' + str(self.axis2.currrentPos)
            self.event_log_update('Starting Rotations...')
            self.active_axis = 2
            self.sig_capture_action.emit(self.axis2, img_name, self.samples_per_step)

    @pyqtSlot(object)
    def slot_echo(self, time_sent):
        print(time_sent)

    def warn(self, text, *args, **kwargs):
        '''Override of the inherent warning system'''
        tb = traceback.extract_stack()
        st = "".join(traceback.format_list(tb)[:-1])
        warning_text = st + '\n' + text

        # log the error here
        # if self.loggers: loggers['event'].log_text(warning_text, module="WARNING")

        # Raise the warning
        real_warn(warning_text)

    def log_angle_csv(self, img_name):
        timeString = datetime.now().strftime("%H:%M:%S")
        splitName = img_name.split("images/",1)[1]
        line = timeString + ',' + str(self.axis1.currrentPos) + ',' + str(self.axis2.currrentPos) + ',' + str(splitName) + '\n'
        if (self.active_directory != 'none'):
            fileName = self.active_directory + '/datalog.csv'
            with open(fileName, 'a') as fp:
                fp.write(line)


def my_excepthook(exctype, value, tb):

    # Ensure that <Ctr+C> still works
    if issubclass(exctype, KeyboardInterrupt):
        sys.__excepthook__(exctype, value, tb)
        return

    # log the exception here
    # if loggers: loggers['event'].log_text(traceback.format_exception(exctype, value, tb), module="EXCEPTION")

    # then call the default handler
    sys.__excepthook__(exctype, value, tb)

if __name__ == "__main__":
    print("Started: app_threaded.py")

    '''Sets up all the loggers and things'''
    # log_path = datetime.now().strftime('Logs/Logs_%Y_%m_%d/')
    # if not os.path.exists(log_path):
    #     os.makedirs(log_path)
    #
    # logger_comms = Logger(filename="comms_log", path=log_path)
    # logger_event = Logger(filename="event_log", path=log_path)
    # logger_data = Logger(filename="data_log", ext='.csv', path=log_path)
    #
    # loggers = {}
    # loggers['comms'] = logger_comms
    # loggers['event'] = logger_event
    # loggers['data'] = logger_data
    # loggers['path'] = log_path
    #
    # loggers['event'].log_text('App started running', module='MAIN')
    # loggers['data'].keys = ["Runtime",
    #                             "Current3V3", "Current3V3Min", "Current3V3Max",
    #                             "CurrentHTPA", "CurrentHTPAMin", "CurrentHTPAMax", "CurrentCubeDock3V3_1",
    #                             "TemperatureMCU", "TemperatureMCUMin", "TemperatureMCUMax",
    #                             "TemperatureHTPA", "TemperatureHTPAMin", "TemperatureHTPAMax",
    #                             "VoltageMCU", "VoltageMCUMin", "VoltageMCUMax",
    #                             "VoltageHTPA", "VoltageHTPAMin", "VoltageHTPAMax",
    #                             "ADSSampleMode", "HTPACalibLevel",
    #                             "HILEnabled", "HILBusy", "HILEnableImgVddComp", "HILEnableImgTobjCalib", "HILEnableEdgeDet", "HILEnableLensDistCorr", "HILEnablePolyFit", "HILEnableAttEst",
    #                             "HTPADeviceID","HTPATableNumber",
    #                             "HtpaPowerState", "HtpaWakeState", "HtpaCaptureState", "HtpaPTAT", "HtpaVdd",
    #                             "HTPAImageLastMin", "HTPAImageLastMean", "HTPAImageLastMax", "HTPAImageAllTimeMin","HTPAImageAllTimeMax",
    #                             "SRAM2ParityError", "FlashCorruption",
    #                             "3V3OverCurrent", "HTPAOverCurrent", "Timer1sOverRun",
    #                             "MCUOverTemperature", "MCUUnderTemperature", "HTPAOverTemperature", "HTPAUnderTemperature","HTPACalibLookupOutOfRange",
    #                             "HTPAIntegrationTimeError", "HTPANotResponding", "HTPAConfigMismatch", "HTPASpiTimeout", "CommsTcSlotsBuffFull", "CommsTcParamBuffFull",
    #                             "CommsUartProtError", "CommsUartMsgIncomplete", "CommsUartTcParamOverflow", "UARTTxOverflow",
    #                             "CommsTcInvalidID", "CommsTcInvalidPar", "CommsTcInvalidParLen", "CommsTcTimeout", "CommsTcCrcError", "CommsTlmInvalidID",
    #                             "UARTTxReadingEmptyBuffer", "UARTRxByteOverrun", "UARTRxFramingError","UartTxTimeout",
    #                             "FRAMStatusNotResponding", "FRAMDeviceNotFound", "FRAMSpiTimeout",
    #                             "TcAckLastTCID","TcAckProcessed", "TcAckErrStatus", "TcAckParamErrIndex",
    #                             "ConfigRewrittenFromMemory", "ConfigExtMemorySlotOverwritten", "ConfigNoConfigFoundInMemory", "ConfigNumOfValidFramSlots", "ConfigNumOfValidFlashSlots", "ConfigNotSaved", "ConfigDataLoss",
    #                             "SwStartupTime", "SwProcessingTime", "SwProcessingTimeMax", "SwLoopTime", "SwLoopTimeMax", "SwImgCaptureTime", "SwLensDistCorrTime", "SwEstimationTime",
    #                             "SwConfigRefreshMaxTime", "SwConfigCheckMemoryMaxTime", "SwAdcMeasurementTime", "SwHtpaIntegrationTime",
    #                             "DetectedBitFlips", "FlashFaults", "FlashLastFaultAddress",
    #                             "MemTestSramMode", "MemTestSramFaultsLastLoop", "MemTestSramFaultsTotal", "MemTestSramLoopNumber", "MemTestSramIterateTime",
    #                             "MemTestFlashMode", "MemTestFlashState", "MemTestFlashIndex", "MemTestFlashFaultPageIndex", "MemTestFlashPageWriteErrors","MemTestFlashEraseErrors","MemTestFlashPageWriteTime", "MemTestFlashPageReadTime", "MemTestFramTimeToWait",
    #                             "SRAM2ParityCheckEnabled", "InterfaceVersion", "FirmwareVersion", "IRCAM SN"]
    # headings = loggers['data'].keys[:]
    # headings.insert(0, 'Time')
    # loggers['data'].log_data(headings, add_time=False)

    #sys.excepthook = my_excepthook      # Show error messages in console

    app = QApplication(sys.argv)
    window = AppThreaded()
    window.show()
    sys.exit(app.exec_())
