from hil import HIL
from cubedock import CubeDock
from logger import Logger
from tctlm_wrapper import TCTLM_SignalWrapper
from tctlm import TCTLM
from PyQt5.QtCore import QThread, QObject, pyqtSignal, pyqtSlot

class Script(QThread):
    '''
    This is a base class to run scripts from
    '''

    _threaded = False       # Is this script using <tctlm> or <tctlm_wrapper>
    _name = "Script Name"
    _write_output_to_textfile = False

    tctlm = None

    def __init__(self, tctlm=None):
        QObject.__init__(self)

        self.tctlm = tctlm
        if isinstance(tctlm, TCTLM):
            # This script wil use non-threaded stuff
            self._threaded = False
        elif isinstance(tctlm, TCTLM_SignalWrapper):
            # This script will use the tctlm wrapper
            self._threaded = True

    def run_script(self):
        self.LogMessage("No <run> function set in script...")

    def LogMessage(self, message):
        print(message)


if __name__ == "__main__":
    script = Script()
    script.run_script()

