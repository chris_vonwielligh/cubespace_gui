import json
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

class HTPA:
    '''
    This class is to emulate the device's image processing
    This does not have anything to do with comms!
    '''

    eeprom = None
    lookup = None

    def __init__(self,eeprom=None,lookup=None):
        if eeprom is not None:
            self.load_eepromdata(eeprom)
        if lookup is not None:
            self.load_lookuptable(lookup)

    def validate(self,eeprom=None,lookup=None):
        '''Makes sure the lookup and the eeprom corresponds'''
        return (eeprom if eeprom is not None else self.eeprom)['TableNumber'] == \
            (lookup if lookup is not None else self.lookup)['TABLENUMBER']

    def load_lookuptable(self, lookup):
        '''Loads look-up table into module. Input is a dictionary'''
        self.lookup = lookup

    def set_ptat_vdd(self, ptat, vdd):
        ''' Sets the HTPA's internal PTAT and VDD'''
        self.ptat = ptat
        self.vdd = vdd

    def load_eepromdata(self, eeprom):
        '''Loads eeprom data into module. Input is a dictionary'''
        self.eeprom = eeprom

    def load_blinds_from_json(self, path):
        '''Loads and returns blind pixels from a json file, as created by the PC Interface Program'''
        with open(path, 'r') as f: json_str = f.read()
        blinds = json.loads(json_str)
        blinds['top'] = np.array(blinds['top'])
        blinds['bottom'] = np.array(blinds['bottom'])
        if np.min(blinds['top']) < 0:
            blinds['top'] += 2**16
            blinds['bottom'] += 2**16
        return blinds['top'], blinds['bottom']

    def remove_electrical_offsets(self, image, blinds_top, blinds_bottom):
        '''Removes electrical offsets (blinds) from raw image'''
        out = np.copy(image)
        w, h = image.shape
        for x in range(w):
            for y in range(int(h / 2)):
                out[x][y] -= blinds_top[x][y % 8]
                out[x][y + 32] -= blinds_bottom[x][y % 8]
        return out

    def remove_thermal_offset(self, image, eeprom=None, ptat=None):
        '''Removes the thermal offset from each pixel'''
        if ptat is None: ptat = self.ptat
        if eeprom is None: eeprom = self.eeprom
        out = np.copy(image)
        for x, y in np.ndindex(out.shape):
            out[x][y] -= eeprom['ThGradij'][x][y] * ptat / \
                           2 ** eeprom['gradScale'] + eeprom['ThOffsetij'][x][y]
        return out

    def do_vdd_compensation(self, image, eeprom=None, ptat=None, vdd=None):
        '''Do the supply voltage compensation'''
        if vdd is None: vdd = self.vdd
        if ptat is None: ptat = self.ptat
        if eeprom is None: eeprom = self.eeprom
        out = np.copy(image)
        for x, y in np.ndindex(out.shape):
            block_i = y % 8 if y < 32 else y % 8 + 8
            out[x][y] -= (eeprom['VddCompGrad'][x][block_i] * ptat / 2 ** eeprom['VddScGrad']
                          + eeprom['VddCompOff'][x][block_i]) / 2 ** eeprom['VddScOff'] \
                          * (vdd - eeprom['VddTh1'] -
                            ((eeprom['VddTh2'] - eeprom['VddTh1']) / (eeprom['PTATTh2'] - eeprom['PTATTh1']))
                            * (ptat - eeprom['PTATTh1']))
        return out

    def do_thermal_calibration(self, image, eeprom=None, lookup=None, ptat=None, force_Ta_seach=False, circ_mask_radius=None):
        '''Determine the object temeprature'''
        if ptat is None: ptat = self.ptat
        if eeprom is None: eeprom = self.eeprom
        if lookup is None: lookup = self.lookup
        out = np.zeros_like(image)
        w, h = image.shape

        # Calculate Ambient Temperature
        Ta = ptat * eeprom['PTATGradient'] + eeprom['PTATOffset']

        # Find lookup table column with ambient temperature
        if lookup['TAEQUIDIST'] and not force_Ta_seach:
            xTa = int(Ta - lookup['XTATemps'][0]) >> lookup['TADISTEXP']
        else:
            xTa = (np.abs(np.array(lookup['HTPA_LU_XTATemps']) - Ta)).argmin()
        dTa = Ta - lookup['XTATemps'][xTa]

        for x, y in np.ndindex(out.shape):

            # Ignore pixels outside the circular mask
            if circ_mask_radius is not None and \
                    (x-w/2)**2 + (y-h/2)**2 > circ_mask_radius**2:
                continue

            # Sensitivity coefficient calculation
            pixc = (eeprom['Pij'][x][y]*(eeprom['PixCMax']-eeprom['PixCMin']) / 65535 + eeprom['PixCMin']) \
                   * eeprom['epsilon'] * eeprom['GlobalGain']

            # Sensitivity compensated IR voltage
            vpixc = image[x][y] * lookup['PCSCALEVAL'] / pixc

            # Find row in lookup table
            val_to_search = vpixc + lookup['TABLEOFFSET']
            yV = int(np.ceil(val_to_search / 2 ** lookup['ADEXPBITS']))

            # Interpolate location in lookup table
            vx = (lookup['table'][yV][xTa+1] - lookup['table'][yV][xTa]) \
                 * dTa / lookup['TAEQUIDISTANCE'] + lookup['table'][yV][xTa]
            vy = (lookup['table'][yV+1][xTa+1] - lookup['table'][yV+1][xTa]) \
                 * dTa / lookup['TAEQUIDISTANCE'] + lookup['table'][yV+1][xTa]

            # Calculate the absolute temperature
            out[x][y] = (vy - vx) * (val_to_search - lookup['YADValues'][yV]) \
                        / lookup['ADEQUIDISTANCE'] + vx

        # If applying circular mask, fill outside with inside's mean
        if circ_mask_radius is not None:
            out[self.get_circular_mask(image.shape, circ_mask_radius, False)] \
                = np.mean(out[self.get_circular_mask(image.shape, circ_mask_radius)])

        return out

    def get_circular_mask(self, size, radius, inside_noutside=True):
        w, h = size
        x, y = np.ogrid[:w, :h]
        if inside_noutside:
            return (x - w / 2) ** 2 + (y - h / 2) ** 2 < radius ** 2
        else:
            return (x - w / 2) ** 2 + (y - h / 2) ** 2 > radius ** 2

if __name__ == '__main__':
    print("Running ", __file__)

    eeprom_path = 'Lookups and EEPROMS/eepromdata_137.json'
    lookup_path = 'Lookups and EEPROMS/LookupTable137.json'

    with open(eeprom_path, 'r') as f: eeprom = json.loads(f.read())
    with open(lookup_path, 'r') as f: lookup = json.loads(f.read())

    htpa = HTPA(eeprom, lookup)
    htpa.set_ptat_vdd(33364, 32538)
    print('Lookup Table and EEPROM Data', 'does' if htpa.validate() else 'DOESNT', 'line Up.')

    image_raw = np.genfromtxt('Scripts/input/imagedata_190815_143613.csv', delimiter=',', dtype=np.int32).transpose()
    blinds_top, blinds_bot = htpa.load_blinds_from_json('Scripts/input/htpa_blind_pixels_190816_111512.json')



    fig = plt.figure()

    ax1 = fig.add_subplot(2, 3, 1)
    im1 = plt.imshow(image_raw.transpose(), interpolation='None', cmap='jet')
    plt.title('Raw Image')
    divider = make_axes_locatable(ax1)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(im1, cax=cax, orientation='vertical')

    image_elloffed = htpa.remove_electrical_offsets(image_raw, blinds_top, blinds_bot)
    ax1 = fig.add_subplot(2, 3, 2)
    im1 = plt.imshow(image_elloffed.transpose(), interpolation='None', cmap='jet')
    plt.title('Image with \nblinds removed')
    divider = make_axes_locatable(ax1)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(im1, cax=cax, orientation='vertical')

    imaged_thermal_offset_removed = htpa.remove_thermal_offset(image_elloffed)
    ax1 = fig.add_subplot(2, 3, 3)
    im1 = plt.imshow(imaged_thermal_offset_removed.transpose(), interpolation='None', cmap='jet')
    plt.title('Image with thermal \noffset removed')
    divider = make_axes_locatable(ax1)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(im1, cax=cax, orientation='vertical')

    image_vdd_compensated = htpa.do_vdd_compensation(imaged_thermal_offset_removed, eeprom)
    ax1 = fig.add_subplot(2, 3, 4)
    im1 = plt.imshow(image_vdd_compensated.transpose(), interpolation='None', cmap='jet')
    plt.title('Image Vdd\nCompensated')
    divider = make_axes_locatable(ax1)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(im1, cax=cax, orientation='vertical')

    image_tobj = htpa.do_thermal_calibration(image_vdd_compensated, eeprom, lookup,
                                             circ_mask_radius=44)
    ax1 = fig.add_subplot(2, 3, 5)
    im1 = plt.imshow(image_tobj.transpose()/10-273.15, interpolation='None', cmap='jet')
    plt.title('Image Tobj\nCalibrated')
    divider = make_axes_locatable(ax1)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(im1, cax=cax, orientation='vertical')

    plt.show()





