from ctypes import *
import time
import os
import sys
import tempfile
import re
import numpy as np

# Import the required libraries and such
# import urllib.parse
# cur_dir = os.path.abspath(os.path.dirname(__file__))
# ximc_dir = os.path.join(cur_dir, "..", "..", "ximc")
# ximc_package_dir = os.path.join(ximc_dir, "crossplatform", "wrappers", "python")
# sys.path.append(ximc_package_dir)  # add ximc.py wrapper to python path
#
# if sys.platform in ("win32", "win64"):
#     libdir = os.path.join(ximc_dir, sys.platform)
#     os.environ["Path"] = libdir + ";" + os.environ["Path"]  # add dll
#
# if sys.version_info >= (3,0):
#     import urllib.parse
#     try:
#         from pyximc import *
#     except ImportError as err:
#         print ("Can't import pyximc module.\
#                The most probable reason is that \
#                you haven't copied pyximc.py to the working directory.\
#                See developers' documentation for details.")
#         exit()
#     except OSError as err:
#         print ("Can't load libximc library.\
#                Please add all shared libraries to the appropriate places \
#                (next to pyximc.py on Windows). \
#                It is decribed in detailes in developers' documentation.")
#         exit()
#
# # variable 'lib' points to a loaded library
# # note that ximc uses stdcall on win
# sbuf = create_string_buffer(64)
# lib.ximc_version(sbuf)

class Motor():
    '''
    This class handles a single STANDA rotation stage.
    '''

    def __init__(self, device_name=None):
        self.device_id = 1
#        self.lib = lib
      #   self.device_id = self.open_device(device_name)

    def rotate_rate(self, rate):
        '''
        Rotate the motor at a <rate> until <stop> is called
        :param rate:
        :return:
        '''
        assert(False,"Constant rate not implemented")

    def rotate_step(self, angle, wait=False):
        '''
        Rotates the stage <angle> radians
        :param angle:
        :param wait: Set this to true if code should wait for movement to finish
        :return:
        '''
        self.lib.command_move(self.device_id, *self.rad2steps(angle))
        if wait:
            self.wait_while_busy()

    def goto(self, angle, wait=False):
        '''
        Command the stage to rotate to <angle>
        :param angle:
        :param wait:
        :return:
        '''
        self.lib.command_movr(self.device_id, *self.rad2steps(angle))
        if wait:
            self.wait_while_busy()

    def position(self):
        '''
        Returns the current angle of the stage
        :return:
        '''
        pos = get_position_t()
        self.lib.get_position(self.device_id, byref(pos))   # Get's position by reference
        return self.steps2rad(pos.Position, pos.uPosition)

    def get_status(self):
        status = status_t()
        result = self.lib.get_status(self.device_id, byref(status))
        if result == Result.Ok:
            return status
        else:
            None

    def stop(self):
        '''
        Stop the stage in it's tracks
        :return:
        '''
        self.lib.command_stop(self.device_id)

    def wait_while_busy(self):
        '''
        Will hold up the program while stage is rotating.
        :return:
        '''
        self.lib.command_wait_for_stop(self.device_id, 25)

    @staticmethod
    def rad2steps(angle):
        step = int(np.rad2deg(angle) * 100)
        ustep = int((np.rad2deg(angle) - step/100.0)*25600)
        return [step, ustep]
    @staticmethod
    def steps2rad(step, ustep):
        return np.deg2rad(float(step)/100.0 + float(ustep)/25600.0)

    def mkvirtual_device(self, device_name):

        # use URI for virtual device when there is new urllib python3 API
        tempdir = tempfile.gettempdir() + "/" + str(device_name) + ".bin"

        # "\" <-> "/"
        if os.altsep:
            tempdir = tempdir.replace(os.sep, os.altsep)

        uri = urllib.parse.urlunparse(urllib.parse.ParseResult \
                                          (scheme="file", netloc=None, path=tempdir, \
                                           params=None, query=None, fragment=None))
        # converter address to b
        open_name = re.sub(r'^file', 'xi-emu', uri).encode()
        return open_name

    def enum_device(self):
        '''
        Return all connected standa devices
        :return:
        '''

        dev_enum = self.lib.enumerate_devices(EnumerateFlags.ENUMERATE_PROBE, None)
        dev_count = self.lib.get_device_count(dev_enum)

        return dev_enum, dev_count

    def open_device(self, open_name=None):
        '''
        Opens the Standa device <open_name>. If no name is supplied, but there
        is one connected, it will connect to the first one. Otherwise it will
        create and connect to a virtual device. It will also create a virtual
        device if no devices are found
        :param open_name:
        :return:
        '''
        dev_enum, dev_count = self.enum_device()
        if open_name is None:
            if dev_count > 0:
                open_name = self.lib.get_device_name(dev_enum, 0)
            else:
                open_name = self.mkvirtual_device("VirtualDevice")

            if type(open_name) is str:
                open_name = open_name.encode()

            device_id = self.lib.open_device(open_name)
            return device_id
        else:
            if dev_count > 0:
                if type(open_name) is str:
                    open_name = open_name.encode()
                device_id = self.lib.open_device(open_name)
            else:
                open_name = self.mkvirtual_device(open_name)
                device_id = self.lib.open_device(open_name)
            return device_id

if __name__ == "__main__":
    print("Start <standa.py>")
    m = Motor()
    # m.rotate_step(np.deg2rad(2),wait=True)
    # print(m.position())
    # m.goto(np.deg2rad(3))
    # m.wait_while_busy()
    # print(m.position())


    a = np.deg2rad(1.2345)
    print("Angle", np.rad2deg(a))
    steps = m.rad2steps(a)
    print("Steps", steps)
    print("Steps to angle", np.rad2deg(m.steps2rad(*steps)))