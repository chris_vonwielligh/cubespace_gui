from comms import Comms
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot, QThread
from time import sleep
from tctlm import TCTLM


class CubeDock(QObject, Comms):
    sig_comms_error_not_responding = pyqtSignal()
    sig_comms_error_responding = pyqtSignal()


    tlms = {}
    def __init__(self):
        QObject.__init__(self)
        Comms.__init__(self, auto_connect=False)

    def attempt_comms(self, tctlm, params, raise_exception=False):
        '''Attempts comms <tctlm>
        This first argument in <*args> specifies if the error should be raised again'''

        if self.uart.is_open():
            try:
                # Try comms
                rx = tctlm(*params)
                self.sig_comms_error_responding.emit()
                return rx
            except Exception as e:
                # Device didn't respond
                self.sig_comms_error_not_responding.emit()
                if raise_exception:
                    raise e

    @pyqtSlot()
    def slot_update_all_fields(self):
        try:
            self.tlms['Pwr_3V3_1'] = self.attempt_comms(self.tlm, [139, 0.01])[1] & 4 == 4
            self.tlms['Curr_3V3_1'] = TCTLM.bytes2int16(self.attempt_comms(self.tlm, [131, 0.01])[4:6])/100

        except (ValueError, TypeError):
            pass  # Already handled by <attempt_comms>

    @pyqtSlot(bool)
    def slot_set_pwr_3V3_1(self, pwr):
        self.attempt_comms(self.tc, [4, [pwr << 2]])


if __name__ == '__main__':
    print('Started <cubedock.py>')

    cd = CubeDock()
