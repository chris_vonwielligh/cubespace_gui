from collections import namedtuple
from PyQt5.QtCore import QThread, QObject, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QFileDialog

from PyQt5 import QtCore, QtWidgets
from tctlm_wrapper import TCTLM_SignalWrapper
from time import sleep
import csv
from datetime import datetime
import numpy as np
import json
import os
import math
import matplotlib as mpl
from PIL import Image, ImageQt

class BLINDS2JSON(QThread):
    '''Downloads blind pixels from the processor and stores in json file'''
    # TODO This function is not threaded yet!

    sig_set_progress_bar = QtCore.pyqtSignal(float, float)

    def __init__(self, tctlm, json_path):
        super(BLINDS2JSON, self).__init__()
        self.tctlm = tctlm
        self.json_path = json_path

    def run(self):
        # Download the blinds
        blinds = self.tctlm.get_blind_pixels()
        blinds_json = {'top':blinds.Top.tolist(), 'bottom':blinds.Bottom.tolist()}

        # Save the JSON file
        with open(self.json_path + "/htpa_blind_pixels_" + datetime.now().strftime('%y%m%d_%H%M%S') + ".json", 'w') as fp:
            json.dump(blinds_json, fp)


class CSV2BMP(QThread):
    '''Turns a list of *'csv images into bmp images'''

    sig_set_progress_bar = QtCore.pyqtSignal(float, float)

    def __init__(self, csv_paths, bmp_path, min_val, max_val):
        super(CSV2BMP, self).__init__()
        self.csv_paths = csv_paths
        self.bmp_path = bmp_path
        self.min_val = min_val
        self.max_val = max_val

    def run(self):

        for i, path in enumerate(self.csv_paths):
            # Found a file and now read it
            image = np.genfromtxt(path, delimiter=',', dtype=np.int16)
            name_to_save = 'csv_img_' + os.path.basename(path)[-17:-4] + '.bmp'

            image = ((image-self.min_val)/(self.max_val - self.min_val) * 255)
            image = np.clip(image, 0, 255).astype(np.uint8)

            cm_hot = mpl.cm.get_cmap('jet')
            image = cm_hot(image)
            image = np.uint8(image * 255)
            image = Image.fromarray(image)
            image.save("/".join(self.bmp_path.split('/')[:-1]) + '/' + name_to_save)

            self.sig_set_progress_bar.emit(i+1, len(self.csv_paths))

class DeviceEeprom2Csv(QThread):
    '''The idea is that this thread creates a thread for each task it has to do'''

    _sig_send_generic_tctlm = QtCore.pyqtSignal(object)

    EEPROM_SIZE = 32768  # In bytes
    EEPROM_READ_SIZE = 64  # As done in firmware
    ROW_LENGTH = 16

    def __init__(self, path, tctlm):
        super(DeviceEeprom2Csv, self).__init__()
        self.tctlm = tctlm
        self.path = path

        self._sig_send_generic_tctlm.connect(self.tctlm.slot_handle_generic_tctlm)
        self.tctlm.sig_return_generic_TCTLM.connect(self.slot_receive_eeprom_page)

    @pyqtSlot(object)
    def slot_receive_eeprom_page(self, tctlm):

        if tctlm.tctlm != self.tctlm.EEPROM_Chunk:
            return      # This packet wasn't measnt for us
        page = tctlm.reply

        for j in range(0, 64, 16):  # Write the downloaded page
            self.wr.writerow(page[j:(j + self.ROW_LENGTH)])

        if isinstance(self.tctlm.sig_set_progress_bar, QtCore.pyqtBoundSignal):
            self.tctlm.sig_set_progress_bar.emit(self.pi + 1, self.EEPROM_SIZE / self.EEPROM_READ_SIZE)

        self.pi += 1
        if self.pi < int(self.EEPROM_SIZE/self.EEPROM_READ_SIZE):
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.EEPROM_read_at,
                    params=[self.pi * self.EEPROM_READ_SIZE]
                )])
            sleep(0.001)  # Assume this is how long it will take
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tctlm=self.tctlm.EEPROM_Chunk,
                    tlm_fun=self.tctlm.get_EEPROM_read
                )])
        else:
            self.file.close()
            self.done = True

    def run(self):
        name = self.path + "eepromdata_" + datetime.now().strftime('%y%m%d_%H%M%S') + '.csv'
        self.file = open(name, 'a', newline='')
        self.wr = csv.writer(self.file, dialect='excel')
        self.pi = 0
        self.done = False

        self._sig_send_generic_tctlm.emit([
            self.tctlm.Generic_TCTLM_t(
                tc_fun=self.tctlm.EEPROM_read_at,
                params=[self.pi * self.EEPROM_READ_SIZE]
            )])
        sleep(0.001)  # Assume this is how long it will take
        self._sig_send_generic_tctlm.emit([
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.EEPROM_Chunk,
                tlm_fun=self.tctlm.get_EEPROM_read
            )])

        while not self.done: pass

        self.tctlm.sig_return_generic_TCTLM.disconnect()


class EepromCsv2Json(QThread):
    '''Turn a raw EEPROM csv file into a decoded JSON file'''

    sig_set_progress_bar = QtCore.pyqtSignal(float, float)

    def __init__(self, csv_path, json_path):
        super(EepromCsv2Json, self).__init__()
        self.csv_path = csv_path
        self.json_path = json_path

    def run(self):

        def unpack_mirrored(arr, w, h):
            '''Unpack HTPA data in a typical mirrored fashion'''
            arr = np.reshape(arr, (h, w)).transpose()
            arr[:, int(h / 2):] = np.fliplr(arr[:, int(h / 2):])
            return arr.tolist()

        # Found a file and now read it
        data = np.genfromtxt(self.csv_path, delimiter=',', dtype=np.uint8)
        data = np.reshape(data, 32768).tolist()
        csvfile_name = os.path.basename(self.csv_path)

        self.sig_set_progress_bar.emit(20, 100)

        # Now create a JSON file and place data in it
        json_data = {}
        json_data['DeviceID'] = TCTLM_SignalWrapper.bytes2uint32(data[0x0074:0x00078])
        json_data['TableNumber'] = TCTLM_SignalWrapper.bytes2uint16(data[0x000B:0x000D])
        json_data['DateCreated'] = datetime.now().strftime('%y/%m/%d %H:%M:%S')
        json_data['CSVFilePath'] = self.csv_path
        json_data['CSVFileName'] = csvfile_name
        json_data['PixCMin'] = TCTLM_SignalWrapper.bytes2float(data[0x0000:0x0004])
        json_data['PixCMax'] = TCTLM_SignalWrapper.bytes2float(data[0x0004:0x0008])
        json_data['gradScale'] = data[0x0008]
        json_data['epsilon'] = data[0x000D] / 100
        json_data['MBITc'] = data[0x001A] & 0x0F
        json_data['REFCALc'] = (data[0x001A] & 0x30) >> 4
        json_data['BIASc'] = data[0x001B] & 0x1F
        json_data['CLKc'] = data[0x001C] & 0x3F
        json_data['BPAc'] = data[0x001D] & 0x1F
        self.sig_set_progress_bar.emit(50, 100)
        json_data['PUc'] = data[0x001E]
        json_data['ArrayType'] = data[0x0022]
        json_data['VddTh1'] = TCTLM_SignalWrapper.bytes2uint16(data[0x0026:0x0028])
        json_data['VddTh2'] = TCTLM_SignalWrapper.bytes2uint16(data[0x0028:0x002A])
        json_data['PTATGradient'] = TCTLM_SignalWrapper.bytes2float(data[0x0034:0x0038])
        json_data['PTATOffset'] = TCTLM_SignalWrapper.bytes2float(data[0x0038:0x003C])
        json_data['PTATTh1'] = TCTLM_SignalWrapper.bytes2uint16(data[0x003C:0x003E])
        json_data['PTATTh2'] = TCTLM_SignalWrapper.bytes2uint16(data[0x003E:0x0040])
        json_data['VddScGrad'] = data[0x004E]
        json_data['VddScOff'] = data[0x004F]
        json_data['GlobalOff'] = data[0x0054]
        json_data['GlobalGain'] = TCTLM_SignalWrapper.bytes2uint16(data[0x0055:0x0057]) / 10000
        json_data['MBITu'] = data[0x0060] & 0x0F
        json_data['REFCALu'] = (data[0x0060] & 0x30) >> 4
        json_data['BIASu'] = data[0x0061] & 0x1F
        json_data['CLKu'] = data[0x0062] & 0x3F
        json_data['BPAu'] = data[0x0063] & 0x1F
        json_data['PUu'] = data[0x0064]
        self.sig_set_progress_bar.emit(60, 100)
        json_data['NrOfDefPixels'] = data[0x007F]
        json_data['DeadPixAdr'] = TCTLM_SignalWrapper.bytes2uint16(data[0x0080:0x00B0])
        json_data['DeadPixMask'] = TCTLM_SignalWrapper.bytes2uint8(data[0x00B0:0x00C8])
        json_data['VddCompGrad'] = unpack_mirrored(TCTLM_SignalWrapper.bytes2int16(data[0x0800:0x1200]), 80, 16)
        json_data['VddCompOff'] = unpack_mirrored(TCTLM_SignalWrapper.bytes2int16(data[0x1200:0x1C00]), 80, 16)
        json_data['ThGradij'] = unpack_mirrored(TCTLM_SignalWrapper.bytes2int8(data[0x1C00:0x3000]), 80, 64)
        self.sig_set_progress_bar.emit(80, 100)
        json_data['ThOffsetij'] = unpack_mirrored(TCTLM_SignalWrapper.bytes2int16(data[0x3000:0x5800]), 80, 64)
        json_data['Pij'] = unpack_mirrored(TCTLM_SignalWrapper.bytes2uint16(data[0x5800:]), 80, 64)

        # Save the JSON file
        with open(self.json_path + "/eepromdata_" + csvfile_name[11:-4] + "_" + str(TCTLM_SignalWrapper.bytes2uint32(data[0x0074:0x00078])) + ".json", 'w') as fp:
            json.dump(json_data, fp)

        self.sig_set_progress_bar.emit(100, 100)

class Json2EepromCode(QThread):
    '''Turn a raw EEPROM csv file into a decoded JSON file'''

    sig_set_progress_bar = QtCore.pyqtSignal(float, float)

    def __init__(self, json_path, product_path):
        super(Json2EepromCode, self).__init__()
        self.json_path = json_path
        self.product_path = product_path


    def run(self):

        self.sig_set_progress_bar.emit(1, 24)

        with open(self.json_path, 'r') as f: json_str = f.read()
        data = json.loads(json_str)
        prefix = 'HTPA_ER'
        header = '/*\n' + ' * This file was autogenerated by the Python CubeIR PC Interface\n' + ' * Do not manually edit the contents of this file!\n' + ' *\n' + ' * This file was generated on:\t' + datetime.now().strftime(
            '%Y/%m/%d - %H:%M:%S') + '\n' + ' * The original *.csv file was located here:\n' + ' *\t\t' + data[
                     'CSVFilePath'] + '\n' + ' * The *.json file was located here:\n' + ' *\t\t' + self.json_path + '\n' + ' */\n'

        # Now build the header file
        with open(self.product_path + "/HTPA_eepromdata_" + str(data['DeviceID']) + ".h", 'w') as f:
            f.write(header)
            f.write('\n')
            f.write('#ifndef HTPA_EEPROMDATA_' + str(data['DeviceID']) + '_H_\n')
            f.write('#define HTPA_EEPROMDATA_' + str(data['DeviceID']) + '_H_\n')
            f.write('\n')
            f.write('#include "HTPA.h"   // Here we will make sure we are only adding this file if required\n')
            f.write('#if HTPA_DEVICE_ID == ' + str(data['DeviceID']) + '\n')
            f.write('\n')
            f.write('#include <stdint.h>\n')
            f.write('#include <stdbool.h>\n')
            f.write('#include <string.h>\n')
            f.write('\n')
            f.write('#include "HTPA_lookuptable_' + str(data['TableNumber']) + '.h"\n')
            f.write('\n')
            f.write('%-40s%s\n' % ('#define ' + prefix + '_DeviceID', str(data['DeviceID'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_TableNumber', str(data['TableNumber'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_PixCMax', str(data['PixCMax']) + 'f'))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_PixCMin', str(data['PixCMin']) + 'f'))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_GradScale', str(data['gradScale'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_Epsilon', str(data['epsilon']) + 'f'))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_MBITc', str(data['MBITc'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_REFCALc', str(data['REFCALc'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_BIASc', str(data['BIASc'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_CLKc', str(data['CLKc'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_BPAc', str(data['BPAc'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_PUc', str(data['PUc'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_ArrayType', str(data['ArrayType'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_VddTh1', str(data['VddTh1'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_VddTh2', str(data['VddTh2'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_PTATGradient', str(data['PTATGradient']) + 'f'))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_PTATOffset', str(data['PTATOffset']) + 'f'))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_PTATTh1', str(data['PTATTh1'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_PTATTh2', str(data['PTATTh2'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_VddScGrad', str(data['VddScGrad'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_VddScOff', str(data['VddScOff'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_GlobalGain', str(data['GlobalGain']) + 'f'))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_MBITu', str(data['MBITu'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_REFCALu', str(data['REFCALu'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_BIASu', str(data['BIASu'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_CLKu', str(data['CLKu'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_BPAu', str(data['BPAu'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_PUu', str(data['PUu'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_NrOfDefPixels', str(data['NrOfDefPixels'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_MaxNrOfDefPixels', '24'))
            f.write('%-40s%s\n' % ('extern const uint16_t ' + prefix + '_DeadPixAdr [];', ''))
            f.write('%-40s%s\n' % ('extern const uint8_t ' + prefix + '_DeadPixMask [];', ''))
            f.write('%-40s%s\n' % ('extern const int16_t ' + prefix + '_VddCompGrad [][16];', ''))
            f.write('%-40s%s\n' % ('extern const int16_t ' + prefix + '_VddCompOff [][16];', ''))
            f.write('%-40s%s\n' % ('extern const int8_t ' + prefix + '_ThGradij [][64];', ''))
            f.write('%-40s%s\n' % ('extern const int16_t ' + prefix + '_ThOffsetij [][64];', ''))
            f.write('%-40s%s\n' % ('extern const uint16_t ' + prefix + '_Pij [][64];', ''))
            f.write('\n')
            f.write('#endif // Check for correct DeviceID')
            f.write('\n')
            f.write('#endif // HTPA_EEPROMDATA_' + str(data['DeviceID']) + '_H_\n')

        self.sig_set_progress_bar.emit(2, 24)
        with open(self.product_path + "/HTPA_eepromdata_" + str(data['DeviceID']) + ".c", 'w') as f:
            f.write(header)
            f.write('\n')
            f.write('#include "HTPA_eepromdata_' + str(data['DeviceID']) + '.h"\n')
            f.write('\n')
            f.write('#include "HTPA.h"   // Here we will make sure we are only adding this file if required\n')
            f.write('#if HTPA_DEVICE_ID == ' + str(data['DeviceID']) + '\n')
            f.write('\n')
            f.write('// Make sure this source file corresponds with it\'s header file\n')
            f.write('#if ' + prefix + '_DeviceID != ' + str(data['DeviceID']) + '\n')
            f.write('#error // The generated *.c file doesn\'t correspond with the header file\n')
            f.write('#endif\n')
            f.write('\n')
            f.write('%-40s%s\n' % ('const uint16_t ' + prefix + '_DeadPixAdr []',
                                   '=  {' + ''.join(('%7.d,' % x) for x in data['DeadPixAdr'][:-1]) + '%7.d' %
                                   data['DeadPixAdr'][-1] + '};'))
            f.write('%-40s%s\n' % ('const uint8_t ' + prefix + '_DeadPixMask []',
                                   '=  {' + ''.join(('%7.d,' % x) for x in data['DeadPixMask'][:-1]) + '%7.d' %
                                   data['DeadPixMask'][-1] + '};'))
            f.write('%-40s%s\n' % ('const int16_t ' + prefix + '_VddCompGrad [][16]',
                                   '= ' + self.list2d2stringC(data['VddCompGrad'])))
            self.sig_set_progress_bar.emit(4, 24)
            f.write('%-40s%s\n' % ('const int16_t ' + prefix + '_VddCompOff [][16]',
                                   '= ' + self.list2d2stringC(data['VddCompOff'])))
            self.sig_set_progress_bar.emit(8, 24)
            f.write('%-40s%s\n' % ('const int8_t ' + prefix + '_ThGradij [][64]',
                                   '= ' + self.list2d2stringC(data['ThGradij'])))
            self.sig_set_progress_bar.emit(16, 24)
            f.write('%-40s%s\n' % ('const int16_t ' + prefix + '_ThOffsetij [][64]',
                                   '= ' + self.list2d2stringC(data['ThOffsetij'])))
            self.sig_set_progress_bar.emit(22, 24)
            f.write('%-40s%s\n' % (
            'const uint16_t ' + prefix + '_Pij [][64]', '= ' + self.list2d2stringC(data['Pij'])))
            self.sig_set_progress_bar.emit(24, 24)

            f.write('\n')
            f.write('#endif // Check for correct DeviceID')


    def list2d2stringC(self, l, gap=43, pb = None):
        '''Converts a 2D list to a string like C would read a 2D array'''
        s = '{'
        for x in range(np.shape(l)[0]):
            if x: s += ('%' + str(gap) + 's') % ''
            s += '{'
            for y in range(np.shape(l)[1]):
                s += '%7s' % str(l[x][y])
                if y < (np.shape(l)[1] - 1): s += ','
            s += '}' + (',\n' if x < (np.shape(l)[0] - 1) else '};')

            if pb:
                self.sig_set_progress_bar.emit(pb[0] + (x+1)*pb[1]/np.shape(l)[0], pb[2])
        return s




class Json2LookupCode(QThread):
    '''Turn a raw EEPROM csv file into a decoded JSON file'''

    sig_set_progress_bar = QtCore.pyqtSignal(float, float)

    def __init__(self, json_path, product_path, limit216bits=False):
        super(Json2LookupCode, self).__init__()
        self.json_path = json_path
        self.product_path = product_path
        self.limit216bits = limit216bits # Should we limit YADValues to 16-bits?

    def run(self):
        self.sig_set_progress_bar.emit(1, 100)

        with open(self.json_path, 'r') as f:
            json_str = f.read()
        data = json.loads(json_str)
        prefix = 'HTPA_LU'
        header = '/*\n' + ' * This file was autogenerated by the Python CubeIR PC Interface\n' + ' * Do not manually edit the contents of this file!\n' + ' *\n' + ' * This file was generated on:\t' + datetime.now().strftime(
            '%Y/%m/%d - %H:%M:%S') + '\n * The *.json was generated on:\t' + data[
                     'date_created'] + '\n * The *.json file was located here:\n' + ' *\t\t' + self.json_path + '\n' + ' */\n'

        '''
        # Some custom work, maybe limiting YADValues to use 16-bits
        # ------------------
        nrofadelements = data['NROFADELEMENTS']
        yadvalues = data['YADValues']
        table = data['table']
        if self.limit216bits:
            # We need to limit YAD to fit in 16-bit values. This has not been
            # detrimental to any results
            np_yadvalues = np.array(yadvalues)
            np_table = np.array(table)

            yad_max_index = np.argmin(np.abs(np_yadvalues - 2**16)) - 1
            np_yadvalues = np_yadvalues[:yad_max_index]

            table_max_row = np.argmin(np.abs(np_yadvalues - np_yadvalues[-1]))

            raise NotImplementedError


            This might not be worth the risk of trimming the data set. I'll rather
            just use 1.5kbyte more and use uint32_t

        # ------------------
        '''

        # Now build the files
        with open(self.product_path + "/HTPA_lookuptable_" + str(data['TABLENUMBER']) + ".h", 'w') as f:
            f.write(header)
            f.write('\n')
            f.write('#ifndef HTPA_LOOKUPTABLE_' + str(data['TABLENUMBER']) + '_H_\n')
            f.write('#define HTPA_LOOKUPTABLE_' + str(data['TABLENUMBER']) + '_H_\n')
            f.write('\n')
            f.write('#include <stdint.h>\n')
            f.write('#include <stdbool.h>\n')
            f.write('#include <string.h>\n')
            f.write('\n')
            f.write('%-40s%s\n' % ('#define ' + prefix + '_TABLENUMBER', str(data['TABLENUMBER'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_PCSCALEVAL', str(data['PCSCALEVAL'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_NROFTAELEMENTS', str(data['NROFTAELEMENTS'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_NROFADELEMENTS', str(data['NROFADELEMENTS'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_TAEQUIDISTANCE', str(data['TAEQUIDISTANCE'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_ADEQUIDISTANCE', str(data['ADEQUIDISTANCE'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_ADEXPBITS', str(data['ADEXPBITS'])))
            f.write('%-40s%s\n' % ('#define ' + prefix + '_TABLEOFFSET', str(data['TABLEOFFSET'])))
            try:
                f.write('%-40s%s\n' % ('#define ' + prefix + '_TAEQUIDIST', ''))
                f.write('%-40s%s\n' % ('#define ' + prefix + '_TADISTEXP', str(data['TADISTEXP'])))
            except KeyError:
                pass
            try:
                f.write('%-40s%s\n' % ('#define ' + prefix + '_EQUIADTABLE', ''))
            except KeyError:
                pass
            f.write('%-40s%s\n' % ('extern const uint16_t ' + prefix + '_XTATemps [];', ''))
            f.write('%-40s%s\n' % ('extern const uint32_t ' + prefix + '_YADValues [];', ''))
            f.write('%-40s%s\n' % (
                'extern const uint16_t ' + prefix + '_table [][' + prefix + '_NROFTAELEMENTS' + '];', ''))
            f.write('\n')
            f.write('#endif // HTPA_LOOKUPTABLE_' + str(data['TABLENUMBER']) + '_H_\n')
        with open(self.product_path + "/HTPA_lookuptable_" + str(data['TABLENUMBER']) + ".c", 'w') as f:
            self.sig_set_progress_bar.emit(10, 100)
            f.write(header)
            f.write('\n')
            f.write('#include "HTPA_lookuptable_' + str(data['TABLENUMBER']) + '.h"\n')
            f.write('\n')
            f.write('// Make sure this source file corresponds with it\'s header file\n')
            f.write('#if ' + prefix + '_TABLENUMBER != ' + str(data['TABLENUMBER']) + '\n')
            f.write('#error // The generated *.c file doesn\'t correspond with the header file\n')
            f.write('#endif\n')
            f.write('\n')
            f.write('%-40s%s\n' % ('const uint16_t ' + prefix + '_XTATemps []',
                                   '= {' + ''.join(('%7.d,' % x) for x in data['XTATemps'][:-1]) + '%7.d' %
                                   data['XTATemps'][-1] + '};'))
            self.sig_set_progress_bar.emit(20, 100)
            f.write('%-40s%s\n' % ('const uint32_t ' + prefix + '_YADValues []',
                                   '= ' + self.list1d2stringC(data['YADValues'], col=data['NROFTAELEMENTS'])))
            self.sig_set_progress_bar.emit(30, 100)
            f.write('%-40s\n%-40s%s\n' % (
                'const uint16_t ' + prefix + '_table [][' + 'HTPA_LU_NROFTAELEMENTS' + ']', '',
                '= ' + self.list2d2stringC(data['table'], pb=[30, 70, 100])))
            self.sig_set_progress_bar.emit(100, 100)

    def list2d2stringC(self, l, gap=43, pb = None):
        '''Converts a 2D list to a string like C would read a 2D array'''
        s = '{'
        for x in range(np.shape(l)[0]):
            if x: s += ('%' + str(gap) + 's') % ''
            s += '{'
            for y in range(np.shape(l)[1]):
                s += '%7s' % str(l[x][y])
                if y < (np.shape(l)[1] - 1): s += ','
            s += '}' + (',\n' if x < (np.shape(l)[0] - 1) else '};')

            if pb and x%10==0:
                self.sig_set_progress_bar.emit(pb[0] + (x+1)*pb[1]/np.shape(l)[0], pb[2])
        return s

    def list1d2stringC(self, l, gap=43, col=10):
        '''Converts a 1D list to a string like C would read a 1D array, but with <col> columns'''
        rows = math.ceil(len(l)/col)+1
        i = 0
        s = '{'
        for y in range(rows-1):
            if y: s += ('%' + str(gap) + 's') % ''
            for x in range(col):
                s += '%7s' % str(l[i]) if i<len(l) else ''
                s += ',' if i<len(l)-1 else ''
                i += 1
            if y < rows-2: s += '\n'

        s += '};'
        return s


if __name__ == "__main__":
    filePath = "C:/Users/heinw/OneDrive - CubeSpace/CubeIR/_Completed Units/IRCameras/4927/LookupTable137.json"
    a = Json2LookupCode(filePath, None, limit216bits=True)
    a.run()