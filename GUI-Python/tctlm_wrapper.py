from collections import namedtuple
from PyQt5.QtCore import QThread, QObject, pyqtSignal, pyqtSlot
from comms import Comms
from PyQt5 import QtCore, QtWidgets
from time import sleep
from tctlm import TCTLM

class TCTLM_SignalWrapper(QObject, TCTLM):
    '''
    This is a wrapper to change the <tctlm.py> file to a threaded system.
    Only one TCTLM thread should be created, as there is only one device
    to talk with. The threaded system automatically queues requests
    so that nothing overlaps.
    '''

    class Generic_TCTLM_t():
        def __init__(self, tctlm=None, params=None, tlm_fun=None, tc_fun=None):
            self.tctlm = tctlm      # Named Tuple describing data
            self.params = params
            self.tlm_fun = tlm_fun
            self.tc_fun = tc_fun
            self.reply = None
        def is_tlm(self):
            return True if self.tlm_fun is not None else False
        def set_reply(self, reply):
            self.reply = reply

    '''Signals sending info back to Main GUI Thread'''

    sig_return_edge_coords = QtCore.pyqtSignal(object)
    sig_set_progress_bar = QtCore.pyqtSignal(float, float)
    sig_return_eeprom_page = QtCore.pyqtSignal(object)
    sig_comms_error_not_responding = QtCore.pyqtSignal(object)
    sig_comms_error_responding = QtCore.pyqtSignal()
    _sig_echo = QtCore.pyqtSignal(object)

    sig_return_generic_TCTLM = QtCore.pyqtSignal(object)
    sig_echo = QtCore.pyqtSignal(object)

    sig_capture_complete = QtCore.pyqtSignal(object, object)

    class Image_t(): pass
    class EEPROM_Chunk(): pass


    def __init__(self, auto_connect=True, loggers=None):
        QObject.__init__(self)
        TCTLM.__init__(self, auto_connect, loggers)

    @pyqtSlot(object)
    def slot_handle_generic_tctlm(self, tctlms, echo=False):
        '''
        Handles all functions as specified in <tctlms> where <tctlms> is a list of
        <Generic_TCTLM> objects. Each object will have the function to call <f> and
        the parameters <params>
        '''
        for tctlm in tctlms:
            if tctlm.is_tlm():
                if tctlm.params is None:
                    tctlm.set_reply(self.attempt_comms(tctlm.tlm_fun, raise_exception=True))
                else:   # Sometimes TLMs need params, sorta. Like image downloads
                    tctlm.set_reply(self.attempt_comms(tctlm.tlm_fun, params=tctlm.params, raise_exception=True))
                self.sig_return_generic_TCTLM.emit(tctlm)
            else:
                if tctlm.params is None: tctlm.params = []
                self.attempt_comms(tctlm.tc_fun, params=tctlm.params, raise_exception=True)

    def attempt_comms(self, tctlm, params=[], raise_exception=False):
        '''Attempts comms <tctlm>
        This first argument in <*args> specifies if the error should be raised again'''

        if self.is_open():
            try:
                # Try comms
                rx = tctlm(*params)
                self.sig_comms_error_responding.emit()
                return rx
            except Exception as e:
                # Device didn't respond
                self.sig_comms_error_not_responding.emit("%s. %s" % (tctlm, e.args[0]))
                if raise_exception:
                    raise e

    @pyqtSlot(object)
    def slot_echo(self, time_sent):
        '''Echos, to know when tlms are done for example'''
        self.sig_echo.emit(time_sent)

    @pyqtSlot(object, object, int)
    def slot_capture_img(self, axis, filename, number_of_samples):
        for i in range(number_of_samples):
            img_filename = filename + '_#' + str(i) + '.csv'
            with open(img_filename, 'w') as outfile:
                outfile.write('test\n')
        self.sig_capture_complete.emit(axis, filename)
