import numpy as np
import cv2
import matplotlib.pyplot as plt
import time
import copy

class HIL:

    class Orbit():
        '''
        Class to store orbit information
        '''
        altitude = 500e3
        latitude = None
        aziumuth = None
        def __init__(self, altitude=None, latitude=None, azimuth=None):
            if altitude is not None: self.altitude = altitude
            if latitude is not None and azimuth is not None:
                self.latitude = latitude
                self.aziumuth = azimuth

    class Camera:
        '''Class to store camera information'''
        f = 3.9e-3                  # Focal length
        pitch = 90e-6               # Pixel pitch
        noise = None                # Camera's signal to noise ratio
        height = 64                 # Imager height in pixels
        width = 80                  # Imager width in pixels
        def __init__(self, f=None, pitch=None, noise=None, size=None):
            if f is not None: self.f = f
            if pitch is not None: self.pitch = pitch
            if noise is not None: self.noise = noise
            if size is not None:
                self.width = size[0]
                self.height = size[1]

    _RE = 6378137                   # Earth Radius at equator
    _RP = 6356752                   # Earth Radius at poles
    _RM = (_RE+_RP)/2               # Earth Mean Radius
    _inv_flat = 1/298.257223563     # Earth ellipse inverse flattening factor

    orbit = Orbit()
    camera = Camera()

    NUM_POINTS_IN_POLY = 300        # Does not have to be that big

    def __init__(self, orbit=None, camera=None):
        if orbit is not None: self.orbit = orbit
        if camera is not None: self.camera = camera


    def emul_create_binary_image(self, image):
        '''
        This function will attempt to create a binary image by using thresholding,
        and then destroying any blobs of the wrong size.
        :param image:
        :return:
        '''

        threshold = 50 # In MCU, this value can be auto (max-min)/2 or manual
        bimg = np.zeros(image.shape)
        for x in range(bimg.shape[0]):
            for y in range(bimg.shape[1]):
                bimg[x][y] = 1 if image[x][y] > threshold else 0

        plt.imshow(bimg)
        plt.show()


    def emul_edge_detection(self, img, limit_num_points=31, fine_tune=True):
        '''
        This function replicates the edge detection algorithm on the CubeIR

        Scan completely vertically and/or horizontally depending on <sev>
        For each max we find, we calculate the gradients dx and dy, and decide
        to which side are we gonna calculate the max point. Can even check if
        that coordinate has already been added

        TODO Make sure the edge is big enough each time in the original image
        TODO Make sure we every point we find is unique

        :param image:
        :return:
        '''
        points = []
        pnt_i_ovf = 0
        pnt_i_ovf_inc = 2

        minimum_gradient = (img.max()-img.min())/4
        m = 1

        W,H = img.shape

        grad_img = self.sobel_edge(img)

        threshold = img.min() + (img.max() - img.min()) * 0.75
        num_warm_pixels = num_cold_pixels = x_warm_acc = y_warm_acc = x_cold_acc = y_cold_acc = 0
        for x in range(W):
            for y in range(H):
                if ((x - W / 2) ** 2 + (y - H / 2) ** 2) < 32 ** 2:
                    if img[x, y] > threshold:
                        x_warm_acc += x
                        y_warm_acc += y
                        num_warm_pixels += 1
                    else:
                        x_cold_acc += x
                        y_cold_acc += y
                        num_cold_pixels += 1

        sev = [0, 0]
        transpose = False
        if num_warm_pixels > 0 and num_cold_pixels > 0:

            sev = [float(x_warm_acc) / float(num_warm_pixels) - float(x_cold_acc) / float(num_cold_pixels), float(y_warm_acc) / float(num_warm_pixels) - float(y_cold_acc) / float(num_cold_pixels)]

            transpose = False
            if abs(sev[0]) > abs(sev[1]):
                transpose = True

            # Should we scan column by column?
            if m*abs(sev[0]) <= abs(sev[1]):
                # For each column
                for x in range(1, grad_img.shape[0]-1):

                    max_grad_index = -1

                    # For every row in this column (direction doesn't matter)
                    for y in range(1, grad_img.shape[1]-1):

                        # Did we find a new highest peak?
                        if grad_img[x][y] > grad_img[x][max_grad_index] if max_grad_index != -1 else -1:
                            max_grad_index = y
                    y = max_grad_index
                    # Did we find a maximum point?
                    if y != -1:

                        # Check if it was a valid peak
                        if grad_img[x][y] < minimum_gradient:
                            y = -1

                        # Check if it's the peak in it's local column area
                        if grad_img[x][y] < grad_img[x][y - 1] or grad_img[x][y] < grad_img[x][y + 1]:
                            y = -1

                        # Did we find a valid point?
                        if y != -1:
                            # Store the points. We will fine tune it in a short while
                            if len(points) < limit_num_points:
                                points.append([x, y])
                            else:
                                points[pnt_i_ovf] = [x, y]
                                pnt_i_ovf += pnt_i_ovf_inc

                                if pnt_i_ovf >= limit_num_points:
                                    pnt_i_ovf_inc += 2
                                    pnt_i_ovf = 0

            # Should we do row wise scanning?
            if m*abs(sev[0]) >= abs(sev[1]):
                # For each row
                for y in range(1, grad_img.shape[1] - 1):

                    max_grad_index = -1

                    # For every column in this row (direction doesn't matter)
                    for x in range(1, grad_img.shape[0] - 1):

                        # Did we find a new highest peak?
                        if grad_img[x][y] > grad_img[max_grad_index][y] if max_grad_index != -1 else -1:
                            max_grad_index = x
                    x = max_grad_index
                    # Did we find a maximum point?
                    if x != -1:

                        # Check if it was a valid peak
                        if grad_img[x][y] < minimum_gradient:
                            x = -1

                        # Check if it's the peak in it's local row area
                        if grad_img[x][y] < grad_img[x - 1][y] or grad_img[x][y] < grad_img[x + 1][y]:
                            x = -1

                        # Did we find a valid point?
                        if x != -1:
                            # Store the points. We will fine tune it in a short while
                            if len(points) < limit_num_points:
                                points.append([x, y])
                            else:
                                points[pnt_i_ovf] = [x, y]
                                pnt_i_ovf += pnt_i_ovf_inc

                                if pnt_i_ovf >= limit_num_points:
                                    pnt_i_ovf_inc += 2
                                    pnt_i_ovf = 0

            # Now fine tune each point
            i = 0
            while i < len(points):

                if fine_tune:

                    # Find the x and y of the current point we are looking at
                    x, y = points[i]

                    # Then find the direction of the largest gradient change
                    # _, dx, dy = self.sobel_kernel(grad_img, x, y)
                    _, dx, dy = self.sobel_kernel(img, x+1, y+1)

                    # Check again if we have the peak in the direction of biggest change (chance we missed it)
                    if (dx >= dy and not (grad_img[x][y] > grad_img[x - 1][y] and grad_img[x][y] > grad_img[x + 1][y])) \
                        or (dx <= dy and not (grad_img[x][y] > grad_img[x][y - 1] and grad_img[x][y] > grad_img[x][y + 1])):

                        # This point was not a valid gradient. We need to remove it
                        if i != len(points)-1:
                            points[i] = points[-1]
                        del points[-1]

                    else:
                        # This is valid point. Fine tune it

                        if dy > dx:
                            # We're gonna calculate the gradient in the y direction

                            p0 = float(grad_img[x][y])
                            pm = float(grad_img[x][y - 1])
                            pp = float(grad_img[x][y + 1])
                            pnt_y = y + (pp - pm) / (4 * p0 - 2 * (pp + pm))

                            points[i] = [x, pnt_y]
                        else:
                            # We're gonna calculate the gradient in the x direction

                            p0 = float(grad_img[x][y])
                            pm = float(grad_img[x - 1][y])
                            pp = float(grad_img[x + 1][y])

                            pnt_x = x + (pp - pm) / (4 * p0 - 2 * (pp + pm))

                            points[i] = [pnt_x, y]

                i += 1

        # Get the points ready to be sent back
        if len(points) is not 0:
            points = np.array(points)-(np.array(grad_img.shape)-1)/2 # Go from image bottom left pixel center as origin, to middle of image as origin.
            if transpose:
                points = np.fliplr(points)
        else:
            points = None

        return points, sev, transpose

    def emul_fit_poly_to_points(self, points, order=6):
        return np.polyfit(*zip(*points), order)

    def emul_est_att_from_poly(self, p, sev, transpose=False):

        # Calculate a Distance funtion
        d2 = np.polymul(p, p)
        d2[-3] += 1

        # Find the point on that function closest to the origin (0,0)
        d2der = np.polyder(d2)
        x = np.real([elem for elem in np.roots(d2der) if np.imag(elem) == 0][-1])   # Last real root seems to work
        y = np.polyval(p, x)
        d = np.sqrt(np.polyval(d2, x)) * (-1 if np.sign(np.sign(y)) != np.sign(sev[1] if not transpose else sev[0]) else 1)

        if transpose:
            x, y = [y, x]

        # Estimate rotation
        rot = np.arctan2(-abs(x)*np.sign(sev[0]), -abs(y)*np.sign(sev[1]))

        # Estimate elevation
        elev = np.arctan2(d * self.camera.pitch, self.camera.f)

        return [elev, rot], np.array([x, y])


    def find_com(self, img, co_high_intensity=True, circ_mask_radius = None):
        '''
        Find the center of mass of the image
        :param img:
        :return:
        '''
        max_val = img.max()
        threshold = img.min() + (img.max() - img.min()) * 0.75

        count = 0
        com = np.array([0, 0])
        for x in range(img.shape[0]):
            for y in range(img.shape[1]):
                if circ_mask_radius is None or (x-img.shape[0]/2) ** 2 + (y-img.shape[1]/2) ** 2 < circ_mask_radius ** 2:
                    if co_high_intensity and img[x][y] > threshold:
                        com += np.array([x, y])
                        count += 1
                    elif not co_high_intensity and img[x][y] < threshold:
                        com += np.array([x, y])
                        count += 1
        return com / count

    def sobel_edge(self, img, ideal=False):
        '''
        This function mimics the sobel edge detection on the CubeIR
        :param image:
        :return:
        '''
        out = np.zeros((img.shape[0] - 2, img.shape[1] - 2))
        for x in range(len(out)):
            for y in range(len(out[x])):
                out[x][y] = self.sobel_kernel(img, x + 1, y + 1, ideal)[0]
        return out

    def sobel_kernel(self, img, x, y, ideal=False):
        xd = np.abs(img[x + 1][y + 1] + 2 * img[x + 1][y] + img[x + 1][y - 1] - img[x - 1][y + 1] - 2 * img[x - 1][y] - img[x - 1][y - 1])
        yd = np.abs(img[x - 1][y + 1] + 2 * img[x][y + 1] + img[x + 1][y + 1] - img[x - 1][y - 1] - 2 * img[x][y - 1] - img[x + 1][y - 1])
        return np.sqrt(xd ** 2 + yd ** 2) if ideal else int(np.max([xd, yd]) * 15 // 16 + np.min([xd, yd]) * 15 / 32), xd, yd

    def get_simulated_image(self, elevation, rotation, orbit=None, intensities=None, scale_factor=150, kernel=None, background=0):
        '''
        Combine <calculate_earth_polygons> and <render_image>
        '''
        return self.render_image([self.calculate_earth_polygon(elevation, rotation, orbit)], intensities, scale_factor, kernel, background)

    #def generate_random_artefacts(self, amount, intensity_range, size_range):


    def get_simulated_image_with_artefacts(self, elevation, rotation, artefacts, orbit=None, intensities=None, scale_factor=150, kernel=None, background=0):
        return self.render_image([self.calculate_earth_polygon(elevation, rotation, orbit), *artefacts], intensities, scale_factor, kernel, background)

    def calculate_earth_polygon(self, elevation, rotation, orbit=None):
        '''
        Calculate the polygon representing the Earth.

        This program was compared with the new Matlab <get_simulated_image> function, and it seems to output
        the same images. The oblateness was also tested, and verified, and it seems to work as well.

        :param elevation:
        :param rotation:
        :return: Nx2 Array representing Points on the expected horizon edge in pixel units
        '''

        if orbit is not None: self.orbit = orbit

        # Calculate basic orbital information
        rho = np.arcsin(self._RM/(self._RM+self.orbit.altitude))        # Angle between nadir and horizon from satellite
        r_ = self._RM * np.cos(rho)                                     # Earth disc radius
        h_ = r_ / np.tan(rho)                                           # Satellite height from earth disc

        # Describe the Earth disc with points (assuming nadir pointing) with two angles
        w_ = np.linspace(0, 2 * np.pi, self.NUM_POINTS_IN_POLY) - np.pi/2 - rotation
        v_ = rho * np.ones(w_.size)

        # Should we incorporate Earth Oblateness?
        if self.orbit.latitude is not None and self.orbit.aziumuth is not None:
            # Yes, we are incorporating it. (Good luck figuring out what is going on here. It's written in Hein's book).
            # In short: It looks in each direction from the satellite position (<all_azi>), and then determines
            # what the change in horizon angle will be (<eoff>). This is done with the WGS84 oblateness model.
            all_azi = w_ + self.orbit.aziumuth
            for i in range(all_azi.size):        # Wrap all the azimuth angles
                if all_azi[i] > np.pi: all_azi[i] -= 2*np.pi
            # Determine the latitude angle on disc edge for various azimuth angles
            lats = np.zeros(all_azi.size)
            kkk = [np.cos(self.orbit.latitude), 0, np.sin(self.orbit.latitude)]  # Inverse Nadir Vector
            vvv = [np.cos(self.orbit.latitude + np.pi / 2 - rho), 0,
                   np.sin(self.orbit.latitude + np.pi / 2 - rho)]  # Northern point of FOV(azimuth=0)
            for iii in range(all_azi.size):
                azi = all_azi[iii]   # Rotation direction doesn't matter
                vrot = np.multiply(vvv, np.cos(azi)) + np.multiply(np.cross(kkk, vvv), np.sin(azi)) + np.multiply(
                    np.multiply(kkk, np.dot(kkk, vvv)), (1 - np.cos(azi)))  # Rodriques' Rotation Formula
                lats[iii] = np.arctan(vrot[2] / np.sqrt(np.power(vrot[0], 2) + np.power(vrot[1], 2)))  # Calculate elevation
            for i in range(lats.size):        # Wrap all the latitude angles
                if all_azi[i] > np.pi / 2: all_azi[i] = np.pi - all_azi[i]
                elif all_azi[i] < -np.pi / 2: all_azi[i] = -np.pi - all_azi[i]
            lats_geodetic = np.arctan(np.power((1 - self._inv_flat), 2) * np.tan(lats))   # Convert to geodetic latitudes
            R_t = np.sqrt(np.divide(  # Calculate the Earth radius at those latitudes
                (self._RE ** 4 * np.power(np.cos(lats_geodetic), 2) + self._RP ** 4 * np.power(np.sin(lats_geodetic), 2)),
                (self._RE ** 2 * np.power(np.cos(lats_geodetic), 2) + self._RP ** 2 * np.power(np.sin(lats_geodetic), 2))).astype('float32'))
            dR = self._RM - R_t  # Find the difference in radius (what it should be with/without oblateness)
            d = self._RM/np.tan(rho)        # Distance to the horizon
            eoff = np.arctan(np.divide(dR, d))  # Find the angle error at those latitudes
            v_ = v_ - eoff  # Subtract those angles from the angles we calculated previously

        # Turn points into (x,y,z) points relative focal point
        x_ = h_ * np.multiply(np.tan(v_), np.sin(w_))
        y_ = h_ * np.multiply(np.tan(v_), np.cos(w_))
        z_ = -h_ * np.ones(w_.size)
        points = np.stack((x_, y_, z_))

        # Rotate this disc to the correct location for <elevation> and <rotation>
        b = -(elevation+rho)
        a = -rotation
        R = np.array([[np.cos(a),       -np.cos(b) * np.sin(a),     np.sin(b) * np.sin(a)],
             [np.sin(a),                np.cos(b) * np.cos(a),      -np.sin(b) * np.cos(a)],
             [0,                        np.sin(b),                  np.cos(b)]])
        points = np.dot(R, points)

        # Remove all points below the focal point (other side of hyperbola)
        mask = np.tile((points[2, :] > 0)[np.newaxis, :], (3, 1))
        points = np.ma.array(points, mask=mask).compressed().reshape((3, -1))

        # Project these points onto the image plane
        w = np.arctan2(points[0, :], points[1, :])
        v = np.arctan(np.sqrt(np.power(points[0, :], 2) + np.power(points[1, :], 2)) / -points[2, :])
        xh = self.camera.f * np.tan(v) * np.sin(w) / self.camera.pitch  # Horizon point
        yh = self.camera.f * np.tan(v) * np.cos(w) / self.camera.pitch  # Horizon point

        return np.array([xh, yh]).transpose()

    def remove_points_outside_image(self,points):
        '''
        The function <calculate_earth_polygon> returns points outside the camera image plane. This function can clip those points
        to be within the frame
        :param points:
        :return:
        '''
        out = []
        for i in range(points.shape[0]):
            if -self.camera.width/2 < points[i,0] < self.camera.width/2 and -self.camera.height/2 < points[i,1] < self.camera.height/2:
                out.append(points[i,:])
        return np.array(out)


    def render_image(self, polygons_i, intensities=None, scale_factor=150, kernel=None, background=0):
        '''Render image.
        1. Inherent OpenCV anti-aliasing uses a Gaussian blur, which sucks.
        2. Polygons can only be drawn on discrete locations
        3. The sub-pixel OpenCV polygon draw (using <shift>) has very inaccurate edges
        In the rendered image, corner (0,0) is in the bottom left (x->-inf,y->-inf)

        This renderer can handle polygons inside or outside of the image frame

        TODO: This works. In my example here there is an error of 3/255 = 1.1%. The maximum accuracy is 8bits (I think). Can always be scaled when adding noise

        :param polygon:         list of polygon arrays to draw
        :param intensities:     The maximum intensity of each polygon

        '''

        polygons = copy.deepcopy(polygons_i)
        if intensities is None: intensities = [255 for p in polygons]

        # Create empty upscaled image
        img_s = np.ones((self.camera.width*scale_factor, self.camera.height*scale_factor), np.uint8)*background

        # Fill the upscaled image with
        for polygon, intensity in zip(polygons, intensities):
            # Move the polygon to image coordinates
            polygon = np.fliplr(self.o2c((polygon.astype(float))))
            # Scale te poly
            poly_s = (np.array(polygon)+[.5, .5]) * scale_factor
            # Draw the big poly on the big image
            cv2.fillPoly(img_s, [(poly_s.astype(np.int32))], intensity)

        # self.visualize_image(img_s, scale_factor=scale_factor)
        # plt.show()

        # Convolve the upscaled image with a kernel (if there is one)
        if kernel is not None:
            img_s = cv2.filter2D(img_s, -1, kernel)

        # self.visualize_image(img_s, scale_factor=scale_factor)
        # plt.show()

        # Downscale the image to the correct size
        # My own downsizing method is significantly quicker that <cv2.resize(..., INTER_AREA)>
        img_my = np.zeros((self.camera.width, self.camera.height), np.uint8)
        for x in range(self.camera.width):
            for y in range(self.camera.height):
                img_my[x, y] = img_s[(x * scale_factor):((x + 1) * scale_factor),
                               (y * scale_factor):((y + 1) * scale_factor)].sum() / (scale_factor * scale_factor)

        # Add noise
        if self.camera.noise is not None:
            img_my = (img_my + self.camera.noise * (np.random.rand(*img_my.shape) - 0.5)).clip(0, 255).astype(np.uint8)

        return img_my

    def display_horizon_points(self, points):
        plt.scatter(points[0], points[1], marker='.')
        plt.xlim(np.array([-.5, .5]) * self.camera.width)
        plt.ylim(np.array([-.5, .5]) * self.camera.height)
        plt.grid()

    @staticmethod
    def visualize_image(image, scale_factor=1, nogrid=True, window=False):
        '''Draws an image as displayed on device'''
        plt.imshow(image.transpose(), cmap='gray', vmin=0, vmax=255)
        w, h = image.shape
        o = .5 if scale_factor == 1 else 0
        if nogrid is False:
            ax = plt.gca()
            ax.set_xticks(np.arange(o*scale_factor, (w + o*scale_factor), scale_factor))
            ax.set_yticks(np.arange(o*scale_factor, (h + o*scale_factor), scale_factor))
            plt.grid()
        if window is True:
            plt.xlim(np.array([-o*scale_factor, w-o*scale_factor]))
            plt.ylim(np.array([-o*scale_factor, h-o*scale_factor]))
        plt.gca().invert_yaxis()

    def show_demo(self, timeout=600, showoblate=False):
        from ctypes import windll, Structure, c_long, byref, windll
        def queryMousePosition():
            class POINT(Structure):
                _fields_ = [("x", c_long), ("y", c_long)]
            pt = POINT()
            windll.user32.GetCursorPos(byref(pt))
            return pt.x, pt.y
        sw, sh = windll.user32.GetSystemMetrics(0), windll.user32.GetSystemMetrics(1)
        try:
            start = time.time()
            for i in range(1000):
                mx, my = queryMousePosition()
                if showoblate is not True:
                    pitch = -np.deg2rad((abs(my) - sh / 2) / sh * 180)
                    roll = np.deg2rad((abs(mx) - sw / 2) / sw * 200)
                    self.orbit.latitude = None
                else:
                    self.orbit.latitude = -np.deg2rad((abs(my) - sh / 2) / sh * 180)
                    self.orbit.aziumuth = np.deg2rad((abs(mx) - sw / 2) / sw * 360)
                    pitch = roll = 0
                    # self.orbit.aziumuth = 0
                s = time.time()
                p = self.calculate_earth_polygon(pitch, roll)
                if p.any(): image = self.render_image([p], scale_factor=150)
                self.visualize_image(image, nogrid=True)
                azi = 0 if not self.orbit.aziumuth else self.orbit.aziumuth
                lat = 0 if not self.orbit.latitude else self.orbit.latitude
                print("%-8s:%4.2f%s\t%-8s:%4.2f%s\t%-8s:%4.2f%s\t%-8s:%4.2f%s\t%-8s:%4.2f%s\t" % ((
                    "Pitch", np.rad2deg(pitch), "°", "Roll", np.rad2deg(roll), "°", "SimTime", (time.time() - s) * 1000, 'ms',
                    "Latitude", np.rad2deg(lat), "°", "Azimuth", np.rad2deg(azi), "°")))
                plt.pause(0.05)
                if time.time()-start > timeout: break
        except:
            pass
        print('Demo Done.')
        plt.show()

    def o2m(self, points, w=None, h=None):
        ''' Move the image origin from the corner to the middle'''
        if not w: w = self.camera.width
        if not h: h = self.camera.height
        return points - [(w-1)/2, (h-1)/2]

    def o2c(self, points, w=None, h=None):
        ''' Move the image origin from the middle to the bottom left corner'''
        if not w: w = self.camera.width
        if not h: h = self.camera.height
        return points + [(w-1)/2, (h-1)/2]

if __name__ == '__main__':

    hil = HIL(orbit=HIL.Orbit(latitude=0.3, azimuth=0.3))
    hil._RP = 6056752
    hil._RM = (hil._RE+hil._RP)/2               # Earth Mean Radius
    p = hil.calculate_earth_polygon(0, 0)
    hil.show_demo(showoblate=False)


