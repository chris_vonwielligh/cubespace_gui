from PyQt5.QtCore import QThread, QObject, pyqtSignal, pyqtSlot
from comms import Comms
from PyQt5 import QtCore, QtWidgets
from time import sleep
from standa import Motor

class standa_SignalWrapper(QObject, Motor):

    def __init__(self, auto_connect=False, loggers=None):
        QObject.__init__(self)
        Motor.__init__(self, device_name=None)
        print('test init')

    sig_action_complete = QtCore.pyqtSignal(object, object)
    sig_axis_init_complete = QtCore.pyqtSignal(object)

    @pyqtSlot(object)
    def slot_init_axis(self, axis):
        print("slotrecieved")
        if(axis.number == 1):
            #move axis 1 to start position
            sleep(2) #emulate time it takes
            self.sig_axis_init_complete.emit(axis)
        elif(axis.number == 2):
            #move axis 2 to start position
            sleep(2)
            self.sig_axis_init_complete.emit(axis)



    @pyqtSlot(object, object)
    def slot_rotate_step(self, axis, filename):
        if(axis.direction == 'forward'):
            print('forward')
            #move forward one step
        elif(axis.direction == 'backwards'):
            print('backwards')
            #move backwards one step
        self.sig_action_complete.emit(axis, filename)





