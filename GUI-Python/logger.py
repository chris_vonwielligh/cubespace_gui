from datetime import datetime
import os.path
import csv


class Logger:

    keys = None
    enabled = True
    printed_headings = False

    def __init__(self, filename=None, ext='.txt', path=None, add_date=True, console=False):

        if not console:
            self.console = False
            if path:
                self.path = path
            else:
                self.path = "Logs/"
            if filename:
                self.filename = filename
            else:
                self.filename = "log"
            if add_date:
                self.filename = self.filename + datetime.now().strftime('_%Y%m%d_%H%M')
            self.filename += ext
        else:
            self.console = True

    def log_text(self, text, module=None, add_time=True, ignore_module=False):
        '''Writes text to logfile (used for events)'''
        if self.enabled:
            ttp = ""
            if isinstance(text,list):
                ttp = text[0]
                for idx,t in enumerate(text[1:]): ttp += '%s' % t
            else:
                ttp = text
            if module:              ttp = "%-20s| %s" % (module, ttp)
            elif not ignore_module: ttp = "%-20s| %s" % ("Unknown Module", ttp)
            if add_time:    ttp = datetime.now().strftime('%Y/%m/%d - %H:%M:%S.%f') + " | " + ttp
            if not self.console:
                self._write(ttp+'\n')
            else:
                print(ttp)

    def log_data(self, data, add_time=True):
        '''Writes the data list to the file, seperated by commas'''
        if self.enabled:
            ttp = ""
            if isinstance(data, list):
                for idx, d in enumerate(data): ttp += '%s,' % (str(d))
            else:
                ttp = data
            if add_time:    ttp = datetime.now().strftime('%Y/%m/%d - %H:%M:%S.%f') + ',' + ttp
            if not self.console:
                self._write(ttp + '\n')
            else:
                print(ttp)

    def key_index(self, key):
        '''Returns the index of the given key'''
        return self.keys.index(key)

    def _write(self, text):
        with open(self.path + self.filename, "a") as file: file.write(text)

    def _file_exists(self):
        '''returns true if this log file exists'''
        return os.path.isfile(self.path + self.filename)


if __name__ == "__main__":
    print("Started: logger.py")

    logger = Logger('vis')
    logger.log_text("hello julle!", module='Hi')
    logger.log_text("hello julle!", module='hier is hy')
    logger.log_text("hello julle!", module='Wat is my naam?')
    logger.log_text("hello julle!", module='whooooooooooooooooo')
    logger.log_text(['Hello','julle','piesangs'], module='Wat?')
    logger.log_data([1,2,3,54,5,6])


    import numpy as np
    sl = 17.4           # Side length of lens holder
    ch = [3.75, 3.72]   # Replace [A,B,C,D] with measured heights in mm
    print("Maximum Approximated Error is %.4f°\n"
        % np.rad2deg(np.arctan(max(ch)-min(ch))/sl))

