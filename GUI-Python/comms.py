#!/usr/bin/python3

from enum import Enum
from uart import UART
from i2c import I2C

class Comms:
    '''Handles the communication protocol'''

    SOM = 0x7F
    EOM = 0xFF
    ESC_CHAR = 0x1F
    ACK = 0

    DELAY = 0.05   # How long will I wait for response from device?

    loggers = None
    ERROR_RAISE = True

    class ProtocolError(Exception): pass
    class TimeOutError(Exception): pass
    class NoResponseError(Exception): pass

    def __init__(self, auto_connect=True, loggers=None):
        if loggers: self.loggers = loggers
        self.uart = UART(loggers=self.loggers)
        self.i2c = I2C(loggers=self.loggers)
        self.channel = None # Either <None>, 'UART', or 'I2C'
        if auto_connect and len(self.uart.return_list_of_ports()) > 0:
            self.uart.connect_to_port(self.uart.return_list_of_ports()[0])

    def is_open(self, channel=None):
        if channel is None:
            channel = self.channel
        if channel == 'UART':
            return self.uart.is_open()
        elif channel == 'I2C':
            return self.i2c.is_open()

    def tc(self, id, param=[], channel=None):
        '''Write a telecommand to the connected device. '''
        if channel is None:
            channel = self.channel

        if channel == 'UART':
            self.uart.port.reset_output_buffer()
            self.uart.port.reset_input_buffer()
            tx = param[:]
            tx.insert(0, id)
            self.uart.write_bytes(self.encode(tx[:]))
            rx = self.uart_receive_response()
            return rx[0] == self.ACK

        elif channel == 'I2C':
            return len(self.i2c.transceive(id, param, 0)) == self.ACK

        else:
            raise NotImplementedError()

    def tlm(self, id, tlm_length=0, channel=None):
        '''Writes an telemetry to the connected device'''
        '''<tlm_length> is required for I2C comms'''

        if channel is None:
            channel = self.channel

        if channel == 'UART':
            self.uart.port.reset_output_buffer()
            self.uart.port.reset_input_buffer()
            self.uart.write_bytes(self.encode([id]))
            return self.uart_receive_response()

        elif channel == 'I2C':
            return self.i2c.transceive(id, [], tlm_length, wait_before_reply=tlm_length*10e-6)

        else:
            raise NotImplementedError()

    def uart_receive_response(self):
        '''
        This function waits for a response from the device, decodes it, and returns the data
        :return:
        '''
        RX_State = Enum('RX_State', 'SOM DATA DONE')  # Essentially the part we are waiting for
        state = RX_State['SOM']
        escaped = False
        data = []

        while state is not RX_State['DONE']:

            try:
                byte = self.uart.read_byte()[0]         # This call should have the time out that
            except TypeError:
                raise self.NoResponseError('No Reply from device.')

            if byte is None: break                  # Was a timeout

            if not escaped and byte == self.ESC_CHAR:
                escaped = True
                continue
            elif state == RX_State['SOM'] and byte == self.SOM:
                state = RX_State['DATA']
            elif state == RX_State['DATA']:
                if escaped and byte == self.ESC_CHAR:
                    data.append(self.ESC_CHAR)
                elif escaped and byte == self.EOM:
                    state = RX_State['DONE']
                    # End of the message have been reached
                    break
                else:
                    data.append(byte)
            escaped = False  # If it escaped this iteration the code won't reach here due to the <continue>

        if state is RX_State['SOM']:
            if self.ERROR_RAISE: raise self.TimeOutError('Incomplete message.')
            return None # If the message was unsuccesful, return Nont
        elif state is not RX_State['DONE']:
            if self.ERROR_RAISE: raise self.ProtocolError('Incomplete message.')
            return None # If the message was unsuccesful, return Nont

        return data


    def encode(self, bytes):
        '''Encodes byte lists to the CubeSpace standard'''
        '''It adds all the SOM,EOM and ESC_CHARs'''
        i = 0
        while i < len(bytes):
            if bytes[i] == self.ESC_CHAR:
                bytes.insert(i, self.ESC_CHAR)
                i+=1
            i+=1
        bytes.insert(0, self.SOM)
        bytes.insert(0, self.ESC_CHAR)
        bytes.extend([self.ESC_CHAR, self.EOM])
        return bytes

    def decode(self, bytes, suppress=False):
        '''Decodes byte lists to the CubeSpace standard'''
        '''It removes all the SOM,EOM, and ESC_CHARs'''
        if any(bytes) is not None:
            if len(bytes)<5:
                if self.ERROR_RAISE: raise self.ProtocolError('Decoding failed. Not enough data.')
                return None
            if bytes[0]!=self.ESC_CHAR or bytes[1]!=self.SOM or bytes[-2]!=self.ESC_CHAR or bytes[-1]!=self.EOM:
                if self.ERROR_RAISE: raise self.ProtocolError('Decoding failed. Invalid framing.')
                return None
            bytes = bytes[2:-2]
            i = 0
            while i < len(bytes):
                if bytes[i] == self.ESC_CHAR:
                    del bytes[i]
                i+=1
            return bytes
        else:
            if self.ERROR_RAISE: raise self.TimeOutError('No response received.')



if __name__ == "__main__":
    print("Started: comms.py")

    comms = Comms(False)
    ports = comms.uart.return_list_of_ports()
    comms.channel = 'I2C'
    if comms.i2c.connect_to_port(ports[0]):
        print("I'm in!")
