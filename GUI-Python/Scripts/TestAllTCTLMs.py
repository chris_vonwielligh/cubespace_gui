import sys
sys.path.append("..")
from script import Script
from tctlm import TCTLM
import time
from matplotlib import pyplot as plt
import numpy as np
from hil import HIL


class TestAllTCTLMs(Script):

    def __init__(self, tctlm=None):
        Script.__init__(self, tctlm=tctlm)
        self._name = self.__class__.__name__

    '''
    This function is where your script should be. You can create your own functions as well
    =======================================================================================
    '''
    def run_script(self):
        self.LogMessage("Running Script: " + self._name)

        '''
        TODO
            - Make a nice class framework for tests
        '''

        self.test_htpa_state()


    def test_htpa_state(self):
        '''Tests the HTPA State TCTLMs'''
        '''
        TODO:
            Does not check if NO CHANGE power setting works when power off
            Does not check CAPTURE STATE
        '''
        print('\nTest: HTPA State')
        print("Reset state")
        self.tctlm.set_HTPA_state(3300, 3300, self.tctlm.PowerState_e(1), self.tctlm.PowerState_e(1))
        print(self.tctlm.get_HTPA_state())
        ptat,vdd = [12345, 54321]   # Is uint16
        print("Setting PTAT and Vdd")
        self.tctlm.set_HTPA_state(ptat, vdd, self.tctlm.PowerState_e(1), self.tctlm.PowerState_e(1))
        state = self.tctlm.get_HTPA_state()
        print(state)
        if state.PTAT == ptat and state.VddDigit == vdd:
            print("PTAT/Vdd set successfully")
        else:
            print("FAIL! PTAT/Vdd not set!")
        if state.PowerState == self.tctlm.PowerState_e(1) and state.WakeState == self.tctlm.PowerState_e(1):
            print("Device powered off and went too sleep successfully")
        else:
            print("FAIL! Device did not power down or switch off")
        self.tctlm.set_HTPA_state(0, 0, self.tctlm.PowerState_e(2), self.tctlm.PowerState_e(2))
        state = self.tctlm.get_HTPA_state()
        print(state)
        if state.PTAT == ptat and state.VddDigit == vdd:
            print("PTAT/Vdd remained unchanged")
        else:
            print("FAIL! PTAT/Vdd should reset!")
        if state.PowerState == self.tctlm.PowerState_e(2) and state.WakeState == self.tctlm.PowerState_e(2):
            print("Device powered on and woke up successfully")
        else:
            print("FAIL! Device did not power up!")
        print("The next command should not change anything")
        state = self.tctlm.get_HTPA_state()
        print(state)
        if state.PTAT == ptat and state.VddDigit == vdd:
            print("PTAT/Vdd remained unchanged")
        else:
            print("FAIL! PTAT/Vdd should reset!")
        if state.PowerState == self.tctlm.PowerState_e(2) and state.WakeState == self.tctlm.PowerState_e(2):
            print("Device stayed on")
        else:
            print("FAIL! Device turned off or went to sleep!")
        self.tctlm.set_HTPA_state(0, 0, self.tctlm.PowerState_e(3), self.tctlm.PowerState_e(3))

    '''
    =======================================================================================
    '''







if __name__ == "__main__":
    script = TestAllTCTLMs(tctlm=TCTLM())
    script.run_script()