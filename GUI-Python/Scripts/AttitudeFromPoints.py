import sys
sys.path.append("..")
from script import Script
from tctlm import TCTLM
import time
from matplotlib import pyplot as plt
import numpy as np
from hil import HIL


class AttitudeFromPoints(Script):

    def __init__(self, tctlm=None):
        Script.__init__(self, tctlm=tctlm)
        self._name = self.__class__.__name__

    '''
    This function is where your script should be. You can create your own functions as well
    =======================================================================================
    '''
    def run_script(self):
        self.LogMessage("Running Script: " + self._name)
        assert(False, "This script is outdated")

        print("Setting up simulated attitude")
        elevation = np.deg2rad(0)
        rotation = np.deg2rad(-30)
        pixel_pitch = 90e-6
        focal_length = 3.9e-3

        hil = HIL()
        points = hil.calculate_earth_polygon(elevation, rotation)
        points = hil.remove_points_outside_image(points)
        points = points[::int(np.ceil(len(points)/31))] # Only take 31 evenly spaced points

        plt.plot(*zip(*points), label='HIL')

        # Show expected straight line approximation
        alpha = focal_length * np.tan(elevation) / pixel_pitch
        m = np.tan(-rotation)
        c = -alpha / np.cos(rotation)
        x = np.arange(-30, 30, step=2)
        plt.plot(x, m * x + c, label="Straight Line Approx")

        print("Elevation: %.3f\tRotation: %.3f" % (np.rad2deg(elevation), np.rad2deg(rotation)))

        print("Setting points")
        print("\t" + "Success" if self.tctlm.set_edge_points(points) else "Failed")

        print("Estimate poly on device through HIL")
        self.tctlm.set_ADS_HIL_settings(True, True, False, False, True, True)

        time.sleep(0.3)

        print("How long did poly fitting take?")
        print("\tIt took %d ms." % self.tctlm.get_software_timings().EstimationTime)

        print("Return fitted polynomial")
        pr = self.tctlm.get_fitted_polynomial()
        pr.reverse()
        print("\t" + ("Success" if pr is not None else "Failed") + "(Num of pars: %d" % len(pr) + ")")

        pe = np.polyfit(points[:, 0], points[:, 1], 6)  # Poly expected

        plt.plot(np.arange(-30, 30), np.polyval(pe, np.arange(-30, 30)), '--', label="Expected Fit")
        plt.plot(np.arange(-30, 30), np.polyval(pr, np.arange(-30, 30)), '--', label="Actual Fit")
        plt.ylim([-30, 30])
        plt.grid()
        plt.axis('equal')

        print("Estimated Attitude:")
        estatt = self.tctlm.get_estimated_attitude()
        print(estatt)
        print([a * 180 / np.pi for a in estatt[:2]])

        print("\tError in attitude estimation:")
        print("%.3f\t%.3f" % (np.rad2deg(estatt.Elevation - elevation), np.rad2deg(estatt.Rotation - rotation)))
        print(estatt.Status.name)

        plt.legend()
        plt.show()


    '''
    =======================================================================================
    '''







if __name__ == "__main__":
    script = AttitudeFromPoints(tctlm=TCTLM())
    script.run_script()