import sys
sys.path.append("..")
from script import Script
from tctlm import TCTLM
import time
from matplotlib import pyplot as plt
import numpy as np
from hil import HIL


class LoopFullEstimation(Script):

    def __init__(self, tctlm=None):
        Script.__init__(self, tctlm=tctlm)
        self._name = self.__class__.__name__

    '''
    This function is where your script should be. You can create your own functions as well
    =======================================================================================
    '''
    def run_script(self):
        self.LogMessage("Running Script: " + self._name)

        enable_embedded = False
        enable_emulated = True

        N = 30  # Number of iterations to run
        hil = HIL()
        # hil = HIL(camera=HIL.Camera(noise=20))

        ref_att = {'elevation': np.zeros(N), 'rotation': np.zeros(N)}
        meas_att_emb = {'elevation': np.zeros(N), 'rotation': np.zeros(N)}
        meas_att_emul = {'elevation': np.zeros(N), 'rotation': np.zeros(N)}


        elevation_range = {'enable': True, 'min': np.deg2rad(-50), 'max': np.deg2rad(50), 'constant': np.deg2rad(0)}
        rotation_range = {'enable': False, 'min': np.deg2rad(-180), 'max': np.deg2rad(180), 'constant': np.deg2rad(0)}

        # Caues bug
        # elevation_range = {'enable': False, 'min': np.deg2rad(-25), 'max': np.deg2rad(25), 'constant': np.deg2rad(-20)}
        # rotation_range = {'enable': True, 'min': np.deg2rad(180.24), 'max': np.deg2rad(180.28), 'constant': np.deg2rad(180)}
        # Caues bug

        plt.figure()

        # Loop!
        for i in range(N):

            ref_elev_curr = (elevation_range['max'] - elevation_range['min']) * i / N + elevation_range['min'] if elevation_range['enable'] else elevation_range['constant']
            ref_rot_curr = (rotation_range['max'] - rotation_range['min']) * i / N + rotation_range['min'] if rotation_range['enable'] else rotation_range['constant']

            print("{0}/{1}:\tRef=(Elev: {2:5.3f}°\tRot: {3:5.3f}°)"
                  .format(i + 1, N, *np.rad2deg([ref_elev_curr, ref_rot_curr])))

            # Simulate an image
            img = hil.get_simulated_image(ref_elev_curr, ref_rot_curr)

            # Find edge coordinates
            points = hil.emul_edge_detection(img, limit_num_points=31)

            # Calculate attitude from these points
            est_att_emul = [0, 0]
            if enable_embedded:
                est_att = self.tctlm.EstimatedAttitude_t(0, 0, 0)
                if points is not None:

                    # On embedded device
                    self.tctlm.upload_image(img)
                    self.tctlm.set_ADS_HIL_settings(True, True, True, False, True, True)
                    time.sleep(0.5)
                    # points = self.tctlm.get_edge_coordinates()
                    est_att = self.tctlm.get_estimated_attitude()

            if enable_emulated:
                # Emulated
                all_points, sev, transpose = hil.emul_edge_detection(img, limit_num_points=200)
                if type(all_points) is np.ndarray:
                    p = hil.emul_fit_poly_to_points(all_points)
                    est_att_emul, xy = hil.emul_est_att_from_poly(p, sev, transpose)
                else:
                    est_att_emul = [0,0]
                    xy = np.array([])

            # Store the data
            ref_att['elevation'][i] = ref_elev_curr
            ref_att['rotation'][i] = ref_rot_curr
            if enable_embedded:
                meas_att_emb['elevation'][i] = est_att.Elevation
                meas_att_emb['rotation'][i] = est_att.Rotation
            meas_att_emul['elevation'][i] = est_att_emul[0]
            meas_att_emul['rotation'][i] = est_att_emul[1]

        # Plot some results
        if rotation_range['enable']:
            if enable_embedded:
                error = ref_att['rotation'] - meas_att_emb['rotation']
                error = (error + np.pi) % (2 * np.pi) - np.pi
                plt.plot(np.rad2deg(ref_att['rotation']), np.rad2deg(error), 'b', label="Rot (Embed)")
                plt.plot(np.rad2deg(ref_att['rotation']), np.rad2deg(ref_att['elevation'] - meas_att_emb['elevation']), 'g', label="Elev (Embed)")
            if enable_emulated:
                error = ref_att['rotation'] - meas_att_emul['rotation']
                error = (error + np.pi) % (2 * np.pi) - np.pi
                plt.plot(np.rad2deg(ref_att['rotation']), np.rad2deg(error), 'b--', label="Rot (Emul)")
                plt.plot(np.rad2deg(ref_att['rotation']), np.rad2deg(ref_att['elevation'] - meas_att_emul['elevation']), 'g--', label="Elev (Emul)")
            plt.grid()
            plt.title("Rotation Estimation Error")
            plt.ylabel("Error [°]")
            plt.xlabel("Reference Rotation [°]")
            plt.legend()

        elif elevation_range['enable']:
            if enable_embedded:
                plt.plot(np.rad2deg(ref_att['elevation']), np.rad2deg(ref_att['rotation'] - meas_att_emb['rotation']), 'b', label="Rot (Embed)")
                plt.plot(np.rad2deg(ref_att['elevation']), np.rad2deg(ref_att['elevation'] - meas_att_emb['elevation']), 'g', label="Elev (Embed)")
            if enable_emulated:
                plt.plot(np.rad2deg(ref_att['elevation']), np.rad2deg(ref_att['rotation'] - meas_att_emul['rotation']), 'b--', label="Rot (Emul)")
                plt.plot(np.rad2deg(ref_att['elevation']), np.rad2deg(ref_att['elevation'] - meas_att_emul['elevation']), 'g--', label="Elev (Emul)")
            plt.grid()
            plt.title("Elevation Estimation Error")
            plt.ylabel("Error [°]")
            plt.xlabel("Reference Rotation [°]")
            plt.legend()

        if False:
            plt.figure()
            if enable_embedded:
                plt.plot(np.rad2deg(ref_att['rotation']), np.rad2deg(meas_att_emb['rotation']), 'b', label="Rot (Embed)")
                plt.plot(np.rad2deg(ref_att['rotation']), np.rad2deg(meas_att_emb['elevation']), 'g', label="Elev (Embed)")
            if enable_emulated:
                plt.plot(np.rad2deg(ref_att['rotation']), np.rad2deg(meas_att_emul['rotation']), 'b--', label="Rot (Emul)")
                plt.plot(np.rad2deg(ref_att['rotation']), np.rad2deg(meas_att_emul['elevation']), 'g--', label="Elev (Emul)")
            plt.grid()
            plt.title("Attitude Reference vs. Estimation")
            plt.ylabel("Error [°]")
            plt.xlabel("Reference Rotation [°]")
            plt.legend()

        plt.show()  # This must happen last
    '''
    =======================================================================================
    '''







if __name__ == "__main__":
    script = LoopFullEstimation(tctlm=TCTLM())
    script.run_script()