import sys
sys.path.append("..")
from script import Script
from tctlm import TCTLM
import time
from matplotlib import pyplot as plt
import numpy as np
from hil import HIL


class CompareDetectionTechniques(Script):

    def __init__(self, tctlm=None):
        Script.__init__(self, tctlm=tctlm)
        self._name = self.__class__.__name__

    '''
    This function is where your script should be. You can create your own functions as well
    =======================================================================================
    '''

    class Techinque():
        def __init__(self, name, f):
            self.f = f  # This function will only take an image as input, and return angle
            self.input = []
            self.output = []
            self.last_points = []
            self.name = name
        def detect_and_store_results(self, input, img):
            self.input.append(input)
            o, self.last_points = self.f(img)
            self.output.append(o)
        def results_as_array(self):
            return np.array(self.output)
        def inputs_as_array(self):
            return np.array(self.input)
        def get_errors(self):
            return np.array([
                [
                    self.input[i][0] - self.output[i][0],
                    self.input[i][1] - self.output[i][1]
                ] for i in range(len(self.input))
            ])
        def get_last_error(self):
            return [
                self.input[-1][0] - self.output[-1][0],
                self.input[-1][1] - self.output[-1][1]
            ]

    def run_script(self):
        self.LogMessage("Running Script: " + self._name)

        noise = 5
        hil = HIL(camera=HIL.Camera(noise=5))

        techniques = []
        techniques.append(self.Techinque(
            "gradient_img_with_fine_tuning",
            self.gradient_img_with_fine_tuning))
        techniques.append(self.Techinque(
            "gradient_img_without_fine_tuning",
            self.gradient_img_without_fine_tuning))

        N = 100  # Number of iterations to run
        elevation_range = {'enable': True, 'min': np.deg2rad(-30), 'max': np.deg2rad(30), 'constant': np.deg2rad(0)}
        rotation_range = {'enable': True, 'min': np.deg2rad(-30), 'max': np.deg2rad(30), 'constant': np.deg2rad(0)}

        for i in range(N):
            print("i: ", i)

            elevation = (elevation_range['max'] - elevation_range['min']) * i / N + elevation_range['min'] if elevation_range['enable'] else elevation_range['constant']
            rotation = (rotation_range['max'] - rotation_range['min']) * i / N + rotation_range['min'] if rotation_range['enable'] else rotation_range['constant']

            img = hil.get_simulated_image(elevation, rotation)

            for technique in techniques:
                technique.detect_and_store_results([elevation, rotation], img)


        # Display Results

        plt.subplot(1, 2, 1)
        for t in techniques:
            plt.plot(np.rad2deg(t.get_errors()[:, 0]), '--')
        plt.subplot(1, 2, 2)
        plt.plot(np.rad2deg(techniques[0].get_errors()[:, 0] - techniques[1].get_errors()[:, 0]))


        plt.figure()
        for t in techniques:
            plt.hist(np.rad2deg(t.get_errors()[:, 0]), 20, normed=1, histtype='step', label=t.name)
        plt.legend()
        plt.xlabel("Error [°]")
        plt.title("Histogram of different detection\ntechniques' errors")
        plt.show()





    def gradient_img_with_fine_tuning(self, img):
        hil = HIL()
        all_points, sev, transpose = hil.emul_edge_detection(img, limit_num_points=200)
        if type(all_points) is np.ndarray:
            p = hil.emul_fit_poly_to_points(all_points)
            return hil.emul_est_att_from_poly(p, sev, transpose)[0], all_points
        else:
            return [0, 0], all_points
    def gradient_img_without_fine_tuning(self, img):
        hil = HIL()
        all_points, sev, transpose = hil.emul_edge_detection(img, limit_num_points=200, fine_tune=False)
        if type(all_points) is np.ndarray:
            p = hil.emul_fit_poly_to_points(all_points)
            return hil.emul_est_att_from_poly(p, sev, transpose)[0], all_points
        else:
            return [0, 0], all_points


    '''
    =======================================================================================
    '''


if __name__ == "__main__":
    script = CompareDetectionTechniques(tctlm=TCTLM())
    script.run_script()