import sys
sys.path.append("..")
from script import Script
from tctlm import TCTLM
import time
from matplotlib import pyplot as plt
import numpy as np
from hil import HIL


class LoopAttitudeFromPoints(Script):

    def __init__(self, tctlm=None):
        Script.__init__(self, tctlm=tctlm)
        self._name = self.__class__.__name__

    '''
    This function is where your script should be. You can create your own functions as well
    =======================================================================================
    '''
    def run_script(self):
        self.LogMessage("Running Script: " + self._name)

        N = 50     # Number of iterations to run
        hil = HIL()

        ref_att = {
            'elevation': np.zeros(N),
            'rotation': np.zeros(N)
        }
        meas_att = {
            'elevation': np.zeros(N),
            'rotation': np.zeros(N)
        }

        elevation_range={
            'enable':   True,
            'min':      np.deg2rad(-25),
            'max':      np.deg2rad(25),
            'range':    0
        }
        elevation_range['range'] = elevation_range['max'] - elevation_range['min']

        rotation_range = {
            'enable': True,
            'min': np.deg2rad(-45),
            'max': np.deg2rad(45),
            'range': 0
        }
        rotation_range['range'] = rotation_range['max'] - rotation_range['min']

        print("Iterating Tests...")
        for i in range(N):

            ref_elev_curr = elevation_range['range']*i/N + elevation_range['min'] if elevation_range['enable'] else 0
            ref_rot_curr = rotation_range['range']*i/N + rotation_range['min'] if rotation_range['enable'] else 0

            points = hil.calculate_earth_polygon(ref_elev_curr, ref_rot_curr)
            points = hil.remove_points_outside_image(points)
            points = points[::int(np.ceil(len(points)/31))] # Only take 31 evenly spaced points

            # Set edge points on device
            self.tctlm.set_edge_points(points)

            # Run detection algorithm on device (takes 2ms)
            self.tctlm.set_ADS_HIL_settings(True, True, False, False, True, True)
            time.sleep(0.05)

            # Return estimated attitude from device
            est_att = self.tctlm.get_estimated_attitude()

            # Store the data
            ref_att['elevation'][i] = ref_elev_curr
            ref_att['rotation'][i] = ref_rot_curr
            meas_att['elevation'][i] = est_att.Elevation
            meas_att['rotation'][i] = est_att.Rotation

        if rotation_range['enable']:
            plt.plot(np.rad2deg(ref_att['rotation']), np.rad2deg(ref_att['rotation']-meas_att['rotation']))
            plt.grid()
            plt.title("Rotation Estimation Error")
            plt.ylabel("Error [°]")
            plt.xlabel("Reference Rotation [°]")

        if elevation_range['enable']:
            plt.plot(np.rad2deg(ref_att['elevation']), np.rad2deg(ref_att['elevation']-meas_att['elevation']))
            plt.grid()
            plt.title("Elevation Estimation Error")
            plt.ylabel("Error [°]")
            plt.xlabel("Reference Rotation [°]")


        plt.show()
        print("Done")
    '''
    =======================================================================================
    '''


if __name__ == "__main__":
    script = LoopAttitudeFromPoints(tctlm=TCTLM())
    script.run_script()