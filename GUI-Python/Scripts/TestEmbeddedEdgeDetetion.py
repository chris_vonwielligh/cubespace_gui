import sys
sys.path.append("..")
from script import Script
from tctlm import TCTLM
import time
from matplotlib import pyplot as plt
import numpy as np
from hil import HIL


class TestEmbeddedEdgeDetection(Script):

    def __init__(self, tctlm=None):
        Script.__init__(self, tctlm=tctlm)
        self._name = self.__class__.__name__

    '''
    This function is where your script should be. You can create your own functions as well
    =======================================================================================
    '''
    def run_script(self):
        self.LogMessage("Running Script: " + self._name)

        enable_embedded = False
        enable_emulated = True

        elevation = np.deg2rad(-20)
        rotation = np.deg2rad(30)
        # elevation = np.deg2rad(-20)
        # rotation = np.deg2rad(180.26)

        # hil = HIL(orbit=HIL.Orbit(altitude=5000e3))
        # hil = HIL(camera=HIL.Camera(noise=20))
        hil = HIL()

        img = hil.get_simulated_image(elevation, rotation)

        grad_img = hil.sobel_edge(img)
        img_bore = np.array(img.shape)/2
        grad_img_bore = np.array(grad_img.shape)/2

        plt.figure()
        hil.visualize_image(img)
        plt.pause(0.01)

        if enable_emulated:
            # Emulated
            all_points, sev, transpose = hil.emul_edge_detection(img, limit_num_points=200)
            pe = hil.emul_fit_poly_to_points(all_points)
            est_att_emul, pnt_e = hil.emul_est_att_from_poly(pe, sev, transpose)

        if enable_embedded:
            # On embedded device
            print("Uploading image...")
            self.tctlm.upload_image(img)
            print("Activating hil...")
            self.tctlm.set_ADS_HIL_settings(True, True, True, False, True, True)
            time.sleep(0.5)
            print("Returning result...")
            points = self.tctlm.get_edge_coordinates()
            est_att = self.tctlm.get_estimated_attitude()
            pd = self.tctlm.get_fitted_polynomial()[::-1]

        print("Reference Attitude:")
        print("\tElevation: %.3f°" % np.rad2deg(elevation))
        print("\tRotation: %.3f°" % np.rad2deg(rotation))

        print("Estimated Attitude:")
        if enable_embedded:
            print("\tEmbedded Error:\tElev: {0:.3f}°\tRot: {1:.3f}°".format(np.rad2deg(elevation - est_att.Elevation), np.rad2deg(rotation - est_att.Rotation)))
        if enable_emulated:
            print("\tEmulated Error:\tElev: {0:.3f}°\tRot: {1:.3f}°".format(*np.rad2deg(np.array([elevation, rotation]) - est_att_emul)))

        if enable_embedded:
            print("\tEmbedded Est:\tElev: {0:.3f}°\tRot: {1:.3f}°".format(np.rad2deg(est_att.Elevation), np.rad2deg(est_att.Rotation)))
        if enable_emulated:
            print("\tEmulated Est:\tElev: {0:.3f}°\tRot: {1:.3f}°".format(*np.rad2deg(est_att_emul)))


        print("Other info:")
        if enable_embedded:
            print("\tAttEstStatus ", est_att.Status)
            print("\tNum of embedded points found: ", points.shape[0] if points is not None else "None")
        if enable_emulated:
            print("\tTotal Num Points Avail: ", all_points.shape[0] if all_points is not None else "None")
            print("\tClosest point: [%.f, %.2f]" % (pnt_e[0], pnt_e[1]))
            print("\tTransposed?\t", transpose)

        x_poly = np.arange(-30, 30)
        if enable_embedded:
            y_poly_embd = np.polyval(pd, x_poly)
            if enable_emulated and transpose:
                x_poly, y_poly_embd = [y_poly_embd, x_poly]
        if enable_emulated:
            y_poly_emul = np.polyval(pe, x_poly)
            if transpose:
                x_poly, y_poly_emul = [y_poly_emul, x_poly]


        if enable_embedded:
            if points.shape[0]:
                plt.plot(x_poly + img_bore[0], y_poly_embd + img_bore[1], label="Embedded")
                if enable_emulated and transpose:
                    plt.scatter(*zip(*(np.fliplr(points) + img_bore)), marker='x', c='r', label="Embedded")
                else:
                    plt.scatter(*zip(*points + img_bore), marker='x', c='r', label="Embedded")
        if enable_emulated:
            plt.plot(x_poly + img_bore[0], y_poly_emul + img_bore[1], 'y--', label="Emulated")
            plt.scatter(*zip(*((all_points if not transpose else np.fliplr(all_points)) + img_bore)), marker='+', c='b', label="Emulated")
            plt.scatter(*(pnt_e + img_bore), marker='x', c='r')
            plt.plot(*np.stack((pnt_e + img_bore, img_bore), axis=1), 'y')
        plt.scatter(*img_bore, marker='+', c='y')
        plt.legend()
        plt.xlabel('X [pixels]')
        plt.ylabel('Y [pixels]')

        plt.figure()
        hil.visualize_image(grad_img)
        if enable_embedded:
            if points.shape[0]:
                plt.plot(x_poly + grad_img_bore[0], y_poly_embd + grad_img_bore[1], label="Embedded")
                if enable_emulated and transpose:
                    plt.scatter(*zip(*(np.fliplr(points) + grad_img_bore)), marker='x', c='r', label="Embedded")
                else:
                    plt.scatter(*zip(*points + grad_img_bore), marker='x', c='r', label="Embedded")
        if enable_emulated:
            plt.plot(x_poly + grad_img_bore[0], y_poly_emul + grad_img_bore[1], 'y--', label="Emulated")
            plt.scatter(*zip(*((all_points if not transpose else np.fliplr(all_points)) + grad_img_bore)), marker='+', c='b', label="Emulated")
            plt.scatter(*(pnt_e + grad_img_bore), marker='x', c='r')
            plt.plot(*np.stack((pnt_e + grad_img_bore, grad_img_bore), axis=1), 'y')
        plt.scatter(*grad_img_bore, marker='+', c='y')
        plt.legend()
        plt.xlabel('X [pixels]')
        plt.ylabel('Y [pixels]')

        plt.show()

    '''
    =======================================================================================
    '''


if __name__ == "__main__":
    script = TestEmbeddedEdgeDetection(tctlm=TCTLM())
    script.run_script()