import sys
sys.path.append("..")
from script import Script
from tctlm import TCTLM
import time
from matplotlib import pyplot as plt
import numpy as np
from hil import HIL
from datetime import datetime, timedelta
import json

class DebuggingStuff(Script):

    def __init__(self, tctlm=None):
        Script.__init__(self, tctlm=tctlm)
        self._name = self.__class__.__name__

    '''
    This function is where your script should be. You can create your own functions as well
    =======================================================================================
    '''
    def run_script(self):
        self.LogMessage("Running Script: " + self._name)

        image_raw = np.genfromtxt('input/imagedata_190815_143613.csv', delimiter=',', dtype=np.int32).transpose()
        image_elloff_subtr = np.genfromtxt('input/imagedata_190815_144028.csv', delimiter=',', dtype=np.int32).transpose()
        # with open('input/htpa_blind_pixels_190815_143328.json', 'r') as f: json_str = f.read()
        with open('input/htpa_blind_pixels_190816_111512.json', 'r') as f: json_str = f.read()
        blinds = json.loads(json_str)

        for (x, y), pixel in np.ndenumerate(image_raw):
            if image_raw[x][y] < -29000:
                image_raw[x][y] += 2 ** 16

        blinds['top'] = np.array(blinds['top']) + 2**16
        blinds['bottom'] = np.array(blinds['bottom']) + 2**16

        blinds_top_flat = np.array(blinds['top']).reshape((-1, 1), order='F')
        blinds_bottom_flat = np.array(blinds['bottom']).reshape((-1, 1), order='F')
        image_elloff_subtr_user1 = np.zeros_like(image_elloff_subtr)
        image_elloff_subtr_user2 = np.zeros_like(image_elloff_subtr)

        for x in range(80):
            for y in range(64):
                if y < 32:
                    # Top of image
                    image_elloff_subtr_user1[x][y] = image_raw[x][y] - blinds_top_flat[(x + y * 80) % 640][0]
                    image_elloff_subtr_user2[x][y] = image_raw[x][y] - blinds['top'][x][y % 8]

                else:
                    # Bottom of image
                    image_elloff_subtr_user1[x][y] = image_raw[x][y] - blinds_bottom_flat[(x + y * 80) % 640][0]
                    image_elloff_subtr_user2[x][y] = image_raw[x][y] - blinds['bottom'][x][y % 8]


        plt.figure()
        plt.subplot(2, 4, 1)
        plt.imshow(image_raw.transpose(), cmap='jet')
        plt.title('Image with blinds not removed')

        plt.subplot(2, 4, 2)
        plt.imshow(image_elloff_subtr.transpose(), cmap='jet')
        plt.title('Image with blinds removed')

        plt.subplot(2, 4, 3)
        plt.imshow(image_elloff_subtr_user1.transpose(), cmap='jet')
        plt.title('Image with blinds removed by user 1')

        plt.subplot(2, 4, 4)
        plt.imshow(image_elloff_subtr_user2.transpose(), cmap='jet')
        plt.title('Image with blinds removed by user 2')

        plt.subplot(2, 4, 5)
        plt.imshow(np.concatenate([blinds['top'], blinds['bottom']],axis=1).transpose(), cmap='jet')
        plt.title('Blinds')

        plt.show()

    '''
    =======================================================================================
    '''







if __name__ == "__main__":
    script = DebuggingStuff()
    script.run_script()