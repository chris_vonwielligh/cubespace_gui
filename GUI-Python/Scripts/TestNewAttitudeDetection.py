import sys
sys.path.append("..")
from script import Script
from tctlm import TCTLM
import time
from matplotlib import pyplot as plt
import numpy as np
from hil import HIL


class TestNewAttitudeDetection(Script):

    def __init__(self, tctlm=None):
        Script.__init__(self, tctlm=tctlm)
        self._name = self.__class__.__name__

    '''
    This function is where your script should be. You can create your own functions as well
    =======================================================================================
    '''
    def run_script(self):
        self.LogMessage("Running Script: " + self._name)

        enable_embedded = False
        enable_emulated = True

        elevation = np.deg2rad(-20)
        rotation = np.deg2rad(30)
        # elevation = np.deg2rad(-20)
        # rotation = np.deg2rad(180.26)

        # hil = HIL(orbit=HIL.Orbit(altitude=5000e3))
        # hil = HIL(camera=HIL.Camera(noise=20))
        hil = HIL()

        img = hil.get_simulated_image(elevation, rotation)



    '''
    =======================================================================================
    '''


if __name__ == "__main__":
    script = TestNewAttitudeDetection(tctlm=TCTLM())
    script.run_script()