#!/usr/bin/python3

from comms import Comms
from collections import namedtuple
from struct import *
import numpy as np
import time
from enum import Enum
from PyQt5 import QtGui, QtCore
from matplotlib import pyplot as plt

class TCTLM():

    Identification_t = namedtuple("Identification_t","NodeType InterfaceVersion FirmwareVersionMajor FirmwareVersionMinor RunTimeMilliseconds RunTimeSeconds")
    HardwareState_t = namedtuple("HardwareState_t", "current3V3 currentHTPA voltageMCU voltageHTPA temperatureMCU temperatureHTPA")
    HardwareStateMinMax_t = namedtuple("HardwareStateMinMax_t",
                                       "current3V3Min current3V3Max currentHTPAMin currentHTPAMax "
                                       "voltageMCUMin voltageMCUMax voltageHTPAMin voltageHTPAMax "
                                       "temperatureMCUMin temperatureMCUMax temperatureHTPAMin temperatureHTPAMax")
    DeviceErrorFlags_t = namedtuple("DeviceErrorFlags_t",
                                    "FlashBitFlipDetected FlashCorruption SRAM2ParityError OverCurrent3V3 OverCurrentHTPA OverTemperatureMCU UnderTemperatureMCU OverTemperatureHTPA UnderTemperatureHTPA Timer1sOverRun")
    FRAMErrorFlags_t = namedtuple("FRAMErrorFlags_t", "StatusNotResponding DeviceNotFound SpiTimeout")
    HPTAErrorFlags_t = namedtuple("HTPAErrorFlags_t",
                                  "NotResponding ConfigMismatch CalibLookupOutOfRange IntegrationTimeError SpiTimeout")
    CommsErrorFlags_t = namedtuple("CommsErrorFlags_t", "TcParamBuffFull TcSlotsBuffFull UartProtError UartMsgIncomplete UartTcParamOverflow CommsTcInvalidID CommsTcInvalidParLen CommsTcInvalidPar CommsTlmInvalidID CommsTcTimeout CommsTcCrcError")
    UartErrorFlags_t = namedtuple("UartErrorFlags_t", "TxOverflow TxReadingEmptyBuffer RxByteOverrun RxFramingError TxTimeout")
    ConfigErrorFlags_t = namedtuple("ConfigErrorFlags_t", "RewrittenFromMemory ExtMemorySlotOverwritten NoConfigFoundInMemory NoValidSlotsInMemory")
    ErrorFlags_t = namedtuple("ErrorFlags_t", "DeviceErrorFlags FramErrorFlags HTPAErrorFlags CommsErrorFlags UartErrorFlags ConfigErrorFlags")
    TelecommandAcknowledge_t = namedtuple("TelecommandAcknowledge_t", "lastTCID processed errorStatus parameterErrorIndex")

    RadiationTestInfo_t = namedtuple("RadiationTestInfo_t", "NumOfFaultsLast NumOfFaultsTotal LastFaultIndex IterationsCompleted Mode CheckSRAM CheckFRAM")

    HTPACalibInfo_t = namedtuple("HTPAConfigInfo_t", "DeviceID TableNumber")
    HTPAState_t = namedtuple("HTPAState_t", "PTAT VddDigit PowerState WakeState CaptureState")
    HTPACalibLevel_t = namedtuple("HTPACalibLevel_t", "Level")
    HTPAImageInfo_t = namedtuple("HTPAImageInfo_t", "ImageLastMin ImageLastMean ImageLastMax ImageAllMin ImageAllMax")
    HTPABlindPixels_t = namedtuple("HTPABlindPixels_t", "Top Bottom")
    ADSSettings_t = namedtuple("ADSState_t", "SampleMode ImageCalibLevel")
    HILSettings_t = namedtuple('ADSHILSettings_t', 'Enabled ForceSample EnableImgCapture EnableImgVddComp EnableImgTobjCalib EnableEdgeDet EnableLensDistCorr EnablePolyFit EnableAttEst Busy')
    SoftwareTimings_t = namedtuple('SoftwareTimings_t', "StartupTime ProcessingTime ProcessingTimeMax ImgCaptureTime LensDistTime EstimationTime LoopTime LoopTimeMax ConfigRefreshMax ConfigCheckMemoryMax AdcMeasurement HtpaIntegration")
    EstimatedAttitude_t = namedtuple("EstimatedAttitude_t", "Elevation Rotation Status")

    MemTestSram_t = namedtuple("MemTestSram_t", "Mode LoopNumber FaultsLastLoop FaultsTotal IterateTime")
    MemTestFlash_t = namedtuple("MemTestFlash_t", "Mode State Index FaultLastIndex PageWriteErrors PageEraseErrors PageWriteTime PageEraseTime TimeToWait")

    ConfigState_t = namedtuple("ConfigState_t", "rewritten_from_memory configuration_not_saved data_loss ext_memory_slot_overwritten no_config_found_in_memory num_of_valid_fram_slots num_of_valid_flash_slots")
    SoftwareState_t = namedtuple("SoftwareSate", "DetectedBitFlips FlashFaults FlashLastFaultAddress")
    SoftwareStateExtended_t = namedtuple("ExtendedIdentification_t", "HTPASN RunTimeSeconds FirmwareBuildDate SRAM2ParityCheckEnabled")

    ConfigDeviceName_t = namedtuple("ConfigDeviceName_t", "SerialNumber I2cAdress")
    ConfigDevice_t = namedtuple("ConfigDevice_t", "ResetOnOvercurrent EnableIWDG EnableEWDG ResetOnBitFlip")

    PowerState_e = Enum('PowerState_e', 'Off On NoChange')
    ADSSampleState_e = Enum('ADSSampleState_e', 'Idle Single Continuous NoChange')
    HTPACaptureState_e = Enum('HTPACaptureState_e', "Idle Capturing Calibrating")
    HTPACalibLevel_e = Enum('HTPACalibLevel_e', "None Vdd Tobj NoChange")
    MemTestMode_e = Enum('MemTestMode_e', 'Disabled ReadOnly ReadWrite')
    MemTestFlashState_t = Enum("MemTestFlashState_t", "idle erase write read wait")
    AttitudeEstimationStatus_e = Enum('AttitudeEstimationStatus_e', 'NoDetect Failed_ImageCaptureFailure Failed_HorizonNotInView Failed_TooFewPoints Failed_CantFitPolynomial Failed_CantEstimateAttitudeFromPoly Success')
    TelecommandAckErrorReason_e = Enum('TelecommandAckErrorReason_e', 'NoError, InvalidTcID IncorrectLen IncorrectParams CrcError')

    loggers = None

    def __init__(self, auto_connect=True, loggers=None):
        if loggers: self.loggers = loggers
        self.comms = Comms(auto_connect, loggers=self.loggers)

    def is_open(self): return self.comms.is_open()

    def setget_memorychunk(self, pointer, index):
        '''Sets the memory chunk pointer and index, and the downloads the chunk'''
        self.set_memory_chunk_location(pointer, index)
        time.sleep(0.05)
        return self.get_memory_chunk()

    '''
    .___________. _______  __       _______ .___  ___.  _______ .___________..______       __   _______     _______.
    |           ||   ____||  |     |   ____||   \/   | |   ____||           ||   _  \     |  | |   ____|   /       |
    `---|  |----`|  |__   |  |     |  |__   |  \  /  | |  |__   `---|  |----`|  |_)  |    |  | |  |__     |   (----`
        |  |     |   __|  |  |     |   __|  |  |\/|  | |   __|      |  |     |      /     |  | |   __|     \   \    
        |  |     |  |____ |  `----.|  |____ |  |  |  | |  |____     |  |     |  |\  \----.|  | |  |____.----)   |   
        |__|     |_______||_______||_______||__|  |__| |_______|    |__|     | _| `._____||__| |_______|_______/    
                                                                                                                    
    '''

    def get_identification(self):
        '''Returns a list with the device identification'''
        rx = self.comms.tlm(128, tlm_length=8)
        return self.Identification_t(rx[0], rx[1], rx[2], rx[3], self.bytes2uint16(rx[4:6]), self.bytes2uint16(rx[-2:]))

    def get_software_state(self):
        '''tlms['ExtendedIdentification'] = self.attempt_comms(self.get_extended_identifiction, raise_exception=True)'''
        rx = self.comms.tlm(144, tlm_length=6)
        return self.SoftwareState_t(*self.bytes2uint8(rx[:2]), self.bytes2uint32(rx[2:6]))

    def get_software_state_extended(self):
        '''Returns a list with the device's extended identification'''
        rx = self.comms.tlm(145, tlm_length=21)
        return self.SoftwareStateExtended_t(self.bytes2str(rx[:10]),
                                            self.bytes2uint32(rx[10:14]),
                                            self.bytes2str(rx[14:20]),
                                            self.get_bit(rx[20], 0) == 1)

    def get_hardware_state(self):
        '''Returns the current current consumption and temperature of the device'''
        return self.HardwareState_t(*self.bytes2float(self.comms.tlm(141, tlm_length=24)))

    def get_hardware_state_minmax(self):
        '''Returns the minimum and maximum currents/temperatures measured'''
        rx = self.comms.tlm(142, tlm_length=48)
        return self.HardwareStateMinMax_t(*self.bytes2float(rx))

    def get_software_timings(self):
        '''Returns the software timing information'''
        rx = self.comms.tlm(143, tlm_length=24)
        return self.SoftwareTimings_t(*self.bytes2uint16(rx)[:12])

    def get_memtest_flash(self):
        '''Returns the state of the Flash memory test'''
        rx = self.comms.tlm(148, tlm_length=7)
        return self.MemTestFlash_t(
            self.MemTestMode_e((rx[0] & 0x3) + 1),
            self.MemTestFlashState_t(((rx[0] & 0x70)>>4) + 1),
            *self.bytes2uint8(rx[1:6]),
            *self.bytes2uint16(rx[6:10]),
        )

    def get_memtest_sram(self):
        '''Returns the state of the SRAM memory test'''
        rx = self.comms.tlm(149, tlm_length=7)
        return self.MemTestSram_t(
            self.MemTestMode_e((rx[0] & 0x3) + 1),
            *self.bytes2uint8(rx[1:3]),
            *self.bytes2uint16(rx[3:7]),
        )

    def get_error_flags(self):
        '''Returns ALL the error flags of the device'''
        rx = self.comms.tlm(130, tlm_length=7)
        device = self.DeviceErrorFlags_t._make(self.bytes2bits(self.bytes2uint16(rx[:2]), len=10))
        fram = self.FRAMErrorFlags_t._make(self.bytes2bits(rx[1], len=5)[2:5]) # Only using 3 of the 5 bits
        htpa = self.HPTAErrorFlags_t._make(self.bytes2bits(rx[2], len=5))
        comms = self.CommsErrorFlags_t._make((self.bytes2bits(self.bytes2uint16(rx[3:5]), len=11)))
        uart = self.UartErrorFlags_t._make(self.bytes2bits(rx[5], len=5))
        config = self.ConfigErrorFlags_t._make(self.bytes2bits(rx[6], len=4))
        return self.ErrorFlags_t(device, fram, htpa, comms, uart, config)

    def get_ADS_Settings(self):
        '''Get the state of the ADS'''
        rx = self.comms.tlm(220, tlm_length=1)
        return self.ADSSettings_t(self.ADSSampleState_e((rx[0] & 0x3) + 1), self.HTPACalibLevel_e((rx[0] >> 2 & 0x3) + 1))

    def get_HIL_settings(self):
        '''Get the device's HIL settings'''
        rx = self.comms.tlm(229, tlm_length=2)
        return self.HILSettings_t._make(self.bytes2bits(rx[0], len=10))

    def get_HTPA_Image_Info(self):
        '''Returns information about the HTPA image information'''
        rx = self.comms.tlm(159, tlm_length=10)
        return self.HTPAImageInfo_t(self.bytes2int16(rx[0:2]), self.bytes2int16(rx[2:4]), self.bytes2int16(rx[4:6]),
                                    self.bytes2int16(rx[6:8]), self.bytes2int16(rx[8:10]))

    def get_HTPA_Config_Info(self):
        '''Returns basic information about the HTPA'''
        rx = self.comms.tlm(150, tlm_length=4)
        return self.HTPACalibInfo_t(self.bytes2uint16(rx[:2]), self.bytes2uint16(rx[2:4]))

    def get_HTPA_state(self):
        '''Returns persistent information about the HTPA'''
        rx = self.comms.tlm(151, tlm_length=5)
        return self.HTPAState_t(self.bytes2uint16(rx[0:2]), self.bytes2uint16(rx[2:4]),
                                self.PowerState_e((rx[4] & 0x3) + 1), self.PowerState_e((rx[4] >> 2 & 0x3) + 1),
                                self.HTPACaptureState_e((rx[4] >> 4 & 0x3) + 1))

    def get_memory_chunk(self):
        '''Returns one memory chunk (in bytes) from the selected location in memory'''
        return self.comms.tlm(155, tlm_length=128)

    def get_estimated_attitude(self):
        '''Returns one memory chunk (in bytes) from the selected location in memory'''
        rx = self.comms.tlm(221, tlm_length=9)
        return self.EstimatedAttitude_t(*self.bytes2float(rx[:8]), self.AttitudeEstimationStatus_e(rx[8]+1))

    def get_edge_coordinates(self):
        '''Returns the list of edge coordinates in a (nx2 array)'''
        # Coordinates are placed like [x0, y0, x1, y1, x2, y2 ... )
        rx = self.comms.tlm(225, tlm_length=249)
        qty = self.bytes2uint8(rx[0])
        coords = self.bytes2float(rx[1:])   # Lets unnecessarily convert everything to floats, even the garbage
        return np.array([[coords[2*i], coords[2*i+1]] for i in np.arange(qty)])

    def get_fitted_polynomial(self):
        '''Returns the fitted polynomial's coefficients'''
        rx = self.comms.tlm(226, tlm_length=41)
        return self.bytes2float(rx[1:])[:self.bytes2uint8(rx[0])+1]

    def get_EEPROM_read(self):
        '''Return the data as read by <set_EEPROM_read_at>'''
        return self.comms.tlm(157, tlm_length=64)

    def get_config_state(self):
        '''State of device's internal configuration'''
        rx = self.comms.tlm(135, tlm_length=3)
        return self.ConfigState_t(*self.bytes2bits(rx[0], len=5), *self.bytes2uint8(rx[1:]))

    def get_config_devicename(self):
        '''Gets the config device name'''
        rx = self.comms.tlm(136, tlm_length=11)
        return self.ConfigDeviceName_t(self.bytes2str(rx[0:10]), self.bytes2uint8(rx[10]))

    def get_config_device(self):
        '''Gets the general config of the device'''
        rx = self.comms.tlm(137, tlm_length=2)
        return self.ConfigDevice_t(self.bytes2uint8(rx[0]), *self.bytes2bits(rx[1], len=3))

    def get_telecommand_acknowledge(self):
        ''' To check if the last TC was executed successfully'''
        rx = self.comms.tlm(131, tlm_length=4)
        return self.TelecommandAcknowledge_t(
            self.bytes2uint8(rx[0]),
            self.get_bit(rx[1], 0) == 1,
            self.TelecommandAckErrorReason_e(rx[2] + 1),
            self.bytes2uint8(rx[3])
        )

    def get_blind_pixels(self):
        '''Downloads the blind pixels (also called electrical offsets)'''
        ROWS_IN_BLOCK = 8   # in pixels
        COLUMNS_IN_BLOCK = 80   # in pixels
        BYTES_PER_DOWNLOAD_CHUNK = 128

        assert (ROWS_IN_BLOCK * COLUMNS_IN_BLOCK * 2) % BYTES_PER_DOWNLOAD_CHUNK == 0, 'This implementation assumes blinds can fit into an whole number of memory chunks'

        def download_blinds(top_nbot):
            TOP_BLINDS_POINTER = 3      # Firmware index points to top blinds
            BOTTOM_BLINDS_POINTER = 4
            pointer = TOP_BLINDS_POINTER if top_nbot else BOTTOM_BLINDS_POINTER
            return [blind for chunk in range(int(ROWS_IN_BLOCK * COLUMNS_IN_BLOCK * 2 / BYTES_PER_DOWNLOAD_CHUNK))
                    for blind in self.bytes2int16(self.setget_memorychunk(pointer, chunk))]

        top_blinds = np.array(download_blinds(True)).reshape(COLUMNS_IN_BLOCK, ROWS_IN_BLOCK)
        bot_blinds = np.array(download_blinds(False)).reshape(COLUMNS_IN_BLOCK, ROWS_IN_BLOCK)

        return self.HTPABlindPixels_t(top_blinds, bot_blinds)

    def download_image(self, update_gui=None, pause_sampling=True, width=80, height=64):
        '''Download image of size [w,h]. It is assumed that each pixel consists of two bytes'''

        PIXELS_IN_CHUNK = 64

        if pause_sampling:
            sam = self.get_ADS_Settings().SampleMode
            self.set_ADS_Settings(self.ADSSampleState_e(1), self.HTPACalibLevel_e(4))
            time.sleep(0.5)  # Make sure last image was sampled completely
            # If a long sleep is not here then the chunk can be downloaded during calibration and cause artefacts.

        img_raw = np.zeros((width, height)).astype(np.int32)
        x = 0
        y = 0
        for bi in range(int(np.ceil(width * height / PIXELS_IN_CHUNK))):
            self.set_memory_chunk_location(0, bi)
            time.sleep(0.001)
            rb = self.bytes2int16(self.get_memory_chunk(), True)
            for pixel in rb:
                img_raw[x][y] = pixel

                y += 1
                if y >= height:
                    y = 0
                    x += 1
                if x == width:
                    break

            if update_gui:
                if isinstance(update_gui, QtCore.pyqtBoundSignal):
                    update_gui.emit(bi + 1, 80)
                else:
                    update_gui.ui.progressBar.setValue((bi + 1) / 80 * 100)
                    QtGui.QGuiApplication.processEvents()

        if isinstance(update_gui, QtCore.pyqtBoundSignal):
            update_gui.emit(1, 1)
        elif update_gui is not None:
            update_gui.ui.progressBar.setValue(1)
            QtGui.QGuiApplication.processEvents()

        if pause_sampling: self.set_ADS_Settings(sam, self.HTPACalibLevel_e(4))

        return img_raw

    '''
    .___________. _______  __       _______   ______   ______   .___  ___. .___  ___.      ___      .__   __.  _______       _______.
    |           ||   ____||  |     |   ____| /      | /  __  \  |   \/   | |   \/   |     /   \     |  \ |  | |       \     /       |
    `---|  |----`|  |__   |  |     |  |__   |  ,----'|  |  |  | |  \  /  | |  \  /  |    /  ^  \    |   \|  | |  .--.  |   |   (----`
        |  |     |   __|  |  |     |   __|  |  |     |  |  |  | |  |\/|  | |  |\/|  |   /  /_\  \   |  . `  | |  |  |  |    \   \    
        |  |     |  |____ |  `----.|  |____ |  `----.|  `--'  | |  |  |  | |  |  |  |  /  _____  \  |  |\   | |  '--'  |.----)   |   
        |__|     |_______||_______||_______| \______| \______/  |__|  |__| |__|  |__| /__/     \__\ |__| \__| |_______/ |_______/    
                                                                                                                                     
    '''

    def set_ADS_Settings(self, sampleMode, imageCalibLevel):
        '''Set the ADS state'''
        return self.comms.tc(20, [(sampleMode.value - 1) & 0x3 | (((imageCalibLevel.value - 1) & 0x3)<<2)])

    def set_HIL_settings(self, enable, forceSample=None, EnableImgCapture=None, EnableImgVddComp=None, EnableImgTobjCalib=None, enableEdgeDet=None, enableLensDistCorr=None, enablePolyFit=None, enableAttEst=None):
        '''Sets the HIL settings'''
        self.comms.tc(29, self.uint162bytes([sum([2**i if s else 0 for i, s in enumerate(
            [enable, forceSample, EnableImgCapture, EnableImgVddComp, EnableImgTobjCalib, enableEdgeDet, enableLensDistCorr, enablePolyFit, enableAttEst, False]
        )])]))

    def set_HTPA_state(self, ptat, vdd, power, wake):
        '''Sets the internal state of the HTPA'''
        return self.comms.tc(51, [
            *self.uint162bytes([ptat]),
            *self.uint162bytes([vdd]),
            (power.value - 1) & 0x3 | (((wake.value - 1) & 0x3) << 2)
        ])

    def set_memory_chunk(self, p):
        '''Sets one image memory chunk (in bytes) from pixels <p> (in in16)'''
        return self.comms.tc(55, self.uint162bytes(p))

    def clear_error_flags(self):
        '''Clears all error flags of the device'''
        return self.comms.tc(28)

    def set_memory_chunk_location(self, memory_pointer, chunk_index):
        '''Sets the memory pointer to a specific location and chunk index'''
        return self.comms.tc(56, [memory_pointer, chunk_index])

    def EEPROM_read_at(self, address):
        '''MCU reads HTPA's EEPROM and store it internally'''
        return self.comms.tc(57, [self.lowbyte(address), self.highbyte(address)])

    def set_edge_points(self, coords):
        '''Sets the edge coordinates on the device to be used during HIL'''
        assert coords.shape[0]<=31, "Number of points can't be more than 31."
        qty = coords.shape[0]
        data = self.float2bytes([               # Each float is four bytes
            coords[i // 2][i % 2]
                if i // 2 < qty else 0
                    for i in range(31 * 2)])    # 31 is the total number of points to be sent (even null ones)
        return self.comms.tc(25, [qty] + data)

    def set_fitted_polynomial(self, p):
        '''Overwrites the device's fitted polynomial coefficients'''
        return self.comms.tc(26, [len(p)-1] + [self.float2bytes(p)[i] if i < (len(p) * 4) else 0 for i in range(40)])

    def upload_image(self, image, update_gui=None):
        '''Upload an image to the HTPA'''
        img1d = np.reshape(image, 80*64)
        for bi in range(80):
            self.set_memory_chunk_location(0, bi)
            time.sleep(0.001)
            self.set_memory_chunk(list(img1d[(bi * 64):(bi * 64 + 64)]))

            if update_gui:
                if isinstance(update_gui, QtCore.pyqtBoundSignal):
                    update_gui.emit(bi + 1, 80)
                else:
                    update_gui.ui.progressBar.setValue((bi + 1) / 80 * 100)
                    QtGui.QGuiApplication.processEvents()

    def config_set_devicename(self, serialnumber, i2caddress):
        '''Sets the device name configuration'''
        if len(serialnumber) > 10:
            serialnumber = serialnumber[0:10]   # trim        )
        serialnumber = [' ' for i in range(10 - len(serialnumber))] + list(serialnumber)
        serialnumber = [ord(c) for c in serialnumber]
        return self.comms.tc(36, [*serialnumber, i2caddress])

    def config_set_device(self, reset_overcurrent, enable_iwdg, enable_ewdg, reset_on_bitflip):
        '''Sets the device general configuration'''
        return self.comms.tc(37, [
            reset_overcurrent,
            (1 if enable_iwdg else 0) | (2 if enable_ewdg else 0) | (4 if reset_on_bitflip else 0)
        ])

    def config_set_memtest_sram(self, mode):
        '''Sets the memtest sram (you can only set the mode'''
        return self.comms.tc(49, [
            ((mode.value - 1) & 3),
            *([0] * 6)
        ])

    def config_set_memtest_flash(self, mode):
        '''Sets the memtest flash (you can only set the mode'''
        return self.comms.tc(48, [
            ((mode.value - 1) & 3),
            *([0] * 9)
        ])

    def config_check_memory(self):
        '''Verify configurations in external memory'''
        return self.comms.tc(32)

    def config_clear(self):
        '''Clear configuration in external memory'''
        return self.comms.tc(31)

    def config_save(self):
        '''Save current configuration to external memory'''
        return self.comms.tc(30)

    def config_load(self):
        '''Load configuration from external memory'''
        return self.comms.tc(35)

    def config_load_default(self):
        '''Load default configuration'''
        return self.comms.tc(33)

    def htpa_config_store_from_eeprom(self):
        '''Read HTPA's config from its EEPROM, and store in memory'''
        return self.comms.tc(52)

    def device_force_power_cycle(self):
        '''Forces device to power cycle itself'''
        return self.comms.tc(90)

    '''
     __    __   _______  __      .______    _______ .______          _______.
    |  |  |  | |   ____||  |     |   _  \  |   ____||   _  \        /       |
    |  |__|  | |  |__   |  |     |  |_)  | |  |__   |  |_)  |      |   (----`
    |   __   | |   __|  |  |     |   ___/  |   __|  |      /        \   \    
    |  |  |  | |  |____ |  `----.|  |      |  |____ |  |\  \----.----)   |   
    |__|  |__| |_______||_______|| _|      |_______|| _| `._____|_______/
        
    '''

    @staticmethod
    def bytes2uint32(b, force_return_list=False):
        '''
        Takes to bytes and returns a 32 bit value
        '''
        d = list(b[0] for b in iter_unpack('<I', bytearray(b)))
        if force_return_list or len(d)>1:   return d
        else:                               return d[0]

    @staticmethod
    def bytes2uint16(b, force_return_list=False):
        '''
        Takes to bytes and returns a 16 bit value
        '''
        d = list(b[0] for b in iter_unpack('<H', bytearray(b)))
        if force_return_list or len(d)>1:   return d
        else:                               return d[0]

    @staticmethod
    def uint162bytes(l):
        '''
        Takes to a list of 16 bit values and returns list of bytes
        '''
        b = list(range(2*len(l)))
        for i in b:
            if i & 1:
                b[i] = TCTLM.highbyte(l[int(np.floor(i / 2))])
            else:
                b[i] = TCTLM.lowbyte(l[int(np.floor(i / 2))])
        return b

    @staticmethod
    def bytes2int16(b, force_return_list=False):
        '''
        Takes to bytes and returns a 16 bit value
        '''
        d = list(b[0] for b in iter_unpack('<h', bytearray(b)))
        if force_return_list or len(d)>1:   return d
        else:                               return d[0]

    @staticmethod
    def bytes2int8(b, force_return_list=False):
        '''
        Takes to bytes and returns a 8 bit signed value
        '''
        d = list(b[0] for b in iter_unpack('<b', bytearray(b)))
        if force_return_list or len(d)>1:   return d
        else:                               return d[0]

    @staticmethod
    def bytes2uint8(b, force_return_list=False):
        '''
        Takes to bytes and returns a 8 bit unsigned value
        '''
        if not isinstance(b, list): b = [b]
        d = list(b[0] for b in iter_unpack('<B', bytearray(b)))
        if force_return_list or len(d) > 1:  return d
        else:                                return d[0]

    @staticmethod
    def bytes2float(b, force_return_list=False):
        '''Converts 4 bytes to a float'''
        d = list(b[0] for b in iter_unpack('<f', bytearray(b)))
        if force_return_list or len(d)>1:   return d
        else:                               return d[0]

    @staticmethod
    def float2bytes(l):
        if isinstance(l, list):
            o = []
            for f in l: o.extend(pack('<f', f))
        else:
            o = list(pack('<f', l))
        return o

    @staticmethod
    def get_bit(b, idx):
        return 1 if ((b & (1 << idx))) else 0

    @staticmethod
    def bytes2bits(b, len=32):
        '''Create a list of bit values (true or false)'''
        return list((b & (1 << i)) != 0 for i in range(len))

    @staticmethod
    def bytes2str(b):
        return "".join(str(chr(c)) for c in b)

    @staticmethod
    def lowbyte(b):
        return b & 0xFF

    @staticmethod
    def highbyte(b):
        return (b & 0xFF00) >> 8

if __name__ == "__main__":
    print("Started: tctlm.py")

    tctlm = TCTLM(False)
    ports = tctlm.comms.uart.return_list_of_ports(all=True)
    print(ports[1].device)
    if tctlm.comms.uart.connect_to_port(ports[1].device):
        tctlm.comms.channel = 'UART'
        # print("I'm connected to I2C!")
        # time.sleep(2)

        print(tctlm.get_identification())