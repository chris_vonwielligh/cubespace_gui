import serial
from serial.tools import list_ports
from time import sleep, time

class I2C:
    '''Handles the low level serial communication through a Byvac (bv4221-v2)'''
    '''Adaptation from code written by Douw'''

    active = False
    loggers = None
    ERROR_RAISE = False
    ACK = "*"
    def __init__(self, loggers=None):
        if loggers: self.loggers = loggers
        self.port = None

    def return_list_of_ports(self):
        '''List of COMM Port'''
        all_pos_ports = list(list_ports.comports())
        virt_comm_ports = []
        for this_port in all_pos_ports:
            if "Serial" in this_port[1]:
                virt_comm_ports.append(this_port[0])
        return virt_comm_ports

    def connect_to_port(self, port):
        try:
            if not self.is_open():
                self.port = serial.Serial(port,
                                          baudrate=115200,
                                          bytesize=serial.EIGHTBITS,
                                          parity=serial.PARITY_NONE,
                                          stopbits=serial.STOPBITS_ONE,
                                          timeout=0.5,
                                          xonxoff=0,
                                          rtscts=0)
                self.active = True
                if self.loggers: self.loggers['event'].log_text('Successfully connected to '+port+'.', module="I2C")
                sleep(1)
                try:
                    self.write_read('\r\r')     # Confirm baudrate
                except TimeoutError:
                    pass    # Device already started up.
                self.write_read('P0\r')         # Turn off byvac's prompt mode
                self.write_read('A20\r')        # Set I2C slave address to 0x20.
                print('Successfully connected to I2C')
                return True
            return False
        except serial.SerialException:
            print('Cannot connect to I2C')
            if self.ERROR_RAISE: raise ValueError('Connection failed. Port ' + port + ' already in use.')
            self.active = False
            return False

    def write_read(self, cmd, expect_response=True):
        '''
        This function's time is limited by the ByVac, which takes like 500µm per byte!
        :param cmd:
        :return:
        '''

        k = ''
        v = ''
        # send command
        self.port.reset_input_buffer()
        self.port.write(str.encode(cmd))
        # print "send ",cmd # debug

        if expect_response:
            retry_count = 0
            while k != self.ACK and retry_count < 5:
                try:
                    k = self.port.read(1)
                except serial.SerialTimeoutException:
                    raise TimeoutError("BV4221-V2 timed out")
                k = k.decode("utf-8")
                if k == self.ACK:  # all done
                    if len(v) == 0:
                        v = self.ACK
                    break
                else:
                    if len(k) == 0:
                        retry_count += 1
                        continue
                    else:
                        v = v + k

            if retry_count == 5:
                raise TimeoutError("BV4221-V2 not responding")

            # convert sting into list
            rvl = []
            if len(v) != 0:

                ss = v.rsplit(',')
                for i, s in enumerate(ss):
                    try:
                        rvl.append(int(s, 16))
                    except:
                        rvl = []  # clear list on any error
                        if self.loggers:
                            self.loggers['event'].log_text(s, module="I2C")
            return rvl

    def transceive(self, tctlm_id, params, reply_length, wait_before_reply=0, write_then_read=False):
        '''
        :param tctlm_id:        ID of command you want to send
        :param params:          params to send with TC
        :param reply_length:    Amount of bytes expected in TLM reply
        :param wait_before_reply:   With TLM, how long should wait before wait for reply (in seconds)
        :return:
        '''

        # see if there is anything to write
        if reply_length != 0:        # Probably a TLM
            if write_then_read:
                raise NotImplementedError("Write, then read, is not yet working.")  # This still doesnt work
                self.write_read("s " + format(tctlm_id, '02x') + "r\r", expect_response=False)
                sleep(wait_before_reply)  # This doesn't help yet. Anyway only sends command on "p" it seems
                return self.write_read("g-" + hex(reply_length)[-2:] + " p\r")
            else:
                return self.write_read("s " + format(tctlm_id, '02x') + " r g-" + format(reply_length, '02x') + " p\r")

        else:                       # Probably a TC
            assert len(params < 66), "BV4221-V2 does not support such large parameter lists"
            param_list = ""
            for param in params:        # Build parameter list to send
                param_list += format(param, '02x') + " "
            return self.write_read("s " + format(tctlm_id, '02x') + " " + param_list + "p\r")

    def is_open(self):
        '''Is the port currently open?'''
        if not self.port:
            return False
        else:
            return self.port.is_open

    def close(self):
        if self.port: self.port.close()
        if self.loggers: self.loggers['event'].log_text('Successfully disconneted from port.', module="I2C")
        self.active = False