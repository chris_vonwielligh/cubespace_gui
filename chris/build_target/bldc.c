/*
* bldc.c
*
* Created: 2015/07/21 03:40:23 PM
* Author(s): wjordaan/ghjvv
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include "math.h"
#include "bldc.h"
#include "device.h"
#include "spi.h"
#include "tctlm.h"
#include "timer.h"
#include "interrupt.h"

#define stiction_pwm 0.05


static void initialisePWM(void);
static void initialiseStartStopPWM(void);
static float calculateFloatGain(uint16_t gain,uint8_t mult);
static float calculatePWMFloatGain(int16_t gain,uint8_t mult);

static uint8_t ControlFlag = 0;
static uint8_t BackupControlFlag = 0;
static int16_t CurrentDuty = 0;	// Duty cycle for motor[DU]
static bool MotorSwitch = false;
static float MotorSpeed = 0.0;	// Current measured motor speed from Encoder[rps]
static float MotorSpeedBackup = 0.0;	// Current measured motor speed from Hall sensors[rps]
static float MotorSpeedReference = 0.0;	// Current motor speed reference[rps]
static uint16_t MotorCurrent = 0;	// Measured current through motor[DU]
static uint16_t SupplyCurrent = 0;	// Measured current through 3V3[DU]
static int16_t HallCount = 0;	// Current measured speed from Hall sensors[rpm]

static bool HallSwitch = false;
static bool BackupMode = false;
static uint8_t ControlMode = CONTROLMODE_NONE;
static uint8_t SpeedMeasCounter = 0;
static int8_t BackupDirection = 1;

// Main speed controller
static float Ki = 0.0;	//Control law - integrator constant
static float Ki_old = 0.0;	//Control law - integrator constant old value
static float Kp = 0.0;	//Control law - proportional constant
uint16_t GainI = 8;		//CWS= ;	CWM@14.8V=8;	CWL=
uint8_t MultI = 3;		//CWS= ;	CWM@14.8V=3;	CWL=
#ifdef LARGE
uint16_t GainP = 500;    //CWS= ;    CWM@14.8V=200;    CWL=
#else
uint16_t GainP = 280;    //CWS= ;    CWM@14.8V=200;    CWL=
#endif
uint8_t MultP = 3;		//CWS= ;	CWM@14.8V=3;	CWL=

#ifdef AS5048A
#define ENCODERBITS 4096.0
#else
#define ENCODERBITS 1024.0
#endif

// Backup speed controller
static float KiB = 0.0;	//Control law - integrator constant
static float KiB_old = 0.0;	//Control law - integrator constant old value
static float KpB = 0.0;	//Control law - proportional constant
uint16_t GainIB = 60;	//CWS= ;	CWM@14.8= 60;	CWL=
uint8_t MultIB = 6;		//CWS= ;	CWM@14.8= 6;	CWL=
uint16_t GainPB = 11;	//CWS= ;	CWM@14.8= 11;	CWL=
uint8_t MultPB = 3;		//CWS= ;	CWM@14.8= 3;	CWL=

// PWM gain
static float PWM_Gain = 0.0;		//Gain for motor controller
int16_t GainPWM = 4;
uint8_t MultPWM = 1;

//Max speed define
#ifdef SMALL
float maxspeed=133.33;
#else
float maxspeed=101;
#endif

// Encoder
static float EncoderWeight = 0.0;
static float EncoderWeightInverse = 0.0;
static uint8_t EncoderWeightInt_x2 = 190;
static int16_t PreviousAngle = 0;
static float FilteredDeltaAngle = 0.0;
static float FilteredMotorSpeed = 0.0;

static float ei = 0.0;
static float eiB = 0.0;

uint8_t BRAKE_PWM;
uint8_t BRAKEMODE;

bool EncoderSwitch = false;

void BLDC_Init(void)
{
	// initialize PWM clock
	initialisePWM();
	
	// initialize pulse counter for Hall feedback
	TIMER_PulseCounter_Init();
	
	// setup motor direction pin and clear output
	MDIR_DIR = MDIR_DIR | (1<<MDIR);
	MDIR_PORT = MDIR_PORT & (~(1<<MDIR));

	// setup motor power pin and clear output
	MPOWER_DIR = MPOWER_DIR | (1<<MPOWER);
	MPOWER_PORT = MPOWER_PORT & (~(1<<MPOWER));
	
	// setup motor current pin
	ADMUX=(1<<REFS0);
	ADCSRA=(1<<ADEN)|(1<<ADPS2)|(ADPS1)|(ADPS0); //Enable ADC with Prescalar=Fcpu/128
	
	// setup encoder power pin
	EPOWER_DIR = EPOWER_DIR | (1<<EPOWER);
	BLDC_SetEncoderSwitch(false); // FPF2124
	
	// setup hall sensor power pin
	HPOWER_DIR = HPOWER_DIR | (1<<HPOWER);
	BLDC_SetHallSwitch(false); // FPF2124
	
	// Setup Hall sensor A interrupt pin (input)
	HALLA_DIR = HALLA_DIR & (~(1<<HALLA));		// DDRD_3 = 0 ~ input pin
	HALLA_PORT = HALLA_PORT & (~(1<<HALLA));	// Disable the pull-up resistor
	EICRA = EICRA | (1<<ISC11) | (1<<ISC10);	// Enable INT1 pin interrupt on rising edge
	EIMSK = EIMSK | (1<<INT1);					// Set External Interrupt mask for INT1
	
	// Setup Hall sensor B pin (input)
	HALLB_DIR = HALLB_DIR & (~(1<<HALLB));		// DDRD_6 = 0 ~ input pin
	HALLB_PORT = HALLB_PORT & (~(1<<HALLB));	// Disable the pull-up resistor
	
	//Setup Start-stop pin (output)
	DDRD = DDRD | (1<<PORTD6);
	PORTD = PORTD | (1<<PORTD6);
	
	// initialize SPI with encoder
	SPI_Encoder_Init();
	
	// Initialize global module variables
	ControlFlag = 0;
	BackupControlFlag = 0;
	CurrentDuty = 0;
	MotorSwitch = false;
	MotorSpeed = 0.0;
	PreviousAngle = 0;
	FilteredDeltaAngle = 0.0;
	FilteredMotorSpeed = 0.0;
	ControlMode = CONTROLMODE_NONE;
	MotorSpeedReference = 0.0;
	MotorCurrent = 0;
	SupplyCurrent = 0;
	HallCount = 0;
	SpeedMeasCounter = 0;
	BackupDirection = 1;
	EncoderSwitch = false;
	
	// Calculate PWM gain value
	PWM_Gain = calculatePWMFloatGain(GainPWM,MultPWM);
	
	// Calculate gain values for main speed controller
	Ki = calculateFloatGain(GainI,MultI);
	Kp = calculateFloatGain(GainP,MultP);
	
	// Calculate gain values for backup speed controller
	KiB = calculateFloatGain(GainIB,MultIB);
	KpB = calculateFloatGain(GainPB,MultPB);
	
	// Set encoder weight
	BLDC_SetEncoderWeight(EncoderWeightInt_x2);
	
	BRAKEMODE = 0;
	BRAKE_PWM = 0;
}

void BLDC_DetermineSpeed(void)
{
	int16_t currentAngle = 0;
	int16_t deltaAngle = 0;
	
	
	if (EncoderSwitch)
	{
		//cli();
		// working with encoder - switch to mode 1 SPI
		SPCR0 = ((1<<SPE)|(1<<MSTR)|(0<<CPOL)|(1<<CPHA));  // SPI enable, Master
		
		// read current encoder angle
		currentAngle = SPI_Encoder_Read();
		
		//sei();
		// check validity of encoder angle
		if (currentAngle > ENCODERBITS*4)
		{
			deltaAngle = (int16_t) FilteredDeltaAngle;
		}
		else
		{
			deltaAngle = currentAngle - PreviousAngle;	// determine difference
		}		
	}
	
	// *************************** Rollover ***************************
	// Positive rotation
	if (FilteredDeltaAngle >= 0.0)
	{
		if (FilteredDeltaAngle <= ENCODERBITS*2) // Minder as 6000 rpm
		{
			if (deltaAngle < -ENCODERBITS)
			{
				deltaAngle += ENCODERBITS*4;
				
			}
			else if (deltaAngle > ENCODERBITS*3)
			{
				deltaAngle -= ENCODERBITS*4;
				
			}
		}
		else // Meer as 6000 rpm, dus is deltaAngle > ENCODERBITS*2
		{			
			if (deltaAngle < -ENCODERBITS*3)
			{
				deltaAngle += ENCODERBITS*8; // Tot en met deltaAngle = 5120, m.a.w. 15000 rpm
			}
			else if (deltaAngle < ENCODERBITS)
			{
				deltaAngle += ENCODERBITS*4;
			}
		}
	}
	// Negative rotation
	else
	{
		if (FilteredDeltaAngle >= -ENCODERBITS*2) // Meer as -6000 rpm
		{
			if (deltaAngle > ENCODERBITS)
			{
				deltaAngle -= ENCODERBITS*4;
			}
			else if (deltaAngle < -ENCODERBITS*3)
			{
				deltaAngle += ENCODERBITS*4;
			}
		}
		else // Minder as -6000 rpm, dus is deltaAngle < -2048
		{
			if (deltaAngle > ENCODERBITS*3)
			{
				deltaAngle -= ENCODERBITS*8; // Tot en met deltaAngle = -5120, m.a.w. -15000 rpm
			}
			else if (deltaAngle > -ENCODERBITS)
			{
				deltaAngle -= ENCODERBITS*4;
			}
		}
	}
	// ****************************************************************
	
	// Apply first order low-pass filter to measured delta angle
	FilteredDeltaAngle = EncoderWeight*FilteredDeltaAngle + EncoderWeightInverse*((float) deltaAngle);
	PreviousAngle = currentAngle;
	#ifdef AS5048A
	MotorSpeed = FilteredDeltaAngle*0.012207031;//rps
	#endif
	
	#ifdef AS5055A
	MotorSpeed = FilteredDeltaAngle*0.048828125;//rps
	#endif
	
	#ifdef MA702
	MotorSpeed = FilteredDeltaAngle*-0.048828125;//rps
	#endif

	g_WheelSpeed = (int16_t)(120.0*MotorSpeed);
}

void BLDC_DetermineBackupSpeed(void)
{
	int16_t temp_TCNT3 = 0;
	bool hallInterruptFlag = false;
	
	if(HallSwitch==true)
	{
		SpeedMeasCounter++;
		if(SpeedMeasCounter >= 10)
		{
			// Set backup control flag
			BackupControlFlag = true;
		
			// Disable timer 0	
			TCCR3B &= ~0x07;
		
			// Read timer value and set to zero	
			temp_TCNT3 = (int16_t)TCNT3;
			TCNT3 = 0;
			
			// Check interrupt timer overflow flag
			if (TIFR3 & 0x01)
			{
				hallInterruptFlag = true;
			}
		
			// Enable timer 0
			TCCR3B |= (1 << CS32) | (1 << CS31) | (1 << CS30);
		
			// Compensate for interrupt flag if necessary
			if (hallInterruptFlag)
			{
				// Reset the interrupt flag (must write a one to clear - from data sheet p.110)
				TIFR3 |= 0x01;
			
				// Manually increment overflow counter
				g_Overflow_CNT++;
			}
		
			// Calculate backup speed [rpm]
			HallCount = ((int16_t)BackupDirection)*(((int16_t)g_Overflow_CNT)*256 + temp_TCNT3)*10;
		
			// Clear overflow counter
			g_Overflow_CNT = 0;
		
			// Zero 100 ms counter
			SpeedMeasCounter = 0;
		}
	}
	
}

void BLDC_WheelControl(void)
{
	float motorPWM = 0.0;
	
	float ek = 0.0;
	float ekB = 0.0;
		
	float ui = 0.0;
	float up = 0.0;
	float ud = 0.0;
	
	float uiB = 0.0;
	float upB = 0.0;
	float udB = 0.0;
		
	// convert delta angle to rps
	MotorSpeedBackup = ((float)HallCount)*0.016666667;		// Transform hall sensor measurement rpm to rev/s
	
	switch (ControlMode)
	{
		case CONTROLMODE_NONE:
		case CONTROLMODE_NOCONTROL:
			// Clear die PWM register
			STARTSTOP_LOW; // Release brake
			OCR1A = 0;
			
			BRAKE_PWM = 0;
			BRAKEMODE = 0;
		
			// Clear other control modes variables
			CurrentDuty = 0;
			MotorSpeedReference = 0.0;
		
			// Clear telemetry info
			g_WheelDuty = 0;
			g_WheelReference = 0;
		break;
		
		case CONTROLMODE_DUTY:
			// Set the PWM register with the duty cycle
			STARTSTOP_LOW; // Release brake
			if ((MotorSpeedBackup > maxspeed || MotorSpeedBackup < -maxspeed) || MotorSpeed > maxspeed|| MotorSpeed < -maxspeed)        //Set max speed (166.66 => 10000rpm)
			{
				CurrentDuty = 0;
			}
			
			if (CurrentDuty>0)
			{
				if (MotorSpeed < -23)
				{
					// set register
					OCR1A = 0;
					if ((5.0*60*((float)CurrentDuty)) > 60)
					{
						BRAKE_PWM =60;
					}
					else
					{
						BRAKE_PWM = (uint8_t)(5.0*60*((float)CurrentDuty));	
					}
					
					BRAKEMODE = 1;
				}
				else
				{
					BRAKE_PWM = 0;
					BRAKEMODE = 0;
					// set positive direction
					MDIR_PORT = MDIR_PORT & (~(1<<MDIR));
				
					// set register
					OCR1A = CurrentDuty;				
				}
			}
			else if (CurrentDuty==0)
			{	
				// set register
				OCR1A = 1;
			}
			else
			{		
				if (MotorSpeed > 23)
				{
					// set register
					OCR1A = 0;
					
					if ((5.0*60*((float)CurrentDuty)) < -60)
					{
						BRAKE_PWM = 60;
					}
					else
					{
						BRAKE_PWM = (uint8_t)(5.0*-60*((float)CurrentDuty));
					}
					
					BRAKEMODE = 1;
				}
				else
				{
					BRAKE_PWM = 0;
					BRAKEMODE = 0;
					// set negative direction
					MDIR_PORT = MDIR_PORT | (1<<MDIR);
			
					// set register
					OCR1A = (-1*CurrentDuty);
				}
			}
		
			// Clear other control modes variables
			MotorSpeedReference = 0.0;
			
			// Clear telemetry info
			g_WheelReference = 0;
		break;
		
		case CONTROLMODE_SPEED:
			if(BackupMode==true)
			{
				if (BackupControlFlag==true)
				{
					// Runaway speed protection
					if (MotorSpeedBackup > maxspeed || MotorSpeedBackup < -maxspeed)        //Set max speed (166.66 => 10000rpm)
					{
						motorPWM = 0.0;
					}
				
					// Disable controller at low speeds (0.25 => 15rpm)
					else if((MotorSpeedReference < 0.25) && (MotorSpeedReference > -0.25))
					{
						BRAKE_PWM = 60;
						BRAKEMODE = 1;
						motorPWM = 0.0;
						MotorSpeedReference = 0.0;
						eiB=0;
					}				
					// Perform backup speed controller (using Hall sensors)
					else
					{
						if (KiB_old != KiB)
						{
							eiB = 0;
						}
						
						ekB = MotorSpeedReference - MotorSpeedBackup;
						upB = KiB*ekB;
						uiB = KiB*eiB;
						udB = KpB*MotorSpeedBackup;
						motorPWM = (upB + uiB - udB)*PWM_Gain;
						
						// Integrator wind-up protection
						if ((motorPWM < 1.0) && (motorPWM > -1.0))
						{
							eiB = eiB + ekB;
						}
						
						// The LV8827 driver has an issue with changing direction at high speeds (>1400rpm)
						// Therefor, if at high speed, use braking instead of negative PWM
						if (MotorSpeedBackup > 0)
						{
							if (motorPWM < 0)
							{
								if (MotorSpeedBackup > 20) // Roughly 1400rpm
								{
									OCR1A = 0;
									if ((60*motorPWM*5.0) < -60)
									{
										BRAKE_PWM =60;
									}
									else
									{
										BRAKE_PWM = (uint8_t)(-60*motorPWM*5.0);
									}
									BRAKEMODE = 1;
								}
								else
								{
									if (BRAKEMODE == 1)
									{
										STARTSTOP_LOW; // Release brake
										BRAKE_PWM = 0;
										BRAKEMODE = 0;
									}
								}
							}
							else
							{
								// Release brake
								BRAKE_PWM = 0;
								BRAKEMODE = 0;
								STARTSTOP_LOW;
							}
						}
						else if (MotorSpeedBackup < 0)
						{
							if (motorPWM > 0)
							{
								if (MotorSpeedBackup < -20) // Roughly -1400rpm
								{
									OCR1A = 0;
									if ((60*motorPWM*5.0) > 60)
									{
										BRAKE_PWM = 60;
									}
									else
									{
										BRAKE_PWM = (uint8_t)(60*motorPWM*5.0);
									}
									BRAKEMODE = 1;
								}
								else
								{
									if (BRAKEMODE == 1)
									{
										STARTSTOP_LOW; // Release brake
										BRAKE_PWM = 0;
										BRAKEMODE = 0;
									}
								}
							}
							else
							{
								// Release brake
								BRAKE_PWM = 0;
								BRAKEMODE = 0;
								STARTSTOP_LOW;
							}
						}
						else
						{
							// Release brake
							BRAKE_PWM = 0;
							BRAKEMODE = 0;
							STARTSTOP_LOW;
						}
						
						// Cap PWM signals to max allowable
						if (motorPWM <= -1.0)
						{
							motorPWM = -1.0;
						}
						else if (motorPWM >= 1.0)
						{
							motorPWM = 1.0;
						}
					}	
					CurrentDuty = (int16_t)(motorPWM*PWM_LIMIT_FLOAT);
						
					// Save old Ki to sense when Ki is changed. If Ki is changed while control is running, might cause instability.
					KiB_old = KiB;
				}
			}
			else
			{
				// Perform speed controller with magnetic encoder
				if ((MotorSpeed > maxspeed) || (MotorSpeed < -maxspeed) || !EncoderSwitch)        //Set max speed (133.3 => 8000 rpm)(166.66 => 10000rpm)
				{
					motorPWM = 0.0;
				}
				else
				{	
					// If integral gain changes, controller might become unstable. So reset integrator value
					if (Ki_old != Ki)
					{
						ei = 0;
				    }
				                    
				                    
				    ek = MotorSpeedReference - MotorSpeed;
				                    
				    if((MotorSpeed > -0.04) && (MotorSpeed < 0.04)) // If the speed is 0. Small range to account for rounding errors and speed filtering
				    {
					    // If our little boost in the integrator is still slightly too small to get the wheel going, we want the integrator to grow normally until it reaches enough oomph to get going.
					    // If the (0.02/PWM...) boost is not enough, and if the below check is not included, the integrator will stay at (0.02/PWm...) and the wheel will only get going one the proportional part is large enough.
					    // Our controller relies heavily on the integrator, so the wheel will stay stuck at zero for a VERY long time. This check is crucial.
					    // PS: I am referring to the if (ei < (0.02/(PWM_Gain*Ki))) and if (ei > (-0.02/(PWM_Gain*Ki))) checks
					    // PSS: ei = ei + ... part added to add quicker integration if the boost is not enough
					    // PSSS: We get a slight integrator wind-up effect. Added small proportional boost as well.
					    if(MotorSpeedReference>0)
					    {
						    if (ei < (0.02/(PWM_Gain*Ki)))
						    {
							    ei = 0.02/(PWM_Gain*Ki);
						    }
						    else
						    {
							    ei = ei + 0.0005/(PWM_Gain*Ki);
						    }
						    //ek =  0.01/(PWM_Gain*Ki);
					    }
					    else if(MotorSpeedReference<0)
					    {
						    if (ei > (-0.02/(PWM_Gain*Ki)))
						    {
							    ei = -0.02/(PWM_Gain*Ki);
						    }
						    else
						    {
							    ei = ei - 0.0005/(PWM_Gain*Ki);
						    }
						    //ek =  -0.01/(PWM_Gain*Ki);
					    }
					                    
				    }
					up = Ki*ek;
					ui = Ki*ei;
					ud = Kp*MotorSpeed;
					
					motorPWM = (up + ui - ud)*PWM_Gain;					
				
					// Integrator wind-up protection
					if ((motorPWM < 1.0) && (motorPWM > -1.0))
					{
						ei = ei + ek;
					}			
					
					// The LV8827 driver has an issue with changing direction at high speeds (>1400rpm)
					// Therefor, if at high speed, use braking instead of negative PWM					
					if (MotorSpeed > 0)
					{
						if (motorPWM < 0)						
						{
							if (MotorSpeed > 20) // Roughly 1400rpm
							{
								OCR1A = 0;							
								if ((60*motorPWM*5.0) < -60)
								{
									BRAKE_PWM =60;
								}
								else
								{
									BRAKE_PWM = (uint8_t)(-60*motorPWM*5.0);
								}				
								BRAKEMODE = 1;
							}
							else
							{
								if (BRAKEMODE == 1)
								{
									STARTSTOP_LOW; // Release brake
									BRAKE_PWM = 0;
									BRAKEMODE = 0;
								}							
							}						
						}
						else
						{		
							// Release brake				
							BRAKE_PWM = 0;
							BRAKEMODE = 0;
							STARTSTOP_LOW;
						}				
					}
					else if (MotorSpeed < 0)
					{
						if (motorPWM > 0)
						{
							if (MotorSpeed < -20) // Roughly -1400rpm
							{
								OCR1A = 0;
								if ((60*motorPWM*5.0) > 60)//brake hard
								{
									BRAKE_PWM = 60;
								}
								else
								{
									BRAKE_PWM = (uint8_t)(60*motorPWM*5.0);
								}			
								BRAKEMODE = 1;
							}
							else
							{
								if (BRAKEMODE == 1)
								{
									BRAKEMODE = 0;
									STARTSTOP_LOW; // Release brake
									BRAKE_PWM = 0;
								}
							}
						}
						else
						{
							// Release brake
							BRAKE_PWM = 0;
							BRAKEMODE = 0;
							STARTSTOP_LOW; 						
						}
					}
					else//motor speed equals zero
					{
						// Release brake
						BRAKE_PWM = 0;
						BRAKEMODE = 0;
						STARTSTOP_LOW;
					}
						
					// Cap PWM signals to max allowable		
					if (motorPWM <= -1.0)				
					{
						motorPWM = -1.0;
					}				
					else if (motorPWM >= 1.0)
					{
						motorPWM = 1.0;
					}
				}
			
				CurrentDuty = (int16_t)(motorPWM*PWM_LIMIT_FLOAT);
			
				// Save old Ki to sense when Ki is changed. If Ki is changed while control is running, might cause instability.
				Ki_old = Ki;
			}
		
			// Set the PWM register with the duty cycle
			if (BRAKEMODE)
			{
				OCR1A = 0;
			}
			else
			{
				if (CurrentDuty>0)
				{
					// set positive direction
					MDIR_PORT = MDIR_PORT & (~(1<<MDIR));
				
					// set register
					OCR1A = CurrentDuty;
				}
				else if (CurrentDuty<0)
				{
					// set negative direction
					MDIR_PORT = MDIR_PORT | (1<<MDIR);
				
					// set register
					OCR1A = (-1*CurrentDuty);
				}
				else
				{
					// if (PWM = 0) leave direction the same
					// set register
					OCR1A = 0;
				}
			}		
		break;
		
		default:
			// Release brake
			BRAKE_PWM = 0;
			BRAKEMODE = 0;
			STARTSTOP_LOW;
			ControlMode = CONTROLMODE_NONE;
			break;
	}
	
	// update telemetry variable
	g_MotorSwitch = (uint8_t)MotorSwitch;
	g_HallSwitch = (uint8_t)HallSwitch;
	g_EncoderSwitch = (uint8_t)EncoderSwitch;
	if (CurrentDuty >= 0)
	{
		g_WheelDuty = (int16_t)(((float)CurrentDuty)*100.0/PWM_LIMIT_FLOAT + 0.5);
	}
	else
	{
		g_WheelDuty = (int16_t)(((float)CurrentDuty)*100.0/PWM_LIMIT_FLOAT - 0.5);
	}
	g_ControlMode = (uint8_t)ControlMode;
	g_BackupMode = (uint8_t)BackupMode;
	g_WheelSpeed = (int16_t)(120.0*MotorSpeed);
	g_HallCount = HallCount;
}

void BLDC_SetControlFlag(uint8_t input)
{
	ControlFlag = input;
}

uint8_t BLDC_GetControlFlag(void)
{
	return ControlFlag;
}

void BLDC_SetDutyCycle(int16_t duty)
{
	// set internal variable
	CurrentDuty = (int16_t)((((float)duty)/100.0)*PWM_LIMIT_INT);
	
	// Zero the reference speed (since the mode is no longer SpeedController)
	MotorSpeedReference = 0.0;
	
	// set control mode to duty cycle
	BLDC_SetControlMode(CONTROLMODE_DUTY);
	
	// update telemetry variable
	if (CurrentDuty >= 0)
	{
		g_WheelDuty = (int16_t)(((float)CurrentDuty)*100.0/PWM_LIMIT_FLOAT + 0.5);
	}
	else
	{
		g_WheelDuty = (int16_t)(((float)CurrentDuty)*100.0/PWM_LIMIT_FLOAT - 0.5);
	}
	g_WheelReference = (int16_t)(0);
}

void BLDC_SetWheelSpeed(int16_t speed)
{
	float newMotorSpeedReference = 0.0;
	
	// Store speed reference
	newMotorSpeedReference = ((float)speed)/120.0; // half_rpm -> rpm: divide by 2 ||| rpm -> rps: divide by 60
		
	// set internal variable
	MotorSpeedReference = newMotorSpeedReference;
	if (MotorSpeedReference != newMotorSpeedReference)
	{
		MotorSpeedReference = newMotorSpeedReference;	
	}
	
	// set control mode to speed command following
	BLDC_SetControlMode(CONTROLMODE_SPEED);
	
	// update telemetry variable
	if (MotorSpeedReference >= 0.0)
	{
		g_WheelReference = (int16_t)(120.0*MotorSpeedReference + 0.5);
	}
	else
	{
		g_WheelReference = (int16_t)(120.0*MotorSpeedReference - 0.5);
	}
}

void BLDC_SetMotorSwitch(bool mswitch)
{
	// set internal variable
	MotorSwitch = mswitch;
	
	// set motor power switch
	if(MotorSwitch==true)
	{
		// Set power IO
		MPOWER_PORT = MPOWER_PORT | (1<<MPOWER);
	}
	else
	{
		// Clear power IO
		MPOWER_PORT = MPOWER_PORT & (~(1<<MPOWER));
	}
}

void BLDC_SetEncoderSwitch(bool eswitch)
{
	// set internal variable
	EncoderSwitch = eswitch;
	
	// set motor power switch
	if(EncoderSwitch==true)
	{
		// Set power IO
		EPOWER_PORT = EPOWER_PORT | (1<<EPOWER); // FPF2124
	}
	else
	{
		// Clear power IO
		EPOWER_PORT = EPOWER_PORT & (~(1<<EPOWER)); // FPF2124
	}
}

void BLDC_SetHallSwitch(bool hswitch)
{
	// set internal variable
	HallSwitch = hswitch;
	
	// set motor power switch
	if(HallSwitch==true)
	{
		// Set power IO
		HPOWER_PORT = HPOWER_PORT | (1<<HPOWER); // FPF2124
	}
	else
	{
		// Clear power IO
		HPOWER_PORT = HPOWER_PORT & (~(1<<HPOWER)); // FPF2124
		
		HallCount=0;
	}
}

void BLDC_MeasureMotorCurrent(void)
{
	// Sample motor current
	//Select ADC Channel ch must be 0-7
	uint8_t ch = 0;
	uint8_t timeout=0;
	
	ch=0b01000000;
	ADMUX=ch;

	//Start Single conversion
	ADCSRA|=(1<<ADSC);

	//Wait for conversion to complete
	while(!(ADCSRA & (1<<ADIF)))
	{
		if(timeout>200)
		{
			g_LoopErrorFlags=g_LoopErrorFlags|0b00000001;
			break;
		}
		timeout++;
	}

	//Clear ADIF by writing one to it
	ADCSRA|=(1<<ADIF);
	
	MotorCurrent = ADC;
	
	// update telemetry variable
	g_WheelCurrent = MotorCurrent;
}

void BLDC_Measure3V3Current(void)
{
	// Sample 3V3 current
	//Select ADC Channel ch must be 0-7
	uint8_t ch = 6;
	uint8_t timeout=0;
	ch=0b01000110;
	ADMUX=ch;

	//Start Single conversion
	ADCSRA|=(1<<ADSC);

	//Wait for conversion to complete
	while(!(ADCSRA & (1<<ADIF)))
	{
		if(timeout>200)
		{
			g_LoopErrorFlags=g_LoopErrorFlags|0b00000010;
			break;
		}
		timeout++;
	}

	//Clear ADIF by writing one to it
	ADCSRA|=(1<<ADIF);
	
	SupplyCurrent = ADC;
	
	// update telemetry variable
	g_DigitalCurrent = SupplyCurrent;
}

void BLDC_DetermineBackupDirection(void)
{
	// This function is called when Hall sensor A gives a rising edge.
	// If Hall sensor B (PD6) is low, the direction of the rotation
	// negative. If PD6 is high, the direction is positive.
	
	uint8_t hallB_state = 0;
	
	// Store Hall B state
	hallB_state = PINE & 0b00000001;
	
	// Determine direction based on Hall B's state
	if (hallB_state == 0b00000000)
	{
		// Hall A has now pulsed just before Hall B, hence a negative rotation
		BackupDirection = -1;
	}
	else
	{
		// Hall B has now pulsed just before Hall A, hence a positive rotation
		BackupDirection = 1;
	}
}

void BLDC_SetPWMGain(int16_t gain, uint8_t mult)
{
	GainPWM = gain;
	MultPWM = mult;
	PWM_Gain = calculatePWMFloatGain(GainPWM, MultPWM);
}

void BLDC_SetMainControllerGain(uint16_t intGain, uint8_t intMult, uint16_t feedGain, uint8_t feedMult)
{	
	GainI = intGain;
	MultI = intMult;
	
	GainP = feedGain;
	MultP = feedMult;
	
	Ki = calculateFloatGain(GainI,MultI);
	Kp = calculateFloatGain(GainP,MultP);
}

void BLDC_SetBackupControllerGain(uint16_t intGain, uint8_t intMult, uint16_t feedGain, uint8_t feedMult)
{
	GainIB = intGain;
	MultIB = intMult;
	
	GainPB = feedGain;
	MultPB = feedMult;
	
	KiB = calculateFloatGain(GainIB,MultIB);
	KpB = calculateFloatGain(GainPB,MultPB);
}




void BLDC_SetControlMode(uint8_t mode)
{
	// set only if a valid control mode was selected
	switch(mode)
	{
		case 0:
		ControlMode = CONTROLMODE_NONE;
		eiB = 0;
		ei = 0;
		BLDC_SetMotorSwitch(false);
		BLDC_SetHallSwitch(false);
		BLDC_SetEncoderSwitch(false);
		break;
		
		case 1:
		ControlMode = CONTROLMODE_NOCONTROL;
		eiB = 0;
		ei = 0;
		BLDC_SetMotorSwitch(false);
		BLDC_SetHallSwitch(true);
		if(!BackupMode)
		{
			BLDC_SetEncoderSwitch(true);
		}
		break;
		
		case 2:
		ControlMode = CONTROLMODE_DUTY;
		eiB = 0;
		ei = 0;
		BLDC_SetMotorSwitch(true);
		BLDC_SetHallSwitch(true);
		if(!BackupMode)
		{
			BLDC_SetEncoderSwitch(true);
		}
		break;
		
		case 3:
		ControlMode = CONTROLMODE_SPEED;		
		BLDC_SetMotorSwitch(true);
		BLDC_SetHallSwitch(true);
		if(!BackupMode)
		{
			BLDC_SetEncoderSwitch(true);
		}
		break;
		
		default:
		break;
	}
}

bool BLDC_GetEncoderSwitch()
{
	return EncoderSwitch;
}

void BLDC_SetRunMode(bool state)
{
	// set the run mode - whether backup mode is enabled or not
	BackupMode = state;
	
	// turn off encoder switch if backup mode is selected
	if((EncoderSwitch==true) && (BackupMode==true))
	{
		BLDC_SetEncoderSwitch(false);
	}
	
	// turn on encoder if backup mode is removed any control mode except Idle is selected
	if((BackupMode==false) && (ControlMode>0))
	{
		BLDC_SetEncoderSwitch(true);
	}
}

void BLDC_SetEncoderWeight(uint8_t intWeight_x2)
{
	// Store encoder weight as a percentage_x2
	EncoderWeightInt_x2 = intWeight_x2;
	
	// Calculate actual weight
	EncoderWeight = 0.005*((float)EncoderWeightInt_x2);
	EncoderWeightInverse = 1.0 - EncoderWeight;
	
	// Set global variable
	g_EncoderWeight = EncoderWeightInt_x2;
}

void BLDC_GetPWMGain(uint8_t* buffer)
{
	*( (uint16_t*)(buffer+0) ) = GainPWM;
	*( (uint8_t*)(buffer+2) ) = MultPWM;
}

void BLDC_GetMainGain(uint8_t* buffer)
{
	*( (uint16_t*)(buffer+0) ) = GainI;
	*( (uint8_t*)(buffer+2) ) = MultI;
	*( (uint16_t*)(buffer+3) ) = GainP;
	*( (uint8_t*)(buffer+5) ) = MultP;
}

void BLDC_GetBackupGain(uint8_t* buffer)
{
	*( (uint16_t*)(buffer+0) ) = GainIB;
	*( (uint8_t*)(buffer+2) ) = MultIB;
	*( (uint16_t*)(buffer+3) ) = GainPB;
	*( (uint8_t*)(buffer+5) ) = MultPB;
}

static void initialisePWM(void)
{
	ICR1 = PWM_LIMIT_INT;	// set PWM clock frequency
	
	TCCR1A=(1<<WGM11)|(0 <<WGM10)|(1<<COM1A1)|(1<<COM1A0); // Fast PWM OCR make WGM11,WGM10 to 1...COM1C1 & COM1C0 are for inverting/non-inverting mode
	
	TCCR1B=(1<<WGM13)|(1<<WGM12)|(0<<CS12)|(0<<CS11)|(1<<CS10);//Fast PWM OCR make WGM13,WGM12 to 1...CS12,CS11 & CS10 sets the prescaler/clock divider
	
	MPWM_DIR = MPWM_DIR | (1<<MPWM);  // select OC1A as output pin
	
	// Begin with no PWM signal
	OCR1A = 0;
}

static void initialiseStartStopPWM(void)
{
	
	TCCR0A=(1<<WGM01)|(1<<WGM00)|(1<<COM0A1)|(0<<COM0A0); // Fast PWM OCR...COM1C1 & COM1C0 are for inverting/non-inverting mode
	
	TCCR0B=(0<<WGM02)|(1<<CS02)|(0<<CS01)|(1<<CS00);//Fast PWM OCR...CS12,CS11 & CS10 sets the prescaler/clock divider
	
	SSPWM_DIR = SSPWM_DIR | (1<<SSPWM);  // select OC0A as output pin
	
	// Begin with no PWM signal
	OCR0A = 0;
}

static float calculatePWMFloatGain(int16_t gain,uint8_t mult)
{
	float temp;
	uint8_t i;
	
	temp = (float)gain;
	for(i=0;i<mult;i++)
	{
		temp = temp/10.0;
	}
	
	return temp;
}

static float calculateFloatGain(uint16_t gain,uint8_t mult)
{
	float temp;
	uint8_t i;
	
	temp = (float)gain;
	for(i=0;i<mult;i++)
	{
		temp = temp/10.0;
	}
	
	return temp;
}
