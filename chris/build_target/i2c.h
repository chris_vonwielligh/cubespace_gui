/*
 * i2c.h
 *
 * Created: 2015/07/21 01:55:02 PM
 * Author(s): wjordaan/ghjvv
 */ 


#ifndef I2C_H_
#define I2C_H_

#define I2C_READ_ADRESS_RECEIVED 0xA8
#define I2C_READ_DATA_RECEIVED 0xA8

//setup the I2C hardware to ACK the next transmission
#define I2C_TWACK (TWCR0=(1<<TWINT)|(1<<TWEN)|(1<<TWEA))
//setup the I2C hardware to NACK the next transmission
#define I2C_TWNACK (TWCR0=(1<<TWINT)|(1<<TWEN))
//reset the I2C hardware (used when the bus is in a illegal state)
#define I2C_TWRESET (TWCR0=(1<<TWINT)|(1<<TWEN)|(1<<TWSTO)|(1<<TWEA))

void I2C_Init(uint8_t i2cAddress);

#endif /* I2C_H_ */