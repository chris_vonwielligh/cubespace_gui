/*
 * uart.h
 *
 * Created: 2015/07/21 01:55:31 PM
 * Author(s): wjordaan/ghjvv
 */ 


#ifndef UART_H_
#define UART_H_

#include <stdint.h>
#include "device.h"

// Set baud rate to 115200
#define UBBR_VALUE 10

extern uint8_t g_LoopErrorFlags;

void sendByte(uint8_t data);
void UART_Init(void);
void UART_TxBuffer(uint8_t* buff, uint16_t len);

#endif /* UART_H_ */