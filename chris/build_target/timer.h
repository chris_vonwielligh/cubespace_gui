/*
 * timer.h
 *
 * Created: 2015/07/23 08:18:39 AM
 * Author(s): wjordaan/ghjvv
 */ 


#ifndef TIMER_H_
#define TIMER_H_

#include "device.h"

#define TIMER_COUNTER 12 // for 332.8us timer

void TIMER_Init(void);
void TIMER_PulseCounter_Init(void);

void TIMER_Reset(void);
void TIMER_PulseCounter_Reset(void);
void TIMER_Increment20Milli(void);
void TIMER_Increment100Milli(void);
void TIMER_Increment200Milli(void);

#endif /* TIMER_H_ */