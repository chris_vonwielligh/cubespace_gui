/*
 * can.c
 *
 * Created: 2015/12/10 09:22:37 AM
 * Author(s): wjordaan/ghjvv
 */ 
#include <avr/io.h>
#include "can.h"
#include "device.h"
#include <util/delay.h>
#include "uart.h"
#include "spi.h"

// Still need to test the 1Mbit/s settings
#define CAN_1MBPS_CNF1 0x00
#define CAN_1MBPS_CNF2 0x80
#define CAN_1MBPS_CNF3 0x01

#define CAN_500KBPS_CNF1 0x00
#define CAN_500KBPS_CNF2 0x89
#define CAN_500KBPS_CNF3 0x04

#define CAN_250KBPS_CNF1 0x00
#define CAN_250KBPS_CNF2 0xAC
#define CAN_250KBPS_CNF3 0x07

#define CAN_RX0IDMASK		0x700											///< CAN standard ID mask
#define CAN_RX0IDFILTER		0x500											///< CAN standard ID filter

static uint8_t id2RegH(uint16_t id);
static uint8_t id2RegL(uint16_t id);

void CAN_Init(uint8_t canAddress)
{
	
	// setup CAN chip select pin and set output
	SS_CAN_DIR = SS_CAN_DIR | (1<<SS_CAN);
	SS_CAN_PORT = SS_CAN_PORT | (1<<SS_CAN);
	
	// setup reset pin of CAN and set to low (nRESET)
	CAN_RESET_DIR = CAN_RESET_DIR | (1<<CAN_RESET);
	CAN_RESET_PORT = CAN_RESET_PORT | (0<<CAN_RESET);//DOUW - Changed polarity here to make it properly reset
	
	// setup the external interrupt to receive CAN messages
	DDRD = DDRD & (~(1<<PORTD2));
	PORTD =  PORTD | (1<<PORTD2);
	
	EICRA = EICRA | (1<<ISC01);
	EIMSK = EIMSK | (1<<INT0);
	
	// MCP reset
	CAN_RESET_PORT = CAN_RESET_PORT | (1<<CAN_RESET);
	
	// MCP bit timing setup
	CAN_Write(MCP_CNF1, CAN_1MBPS_CNF1);
	CAN_Write(MCP_CNF2, CAN_1MBPS_CNF2);
	CAN_Write(MCP_CNF3, CAN_1MBPS_CNF3);
	
	// MCP RX masks and filter setup
	CAN_Write(MCP_RXM0SIDH,0x00);
	CAN_Write(MCP_RXM0SIDL,0x00);
	CAN_Write(MCP_RXM0EID8,0x00);
	CAN_Write(MCP_RXM0EID0,0xFF);
	
	CAN_Write(MCP_RXF0SIDH,0x00);
	CAN_Write(MCP_RXF0SIDL,0x08);
	CAN_Write(MCP_RXF0EID8,0x00);
	CAN_Write(MCP_RXF0EID0,canAddress);
	
	// set mask and filter for RX1 => 0x000
	CAN_Write( MCP_RXM1SIDH, id2RegH(0x0FFF) );
	CAN_Write( MCP_RXM1SIDL, id2RegL(0x0FFF) );
	
	// MCP RX interrupt and operation setup
	CAN_Write( MCP_CANINTE, MCP_RX_INT ); // interrupts
	CAN_Write( MCP_CANCTRL, ( MODE_NORMAL | CLKOUT_DISABLE ) ); // control register
}

uint8_t CAN_Read (uint8_t addr)
{
	uint8_t data;

	SS_CAN_LOW;
	
	_delay_us(2);
	
	SPI_Transmit(MCP_READ) ;
	
	SPI_Transmit(addr) ;
	
	data=SPI_Transmit(0xFF) ;

	SS_CAN_HIGH;
	
	_delay_us(2);

	return data;
}

void CAN_Write (uint8_t addr, uint8_t data)
{
	SS_CAN_LOW;
	
	_delay_us(2);

	SPI_Transmit(MCP_WRITE) ;
	
	SPI_Transmit(addr) ;
	
	SPI_Transmit(data) ;

	SS_CAN_HIGH;
	
	_delay_us(2);
}

void CAN_readRxBuffer (BSP_CAN_RxReadAddr_TypeDef startAddr, uint8_t *buffer, uint8_t complete)
{
	uint8_t i, len;

	switch(startAddr)
	{
		case bspCanRx0Id:
		case bspCanRx1Id:
		if (complete)
		{
			len = 13;
		}
		else
		{
			len = 5;
		}
		break;

		case bspCanRx0Data:
		case bspCanRx1Data:
		len = 8;
		break;

		default:
		len = 0;
		break;
	}

	SS_CAN_LOW;
	
	_delay_us(2);

	SPI_Transmit(startAddr) ;

	for (i = 0; i < len; i++)
	{
		buffer[i] = SPI_Transmit(0xFF);	
	}

	SS_CAN_HIGH;
	
	_delay_us(2);
}

void CAN_writeTxBuffer (BSP_CAN_TxLoadAddr_TypeDef startAddr, uint8_t *buffer, uint8_t complete)
{
	uint8_t i, len;

	switch(startAddr)
	{
		case bspCanTx0Id:
		case bspCanTx1Id:
		case bspCanTx2Id:
		if (complete)
		{
			len = 13;
		}
		else
		{
			len = 5;
		}
		break;

		case bspCanTx0Data:
		case bspCanTx1Data:
		case bspCanTx2Data:
		len = 8;
		break;

		default:
		len = 0;
		break;
	}

	SS_CAN_LOW;
	
	_delay_us(2);

	SPI_Transmit(startAddr) ;

	for (i = 0; i < len; i++)
	{
		SPI_Transmit(buffer[i]) ;
	}

	SS_CAN_HIGH;
	
	_delay_us(2);
}

void CAN_requestToSend (BSP_CAN_RequestToSend_TypeDef rtsSelect)
{
	SS_CAN_LOW;
	
	_delay_us(2);

	SPI_Transmit((uint8_t) rtsSelect);

	SS_CAN_HIGH;
	
	_delay_us(2);
}

uint8_t CAN_reg2Id(uint8_t idH,uint8_t idL)
{
	// Retrieve standard ID from MCP2515 register format
	return ((idH << 3) & 0xF8) | ((idL >> 5) & 0x07);
}

static uint8_t id2RegH(uint16_t id)
{
	// Convert standard ID to MCP2515 SIDH format
	return ( (id >> 3) & 0x0FF );
}

static uint8_t id2RegL(uint16_t id)
{
	// Convert standard ID to MCP2515 SIDL format
	return ( (id << 5) & 0x0E0 );
}

uint8_t CAN_extractMessageType(uint8_t buffer0)
{
	// Retrieve messageType
	return ((buffer0 >> 3) & 0x1F);
}

uint8_t CAN_extractChannel(uint8_t buffer0, uint8_t buffer1)
{
	// Retrieve Channel
	return ((buffer0 << 5) & 0xE0) | ((buffer1 >> 3) & 0x1C) | (buffer1 & 0x03);
}

uint8_t CAN_extractSource(uint8_t buffer2)
{
	// Retrieve Source
	return buffer2;
}

uint8_t CAN_extractDestination(uint8_t buffer3)
{
	// Retrieve Destination
	return buffer3;
}

void CAN_createMessageID(uint8_t* buffer, uint8_t messageType,uint8_t channel, uint8_t source, uint8_t destination)
{
	buffer[0] = ((messageType << 3) & 0xF8) | ((channel >> 5) & 0x07);
	buffer[1] = ((channel << 3) & 0xE0) | (channel & 0x03);
	buffer[2] = source;
	buffer[3] = destination;
}