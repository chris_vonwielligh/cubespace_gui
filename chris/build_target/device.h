/*
 * device.h
 *
 * Created: 2015/07/21 01:53:27 PM
 * Author(s): wjordaan/ghjvv
 */ 
#include <avr/io.h>
#include "config.h"

#ifndef DEVICE_H_
#define DEVICE_H_

#define F_CPU 10000000UL			// 10MHz crystal
#include <util/delay.h>
#include <stdbool.h>

//Encoder used
#define AS5048A
//#define AS5055A
//#define MA702

// Identification defines
#define NODE_ID	8					// Node ID

//Temperature sensor calibration values
#define TEMPGAIN 1.2
#define TEMPOFFSET -83

// Encoder SPI defines
#define SS_ENCODER_DIR DDRB
#define SS_ENCODER_PORT PORTB
#define SS_ENCODER PORTB2
#define SS_ENCODER_HIGH (SS_ENCODER_PORT |= (1<<SS_ENCODER))
#define SS_ENCODER_LOW (SS_ENCODER_PORT &= ~(1<<SS_ENCODER))

// CAN defines
#define SS_CAN_DIR DDRD
#define SS_CAN_PORT PORTD
#define SS_CAN PORTD4
#define SS_CAN_HIGH (SS_CAN_PORT |= (1<<SS_CAN))
#define SS_CAN_LOW (SS_CAN_PORT &= ~(1<<SS_CAN))

#define CAN_RESET_DIR DDRC
#define CAN_RESET_PORT PORTC
#define CAN_RESET PORTC1

// Output and input pin defines
#define STARTSTOP_LOW (PORTD &= ~(1<<PORTD6))
#define STARTSTOP_HIGH (PORTD |= (1<<PORTD6))


#define MDIR_DIR	DDRD
#define MDIR_PORT	PORTD
#define MDIR		PORTD5

#define MPOWER_DIR	DDRD
#define MPOWER_PORT PORTD
#define MPOWER		PORTD7

#define EPOWER_DIR	DDRC
#define EPOWER_PORT PORTC
#define EPOWER		PORTC3

#define HPOWER_DIR	DDRC
#define HPOWER_PORT PORTC
#define HPOWER		PORTC2

#define MPWM_DIR	DDRB
#define MPWM		DDRB1

#define SSPWM_DIR	DDRD
#define SSPWM		DDRD6

#define HALLA_DIR	DDRD
#define HALLA_PORT	PORTD
#define HALLA		PORTD3
#define HALLA_PIN	PIND3

#define HALLB_DIR	DDRE
#define HALLB_PORT	PORTE
#define HALLB		PORTE0
#define HALLB_PIN	PINE0

// List of error codes
#define ERROR_CODE_TLM 1
#define ERROR_CODE_TCM 2
#define ERROR_CODE_ENCODER 4
#define ERROR_CODE_UART 8
#define ERROR_CODE_I2C 16
#define ERROR_CODE_CAN 32
#define ERROR_CODE_CONFIG 64
#define ERROR_CODE_MOTOR 128

// List of EEPROM addresses
#define SERIAL_NUMBER_EEPROM_ADDRESS_1 0x80
#define SERIAL_NUMBER_EEPROM_ADDRESS_2 0x81
#define I2C_ADDRESS_EEPROM_ADDRESS 0x82
#define CAN_ADDRESS_EEPROM_ADDRESS 0x83
#define PWM_GAIN_ADDRESS_EEPROM_ADDRESS_1 0x84
#define PWM_GAIN_ADDRESS_EEPROM_ADDRESS_2 0x85
#define PWM_GAIN_MULT_ADDRESS_EEPROM_ADDRESS 0x86
#define MAIN_GAIN_I_ADDRESS_EEPROM_ADDRESS_1 0x87
#define MAIN_GAIN_I_ADDRESS_EEPROM_ADDRESS_2 0x88
#define MAIN_GAIN_I_MULT_ADDRESS_EEPROM_ADDRESS 0x89
#define MAIN_GAIN_D_ADDRESS_EEPROM_ADDRESS_1 0x8A
#define MAIN_GAIN_D_ADDRESS_EEPROM_ADDRESS_2 0x8B
#define MAIN_GAIN_D_MULT_ADDRESS_EEPROM_ADDRESS 0x8C
#define BACK_GAIN_I_ADDRESS_EEPROM_ADDRESS_1 0x8D
#define BACK_GAIN_I_ADDRESS_EEPROM_ADDRESS_2 0x8E
#define BACK_GAIN_I_MULT_ADDRESS_EEPROM_ADDRESS 0x8F
#define BACK_GAIN_D_ADDRESS_EEPROM_ADDRESS_1 0x90
#define BACK_GAIN_D_ADDRESS_EEPROM_ADDRESS_2 0x91
#define BACK_GAIN_D_MULT_ADDRESS_EEPROM_ADDRESS 0x92

extern uint16_t g_SerialNumber;
extern uint8_t g_I2CAddress;
extern uint16_t g_CANAddress;
extern bool g_WritevaluesFlag;

extern  uint16_t GainI;
extern  uint8_t MultI;
extern  uint16_t GainP;
extern  uint8_t MultP;
extern  uint16_t GainIB;
extern  uint8_t MultIB;
extern  uint16_t GainPB;
extern  uint8_t MultPB;
extern  int16_t GainPWM;
extern  uint8_t MultPWM;

// General device functions
void DEVICE_SetError(uint8_t error);
void DEVICE_ClearError(void);
void DEVICE_MeasureTemperature(void);
void DEVICE_ExtractConfiguration(void);
void EEPROM_readAllValues(void);
void DEVICE_WriteSerialEEPROM(uint16_t serialNumber);
void DEVICE_WriteI2CAddressEEPROM(uint8_t i2cAddress);
void DEVICE_WriteCANAddressEEPROM(uint8_t canAddress);
void DEVICE_WritePWMGainEEPROM(int16_t gain, uint8_t mult);
void DEVICE_WriteMainGainEEPROM(uint16_t gainI, uint8_t multI, uint16_t gainD, uint8_t multD);
void DEVICE_WriteBackupGainEEPROM(uint16_t gainI, uint8_t multI, uint16_t gainD, uint8_t multD);

void EEPROM_write(unsigned int uiAddress, unsigned char ucData);
unsigned char EEPROM_read(unsigned int uiAddress);

#endif /* INCFILE1_H_ */