/*
* comms.c
*
* Created: 2015/07/21 01:56:44 PM
* Author(s): wjordaan/ghjvv
*/

#include <avr/interrupt.h>
#include <util/twi.h>
#include <stdbool.h>

#include "comms.h"
#include "uart.h"
#include "i2c.h"
#include "can.h"
#include "tctlm.h"
#include "device.h"

// Static variables
COMMS_TCMD_TypeDef tcmdBuffer[COMMS_TCMD_BUFFLEN];

static uint8_t tcmdReadIndex;
static uint8_t tcmdWriteIndex;
static uint8_t tcmdBuffFull;

static uint16_t tcmdCount;
static uint16_t tlmCount;

static uint16_t commsErr;

typedef struct {
	uint8_t value[64];
	uint8_t head;
	uint8_t tail;
}COMMbuffer;


static uint8_t uartTxBuffer[64];
static COMMbuffer uartRxBuffer;

static bool UartEscape;
static bool UartProtocolErr;
static bool UartTcPending;
static bool UartTlmReqPending;
static uint8_t UartTcId;
static uint8_t UartTlmId;
static uint8_t UartTcParam[COMMS_TCMD_PARAMLEN];
static uint8_t UartTcParamLen;

static uint8_t formattedBuffer[COMMS_TCMD_PARAMLEN];
static uint8_t UartTxLen;

static uint8_t i2cRxIndex;
static uint8_t i2cTxIndex;
static uint8_t i2cTxBuffer[64];

static uint8_t CanRxBuffer[13];
static uint8_t CanTxBuffer[13];

// Function definitions
static uint8_t processTLM(uint8_t id, uint8_t* txBuffer);
static uint8_t processTC(uint8_t tcID, uint8_t paramLen, uint8_t* tcBuffer);
static uint8_t identifyTCMD (uint8_t id);

// Public functions
void COMMS_Init(void)
{
	uint8_t i;
	
	UART_Init();
	
	I2C_Init(g_I2CAddress);
	
	CAN_Init(g_CANAddress);
	
	for(i = 0; i < COMMS_TCMD_BUFFLEN; i++)
	{
		tcmdBuffer[i].error = 0;
		tcmdBuffer[i].processed = 1;
	}
	
	tcmdReadIndex  = 0;
	tcmdWriteIndex = 0;
	tcmdBuffFull   = 0;

	tcmdCount = 0;
	tlmCount  = 0;

	commsErr  = 0x00;
	
	uartState = uartIdle;
	UartEscape = false;
	UartProtocolErr = false;
	
	UartTcPending = false;
	UartTlmReqPending = false;
}

void COMMS_processTCMD(void)
{
	// loop through TCMD buffer until all TCMDs have been processed
	while( !( tcmdBuffer[(tcmdReadIndex + 1) % COMMS_TCMD_BUFFLEN].processed ) )
	{
		tcmdReadIndex = (tcmdReadIndex + 1) % COMMS_TCMD_BUFFLEN;
		
		// process errors
		if(tcmdBuffer[tcmdReadIndex].error)
		{
			// clear error
			tcmdBuffer[tcmdReadIndex].error = 0;
		}
		// process telecommands
		else
		{
			switch(tcmdBuffer[tcmdReadIndex].id)
			{
				case 1:
				// Reset Controller
				if (tcmdBuffer[tcmdReadIndex].len == 1)
					TCTLM_TCM_ResetMCU(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 2:
				if (tcmdBuffer[tcmdReadIndex].len == 2)
					TCTLM_TCM_WheelSpeed(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 3:
				if (tcmdBuffer[tcmdReadIndex].len == 2)
					TCTLM_TCM_WheelDutyCycle(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 7:
				if (tcmdBuffer[tcmdReadIndex].len == 1)
					TCTLM_TCM_MotorSwitch(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 8:
				// Encoder Power State
				if (tcmdBuffer[tcmdReadIndex].len == 1)
					TCTLM_TCM_EncoderSwitch(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 9:
				// Hall Power State
				if (tcmdBuffer[tcmdReadIndex].len == 1)
					TCTLM_TCM_HallSwitch(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 10:
				// Control Mode
				if (tcmdBuffer[tcmdReadIndex].len == 1)
					TCTLM_TCM_MotorController(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 12:
				// Backup Run Mode
				if (tcmdBuffer[tcmdReadIndex].len == 1)
					TCTLM_TCM_RunMode(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 20:
				if (tcmdBuffer[tcmdReadIndex].len == 1)
					TCTLM_TCM_ClearErrors(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 30:
				// Set serial address
				if (tcmdBuffer[tcmdReadIndex].len == 2)
					TCTLM_TCM_SetSerial(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 31:
				// Set I2C Address
				if (tcmdBuffer[tcmdReadIndex].len == 1)
					TCTLM_TCM_SetI2CAddress(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 32:
				// Set CAN Address
				if (tcmdBuffer[tcmdReadIndex].len == 1)
					TCTLM_TCM_SetCANAddress(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 33:
				// Set PWM gain
				if (tcmdBuffer[tcmdReadIndex].len == 3)
					TCTLM_TCM_SetPWMGain(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 34:
				// Set Main Speed Controller Gain
				if (tcmdBuffer[tcmdReadIndex].len == 6)
					TCTLM_TCM_SetMainGain(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 35:
				// Set Backup Speed Controller Gain
				if (tcmdBuffer[tcmdReadIndex].len == 6)
					TCTLM_TCM_SetBackupGain(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;
				
				case 36:
				// Set encoder weight
				if (tcmdBuffer[tcmdReadIndex].len == 1)
					TCTLM_TCM_SetEncoderWeight(tcmdBuffer[tcmdReadIndex].params);
				else
					DEVICE_SetError(ERROR_CODE_TCM);
				break;

				default:
				break;
			}
			tcmdCount++;
		}
		tcmdBuffer[tcmdReadIndex].processed = 1;
	}
}

// Static functions
static uint8_t processTLM(uint8_t id, uint8_t* txBuffer)
{
	uint8_t tlmLen;
	
	switch (id)
	{
		case 128:
		TCTLM_GetIdentification(txBuffer);
		tlmLen = 8;
		break;
		
		case 129:
		// Extended Identification
		TCTLM_TLM_GetExtendedIdentification(txBuffer);
		tlmLen = 4;
		break;
		
		case 130:
		TCTLM_GetWheelStatus(txBuffer);
		tlmLen = 8;
		break;
		
		case 133:
		TCTLM_GetWheelSpeed(txBuffer);
		tlmLen = 2;
		break;
		
		case 134:
		TCTLM_GetWheelReference(txBuffer);
		tlmLen = 2;
		break;
		
		case 135:
		TCTLM_GetWheelCurrent(txBuffer);
		tlmLen = 2;
		break;
		
		case 137:
		TCTLM_GetWheelData(txBuffer);
		tlmLen = 6;
		break;
		
		case 138:
		TCTLM_TLM_GetWheelDataAdditional(txBuffer);
		tlmLen = 4;
		break;
		
		case 139:
		// General proportional gain
		TCTLM_TLM_GetPWMGain(txBuffer);
		tlmLen = 3;
		break;
		
		case 140:
		// Main Gain Values
		TCTLM_TLM_GetMainGain(txBuffer);
		tlmLen = 6;
		break;
		
		case 141:
		// Backup Gain Values
		TCTLM_TLM_GetBackupGain(txBuffer);
		tlmLen = 6;
		break;
		
		case 142:
		// Encoder weight
		TCTLM_TLM_GetEncoderWeight(txBuffer);
		tlmLen = 1;
		break;
		
		case 145:
		// Error flags
		TCTLM_GetErrorFlags(txBuffer);
		tlmLen = 1;
		break;
		
		case 146:
		// Error flags
		TCTLM_GetLoopErrorFlags(txBuffer);
		tlmLen = 1;
		break;
		
		default:
		// error: unknown telemetry id
		DEVICE_SetError(ERROR_CODE_TLM);
		tlmLen = 0;
		break;
	}
	
	tlmCount++;
	return tlmLen;
}

static uint8_t identifyTCMD (uint8_t id)
{
	uint8_t tcmdLen;

	switch(id)
	{
		case 1:
		case 7:
		case 8:
		case 9:
		case 10:
		case 12:
		case 20:
		case 31:
		case 32:
		case 36:
		tcmdLen = 1;
		break;
		
		case 2:
		case 3:
		case 30:
		tcmdLen = 2;
		break;
		
		case 33:
		tcmdLen = 3;
		break;
		
		case 34:
		case 35:
		tcmdLen = 6;
		break;
		
		default:
		DEVICE_SetError(ERROR_CODE_TCM);
		tcmdLen = 0;
		break;
	}

	return tcmdLen;
}


void UART_IRQ(void)
{
	uartRxBuffer.value[uartRxBuffer.head++]=UDR0;
	if(uartRxBuffer.head>=63)
	uartRxBuffer.head=0;
	
	if(uartRxBuffer.head+1==uartRxBuffer.tail)//check for buffer full
	{
		DEVICE_SetError(ERROR_CODE_UART);
		UartProtocolErr = true;
	}
}



void UART_IRQHandler(void)
{
	// SM and EM CubeComputer UART RX interrupt
	uint8_t rxByte;
	uint8_t som, eom;
	
	    
	if(uartRxBuffer.tail!=uartRxBuffer.head)//if buffer is not empty
	rxByte=uartRxBuffer.value[uartRxBuffer.tail];
	else
	return;
	    

	som = 0;
	eom = 0;
	
	if(!UartEscape && (rxByte == UART_ESCAPECHAR))
	{
		UartEscape = true;
		uartRxBuffer.tail++;
		if(uartRxBuffer.tail>=63)
		uartRxBuffer.tail=0;

		return;
	}
	else if(UartEscape)
	{
		if (rxByte == UART_SOM)
		{
			som = 1;
		}
		else if (rxByte == UART_EOM)
		{
			eom = 1;
		}
		else if (rxByte != UART_ESCAPECHAR)
		{
			UartProtocolErr = true;
		}

		UartEscape = false;
	}
	// reset everything on start of message, but record errors in case an incomplete message was received
	if(som)
	{
		if (uartState != uartIdle)
		{
			// previous message was incomplete
			DEVICE_SetError(ERROR_CODE_UART);
		}

		UartProtocolErr = false;
		uartState = uartMsgType;
	}
	// if the protocol error flag is set, ignore everything (until a next SOM received)
	else if(UartProtocolErr)
	{
		uartState = uartIdle;
	}
	else if(eom)
	{
		if (uartState == uartTcData)
		{
			UartTcPending = true;
		}
		else if (uartState == uartTlmReq)
		{
			UartTlmReqPending = true;
		}
		else
		UartProtocolErr = true;

		uartState = uartIdle;
	}
	else if (uartState == uartMsgType)
	{
		if (rxByte >= 128)
		{
			UartTlmId = rxByte;
			uartState = uartTlmReq;
		}
		else
		{
			UartTcId = rxByte;
			uartState = uartTcData;
			UartTcParamLen = 0;
		}
	}
	else if (uartState == uartTcData)
	{
		if (UartTcParamLen < COMMS_TCMD_PARAMLEN)
		{
			UartTcParam[UartTcParamLen++] = rxByte;
		}
		else
		{
			// Set UART Error
			DEVICE_SetError(ERROR_CODE_UART);
			UartProtocolErr = true;
		}
	}
	else
	{
		UartProtocolErr = true;
	}
	uartRxBuffer.tail++;
	if(uartRxBuffer.tail>=63)
	uartRxBuffer.tail=0;
}

void CAN_IRQHandler(void)
{
	uint8_t i;
	
	// working with can controller - switch to mode 0 SPI
	SPCR0 = ((1<<SPE)|(1<<MSTR)|(0<<CPOL)|(0<<CPHA));  // SPI enable, Master
	
	CAN_readRxBuffer(bspCanRx0Id, CanRxBuffer, 0);
	
	// Check from message type whether message is telemetry or telecommand
	if(CAN_extractMessageType(CanRxBuffer[0]) == 0x04)
	{
		// message is telemetry request
		
		CanTxBuffer[4] = processTLM(CAN_extractChannel(CanRxBuffer[0],CanRxBuffer[1]),CanTxBuffer+5);
		
		CAN_createMessageID(CanTxBuffer, 5, CAN_extractChannel(CanRxBuffer[0],CanRxBuffer[1]), CAN_extractDestination(CanRxBuffer[3]), CAN_extractSource(CanRxBuffer[2]));
		
		CanTxBuffer[1] = CanTxBuffer[1] | 0x08; // sending extended ID
		
		CAN_writeTxBuffer(bspCanTx0Id,CanTxBuffer,1);
		CAN_requestToSend(bspCanRtsTx0);
	}
	else if(CAN_extractMessageType(CanRxBuffer[0]) == 0x01)
	{
		// message is telecommand
		
		// check for tcmd buffer overflow
		if(tcmdBuffer[(tcmdWriteIndex + 1) % COMMS_TCMD_BUFFLEN].processed == 0)
		{
			tcmdBuffFull = 1;
			commsErr = COMMS_ERROR_TCMDBUFOF;
		}
		else
		{
			tcmdWriteIndex = (tcmdWriteIndex + 1) % COMMS_TCMD_BUFFLEN;
			tcmdBuffFull = 0;

			tcmdBuffer[tcmdWriteIndex].id  = CAN_extractChannel(CanRxBuffer[0],CanRxBuffer[1]);
			tcmdBuffer[tcmdWriteIndex].len = identifyTCMD (CAN_extractChannel(CanRxBuffer[0],CanRxBuffer[1]));
			
			// check whether the correct size of message was received
			if(tcmdBuffer[tcmdWriteIndex].len == (CanRxBuffer[4] & 0x07))
			{
				CAN_readRxBuffer(bspCanRx0Data, CanRxBuffer+5, 0);
				
				// extract data from can
				for(i = 0; i < tcmdBuffer[tcmdWriteIndex].len; i++)
				{
					tcmdBuffer[tcmdWriteIndex].params[i] = CanRxBuffer[i+5];
				}
				
				tcmdBuffer[tcmdWriteIndex].processed = 0;
				
				// send CAN telecommand response
				CAN_createMessageID(CanTxBuffer, 2, CAN_extractChannel(CanRxBuffer[0],CanRxBuffer[1]), CAN_extractDestination(CanRxBuffer[3]), CAN_extractSource(CanRxBuffer[2]));
				
				CanTxBuffer[1] = CanTxBuffer[1] | 0x08; // sending extended ID
				
				CanTxBuffer[4] = 0;  // zero size - just responding that valid telecommand was acquired
				
				CAN_writeTxBuffer(bspCanTx0Id,CanTxBuffer,0);
				CAN_requestToSend(bspCanRtsTx0);
			}
			else
			{
				// message not correct send error message back and set can error flag
				// send CAN telecommand not acknowledge response
				CAN_createMessageID(CanTxBuffer, 3, CAN_extractChannel(CanRxBuffer[0],CanRxBuffer[1]), CAN_extractDestination(CanRxBuffer[3]), CAN_extractSource(CanRxBuffer[2]));
				
				CanTxBuffer[1] = CanTxBuffer[1] | 0x08; // sending extended ID
				
				CanTxBuffer[4] = 0;  // zero size - just responding that valid telecommand was acquired
				
				CAN_writeTxBuffer(bspCanTx0Id,CanTxBuffer,0);
				CAN_requestToSend(bspCanRtsTx0);
			}
		}
	}
}

void I2C_IRQHandler(void)
{
	uint8_t tempId, tempData;
	
	switch (i2cState)
	{
		case waitForId:
		if((TWSR0 & TW_STATUS_MASK) == TW_SR_SLA_ACK)	// Write Address Received
		{
			// Clear interrupt and ACK
			TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
		}
		else if ((TWSR0 & TW_STATUS_MASK) == TW_ST_SLA_ACK)	// Read Address Received
		{
			// should not receive read request while waiting for ID
			commsErr = COMMS_ERROR_I2CTLM;

			// send previous TLM already in buffer
			i2cState = waitForData;
			i2cTxIndex = 0;
			TWDR0 = i2cTxBuffer[i2cTxIndex++];
			
			// Clear interrupt and ACK
			TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
		}
		else if ((TWSR0 & TW_STATUS_MASK) == TW_SR_DATA_ACK)	// Write Data Received
		{
			// receive data byte
			tempId = TWDR0;
			
			// Telemetry ID
			if( (tempId & COMMS_ID_TYPE) == COMMS_ID_TLM)
			{
				processTLM (tempId, i2cTxBuffer);

				i2cTxIndex = 0;
				i2cState = waitForData;
				
				// Clear interrupt and ACK
				TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
			}
			// Telecommand ID
			else
			{
				// check for tcmd buffer overflow
				if(tcmdBuffer[(tcmdWriteIndex + 1) % COMMS_TCMD_BUFFLEN].processed == 0)
				{
					tcmdBuffFull = 1;
					commsErr = COMMS_ERROR_TCMDBUFOF;

					i2cState = waitForData;
					
					// Do not ACK
					TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEN);
				}
				else
				{
					tcmdWriteIndex = (tcmdWriteIndex + 1) % COMMS_TCMD_BUFFLEN;
					tcmdBuffFull = 0;

					tcmdBuffer[tcmdWriteIndex].id  = tempId;
					tcmdBuffer[tcmdWriteIndex].len = identifyTCMD (tempId);

					i2cRxIndex = 0;
					i2cState = waitForData;
					
					// Clear interrupt and ACK
					TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
				}
			}
		}
		else if ((TWSR0 & TW_STATUS_MASK) ==  TW_BUS_ERROR)
		{
			DEVICE_SetError(ERROR_CODE_I2C);
			commsErr = COMMS_ERROR_I2CTLM;
			TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN) | (1<<TWSTO);
		}
		else
		{
			DEVICE_SetError(ERROR_CODE_I2C);
			commsErr = COMMS_ERROR_I2CTLM;
			TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN) | (1<<TWSTO);
		}
		break;
		
		case waitForData:
		if((TWSR0 & TW_STATUS_MASK) == TW_SR_DATA_ACK)	// Write Data Received
		{
			// Valid telecommand data received
			if (tcmdBuffFull)
			{
				// read data to clear interrupt
				tempData = TWDR0;
			}
			else if (i2cRxIndex < COMMS_TCMD_PARAMLEN)
			{
				// save data in TCMD buffer
				tcmdBuffer[tcmdWriteIndex].params[i2cRxIndex++] = TWDR0;
			}
			else
			{
				// read data to clear interrupt
				tempData = TWDR0;

				// increment index counter for error checking when stop received
				i2cRxIndex++;
			}
			
			// Clear interrupt and ACK
			TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
		}
		else if ((TWSR0 & TW_STATUS_MASK) == TW_ST_SLA_ACK)	// Read Address Received
		{
			// Send first byte of buffer
			TWDR0 = i2cTxBuffer[i2cTxIndex++];
			
			// Clear interrupt and ACK
			TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
		}
		else if((TWSR0 & TW_STATUS_MASK) == TW_ST_DATA_ACK)	// ACK received, another TLM byte requested
		{
			// Send byte to buffer
			TWDR0 = i2cTxBuffer[i2cTxIndex++];
			
			// Clear interrupt and ACK
			TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
		}
		else if ((TWSR0 & TW_STATUS_MASK) == TW_ST_DATA_NACK)	// NACK received, Tlm transmission ends
		{
			i2cState = waitForId;
			
			// Clear interrupt and ACK
			TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
		}
		else if ((TWSR0 & TW_STATUS_MASK) == TW_SR_STOP)	// Stop or repeated start condition
		{
			// Transmission Stop condition received
			if(i2cRxIndex == tcmdBuffer[tcmdWriteIndex].len)
			{
				// flag message to be processed
				tcmdBuffer[tcmdWriteIndex].processed = 0;
			}
			else if (i2cRxIndex < tcmdBuffer[tcmdWriteIndex].len)
			{
				tcmdBuffer[tcmdWriteIndex].error = COMMS_TCMDERR_PARAMUF;
				tcmdBuffer[tcmdWriteIndex].processed = 0;
			}
			else
			{
				tcmdBuffer[tcmdWriteIndex].error = COMMS_TCMDERR_PARAMOF;
				tcmdBuffer[tcmdWriteIndex].processed = 0;
			}

			// Telecommand data reception complete
			i2cState = waitForId;
			
			// Clear interrupt and ACK
			TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
		}
		else if ((TWSR0 & TW_STATUS_MASK) ==  TW_BUS_ERROR)
		{
			DEVICE_SetError(ERROR_CODE_I2C);
			commsErr = COMMS_ERROR_I2CTLM;
			TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN) | (1<<TWSTO);
		}
		else
		{
			DEVICE_SetError(ERROR_CODE_I2C);
			commsErr = COMMS_ERROR_I2CTLM;
			TWCR0 |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN) | (1<<TWSTO);
		}
		break;
	}
}

void UART_FormatAndSend(uint8_t* buffer, int len)
{
	int i;
	bool newMsg = true;

	for (i = 0; i < len; i++)
	{
		if (newMsg)
		{
			formattedBuffer[0] = 0x1f; //start of message
			formattedBuffer[1] = 0x7f;

			UartTxLen = 2;

			newMsg = false;
		}

		formattedBuffer[UartTxLen++] = buffer[i];
		if (buffer[i] == 0x1f)
		{
			formattedBuffer[UartTxLen++] = 0x1f;
		}
	}
	if (!newMsg)
	{
		formattedBuffer[UartTxLen++] = 0x1f;
		formattedBuffer[UartTxLen++] = 0xff; //end of message

		UART_TxBuffer((uint8_t*)formattedBuffer,UartTxLen);
	}
}

void ProcessUart(void)
{
	uint8_t tcResult;
	uint32_t len;

	if (UartTcPending)
	{
		tcResult = processTC(UartTcId, UartTcParamLen, UartTcParam);
		UART_FormatAndSend(&tcResult, 1); // send ACK
		UartTcPending = false;
	}
	if (UartTlmReqPending)
	{
		len = processTLM(UartTlmId, uartTxBuffer);
		UART_FormatAndSend(uartTxBuffer, len);
		UartTlmReqPending = false;
	}
}

static uint8_t processTC(uint8_t tcID, uint8_t paramLen, uint8_t* tcBuffer)
{
	switch(tcID)
	{
		case 1:
		// Reset Controller
			if (paramLen == 1)
				TCTLM_TCM_ResetMCU(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 2:
			if (paramLen == 2)
				TCTLM_TCM_WheelSpeed(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 3:
			if (paramLen == 2)
				TCTLM_TCM_WheelDutyCycle(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);		
		return 0;
		break;
		
		case 7:
			if (paramLen == 1)
				TCTLM_TCM_MotorSwitch(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 8:
		// Encoder Power State
			if (paramLen == 1)
				TCTLM_TCM_EncoderSwitch(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 9:
		// Hall Power State
			if (paramLen == 1)
				TCTLM_TCM_HallSwitch(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 10:
		// Control Mode
			if (paramLen == 1)
				TCTLM_TCM_MotorController(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 12:
		// Backup Run Mode
			if (paramLen == 1)
				TCTLM_TCM_RunMode(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 20:
			if (paramLen == 1)
				TCTLM_TCM_ClearErrors(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 30:
		// Set serial address
			if (paramLen == 2)
				TCTLM_TCM_SetSerial(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 31:
		// Set I2C Address
			if (paramLen == 1)
				TCTLM_TCM_SetI2CAddress(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 32:
		// Set CAN Address
			if (paramLen == 1)
				TCTLM_TCM_SetCANAddress(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 33:
		// Set PWM gain
			if (paramLen == 3)
				TCTLM_TCM_SetPWMGain(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 34:
		// Set Main Speed Controller Gain
			if (paramLen == 6)
				TCTLM_TCM_SetMainGain(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 35:
		// Set Backup Speed Controller Gain
			if (paramLen == 6)
				TCTLM_TCM_SetBackupGain(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		case 36:
		// Set encoder weight
			if (paramLen == 1)
				TCTLM_TCM_SetEncoderWeight(tcBuffer);
			else
				DEVICE_SetError(ERROR_CODE_TCM);
		return 0;
		break;
		
		default:
		return 1;
	}
}