/*
 * i2c.c
 *
 * Created: 2015/07/21 01:54:43 PM
 * Author(s): wjordaan/ghjvv
 */ 

#include <avr/io.h>
#include "device.h"
#include "i2c.h"

void I2C_Init(uint8_t i2cAddress)
{
	// Set I2C slave address
	TWAR0 = i2cAddress;
	
	// Setup of I2C to be slave on bus (??Must TWINT be set also??)
	TWCR0 = (1<<TWIE)|(1<<TWEN)|(1<<TWEA)|(0<<TWSTA)|(0<<TWSTO);
}