/*
 * timer.c
 *
 * Created: 2015/07/23 08:18:28 AM
 * Author(s): wjordaan/ghjvv
 */ 

#include <avr/io.h>
#include "timer.h"
#include "device.h"
#include "tctlm.h"

static uint16_t RunTime_Seconds = 0;
static uint16_t RunTime_MilliSeconds = 0;

void TIMER_Init(void)
{
	// Set timer counter value
	OCR2A = TIMER_COUNTER;
	
	// Standard timer with CTC mode
	TCCR2A = (1<<WGM21)|(0<<COM2A1)|(0<<COM2A0);
	
	// Setup timer with prescaler of 256
	TCCR2B = (1<<CS22)|(1<<CS21)|(0<<CS20);
	
	// Clear timer register
	TCNT2 = 0;
	
	// Set interrupt
	TIMSK2 = (1<<OCIE2A);
	
	// Clear timer variables
	RunTime_Seconds = 0;
	RunTime_MilliSeconds = 0;
}

void TIMER_PulseCounter_Init(void)
{
	TCCR3B |= (1 << CS32) | (1 << CS31) | (1 << CS30);	// using external clock source (Hall sensor XOR, rising edge)
	TIMSK3 = (1<<TOIE3); // enabled overflow interrupt
	TCNT3 = 0;
}


void TIMER_Reset(void)
{
	TCNT2 = 0;
}

void TIMER_PulseCounter_Reset(void)
{
	TCNT3 = 0;
}

void TIMER_Increment20Milli(void)
{
	RunTime_MilliSeconds = RunTime_MilliSeconds + 20;
	if(RunTime_MilliSeconds>=1000)
	{
		RunTime_Seconds++;
		RunTime_MilliSeconds = 0;
	}
	
	// update telemetry variables
	g_RunTimeSecond = RunTime_Seconds;
	g_RunTimeMilliSecond = RunTime_MilliSeconds;
}

void TIMER_Increment100Milli(void)
{
	RunTime_MilliSeconds = RunTime_MilliSeconds + 100;
	if(RunTime_MilliSeconds>=1000)
	{
		RunTime_Seconds++;
		RunTime_MilliSeconds = 0;
	}
	
	// update telemetry variables
	g_RunTimeSecond = RunTime_Seconds;
	g_RunTimeMilliSecond = RunTime_MilliSeconds;
}

void TIMER_Increment200Milli(void)
{
	RunTime_MilliSeconds = RunTime_MilliSeconds + 200;
	if(RunTime_MilliSeconds>=1000)
	{
		RunTime_Seconds++;
		RunTime_MilliSeconds = 0;
	}
	
	// update telemetry variables
	g_RunTimeSecond = RunTime_Seconds;
	g_RunTimeMilliSecond = RunTime_MilliSeconds;
}