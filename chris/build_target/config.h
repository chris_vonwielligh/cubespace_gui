/*
    CubeWheel Configuration Parameters
*/
#ifndef CONFIG_H_
#define CONFIG_H_

// Identification
#define SERIAL_NUMBER    UNDEFINED
#define FIRMWARE_MAJOR_VERSION   UNDEFINED
#define FIRMWARE_MINOR_VERSION   UNDEFINED
#define INTERFACE_VERSION        UNDEFINED
#define I2C_ADRESS     			UNDEFINED
#define CAN_ADDRESS      		SIZE_UNDEFINED

// Wheel Size
#define UNDEFINED

// File generated on 29/11/2019 16:54:57 
#endif /* CONFIG_H_ */