/*
 * uart.c
 *
 * Created: 2015/07/21 01:55:16 PM
 * Author(s): wjordaan/ghjvv
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "uart.h"
#include <stdbool.h>


void UART_Init(void)
{
	// Set Baud Rate
	UBRR0H = (uint8_t)(UBBR_VALUE>>8);
	UBRR0L = (uint8_t)UBBR_VALUE;
	
	// Set frame format to 8 data bits, no parity, 1 stop bit
	UCSR0C |= (1<<UCSZ01)|(1<<UCSZ00);
	
	// Doubled baud rate by reducing prescaler
	UCSR0A |= (1<<U2X0);
	
	// Enable transmission and reception
	UCSR0B |= (1<<RXEN0)|(1<<TXEN0);
	
	// Enable the USART Receive Complete interrupt (USART_RXC)
	UCSR0B |= (1 << RXCIE0);
}

void UART_TxBuffer(uint8_t* buff, uint16_t len)
{
	uint8_t i = 0;
	while (i<len)
	{
		sendByte(buff[i]);
		i++;
	}
}

void sendByte(uint8_t data)
{
	uint16_t timeout = 0;
	//wait while previous byte is completed
	while(!(UCSR0A&(1<<UDRE0)))
	{
		if(timeout>1000)
		{
			g_LoopErrorFlags=g_LoopErrorFlags|0b01000000;
			break;
		}
		timeout++;
	}
	// Transmit data
	UDR0 = data;
}