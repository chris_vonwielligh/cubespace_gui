/*
 * spi.c
 *
 * Created: 2015/07/29 09:37:35 AM
 * Author(s): wjordaan/ghjvv
 */ 
#include "spi.h"
#include "device.h"

#include "bldc.h"

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

uint8_t M_Error = 0;
uint8_t data = 0;

void SPI_Encoder_Init(void)
{
	// Setup SPI pins on port B: MOSI, SCK, SS are outputs
	DDRB |= (1<<5)|(1<<3)|(1<<2); 
	
	// Encoder - CAN Mode
	SPCR0 = ((1<<SPE)|(1<<MSTR)|(0<<CPOL)|(0<<CPHA));  // SPI enable, Master
}

void SPI_Encoder_Reset(void)
{
	#ifdef AS5048A
	BLDC_SetEncoderSwitch(false);
	_delay_us(20);
	BLDC_SetEncoderSwitch(true);
	SS_ENCODER_LOW;
	_delay_us(2);
	data = SPI_Transmit(0xFF); // Request angular rate
	data = SPI_Transmit(0xFF); // Request angular rate
	_delay_us(2);
	SS_ENCODER_HIGH;
	_delay_us(2);
	#endif
	
	#ifdef AS5055A
	_delay_us(2);
	SS_ENCODER_HIGH;
	_delay_us(2);
	SS_ENCODER_LOW;
	_delay_us(2);
	data = SPI_Transmit(0x78); // Reset command
	data = SPI_Transmit(0x00); // Reset command
	SS_ENCODER_HIGH;
	_delay_us(2);
	SS_ENCODER_LOW;
	_delay_us(2);
	data = SPI_Transmit(0x00); // Reset SPI registers as well
	data = SPI_Transmit(0x05); // Reset SPI registers as well
	_delay_us(2);
	SS_ENCODER_HIGH;
	_delay_us(2);
	SS_ENCODER_LOW;
	_delay_us(2);
	data = SPI_Transmit(0xFF); // Request angular rate
	data = SPI_Transmit(0xFF); // Request angular rate
	_delay_us(2);
	SS_ENCODER_HIGH;
	_delay_us(2);
	#endif	
	
	#ifdef MA702
	BLDC_SetEncoderSwitch(false);
	_delay_us(20);
	BLDC_SetEncoderSwitch(true);
	_delay_us(20);
	#endif	

}

void SPI_Encoder_ClearError (void)
{
	_delay_us(2);
	SS_ENCODER_LOW;
	_delay_us(2);
	#ifdef AS5048A
	data = SPI_Transmit(0x40); // Clear error
	data = SPI_Transmit(0x01); // Clear error
	#endif
	#ifdef AS5055A
	data = SPI_Transmit(0xE7); // Clear error
	data = SPI_Transmit(0x00); // Clear error
	#endif
	_delay_us(2);
	SS_ENCODER_HIGH;
	_delay_us(2);
}

uint16_t SPI_Encoder_Read (void)
{
	uint8_t RMS_MSB = 0;
	uint8_t RMS_LSB = 0;
	uint16_t MSB = 0;
	uint16_t LSB = 0;
	bool encoderErrorFlag = false;

	_delay_us(2);
	
	// Encoder read angle
	SS_ENCODER_LOW;
	_delay_us(2);
	#ifdef MA702
	RMS_MSB = SPI_Transmit(0x00);
	RMS_LSB = SPI_Transmit(0x00);
	#else
	RMS_MSB = SPI_Transmit(0xFF);
	RMS_LSB = SPI_Transmit(0xFF);
	#endif
	
	_delay_us(2);
	SS_ENCODER_HIGH;
	
	// Check errors and retry once
	#ifdef AS5048A
	if ((RMS_MSB & 0b01000000) != 0b00000000)
	#endif
	#ifdef AS5055A
	if ((RMS_LSB & 0b00000010) != 0b00000000)
	#endif
	#ifdef MA702
	if ((RMS_LSB & 0b00000011) != 0b00000000)
	#endif
	{
		
		DEVICE_SetError(ERROR_CODE_ENCODER);
		SPI_Encoder_Reset();
		#ifndef MA702
		SPI_Encoder_ClearError();
		#endif
		_delay_us(2);
		
		// Encoder read angle
		SS_ENCODER_LOW;
		_delay_us(2);
		#ifdef MA702
		RMS_MSB = SPI_Transmit(0x00);
		RMS_LSB = SPI_Transmit(0x00);
		#else
		RMS_MSB = SPI_Transmit(0xFF);
		RMS_LSB = SPI_Transmit(0xFF);
		#endif
		_delay_us(2);
		SS_ENCODER_HIGH;
		
		//If the encoder still does not respond, set the mode to no control
		#ifdef AS5048A
		if ((RMS_MSB & 0b01000000) != 0b00000000)
		#endif
		#ifdef AS5055A
		if ((RMS_LSB & 0b00000010) != 0b00000000)
		#endif
		#ifdef MA702
		if ((RMS_LSB & 0b00000011) != 0b00000000)
		#endif
		{
			BLDC_SetControlMode(0);
		}
		
		
		// Set the error flag to indicate an error
		encoderErrorFlag = true;
		
	}
	
	if (encoderErrorFlag)
	{
		MSB = 5000;
	}
	else
	{
		#ifdef AS5048A
		MSB = (RMS_MSB & 0b00111111);
		LSB = RMS_LSB;
		MSB = MSB*256 + LSB;
		#endif
		#ifdef AS5055A
		MSB = (RMS_MSB & 0b00111111);
		LSB = RMS_LSB;
		LSB = LSB>>2;
		MSB = MSB*64 + LSB;
		#endif
		#ifdef MA702
		MSB = RMS_MSB;
		LSB = RMS_LSB;
        MSB = (MSB*256 + LSB)>>4;
		#endif
	}
	
	return MSB;
}

void SPI_Encoder_Check (void)
{
	uint8_t dummydata=0;
	
	#ifdef AS5048A
	uint8_t errorcheck=0xFF;
	static uint8_t count_errors=0;
	#endif
	
	#ifdef MA702
	static uint8_t count_errors=0;
	#else
	// working with encoder - switch to mode 1 SPI
	//cli();
	SPCR0 = ((1<<SPE)|(1<<MSTR)|(0<<CPOL)|(1<<CPHA));  // SPI enable, Master
	#endif
	
	
	
	
	_delay_us(2);
	SS_ENCODER_LOW;
	
	_delay_us(2);
	#ifdef AS5048A
	dummydata = SPI_Transmit(0x7F); // System Config address command
	dummydata = SPI_Transmit(0xFD); // System Config command
	#endif
	#ifdef AS5055A
	dummydata = SPI_Transmit(0xFE); // System Config address command
	dummydata = SPI_Transmit(0x40); // System Config command
	#endif
	#ifdef MA702
	dummydata = SPI_Transmit(0b01000110); //Request MAG high low thresholds
	dummydata = SPI_Transmit(0x00); //Request MAG high low thresholds
	#endif

	_delay_us(2);
	
	SS_ENCODER_HIGH;
	_delay_us(2);
	SS_ENCODER_LOW;
	_delay_us(2);
	
	#ifdef AS5048A
	errorcheck = (SPI_Transmit(0xFF)&0b00001111); // Request angular rate and read back System Config
	dummydata = SPI_Transmit(0xFF); // Request angular rate
	#endif
	#ifdef AS5055A
	dummydata = SPI_Transmit(0xFF); // Request angular rate and read back System Config
	dummydata += SPI_Transmit(0xFF); // Request angular rate
	#endif
	#ifdef MA702	
	dummydata = SPI_Transmit(0x00); //Read MAG high low thresholds
	dummydata += SPI_Transmit(0x00); //Read MAG high low thresholds
	#endif
		
		
	_delay_us(2);
	SS_ENCODER_HIGH;

	_delay_us(2);
	//If encoder does not respond, set to none mode
	#ifdef AS5048A
	if(errorcheck!=0b00000001&&((count_errors++)>1))
	#endif
	#ifdef AS5055A
	if(dummydata!=17)
	#endif
	#ifdef MA702
	if(dummydata!=0b00011100&&((count_errors++)>1))
	#endif
	{
		SPI_Encoder_Reset();
		SPI_Encoder_ClearError();
		BLDC_SetControlMode(0);
		DEVICE_SetError(ERROR_CODE_ENCODER);
		#ifndef AS5055A
		count_errors=0;
		#endif
	}
	//sei();
}

uint8_t SPI_Transmit(unsigned char output)//This should detect when no data is received
{
	SPDR0 = output;
	uint8_t timeout=0;
	/* Wait for completion of previous write */
	while(!(SPSR0 & (1<<SPIF)))
	{
		if(timeout>250)
		{
			g_LoopErrorFlags=g_LoopErrorFlags|0b00100000;
			break;
		}
		timeout++;
	}
	
	return SPDR0;
}