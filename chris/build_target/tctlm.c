/*
 * tctlm.c
 *
 * Created: 2015/07/28 09:55:57 AM
 * Author(s): wjordaan/ghjvv
 */

#include <avr/wdt.h>
#include "tctlm.h"
#include "device.h"
#include "bldc.h"
#include "i2c.h"
#include "can.h"

uint16_t g_RunTimeSecond;
uint16_t g_RunTimeMilliSecond;
int16_t g_WheelSpeed;
//int16_t g_WheelSpeedRaw;
int16_t g_WheelReference;
int16_t g_WheelDuty;
uint16_t g_WheelCurrent;
uint16_t g_DigitalCurrent;
uint16_t g_MCUTemperature;
uint8_t g_ControlMode;
uint8_t g_BackupMode;
uint8_t g_MotorSwitch;
uint8_t g_EncoderSwitch;
uint8_t g_HallSwitch;
uint8_t g_ErrorFlags;
uint8_t g_LoopErrorFlags;
uint8_t g_EncoderWeight;

void TCTLM_Init(void)
{
	// Clear all telemetry information
	g_RunTimeSecond = 0;
	g_RunTimeMilliSecond = 0;
	g_WheelSpeed = 0;
	//g_WheelSpeedRaw = 0;
	g_WheelReference = 0;
	g_WheelDuty = 0;
	g_WheelCurrent = 0;
	g_DigitalCurrent = 0;
	g_MCUTemperature = 0;
	g_ErrorFlags = 0;
	g_LoopErrorFlags=0;
	g_ControlMode = 0;
	g_BackupMode = 0;
	g_MotorSwitch = 0;
	g_EncoderSwitch = 0;
	g_HallSwitch = 0;
}

// Telecommands
void TCTLM_TCM_ResetMCU(uint8_t*buffer)
{
	if(buffer[0]==85)
	{
		//sendByte(0x00);
		wdt_disable();
		wdt_enable(WDTO_15MS);
		while (1) {};
	}
}

void TCTLM_TCM_WheelSpeed(uint8_t*buffer)
{
	// extract data
	int16_t data = ((int16_t)(*(uint16_t*)(buffer+0)));
	
	BLDC_SetWheelSpeed(data);
}

void TCTLM_TCM_WheelDutyCycle(uint8_t*buffer)
{
	// extract data
	int16_t data = ((int16_t)(*(uint16_t*)(buffer+0)));
	
	BLDC_SetDutyCycle(data);
}

void TCTLM_TCM_MotorSwitch(uint8_t*buffer)
{
	// extract data
	bool state = ((bool)(*(uint8_t*)(buffer+0)));
	
	BLDC_SetMotorSwitch(state);
}

void TCTLM_TCM_EncoderSwitch(uint8_t*buffer)
{
	// extract data
	bool state = ((bool)(*(uint8_t*)(buffer+0)));
	
	BLDC_SetEncoderSwitch(state);
}

void TCTLM_TCM_HallSwitch(uint8_t*buffer)
{
	// extract data
	bool state = ((bool)(*(uint8_t*)(buffer+0)));
	
	BLDC_SetHallSwitch(state);
}

void TCTLM_TCM_MotorController(uint8_t*buffer)
{
	// extract data
	uint8_t select = buffer[0];
	
	BLDC_SetControlMode(select);
}

void TCTLM_TCM_RunMode(uint8_t*buffer)
{
	// extract data
	bool state = ((bool)(*(uint8_t*)(buffer+0)));
	
	BLDC_SetRunMode(state);
}

void TCTLM_TCM_ClearErrors(uint8_t* buffer)
{
	// extract data
	uint8_t data = ((uint8_t)(*(uint8_t*)(buffer+0)));
	
	if(data == 85)
	{
		DEVICE_ClearError();
	}
}

void TCTLM_TCM_SetSerial(uint8_t* buffer)
{
	// extract data
	uint16_t data = (*(uint16_t*)(buffer+0));
	
	g_SerialNumber = data;
	
	//DEVICE_WriteSerialEEPROM(g_SerialNumber);
	//EEPROM_writeAllValues();
	g_WritevaluesFlag=true;
}

void TCTLM_TCM_SetI2CAddress(uint8_t* buffer)
{
	g_I2CAddress = buffer[0];
	
	//DEVICE_WriteI2CAddressEEPROM(g_I2CAddress);
	
	I2C_Init(g_I2CAddress);
	//EEPROM_writeAllValues();
	g_WritevaluesFlag=true;
}

void TCTLM_TCM_SetCANAddress(uint8_t* buffer)
{
	g_CANAddress = buffer[0];
	
	//DEVICE_WriteCANAddressEEPROM(g_CANAddress);
	
	CAN_Init(g_CANAddress);
	//EEPROM_writeAllValues();
	g_WritevaluesFlag=true;
}

void TCTLM_TCM_SetPWMGain(uint8_t* buffer)
{
	// extract data
	int16_t gain = (*(int16_t*)(buffer+0));
	uint8_t mult = (*(uint8_t*)(buffer+2));
	
	// set internal variables
	BLDC_SetPWMGain(gain,mult);
	
	// save values to EEPROM
	//DEVICE_WritePWMGainEEPROM(gain,mult);
	//EEPROM_writeAllValues();
	g_WritevaluesFlag=true;
}

void TCTLM_TCM_SetMainGain(uint8_t* buffer)
{
	// extract data
	uint16_t gainI = (*(uint16_t*)(buffer+0));
	uint8_t multI = (*(uint8_t*)(buffer+2));
	uint16_t gainD = (*(uint16_t*)(buffer+3));
	uint8_t multD = (*(uint8_t*)(buffer+5));
	
	// set internal variables
	BLDC_SetMainControllerGain(gainI,multI,gainD,multD);
	
	// save values to EEPROM
	//DEVICE_WriteMainGainEEPROM(gainI,multI,gainD,multD);
	//EEPROM_writeAllValues();
	g_WritevaluesFlag=true;
}

void TCTLM_TCM_SetBackupGain(uint8_t* buffer)
{
	// extract data
	uint16_t gainI = (*(uint16_t*)(buffer+0));
	uint8_t multI = (*(uint8_t*)(buffer+2));
	uint16_t gainD = (*(uint16_t*)(buffer+3));
	uint8_t multD = (*(uint8_t*)(buffer+5));
	
	// set internal variables
	BLDC_SetBackupControllerGain(gainI,multI,gainD,multD);
	
	// save values to EEPROM
	//DEVICE_WriteBackupGainEEPROM(gainI,multI,gainD,multD);
	//EEPROM_writeAllValues();
	g_WritevaluesFlag=true;
}

void TCTLM_TCM_SetEncoderWeight(uint8_t* buffer)
{
	// Extract data from buffer
	uint8_t weight_x2 = (*(uint8_t*)(buffer+0));
	
	// Set encoder weight
	BLDC_SetEncoderWeight(weight_x2);
}

// Telemetry requests
void TCTLM_GetIdentification(uint8_t* buffer)
{
	*( (uint8_t*)(buffer+0) ) = NODE_ID;
	*( (uint8_t*)(buffer+1) ) = INTERFACE_VERSION;
	*( (uint8_t*)(buffer+2) ) = FIRMWARE_MAJOR_VERSION;
	*( (uint8_t*)(buffer+3) ) = FIRMWARE_MINOR_VERSION;
	*( (uint16_t*)(buffer+4) ) = g_RunTimeSecond;
	*( (uint16_t*)(buffer+6) ) = g_RunTimeMilliSecond;
}

void TCTLM_TLM_GetExtendedIdentification(uint8_t* buffer)
{
	*( (uint16_t*)(buffer+0) ) = g_SerialNumber;
	*( (uint8_t*)(buffer+2) ) = g_I2CAddress;
	*( (uint8_t*)(buffer+3) ) = g_CANAddress;
}

void TCTLM_GetWheelData(uint8_t* buffer)
{
	*( (uint16_t*)(buffer+0) ) = g_WheelSpeed;
	*( (uint16_t*)(buffer+2) ) = g_WheelReference;
	*( (uint16_t*)(buffer+4) ) = g_WheelCurrent;
}

void TCTLM_TLM_GetWheelDataAdditional(uint8_t* buffer)
{
	*( (uint16_t*)(buffer+0) ) = g_WheelDuty;
	*( (uint16_t*)(buffer+2) ) = g_HallCount*2;
}

void TCTLM_GetWheelStatus(uint8_t* buffer)
{
	char bits = 0;
	*( (uint16_t*)(buffer+0) ) = g_RunTimeSecond;
	*( (uint16_t*)(buffer+2) ) = g_RunTimeMilliSecond;
	*( (uint16_t*)(buffer+4) ) = g_MCUTemperature;
	*( (uint8_t*)(buffer+6) ) = g_ControlMode;
	
	if(g_BackupMode)
	{
		bits = bits | 0x01;
	}
	
	if(g_MotorSwitch)
	{
		bits = bits | 0x02;
	}
	
	if(g_HallSwitch)
	{
		bits = bits | 0x04;
	}
	
	if(g_EncoderSwitch)
	{
		bits = bits | 0x08;
	}
	
	if(g_ErrorFlags>0)
	{
		bits = bits | 0x10;
	}
	
	
	*( (uint8_t*)(buffer+7) ) = bits;
}

void TCTLM_GetWheelSpeed(uint8_t* buffer)
{
	*( (uint16_t*)(buffer+0) ) = g_WheelSpeed;
}

void TCTLM_GetWheelReference(uint8_t* buffer)
{
	*( (uint16_t*)(buffer+0) ) = g_WheelReference;
}

void TCTLM_GetWheelCurrent(uint8_t* buffer)
{
	*( (uint16_t*)(buffer+0) ) = g_WheelCurrent;
}

void TCTLM_TLM_GetPWMGain(uint8_t* buffer)
{
	BLDC_GetPWMGain(buffer);
}

void TCTLM_TLM_GetMainGain(uint8_t* buffer)
{
	BLDC_GetMainGain(buffer);
}

void TCTLM_TLM_GetBackupGain(uint8_t* buffer)
{
	BLDC_GetBackupGain(buffer);
}

void TCTLM_GetErrorFlags(uint8_t* buffer)
{
	*( (uint8_t*)(buffer+0) ) = g_ErrorFlags;
}

void TCTLM_TLM_GetEncoderWeight(uint8_t* buffer)
{
	*( (uint8_t*)(buffer+0) ) = g_EncoderWeight;
}

void TCTLM_GetLoopErrorFlags(uint8_t* buffer)
{
	*( (uint8_t*)(buffer+0) ) = g_LoopErrorFlags;
}