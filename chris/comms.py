#!/usr/bin/python3

from enum import Enum
from uart import UART
from i2c import I2C
import numpy as np
from PCANBasic import *
from time import sleep
import ctypes


class Comms:
    '''Handles the communication protocol'''

    SOM = 0x7F
    EOM = 0xFF
    ESC_CHAR = 0x1F
    ACK = 0
    CANMASK = '01'
    DELAY = 0.05  # How long will I wait for response from device?

    loggers = None
    ERROR_RAISE = True

    class ProtocolError(Exception):
        pass

    class TimeOutError(Exception):
        pass

    class NoResponseError(Exception):
        pass

    def __init__(self, auto_connect=True, loggers=None):
        if loggers: self.loggers = loggers
        self.uart = UART(loggers=self.loggers)
        self.can = PCANBasic()
        self.i2c = I2C(loggers=self.loggers)
        result = self.can.Initialize(PCAN_USBBUS1, PCAN_BAUD_1M)
        if result != PCAN_ERROR_OK:
            self.can.Uninitialize(PCAN_USBBUS1)
            print('Cannot connect to PCAN device.')
            # if self.ERROR_RAISE: raise self.ProtocolError('Cannot connect to PCAN device.')
        if auto_connect and len(self.uart.return_list_of_ports()) > 0:
            self.uart.connect_to_port(self.uart.return_list_of_ports()[0])
        print('Status: '+str(self.can.GetStatus(PCAN_USBBUS1)))

    def tc(self, channel, id, param=[]):
        '''Write a telecommand to the connected device.'''
        if channel == 'UART':
            self.uart.port.reset_output_buffer()
            self.uart.port.reset_input_buffer()
            tx = param[:]
            tx.insert(0, id)
            self.uart.write_bytes(self.encode(tx[:]))
            rx = self.uart_receive_response()
            return rx[0] == self.ACK
        elif channel=='I2C':
            return len(self.i2c.transceive(id,param,0))==0
        elif channel == 'CAN':
            msg_out = TPCANMsg(int('01' + "{0:#0{1}x}".format(id,4)[-2:] + '03' + self.CANMASK, 16), PCAN_MESSAGE_EXTENDED, len(param),
                               (ctypes.c_ubyte * 8)(*param))
            result=self.can.Write(PCAN_USBBUS1, msg_out)
            if result != PCAN_ERROR_OK:
                self.can.Uninitialize(PCAN_USBBUS1)
                if self.ERROR_RAISE: raise self.ProtocolError('Cannot write to device via CAN.')
            # return result == PCAN_ERROR_OK
            retrycount = 0
            msg_in = PCAN_ERROR_QRCVEMPTY,
            while (msg_in[0] & PCAN_ERROR_QRCVEMPTY == PCAN_ERROR_QRCVEMPTY):
                sleep(0.001)
                msg_in = self.can.Read(PCAN_USBBUS1)
                if msg_in[0] == PCAN_ERROR_OK:
                    # print("{0:#0{1}x}".format(msg_in[1].ID,10))
                    return True# list(msg_in[1].DATA)
                retrycount += 1
                if retrycount > 200:
                    self.can.Uninitialize(PCAN_USBBUS1)
                    if self.ERROR_RAISE: raise self.ProtocolError('No reply from device via CAN.')

    def tlm(self, channel, id, tlmlength, delay=0):
        '''Writes an telemetry to the connected device'''
        if channel == 'UART':
            self.uart.port.reset_output_buffer()
            self.uart.port.reset_input_buffer()
            self.uart.write_bytes(self.encode([id]))
            return self.uart_receive_response()
        elif channel=='I2C':
            return self.i2c.transceive(id,[],tlmlength)
        elif channel == 'CAN':
            msg_out = TPCANMsg(int('04' + "{0:#0{1}x}".format(id, 4)[-2:] + '03' + self.CANMASK, 16),
                               PCAN_MESSAGE_EXTENDED, 0,(ctypes.c_ubyte * 8)(*[0]*8))
            result = self.can.Write(PCAN_USBBUS1, msg_out)
            if result != PCAN_ERROR_OK:
                self.can.Uninitialize(PCAN_USBBUS1)
                if self.ERROR_RAISE: raise self.ProtocolError('Cannot write to device via CAN.')
            retrycount = 0
            msg_in = PCAN_ERROR_QRCVEMPTY,
            while (msg_in[0] & PCAN_ERROR_QRCVEMPTY == PCAN_ERROR_QRCVEMPTY):
                sleep(0.001)
                msg_in = self.can.Read(PCAN_USBBUS1)
                if msg_in[0] == PCAN_ERROR_OK:
                    # print("{0:#0{1}x}".format(msg_in[1].ID,10))
                    return list(msg_in[1].DATA)
                retrycount += 1
                if retrycount > 200:
                    self.can.Uninitialize(PCAN_USBBUS1)
                    if self.ERROR_RAISE: raise self.ProtocolError('No reply from device via CAN.')

    def uart_receive_response(self):
        '''
        This function waits for a response from the device, decodes it, and returns the data
        :return:
        '''
        RX_State = Enum('RX_State', 'SOM DATA DONE')  # Essentially the part we are waiting for
        state = RX_State['SOM']
        escaped = False
        data = []

        while state is not RX_State['DONE']:

            try:
                byte = self.uart.read_byte()[0]  # This call should have the time out that
            except TypeError:
                raise self.NoResponseError('No Reply from device.')

            if byte is None: break  # Was a timeout

            if not escaped and byte == self.ESC_CHAR:
                escaped = True
                continue
            elif state == RX_State['SOM'] and byte == self.SOM:
                state = RX_State['DATA']
            elif state == RX_State['DATA']:
                if escaped and byte == self.ESC_CHAR:
                    data.append(self.ESC_CHAR)
                elif escaped and byte == self.EOM:
                    state = RX_State['DONE']
                    # End of the message have been reached
                    break
                else:
                    data.append(byte)
            escaped = False  # If it escaped this iteration the code won't reach here due to the <continue>

        if state is RX_State['SOM']:
            if self.ERROR_RAISE: raise self.TimeOutError('Incomplete message.')
            return None  # If the message was unsuccesful, return Nont
        elif state is not RX_State['DONE']:
            if self.ERROR_RAISE: raise self.ProtocolError('Incomplete message.')
            return None  # If the message was unsuccesful, return Nont

        return data

    def encode(self, bytes):
        '''Encodes byte lists to the CubeSpace standard'''
        '''It adds all the SOM,EOM and ESC_CHARs'''
        i = 0
        while i < len(bytes):
            if bytes[i] == self.ESC_CHAR:
                bytes.insert(i, self.ESC_CHAR)
                i += 1
            i += 1
        bytes.insert(0, self.SOM)
        bytes.insert(0, self.ESC_CHAR)
        bytes.extend([self.ESC_CHAR, self.EOM])
        return bytes

    def decode(self, bytes, suppress=False):
        '''Decodes byte lists to the CubeSpace standard'''
        '''It removes all the SOM,EOM, and ESC_CHARs'''
        if any(bytes) is not None:
            if len(bytes) < 5:
                if self.ERROR_RAISE: raise self.ProtocolError('Decoding failed. Not enough data.')
                return None
            if bytes[0] != self.ESC_CHAR or bytes[1] != self.SOM or bytes[-2] != self.ESC_CHAR or bytes[-1] != self.EOM:
                if self.ERROR_RAISE: raise self.ProtocolError('Decoding failed. Invalid framing.')
                return None
            bytes = bytes[2:-2]
            i = 0
            while i < len(bytes):
                if bytes[i] == self.ESC_CHAR:
                    del bytes[i]
                i += 1
            return bytes
        else:
            if self.ERROR_RAISE: raise self.TimeOutError('No response received.')

    @staticmethod
    def uint162bytes2(l):
        '''
        Takes to a list of 16 bit values and returns list of bytes
        '''
        b = list(range(2 * len(l)))
        for i in b:
            if i & 1:
                b[i] = Comms.highbyte(l[int(np.floor(i / 2))])
            else:
                b[i] = Comms.lowbyte(l[int(np.floor(i / 2))])
        return b

    @staticmethod
    def lowbyte(b):
        return b & 0xFF

    @staticmethod
    def highbyte(b):
        return (b & 0xFF00) >> 8


if __name__ == "__main__":
    print("Started: comms.py")

    comms = Comms(False)
    ports = comms.uart.return_list_of_ports()
    print (ports)
    # if comms.uart.connect_to_port(ports[0]):

    # comms.uart_receive_response()

    num = 31
    print("\nTest Encoding/Decoding: " + str(num))
    print(comms.encode([num]))
    print(comms.decode(comms.encode([num])))

    tlm = 128
    print("\nTest TLM: " + str(tlm))
    print(comms.tlm('CAN',tlm))

    tc = 2
    print("\nTest TC: " + str(tc))
    print(comms.tc('CAN', tc, Comms.uint162bytes2([2000])))

    # comms.uart.close()
    # else:
    #     print("Didn't connect to comms for some reason")
