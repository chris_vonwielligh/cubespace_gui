import serial
from serial.tools import list_ports
import time

class UART:
    '''Handles the low level serial communication'''

    active = False
    loggers = None
    ERROR_RAISE = False

    def __init__(self, loggers=None):
        if loggers: self.loggers = loggers
        self.port = None

    def return_list_of_ports(self):
        '''List of COMM Port'''
        all_pos_ports = list(list_ports.comports())
        virt_comm_ports = []
        for this_port in all_pos_ports:
            if "Serial" in this_port[1]:
                virt_comm_ports.append(this_port[0])
        return virt_comm_ports

    def connect_to_port(self, port):
        try:
            if not self.is_open():
                self.port = serial.Serial(port,
                                          baudrate=115200,
                                          bytesize=serial.EIGHTBITS,
                                          parity=serial.PARITY_NONE,
                                          stopbits=serial.STOPBITS_ONE,
                                          timeout=0.3,
                                          xonxoff=0,
                                          rtscts=0)
                self.active = True
                if self.loggers: self.loggers['event'].log_text('Successfully connected to '+port+'.', module="CommsUART")
                return True
            return False
        except serial.SerialException:
            if self.ERROR_RAISE: raise ValueError('Connection failed. Port ' + port + ' already in use.')
            self.active = False
            return False

    def write_bytes(self, bytes):
        '''Writes an list of bytes to the comm port'''
        try:
            self.port.write(bytearray(bytes))
            if self.loggers: self.loggers['comms'].log_text('TX:\t' + str(bytes), ignore_module=True)
        except AttributeError:
            self.active = False
            if self.ERROR_RAISE:  ValueError('Connection failed. Port not initialised.')

    def read_byte(self):
        try:
            rx = self.port.read()
            if len(rx) == 0 : return None
            return [int(byte) for byte in rx]
        except AttributeError:
            if self.ERROR_RAISE: ValueError('Connection failed. Port not initialised.')
            self.active = False
            return None

    def read_bytes(self):
        '''Read all bytes currently in input buffer'''
        try:
            rx =  self.port.read(self.port.in_waiting)
            bytes = []
            for byte in rx:
                bytes.append(int(byte))
            if self.loggers: self.loggers['comms'].log_text('RX:\t' + str(bytes), ignore_module=True)
            return bytes
        except AttributeError:
            if self.ERROR_RAISE: ValueError('Connection failed. Port not initialised.')
            self.active = False
            return None

    def is_open(self):
        '''Is the port currently open?'''
        if not self.port:
            return False
        else:
            return self.port.is_open

    def close(self):
        if self.port: self.port.close()
        if self.loggers: self.loggers['event'].log_text('Successfully disconneted from port.', module="CommsUART")
        self.active = False