import serial
from serial.tools import list_ports
from time import sleep

class I2C:
    '''Handles the low level serial communication'''

    active = False
    loggers = None
    ERROR_RAISE = False
    ACK = "*"
    def __init__(self, loggers=None):
        if loggers: self.loggers = loggers
        self.port = None

    def return_list_of_ports(self):
        '''List of COMM Port'''
        all_pos_ports = list(list_ports.comports())
        virt_comm_ports = []
        for this_port in all_pos_ports:
            if "Serial" in this_port[1]:
                virt_comm_ports.append(this_port[0])
        return virt_comm_ports

    def connect_to_port(self, port):
        try:
            if not self.is_open():
                self.port = serial.Serial(port,
                                          baudrate=115200,
                                          bytesize=serial.EIGHTBITS,
                                          parity=serial.PARITY_NONE,
                                          stopbits=serial.STOPBITS_ONE,
                                          timeout=0.5,
                                          xonxoff=0,
                                          rtscts=0)
                self.active = True
                if self.loggers: self.loggers['event'].log_text('Successfully connected to '+port+'.', module="CommsUART")
                sleep(1)
                self.write_read('\r\r')
                self.write_read('P0\r')
                self.write_read('Ad0\r')
                print('Successfully connected to I2C')
                return True
            return False
        except serial.SerialException:
            print('Cannot connect to I2C')
            if self.ERROR_RAISE: raise ValueError('Connection failed. Port ' + port + ' already in use.')
            self.active = False
            return False

    def write_read(self,cmd):
        k = ''
        v = ''
        # send command
        self.port.reset_input_buffer()
        self.port.write(str.encode(cmd))
        # print "send ",cmd # debug
        # return value
        retrycount=0
        while k != self.ACK and retrycount<5:
            k = self.port.read(1)
            k =k.decode("utf-8")
            if k == self.ACK:  # all done
                if len(v) == 0:
                    v = self.ACK
                break
            else:
                if len(k) == 0:
                    retrycount+=1
                    continue
                else:
                    v = v + k
        # convert sting into list
        rvl = []
        if len(v) != 0:

            ss = v.rsplit(',')

            for s in ss:
                try:
                    rvl.append(int(s,16))
                except:
                    rvl = []  # clear list on any error

        return rvl

    def transceive(self, adr, listWrite, nRead):
        wl = ""
        rvList = []  # may return an empty list
        adrStr=("{0:#0{1}x}".format(adr, 4)[-2:])
        # see if there is anything to write
        if len(listWrite) == 0:
            rvList = self.write_read("s " +adrStr+ " r g-" + str(nRead) + " p\r")
        else:
            # convert write list to a string
            for n in listWrite:
                wl += ("{0:#0{1}x}".format(n, 4)[-2:]) + " "
            rvList = self.write_read("s " + ("{0:#0{1}x}".format(adr, 4)[-2:])+ " " + wl + "p\r")
        return rvList

    def is_open(self):
        '''Is the port currently open?'''
        if not self.port:
            return False
        else:
            return self.port.is_open

    def close(self):
        if self.port: self.port.close()
        if self.loggers: self.loggers['event'].log_text('Successfully disconneted from port.', module="CommsUART")
        self.active = False