#!/usr/bin/python3

from comms import Comms
from collections import namedtuple
from struct import *
import numpy as np
import atexit
from time import sleep
from enum import Enum


# from matplotlib import pyplot as plt

def exit_handler():
    print('Properly exiting program...')
    #TCTLM.comms.can.Uninitialize(PCAN_USBBUS1)


class TCTLM():
    # Telemetry IDs
    Identification = 128
    ExtendedId = 129
    CurrentStatus = 130
    WheelSpeed = 133
    WheelReference = 134
    WheelCurrent = 135
    WheelData = 137
    AdditionalWheelData = 138
    PWMGain = 139
    MainGains = 140
    BackupGains = 141
    ErrorFlags = 145

    # Telemetry Lengths
    Identification_len = 8
    ExtendedId_len = 4
    CurrentStatus_len = 8
    WheelSpeed_len = 2
    WheelReference_len = 2
    WheelCurrent_len = 2
    WheelData_len = 6
    AdditionalWheelData_len = 4
    PWMGain_len = 3
    MainGains_len = 6
    BackupGains_len = 6
    ErrorFlags_len = 1

    # Telecommand IDs
    Reset = 1
    SetWheelReference = 2
    WheelCommandedTorque = 3
    MotorPowerState = 7
    EncoderPowerState = 8
    HallPowerState = 9
    ControlMode = 10
    BackupWheelMode = 12
    ClearErrors = 20
    SetSerial = 30
    SetI2CAddress = 31
    SetCANMask = 32
    SetPWMGain = 33
    SetMainGain = 34
    SetBackupGain = 35



    Identification_t = namedtuple("Identification_t", "NodeType InterfaceVersion FirmwareVersionMajor FirmwareVersionMinor RunTimeSeconds RunTimeMilliSeconds")

    ExtendedIdentification_t = namedtuple("ExtendedIdentification_t", ['Serial' , 'I2CAddress' , 'CANMask'])


    CurrentStatus_t = namedtuple("CurrentStatus_t",
                                 "RunTimeSeconds RunTimeMilliSeconds MotorControlMode BackupMode MotorSwitch HallSwitch EncoderSwitch ErrorFlag")
    WheelSpeed_t = None
    WheelReference_t = None
    WheelCurrent_t = None
    WheelData_t = namedtuple("WheelData_t", "WheelSpeed WheelReference WheelCurrent")
    AdditionalWheelData_t = namedtuple("AdditionalWheelData_t", "WheelDuty WheelSpeedBackup")
    PWMGain_t = namedtuple("PWMGain_t", "MainGain Multiplier")
    MainGains_t = namedtuple("MainGains_t",
                             "IntegratorGain IntegratorGainMultiplier FeedbackGain FeedbackGainMultiplier")
    BackupGains_t = namedtuple("BackupGains_t",
                               "IntegratorGain IntegratorGainMultiplier FeedbackGain FeedbackGainMultiplier")
    ErrorFlags_t = namedtuple("ErrorFlags_t",
                              "InvalidTLM InvalidTCMD EncoderError UARTError I2CError CANError ConfigError SpeedError")

    loggers = None

    def __init__(self, auto_connect=True, loggers=None):
        if loggers: self.loggers = loggers
        self.comms = Comms(auto_connect, loggers=self.loggers)

    def is_open(self):
        return self.comms.uart.is_open()

    '''
    .___________. _______  __       _______ .___  ___.  _______ .___________..______       __   _______     _______.
    |           ||   ____||  |     |   ____||   \/   | |   ____||           ||   _  \     |  | |   ____|   /       |
    `---|  |----`|  |__   |  |     |  |__   |  \  /  | |  |__   `---|  |----`|  |_)  |    |  | |  |__     |   (----`
        |  |     |   __|  |  |     |   __|  |  |\/|  | |   __|      |  |     |      /     |  | |   __|     \   \    
        |  |     |  |____ |  `----.|  |____ |  |  |  | |  |____     |  |     |  |\  \----.|  | |  |____.----)   |   
        |__|     |_______||_______||_______||__|  |__| |_______|    |__|     | _| `._____||__| |_______|_______/    
                                                                                                                    
    '''

    def get_identification(self, channel):
        '''Returns a list with the device identification'''
        rx = self.comms.tlm(channel, self.Identification, self.Identification_len)
        return self.Identification_t(rx[0], rx[1], rx[2], rx[3], self.bytes2uint16(rx[4:6]), self.bytes2uint16(rx[-2:]))


    def get_extended_identifiction(self, channel):
        '''Returns a list with the device's extended identification'''
        rx = self.comms.tlm(channel, self.ExtendedId, self.ExtendedId_len)
        return self.ExtendedIdentification_t(self.bytes2uint16(rx[:2]), rx[2], rx[3])

    def get_current_status(self, channel):
        rx = self.comms.tlm(channel, self.CurrentStatus, self.CurrentStatus_len)
        controlmode = (['Idle', 'NoControl', 'DutyCycleInput', 'SpeedController'])[rx[6]]
        return self.CurrentStatus_t(self.bytes2uint16(rx[0:2]), self.bytes2uint16(rx[2:4]), controlmode, (rx[7]) & 1,
                                    (rx[7] >> 1) & 1, (rx[7] >> 2) & 1, (rx[7] >> 3) & 1, (rx[7] >> 4) & 1)

    def get_wheel_speed(self, channel):
        rx = self.comms.tlm(channel, self.WheelSpeed, self.WheelSpeed_len)
        self.WheelSpeed_t = (self.bytes2int16(rx[0:2])) / 2.0
        return self.WheelSpeed_t

    def get_wheel_reference(self, channel):
        rx = self.comms.tlm(channel, self.WheelReference, self.WheelReference_len)
        self.WheelReference_t = (self.bytes2uint16(rx[0:2])) / 2.0
        return self.WheelReference_t

    def get_wheel_current(self, channel):
        rx = self.comms.tlm(channel, self.WheelCurrent, self.WheelCurrent_len)
        self.WheelCurrent_t = (self.bytes2uint16(rx[0:2])) * 0.48828125
        return self.WheelCurrent_t

    def get_wheel_data(self, channel):
        rx = self.comms.tlm(channel, self.WheelData, self.WheelData_len)
        return self.WheelData_t(self.bytes2uint16(rx[0:2]) / 2.0, self.bytes2uint16(rx[2:4]) / 2.0,
                                self.bytes2uint16(rx[4:6]) * 0.48828125)

    def get_additional_wheel_data(self, channel):
        rx = self.comms.tlm(channel, self.AdditionalWheelData, self.AdditionalWheelData_len)
        return self.AdditionalWheelData_t(self.bytes2uint16(rx[0:2]), self.bytes2uint16(rx[2:4]) / 2.0)

    def get_PWM_gain(self, channel):
        rx = self.comms.tlm(channel, self.PWMGain, self.PWMGain_len)
        return self.PWMGain_t(self.bytes2uint16(rx[0:2]), rx[2])

    def get_main_gains(self, channel):
        rx = self.comms.tlm(channel, self.MainGains, self.MainGains_len)
        return self.MainGains_t(self.bytes2uint16(rx[0:2]), rx[2], self.bytes2uint16(rx[3:5]), rx[5])

    def get_backup_gains(self, channel):
        rx = self.comms.tlm(channel, self.BackupGains, self.BackupGains_len)
        return self.BackupGains_t(self.bytes2uint16(rx[0:2]), rx[2], self.bytes2uint16(rx[3:5]), rx[5])

    def get_error_flags(self, channel):
        rx = self.comms.tlm(channel, self.ErrorFlags, self.ErrorFlags_len)
        return self.ErrorFlags_t(rx[0] & 1, (rx[0] >> 1) & 1, (rx[0] >> 2) & 1, (rx[0] >> 3) & 1, (rx[0] >> 4) & 1,
                                 (rx[0] >> 5) & 1, (rx[0] >> 6) & 1, (rx[0] >> 7) & 1)

    '''
    .___________. _______  __       _______   ______   ______   .___  ___. .___  ___.      ___      .__   __.  _______       _______.
    |           ||   ____||  |     |   ____| /      | /  __  \  |   \/   | |   \/   |     /   \     |  \ |  | |       \     /       |
    `---|  |----`|  |__   |  |     |  |__   |  ,----'|  |  |  | |  \  /  | |  \  /  |    /  ^  \    |   \|  | |  .--.  |   |   (----`
        |  |     |   __|  |  |     |   __|  |  |     |  |  |  | |  |\/|  | |  |\/|  |   /  /_\  \   |  . `  | |  |  |  |    \   \    
        |  |     |  |____ |  `----.|  |____ |  `----.|  `--'  | |  |  |  | |  |  |  |  /  _____  \  |  |\   | |  '--'  |.----)   |   
        |__|     |_______||_______||_______| \______| \______/  |__|  |__| |__|  |__| /__/     \__\ |__| \__| |_______/ |_______/    
                                                                                                                                     
    '''

    def set_mcu_reset(self, channel):
        return self.comms.tc(channel, self.Reset, [85])

    def set_wheel_reference(self, channel, rpm):
        return self.comms.tc(channel, self.SetWheelReference, self.uint162bytes2([(2*rpm)]))

    def set_wheel_commanded_torque(self, channel, duty):
        return self.comms.tc(channel, self.WheelCommandedTorque, self.uint162bytes2([duty]))

    def set_motor_power_state(self, channel, switch):
        return self.comms.tc(channel, self.MotorPowerState, [switch])

    def set_encoder_power_state(self, channel, switch):
        return self.comms.tc(channel, self.EncoderPowerState, [switch])

    def set_hall_power_state(self, channel, switch):
        return self.comms.tc(channel, self.HallPowerState, [switch])

    def set_control_mode(self, channel, mode):
        return self.comms.tc(channel, self.ControlMode, [mode])

    def set_backup_wheel_mode(self, channel, backupmode):
        return self.comms.tc(channel, self.BackupWheelMode, [backupmode])

    def set_clear_errors(self, channel):
        return self.comms.tc(channel, self.ClearErrors, [85])

    def set_serial(self, channel, serial):
        rx = self.comms.tc(channel, self.SetSerial, self.uint162bytes2([serial]))
        sleep(5)
        return rx

    def set_i2c_address(self, channel, address):
        rx = self.comms.tc(channel, self.SetI2CAddress, [address])
        sleep(5)
        return rx

    def set_can_mask(self, channel, address):
        rx = self.comms.tc(channel, self.SetCANMask, [address])
        sleep(5)
        return rx

    def set_pwm_gain(self, channel, gain, multiplier):
        rx = self.comms.tc(channel, self.SetPWMGain, self.uint162bytes2([gain]) + [multiplier])
        sleep(5)
        return rx

    def set_main_gain(self, channel, gainI, multiplierI, gainF, multiplierF):
        rx = self.comms.tc(channel, self.SetMainGain,
                           self.uint162bytes2([gainI]) + [multiplierI] + self.uint162bytes2([gainF]) + [multiplierF])
        sleep(5)
        return rx

    def set_backup_gain(self, channel, gainI, multiplierI, gainF, multiplierF):
        rx = self.comms.tc(channel, self.SetBackupGain,
                           self.uint162bytes2([gainI]) + [multiplierI] + self.uint162bytes2([gainF]) + [multiplierF])
        sleep(5)
        return rx

    '''
     __    __   _______  __      .______    _______ .______          _______.
    |  |  |  | |   ____||  |     |   _  \  |   ____||   _  \        /       |
    |  |__|  | |  |__   |  |     |  |_)  | |  |__   |  |_)  |      |   (----`
    |   __   | |   __|  |  |     |   ___/  |   __|  |      /        \   \    
    |  |  |  | |  |____ |  `----.|  |      |  |____ |  |\  \----.----)   |   
    |__|  |__| |_______||_______|| _|      |_______|| _| `._____|_______/
        
    '''

    @staticmethod
    def bytes2uint32(b, force_return_list=False):
        '''
        Takes to bytes and returns a 32 bit value
        '''
        d = list(b[0] for b in iter_unpack('<I', bytearray(b)))
        if force_return_list or len(d) > 1:
            return d
        else:
            return d[0]

    @staticmethod
    def bytes2uint16(b, force_return_list=False):
        '''
        Takes to bytes and returns a 16 bit value
        '''
        d = list(b[0] for b in iter_unpack('<H', bytearray(b)))
        if force_return_list or len(d) > 1:
            return d
        else:
            return d[0]

    @staticmethod
    def uint162bytes2(l):
        '''
        Takes to a list of 16 bit values and returns list of bytes
        '''
        b = list(range(2 * len(l)))
        for i in b:
            if i & 1:
                b[i] = TCTLM.highbyte(l[int(np.floor(i / 2))])
            else:
                b[i] = TCTLM.lowbyte(l[int(np.floor(i / 2))])
        return b

    @staticmethod
    def bytes2int16(b, force_return_list=False):
        '''
        Takes to bytes and returns a 16 bit value
        '''
        d = list(b[0] for b in iter_unpack('<h', bytearray(b)))
        if force_return_list or len(d) > 1:
            return d
        else:
            return d[0]

    @staticmethod
    def bytes2int8(b, force_return_list=False):
        '''
        Takes to bytes and returns a 8 bit signed value
        '''
        d = list(b[0] for b in iter_unpack('<b', bytearray(b)))
        if force_return_list or len(d) > 1:
            return d
        else:
            return d[0]

    @staticmethod
    def bytes2uint8(b, force_return_list=False):
        '''
        Takes to bytes and returns a 8 bit unsigned value
        '''
        if not isinstance(b, list): b = [b]
        d = list(b[0] for b in iter_unpack('<B', bytearray(b)))
        if force_return_list or len(d) > 1:
            return d
        else:
            return d[0]

    @staticmethod
    def bytes2float(b, force_return_list=False):
        '''Converts 4 bytes to a float'''
        d = list(b[0] for b in iter_unpack('<f', bytearray(b)))
        if force_return_list or len(d) > 1:
            return d
        else:
            return d[0]

    @staticmethod
    def float2bytes(l):
        if isinstance(l, list):
            o = []
            for f in l: o.extend(pack('<f', f))
        else:
            o = list(pack('<f', l))
        return o

    @staticmethod
    def get_bit(b, idx):
        return 1 if ((b & (1 << idx))) else 0

    @staticmethod
    def bytes2bits(b, len=32):
        '''Create a list of bit values (true or false)'''
        return list((b & (1 << i)) != 0 for i in range(len))

    @staticmethod
    def bytes2str(b):
        return "".join(str(chr(c)) for c in b)

    @staticmethod
    def lowbyte(b):
        return b & 0xFF

    @staticmethod
    def highbyte(b):
        return (b & 0xFF00) >> 8


def main():
    tctlm = TCTLM(False)

    ports = tctlm.comms.uart.return_list_of_ports()
    for i, c in enumerate(ports):
        print(i, c)

    # var=int(input('Select COM port for UART: '))
    # if var < len(ports):
    #     tctlm.comms.uart.connect_to_port(ports[var])

    # var = int(input('Select COM port for I2C: '))
    # if var < len(ports):
    #     tctlm.comms.i2c.connect_to_port(ports[var])

    tctlm.comms.uart.connect_to_port(ports[1])
    tctlm.comms.i2c.connect_to_port(ports[0])

    # for i in range(3):
    #     print(tctlm.get_identification('UART'))
    #     sleep(0.1)
    # print(tctlm.get_backup_gains('UART'))
    # print(tctlm.get_extended_identifiction('UART'))
    # print('Setting serial: '+str(tctlm.set_serial('I2C',1999)))
    # print('Setting CAN mask: '+str(tctlm.set_can_mask('I2C', 2)))
    # print('Resetting MCU....')
    # tctlm.set_mcu_reset('I2C')
    # sleep(2)
    # print('MCU reset')

    print(tctlm.get_identification('UART'))
    print(tctlm.get_extended_identifiction('UART'))

    # sleep(1)
    # tctlm.set_serial('CAN', 1991)
    # sleep(5)

    # for speed_rpm in range(20, 2000, 20):
    #     print('Increment speed with 20 RPM to: ' + str(speed_rpm))
    #     tctlm.set_wheel_reference('CAN', speed_rpm)
    #     sleep(3)
    #     print("Current measured speed: " + str(tctlm.get_wheel_speed('CAN')))
    #     sleep(1)
    #     if speed_rpm==200:
    #         tctlm.set_can_mask('CAN',4)
    #         sleep(5)

    # print('Setting speed to 1000 RPM...')
    # tctlm.set_wheel_reference('CAN', 1000)
    # sleep(5)
    # while(1):
    #     print("Current measured speed: " + str(tctlm.get_wheel_speed('CAN')))
    #     sleep(1)

    tctlm.set_wheel_reference('CAN', 0)


if __name__ == "__main__":
    atexit.register(exit_handler)
    print("Started: tctlm.py")
    try:
        main()
    finally:
        exit_handler()

