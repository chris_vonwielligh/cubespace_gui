from tctlm import TCTLM
from time import sleep
import atexit

def exit_handler():
    print('Properly exiting program...')
   # TCTLM.comms.can.Uninitialize(PCAN_USBBUS1)



def main():
    serialnumber=1600

    tctlm = TCTLM(False)

    ports = tctlm.comms.uart.return_list_of_ports()
    for i, c in enumerate(ports):
        print(i, c)



    # tctlm.comms.uart.connect_to_port(ports[1])
    # tctlm.comms.i2c.connect_to_port(ports[0])



    var=int(input('Select COM port for UART: '))
    if var < len(ports):
        tctlm.comms.uart.connect_to_port(ports[var])

    var = int(input('Select COM port for I2C: '))
    if var < len(ports):
        tctlm.comms.i2c.connect_to_port(ports[var])

    print("Testing CAN...")
    if(tctlm.get_identification('CAN').NodeType == 8)
        print("CAN comms successful")
    else
        print("ERROR: CAN comms failed!")
        return
    print("Testing I2C...")
    if (tctlm.get_identification("I2C").NodeType == 8)
        print("I2C comms successful")
    else:
        print("ERROR: I2C comms failed!")
        return
    print("Testing UART...")
    if (tctlm.get_identification("UART").NodeType == 8)
        print("UART comms successful")
    else
        print("ERROR: UART comms failed!")
        return


    print("Testing if motor can be driven by sending 2000RPM command...")
    tctlm.set_wheel_reference('UART', 2000)
    sleep(3)
    if(tctlm.get_wheel_speed('UART')>=1000)
        print("Encoder and Vbat healthy")
    elif(tctlm.get_error_flags("UART").EncoderError)
        print("ERROR: Encoder is faulty!")
        return
    else
        print("ERROR: Vbat is faulty!")
        return
    print("Motor driven successfully. Returning to zero speed...")
    tctlm.set_wheel_reference('UART', 0)
    sleep(3)


    print("Setting wheel serial number to "+str(serialnumber)+"...")
    tctlm.set_serial('UART', serialnumber)
    sleep(5)

    if (tctlm.get_extended_identifiction("UART").Serial==serialnumber)
        print("Serial number set successfully")
        tctlm.set_clear_errors("UART")
    else
        print("ERROR: Unable to set serial number")
        return

    print("Resetting wheel...")
    tctlm.set_mcu_reset("UART")
    sleep(3)

    if (tctlm.get_error_flags("UART").ConfigError==0 and tctlm.get_extended_identifiction("UART").Serial==serialnumber)
        print("Configuration held successfully")
    else
        print("ERROR: EERPOM configuration is faulty")
        return


    if (tctlm.get_identification("UART")[0:3] == [8,1])
        print("UART comms successful")
    else
        print("ERROR: UART comms failed!")
        return




    tctlm.set_serial('UART', serialnumber)
    sleep(5)
    for speed_rpm in range(20, 2000, 20):
        print('Increment speed with 20 RPM to: ' + str(speed_rpm))
        tctlm.set_wheel_reference('CAN', speed_rpm)
        sleep(3)
        print("Current measured speed: " + str(tctlm.get_wheel_speed('CAN')))
        sleep(1)
        if speed_rpm == 200:
            tctlm.set_can_mask('CAN', 4)
            sleep(5)

    print('Setting speed to 1000 RPM...')
    tctlm.set_wheel_reference('CAN', 1000)
    sleep(5)
    while(1):
        print("Current measured speed: " + str(tctlm.get_wheel_speed('CAN')))
        sleep(1)

    tctlm.set_wheel_reference('CAN', 0)






if __name__ == "__main__":
    atexit.register(exit_handler)
    print("Started: tctlm.py")
    try:
        main()
        atexit.unregister(exit_handler)
    except:
        print("Running exception handler")
        exit_handler()