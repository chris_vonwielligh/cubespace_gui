import os
import subprocess
from git import Repo
import json
import shutil
from tctlm import TCTLM
from datetime import datetime
from time import sleep
import atexit

tctlm = TCTLM(False)

def print_repository(repo):
    print('Repo description: {}'.format(repo.description))
    print('Repo active branch is {}'.format(repo.active_branch))
    for remote in repo.remotes:
        print('Remote named "{}" with URL "{}"'.format(remote, remote.url))

def print_commit(commit):
    print('Last commit for repo is {}.'.format(str(repo.head.commit.hexsha)))
    print("\"{}\" by {} ({})".format(commit.summary,
                                     commit.author.name,
                                     commit.author.email))
    print(str(commit.authored_datetime))
    print(str("count: {} and size: {}".format(commit.count(),
                                              commit.size)))
    print('----')

def clean_temp_files():
    # Source Files
    source_directory = os.getcwd() + "\\build_target"
    for file in os.listdir(source_directory):
        os.remove(os.path.join(source_directory, file))

    # Build Files
    build_directory = os.getcwd() + "\\build_target\\release"
    for file in os.listdir(build_directory):
        filename = os.fsdecode(file)
        if filename.endswith('Makefile') or filename.endswith("makedep.mk"):
            continue
        else:
            os.remove(os.path.join(build_directory, file))

def get_cw_config_vals(sn):
    # Get a list of available serial numbers
    config_dir = os.getcwd() + "\\config\\device_configs"
    full_file_name = os.path.join(config_dir, 'cw' + str(sn) + '.json')
    config_vals_cw = None
    if(os.path.isfile(full_file_name)):
        try:
            f = open(full_file_name, "r")
            config_vals_cw = dict(json.load(f))
        finally:
            try:
                f.close()
            except:
                pass
    else:
        print("ERROR: Could not get config for cw{}".format(sn))
        return False
    return config_vals_cw

def generate_config(sn):
    # Read the configuration values of the requested serial number
    config = get_cw_config_vals(sn)
    # If the configuration values exist, create the config file
    if config is not None:
        return create_config_file(config)
    else:
        print("Configuration values for CW{} not found.".format(sn))
    return False

def create_config_file(c):
    fname = os.getcwd() + "/build_target/config.h"
    fs = ""
    try:
        fs += "/*\n"
        fs += "    CubeWheel Configuration Parameters\n"
        fs += "*/\n"
        fs += "#ifndef CONFIG_H_\n"
        fs += "#define CONFIG_H_\n"
        fs += "\n"
        fs += "// Identification\n"
        fs += "#define SERIAL_NUMBER    {}\n".format(c["sn"])
        fs += "#define FIRMWARE_MAJOR_VERSION   {}\n".format(c["sw_ver"].split(".")[0])
        fs += "#define FIRMWARE_MINOR_VERSION   {}\n".format(c["sw_ver"].split(".")[1])
        fs += "#define INTERFACE_VERSION    {}\n".format(c["if_ver"])
        fs += "#define I2C_ADRESS   {}\n".format(c["i2c_address"])
        fs += "#define CAN_ADDRESS  {}\n".format(c["can_address"])
        fs += "\n"
        fs += "// Wheel Size\n"
        fs += "#define {}\n".format(c["wheel_size"])
        fs += "\n"
        fs += "// File generated on {} \n".format(datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
        fs += "#endif /* CONFIG_H_ */"

        with open(fname, "w") as config_file:
            config_file.write(fs)
            config_file.close()
            print("Config written to \"{}\"".format(fname))

        return True
    except:
        print("Failed to write configuration file for CW{}".format(c["sn"]))
        return False

def reset_config_file():
    fname = os.getcwd() + "/build_target/config.h"
    fs = ""
    try:
        fs += "/*\n"
        fs += "    CubeWheel Configuration Parameters\n"
        fs += "*/\n"
        fs += "#ifndef CONFIG_H_\n"
        fs += "#define CONFIG_H_\n"
        fs += "\n"
        fs += "// Identification\n"
        fs += "#define SERIAL_NUMBER    UNDEFINED\n"
        fs += "#define FIRMWARE_MAJOR_VERSION   UNDEFINED\n"
        fs += "#define FIRMWARE_MINOR_VERSION   UNDEFINED\n"
        fs += "#define INTERFACE_VERSION        UNDEFINED\n"
        fs += "#define I2C_ADRESS     			UNDEFINED\n"
        fs += "#define CAN_ADDRESS      		SIZE_UNDEFINED\n"
        fs += "\n"
        fs += "// Wheel Size\n"
        fs += "#define UNDEFINED\n"
        fs += "\n"
        fs += "// File generated on {} \n".format(datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
        fs += "#endif /* CONFIG_H_ */"

        with open(fname, "w") as config_file:
            config_file.write(fs)
            config_file.close()
            print("Config \"{}\" is reset".format(fname))

        return True
    except:
        print("Failed to write configuration file for CW{}".format(c["sn"]))
        return False

def connect_to_comms():
    ports = tctlm.comms.uart.return_list_of_ports()
    for i, c in enumerate(ports):
        print(i, c)

    var=int(input('Select COM port for UART: '))
    if var < len(ports):
        tctlm.comms.uart.connect_to_port(ports[var])

    print("Testing UART...")
    if (tctlm.get_identification("UART").NodeType == 8):
        print("UART comms successful")
    else:
        print("ERROR: UART comms failed!")
        return

    for i, c in enumerate(ports):
        print(i, c)
    var = int(input('Select COM port for I2C: '))
    if var < len(ports):
        tctlm.comms.i2c.connect_to_port(ports[var])
    print("Testing I2C...")
    if (tctlm.get_identification("I2C").NodeType == 8):
        print("I2C comms successful")
    else:
        print("ERROR: I2C comms failed!")

def log_identification(sn):
    config_vals = get_cw_config_vals(sn)
    ident = tctlm.get_identification("UART")
    id_data = {}
    id_data['sn'] = config_vals['sn']
    id_data['can_mask'] = config_vals['can_address']
    id_data['i2c_address'] = config_vals['i2c_address']
    id_data['node_type'] = ident.NodeType
    id_data['interface_version'] = ident.InterfaceVersion
    id_data['firmware_version'] = "{}.{}".format(ident.FirmwareVersionMajor, ident.FirmwareVersionMinor)
    id_data['runtime1'] = ident.RunTimeSeconds
    sleep(0.5)
    ident2 = tctlm.get_identification("UART")
    id_data['runtime2'] = ident2.RunTimeSeconds

    data = {}
    data['identification'] = id_data
    log_filename = "{}\\report_logs\\log_cw{}.json".format(os.getcwd(), sn)
    with open(log_filename, 'w') as outfile:
        json.dump(data, outfile)

def log_wheel_status(sn):
    cw_status = tctlm.get_current_status("UART")
    wdata = {}
    wdata['backup_mode'] = cw_status.BackupMode
    wdata['control_mode'] = cw_status.MotorControlMode
    wdata['motor_switch_state'] = cw_status.MotorSwitch
    wdata['hall_sensor_switch_state'] = cw_status.HallSwitch
    wdata['encoder_switch_state'] = cw_status.EncoderSwitch
    wdata['error_code'] = cw_status.ErrorFlag
    log_filename = "{}\\report_logs\\log_cw{}.json".format(os.getcwd(), sn)

    with open(log_filename, 'r') as f:
        data = json.load(f)
        data['status'] = wdata
    os.remove(log_filename)
    with open(log_filename, 'w') as f:
        json.dump(data, f, indent=4)

def log_wheel_data(sn):
    cw_data = tctlm.get_wheel_data("UART")
    cw_data_add = tctlm.get_additional_wheel_data("UART")
    wdata = {}
    wdata['speed'] = cw_data.WheelSpeed
    wdata['reference'] = cw_data.WheelReference
    wdata['current'] = cw_data.WheelCurrent
    wdata['duty_cycle'] = cw_data_add.WheelDuty
    wdata['bu_speed'] = cw_data_add.WheelSpeedBackup

    log_filename = "{}\\report_logs\\log_cw{}.json".format(os.getcwd(), sn)
    with open(log_filename, 'r') as f:
        data = json.load(f)
        data['data'] = wdata
    os.remove(log_filename)
    with open(log_filename, 'w') as f:
        json.dump(data, f, indent=4)

def log_controller_gains(sn):
    cw_main = tctlm.get_main_gains("UART")
    cw_backup = tctlm.get_backup_gains("UART")
    cw_pwm = tctlm.get_PWM_gain("UART")
    wdata = {}
    a = {}
    a['gain'] = cw_main.IntegratorGain
    a['mult'] = cw_main.IntegratorGainMultiplier
    a['answ'] = (cw_main.IntegratorGain / (10 ** cw_main.IntegratorGainMultiplier))
    wdata['main_integrator_gain'] = a
    a = {}
    a['gain'] = cw_main.FeedbackGain
    a['mult'] = cw_main.FeedbackGainMultiplier
    a['answ'] = (cw_main.FeedbackGain / (10 ** cw_main.FeedbackGainMultiplier))
    wdata['main_feedback_gain'] = a
    a = {}
    a['gain']= cw_backup.IntegratorGain
    a['mult'] = cw_backup.IntegratorGainMultiplier
    a['answ'] = (cw_backup.IntegratorGain / (10 ** cw_backup.IntegratorGainMultiplier))
    wdata['backup_integrator_gain'] = a

    a = {}
    a['gain'] = cw_backup.FeedbackGain
    a['mult'] = cw_backup.FeedbackGainMultiplier
    a['answ'] = (cw_backup.FeedbackGain / (10 ** cw_backup.FeedbackGainMultiplier))
    wdata['backup_feedback_gain'] = a

    a = {}
    a['gain'] = cw_pwm.MainGain
    a['mult'] = cw_pwm.Multiplier
    a['answ'] = (cw_pwm.MainGain / (10 ** cw_pwm.Multiplier))
    wdata['pwm_gain'] = a

    log_filename = "{}\\report_logs\\log_cw{}.json".format(os.getcwd(), sn)
    with open(log_filename, 'r') as f:
        data = json.load(f)
        data['controller_gains'] = wdata
    os.remove(log_filename)
    with open(log_filename, 'w') as f:
        json.dump(data, f, indent=4)

def terminate_program():
    clean_temp_files()
    reset_config_file()

if __name__ == "__main__":

    print("CubeWheel Health Check")

    with open('config/directories_config.json') as json_file:
        dir_config_data = json.load(json_file)
    # Pull latest update from repository:
    repo_path = dir_config_data['repo_directory']
    # Repo object used to programmatically interact with Git repositories
    repo = Repo(repo_path)
    repo.git.checkout('release')
    if not repo.bare:
        print('Repo at {} successfully loaded.'.format(repo_path))
        print('----')
        print_repository(repo)
        print_commit(repo.head.commit)
        print("Pulling from branch {}".format(repo.active_branch))
        repo.git.pull()
    else:
        print('Could not load repository at {} :('.format(repo_path))

    # Copy source files to build target directory
    current_dir = os.getcwd()
    src_files = dir_config_data['source_file_list']
    print("replacing source files...")
    for file_name in src_files:
        # First delete old source files
        build_target_dir = current_dir + '\\build_target'
        old_full_file_name = os.path.join(build_target_dir, file_name)
        if(os.path.isfile(old_full_file_name)):
            os.remove(old_full_file_name)
        # Now copy updated files from repo to build directory
        full_file_name = os.path.join(dir_config_data['source_directory'], file_name)
        if(os.path.isfile(full_file_name)):
            shutil.copyfile(full_file_name, os.path.join(build_target_dir, file_name))
            print(file_name)
        else:
            print("Error, {} not found in source directory: {}".format(file_name, full_file_name))

    # Generate Config File
    serial_number = input('Type CubeWheel Serial Number to start health check: ')
    if(generate_config(serial_number) == False):
        terminate_program()

    # Build the firmware
    final_dir = os.path.join(build_target_dir, "release")
    print("building firmware at {}".format(final_dir))
    print("This may take a minute...")
    command_string = "make clean all TOOLS_DIR=\"{}\" -C \"{}\" ".format(current_dir, final_dir)
    print(command_string)
    os.system('cmd /c ' + command_string)
    print("--------")

    # Program the Device
    # hex_file_full_name = os.path.join(final_dir, "ReactionWheelFirmware_6_2.hex")
    # print("programming hex file: {}".format(hex_file_full_name))
    # programming_command_string = "\"\"{}\\atbackend\\atprogram.exe\" -t atmelice -i isp -d atmega328pb -v program -f \"{}\"\"".format(current_dir, hex_file_full_name)
    # os.system('cmd /c' + programming_command_string)

    # Start with basic tests
    connect_to_comms()
    log_identification(serial_number)
    log_wheel_status(serial_number)
    log_controller_gains(serial_number)

    #clean_temp_files()
    reset_config_file()
