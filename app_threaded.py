import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QWidget, QVBoxLayout
from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot, QThread
from PyQt5 import QtCore
from PyQt5 import QtGui

from logger import Logger
import pyqt_gui
from tctlm_wrapper import TCTLM_SignalWrapper
from file_handlers import *
import time
from datetime import datetime, timedelta
import os
import numpy as np
from PIL import Image, ImageQt
import csv
import json
from struct import *
import traceback
import matplotlib as mpl
from matplotlib import dates
import math
from warnings import warn as real_warn
from cubedock import CubeDock

class AppThreaded(QMainWindow):
    '''This class handles the entire program, the GUI and input things'''

    auto_update = False
    current_image = None
    loggers = None


    _sig_send_generic_tctlm = QtCore.pyqtSignal(object)
    _sig_echo = QtCore.pyqtSignal(object)

    def __init__(self, loggers=None):
        super(AppThreaded, self).__init__()

        if loggers: self.loggers = loggers
        self.data_to_log = None

        '''Initialize TCTLM Thread'''
        self.tctlm = TCTLM_SignalWrapper(False, loggers=self.loggers)
        self.tctlm_thread = QThread()
        self.tctlm.moveToThread(self.tctlm_thread)
        self.tctlm_thread.start()

        '''Miscieasfjndlfsdfdsfds'''
        self.cubedock = CubeDock()

        '''Initialise UI'''
        self.ui = pyqt_gui.Ui_MainWindow()
        self.ui.setupUi(self)
        self._setup_my_ui()
        self._setup_tctlm_signals()

        '''Miscieasfjndlfsdfdsfds'''
        self.log_imgdwl_counter = 0
        self.handler = QThread()
        self.dwnlding_img = False
        self.device_not_responding = None
        if not self.ui.check_log_commsenable.isChecked():
            self.loggers['comms'].enabled = False

        '''Start the recurrent timer'''
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.timer_handler)
        self.timer.start(1000)

    def _setup_my_ui(self):
        '''Setup manual button controls (Some are straight connected to signals'''

        self.ui.combo_comm_ports.addItems(self.tctlm.comms.uart.return_list_of_ports())
        self.ui.butt_comm_refresh.clicked.connect(self.butt_fcn_refresh_comm_list)

        self.ui.comboBox_cubeIR_comms.addItems(self.tctlm.comms.uart.return_list_of_ports())
        self.ui.btn_refresh_comms2.clicked.connect(self.butt_fcn_refresh_comm_list())

        self.ui.btn_start.clicked.connect()

    def _setup_tctlm_signals(self):
        '''Sets up all the signals for the TCTLM'''
        self.tctlm.sig_set_progress_bar.connect(self.slot_update_progress_bar)
        self._sig_send_generic_tctlm.connect(self.tctlm.slot_handle_generic_tctlm)
        self.tctlm.sig_return_generic_TCTLM.connect(self.slot_receive_generic_tctlm)
        self._sig_echo.connect(self.tctlm.slot_echo)
        self.tctlm.sig_echo.connect(self.slot_echo)

        self.tctlm.sig_comms_error_not_responding.connect(self.slot_device_not_responding)
        self.tctlm.sig_comms_error_responding.connect(self.slot_device_responding)

    @pyqtSlot(object)
    def slot_receive_generic_tctlm(self, tctlm):
        '''
        Receives a generic TCTLM of type <self.tctlm.Generic_TCTLM_t
        All incoming things gets redirected from here
        '''
        if tctlm.is_tlm():

            # Handle incoming images
            if tctlm.tctlm == self.tctlm.Image_t:
                self.receive_image(tctlm.reply)
                return

            if tctlm.tctlm == self.tctlm.EEPROM_Chunk:
                # Not meant for us
                return

            # Handle incoming telemetries
            self.update_gui_fields(tctlm)

    def update_gui_fields(self, tlm):
        '''Populates the GIU and data to log'''

        def update_graph_data(data_handle, point, max_points=300):
            '''Adds a point to line data'''
            X = np.append(data_handle.get_xdata(), point[0])
            Y = np.append(data_handle.get_ydata(), point[1])
            if X.size > max_points:
                X = np.delete(X, 0)
                Y = np.delete(Y, 0)
            data_handle.set_xdata(X)
            data_handle.set_ydata(Y)

        if tlm.tctlm == self.tctlm.Identification_t:
            self.ui.text_iden_node.setText(str(tlm.reply.NodeType))
            self.ui.text_iden_interface.setText(str(tlm.reply.InterfaceVersion))
            self.ui.text_iden_firm.setText("V%d.%d" % (tlm.reply.FirmwareVersionMajor, tlm.reply.FirmwareVersionMinor))
            hours, remainder = divmod(tlm.reply.RunTimeSeconds, 3600)
            minutes, seconds = divmod(remainder, 60)
            self.ui.text_iden_runtime.setText("%d:%2.d:%2.d.%.3d" % (hours, minutes, seconds, tlm.reply.RunTimeMilliseconds))
            self.data_to_log[self.loggers['data'].key_index("InterfaceVersion")] = str(tlm.reply.InterfaceVersion)
            self.data_to_log[self.loggers['data'].key_index("FirmwareVersion")] = "V%d.%d" % (
                tlm.reply.FirmwareVersionMajor, tlm.reply.FirmwareVersionMinor)

        if tlm.tctlm == self.tctlm.SoftwareStateExtended_t:
            self.ui.text_extiden_htpasn.setText(tlm.reply.HTPASN)
            hours, remainder = divmod(tlm.reply.RunTimeSeconds, 3600)
            minutes, seconds = divmod(remainder, 60)
            self.ui.text_extiden_runtime.setText("%d:%2.d:%2.d" % (hours, minutes, seconds))
            self.ui.text_extiden_firmbuilddate.setText("%s" % tlm.reply.FirmwareBuildDate)
            self.ui.text_swstateex_sram2parityen.setText("True" if tlm.reply.SRAM2ParityCheckEnabled else "False")
            self.data_to_log[self.loggers['data'].key_index("IRCAM SN")] = str(tlm.reply.HTPASN)
            self.data_to_log[self.loggers['data'].key_index("Runtime")] = "%d" % tlm.reply.RunTimeSeconds
            self.data_to_log[self.loggers['data'].key_index("SRAM2ParityCheckEnabled")] = "True" if tlm.reply.SRAM2ParityCheckEnabled else "False"

        if tlm.tctlm == self.tctlm.SoftwareState_t:
            self.ui.text_swstate_bitflips.setText(str(tlm.reply.DetectedBitFlips))
            self.ui.text_swstate_flashfaults.setText(str(tlm.reply.FlashFaults))
            self.ui.text_swstate_lastfaultaddress.setText(str(tlm.reply.FlashLastFaultAddress))
            self.data_to_log[self.loggers['data'].key_index("DetectedBitFlips")] = str(tlm.reply.DetectedBitFlips)
            self.data_to_log[self.loggers['data'].key_index("FlashFaults")] = str(tlm.reply.FlashFaults)
            self.data_to_log[self.loggers['data'].key_index("FlashLastFaultAddress")] = str(tlm.reply.FlashLastFaultAddress)

        if tlm.tctlm == self.tctlm.HTPACalibInfo_t:
            self.ui.text_extiden_DeviceID.setText(str(tlm.reply.DeviceID))
            self.ui.text_extiden_tablenumber.setText(str(tlm.reply.TableNumber))
            self.data_to_log[self.loggers['data'].key_index("HTPADeviceID")] = str(tlm.reply.DeviceID)
            self.data_to_log[self.loggers['data'].key_index("HTPATableNumber")] = str(tlm.reply.TableNumber)

        if tlm.tctlm == self.tctlm.ADSSettings_t:
            self.ui.text_adsstate_samplemode.setText(tlm.reply.SampleMode.name)
            self.data_to_log[self.loggers['data'].key_index("ADSSampleMode")] = tlm.reply.SampleMode.name
            self.ui.text_adsstate_caliblevel.setText(tlm.reply.ImageCalibLevel.name)
            self.data_to_log[self.loggers['data'].key_index("HTPACalibLevel")] = tlm.reply.ImageCalibLevel.name

        if tlm.tctlm == self.tctlm.HILSettings_t:
            self.ui.text_hilsettings_enabled.setText("Enabled" if tlm.reply.Enabled else "Disabled")
            self.ui.text_hilsettings_busy.setText("Busy" if tlm.reply.Busy else "Idle")
            self.ui.text_hilsettings_imgvddcomp.setText("Enabled" if tlm.reply.EnableImgVddComp else "Disabled")
            self.ui.text_hilsettings_tobjcalib.setText("Enabled" if tlm.reply.EnableImgTobjCalib else "Disabled")
            self.ui.text_hilsettings_enableedgedet.setText("Enabled" if tlm.reply.EnableEdgeDet else "Disabled")
            self.ui.text_hilsettings_enablelensdistcorr.setText("Enabled" if tlm.reply.EnableLensDistCorr else "Disabled")
            self.ui.text_hilsettings_enablepolyfit.setText("Enabled" if tlm.reply.EnablePolyFit else "Disabled")
            self.ui.text_hilsettings_enableattest.setText("Enabled" if tlm.reply.EnableAttEst else "Disabled")
            self.data_to_log[self.loggers['data'].key_index("HILEnabled")] = tlm.reply.Enabled
            self.data_to_log[self.loggers['data'].key_index("HILBusy")] = tlm.reply.Busy
            self.data_to_log[self.loggers['data'].key_index("HILEnableImgVddComp")] = tlm.reply.EnableImgVddComp
            self.data_to_log[self.loggers['data'].key_index("HILEnableImgTobjCalib")] = tlm.reply.EnableImgTobjCalib
            self.data_to_log[self.loggers['data'].key_index("HILEnableEdgeDet")] = tlm.reply.EnableEdgeDet
            self.data_to_log[self.loggers['data'].key_index("HILEnableLensDistCorr")] = tlm.reply.EnableLensDistCorr
            self.data_to_log[self.loggers['data'].key_index("HILEnablePolyFit")] = tlm.reply.EnablePolyFit
            self.data_to_log[self.loggers['data'].key_index("HILEnableAttEst")] = tlm.reply.EnableAttEst

        # HTPA State
        if tlm.tctlm == self.tctlm.HTPAState_t:
            self.ui.text_htpastate_ptat.setText(str(tlm.reply.PTAT))
            self.ui.text_htpastate_vdd.setText(str(tlm.reply.VddDigit))
            self.ui.text_htpastate_pwr.setText(tlm.reply.PowerState.name)
            self.ui.text_htpastate_wake.setText(tlm.reply.WakeState.name)
            self.ui.text_htpastate_capture.setText(tlm.reply.CaptureState.name)
            self.data_to_log[self.loggers['data'].key_index("HtpaPTAT")] = str(tlm.reply.PTAT)
            self.data_to_log[self.loggers['data'].key_index("HtpaVdd")] = str(tlm.reply.VddDigit)
            self.data_to_log[self.loggers['data'].key_index("HtpaPowerState")] = tlm.reply.PowerState.name
            self.data_to_log[self.loggers['data'].key_index("HtpaWakeState")] = tlm.reply.WakeState.name
            self.data_to_log[self.loggers['data'].key_index("HtpaCaptureState")] = tlm.reply.CaptureState.name

        # HTPA Image Info
        if tlm.tctlm == self.tctlm.HTPAImageInfo_t:
            self.ui.text_htpaimg_lastmin.setText("%d" % tlm.reply.ImageLastMin)
            self.ui.text_htpaimg_lastmean.setText("%d" % tlm.reply.ImageLastMean)
            self.ui.text_htpaimg_lastmax.setText("%d" % tlm.reply.ImageLastMax)
            self.ui.text_htpaimg_allmin.setText("%d" % tlm.reply.ImageAllMin)
            self.ui.text_htpaimg_allmax.setText("%d" % tlm.reply.ImageAllMax)
            self.data_to_log[self.loggers['data'].key_index("HTPAImageLastMin")] = str(tlm.reply.ImageLastMin)
            self.data_to_log[self.loggers['data'].key_index("HTPAImageLastMean")] = str(tlm.reply.ImageLastMean)
            self.data_to_log[self.loggers['data'].key_index("HTPAImageLastMax")] = str(tlm.reply.ImageLastMax)
            self.data_to_log[self.loggers['data'].key_index("HTPAImageAllTimeMin")] = str(tlm.reply.ImageAllMin)
            self.data_to_log[self.loggers['data'].key_index("HTPAImageAllTimeMax")] = str(tlm.reply.ImageAllMax)

        # Config State
        if tlm.tctlm == self.tctlm.ConfigState_t:
            self.ui.text_config_configurationnotsaved.setText("%d" % tlm.reply.configuration_not_saved)
            self.ui.text_config_dataloss.setText("%d" % tlm.reply.data_loss)
            self.ui.text_config_externalmemoryslotoverwritten.setText("%d" % tlm.reply.ext_memory_slot_overwritten)
            self.ui.text_config_noconfigfoundinmemory.setText("%d" % tlm.reply.no_config_found_in_memory)
            self.ui.text_config_numofvalidframslots.setText("%d" % tlm.reply.num_of_valid_fram_slots)
            self.ui.text_config_numofvalidflashslots.setText("%d" % tlm.reply.num_of_valid_flash_slots)
            self.ui.text_config_rewrittenfrommemory.setText("%d" % tlm.reply.rewritten_from_memory)
            self.data_to_log[self.loggers['data'].key_index("ConfigRewrittenFromMemory")] = str(tlm.reply.rewritten_from_memory)
            self.data_to_log[self.loggers['data'].key_index("ConfigExtMemorySlotOverwritten")] = str(tlm.reply.ext_memory_slot_overwritten)
            self.data_to_log[self.loggers['data'].key_index("ConfigNoConfigFoundInMemory")] = str(tlm.reply.no_config_found_in_memory)
            self.data_to_log[self.loggers['data'].key_index("ConfigNumOfValidFramSlots")] = str(tlm.reply.num_of_valid_fram_slots)
            self.data_to_log[self.loggers['data'].key_index("ConfigNumOfValidFlashSlots")] = str(tlm.reply.num_of_valid_flash_slots)
            self.data_to_log[self.loggers['data'].key_index("ConfigNotSaved")] = str(tlm.reply.configuration_not_saved)
            self.data_to_log[self.loggers['data'].key_index("ConfigDataLoss")] = str(tlm.reply.data_loss)

        # Hardware State
        if tlm.tctlm == self.tctlm.HardwareState_t:
            self.ui.text_hw_curr3v3.setText("%.3f" % tlm.reply.current3V3)
            self.ui.text_hw_currhtpa.setText("%.3f" % tlm.reply.currentHTPA)
            self.ui.text_hw_tempmcu.setText("%.3f" % tlm.reply.temperatureMCU)
            self.ui.text_hw_temphtpa.setText("%.3f" % tlm.reply.temperatureHTPA)
            self.ui.text_hw_voltmcu.setText("%.3f" % tlm.reply.voltageMCU)
            self.ui.text_hw_volthtpa.setText("%.3f" % tlm.reply.voltageHTPA)
            self.data_to_log[self.loggers['data'].key_index("Current3V3")] = str(tlm.reply.current3V3)
            self.data_to_log[self.loggers['data'].key_index("CurrentHTPA")] = str(tlm.reply.currentHTPA)
            self.data_to_log[self.loggers['data'].key_index("TemperatureMCU")] = str(tlm.reply.temperatureMCU)
            self.data_to_log[self.loggers['data'].key_index("TemperatureHTPA")] = str(tlm.reply.temperatureHTPA)
            self.data_to_log[self.loggers['data'].key_index("VoltageMCU")] = str(tlm.reply.voltageMCU)
            self.data_to_log[self.loggers['data'].key_index("VoltageHTPA")] = str(tlm.reply.voltageHTPA)

            mpl_now = mpl.dates.date2num(datetime.now())
            update_graph_data(self.graph_currents_data_3V3, [mpl_now, tlm.reply.current3V3])
            update_graph_data(self.graph_currents_data_htpa, [mpl_now, tlm.reply.currentHTPA])
            self.ui.graph_currents.canvas.ax.relim()
            self.ui.graph_currents.canvas.ax2.relim()
            self.ui.graph_currents.canvas.ax.autoscale_view(True, True, False)
            self.ui.graph_currents.canvas.ax2.autoscale_view(True, True, False)
            d = (self.graph_currents_data_3V3.get_ydata().max() - self.graph_currents_data_3V3.get_ydata().min())
            if d == 0: d = 1  # Do avoid annoying warnings
            m = d * 0.1
            self.ui.graph_currents.canvas.ax.set_ylim(self.graph_currents_data_3V3.get_ydata().min() - m - d * .5,
                                                      self.graph_currents_data_3V3.get_ydata().max() + m)
            m = (self.graph_currents_data_htpa.get_ydata().max() - self.graph_currents_data_htpa.get_ydata().min()) * 0.1
            self.ui.graph_currents.canvas.ax2.set_ylim(self.graph_currents_data_htpa.get_ydata().min() - m,
                                                       self.graph_currents_data_htpa.get_ydata().max() + m + d * .5)
            self.ui.graph_currents.canvas.draw()
            update_graph_data(self.graph_temps_data_mcu, [mpl_now, tlm.reply.temperatureMCU])
            update_graph_data(self.graph_temps_data_htpa, [mpl_now, tlm.reply.temperatureHTPA])
            self.ui.graph_temps.canvas.ax.relim()
            self.ui.graph_temps.canvas.ax.autoscale_view(True, True, True)
            self.ui.graph_temps.canvas.draw()

        # Hardware State Max/Min
        if tlm.tctlm == self.tctlm.HardwareStateMinMax_t:
            self.ui.text_hwmm_curr3v3min.setText("%.3f" % tlm.reply.current3V3Min)
            self.ui.text_hwmm_curr3v3max.setText("%.3f" % tlm.reply.current3V3Max)
            self.ui.text_hwmm_currhtpamin.setText("%.3f" % tlm.reply.currentHTPAMin)
            self.ui.text_hwmm_currhtpamax.setText("%.3f" % tlm.reply.currentHTPAMax)
            self.ui.text_hwmm_tempmcumin.setText("%.3f" % tlm.reply.temperatureMCUMin)
            self.ui.text_hwmm_tempmcumax.setText("%.3f" % tlm.reply.temperatureMCUMax)
            self.ui.text_hwmm_temphtpamin.setText("%.3f" % tlm.reply.temperatureHTPAMin)
            self.ui.text_hwmm_temphtpamax.setText("%.3f" % tlm.reply.temperatureHTPAMax)
            self.ui.text_hwmm_voltmcumin.setText("%.3f" % tlm.reply.voltageMCUMin)
            self.ui.text_hwmm_voltmcumax.setText("%.3f" % tlm.reply.voltageMCUMax)
            self.ui.text_hwmm_volthtpamin.setText("%.3f" % tlm.reply.voltageHTPAMin)
            self.ui.text_hwmm_volthtpamax.setText("%.3f" % tlm.reply.voltageHTPAMax)
            self.data_to_log[self.loggers['data'].key_index("Current3V3Min")] = str(tlm.reply.current3V3Min)
            self.data_to_log[self.loggers['data'].key_index("Current3V3Max")] = str(tlm.reply.current3V3Max)
            self.data_to_log[self.loggers['data'].key_index("CurrentHTPAMin")] = str(tlm.reply.currentHTPAMin)
            self.data_to_log[self.loggers['data'].key_index("CurrentHTPAMax")] = str(tlm.reply.currentHTPAMax)
            self.data_to_log[self.loggers['data'].key_index("TemperatureMCUMin")] = str(tlm.reply.temperatureMCUMin)
            self.data_to_log[self.loggers['data'].key_index("TemperatureMCUMax")] = str(tlm.reply.temperatureMCUMax)
            self.data_to_log[self.loggers['data'].key_index("TemperatureHTPAMin")] = str(tlm.reply.temperatureHTPAMin)
            self.data_to_log[self.loggers['data'].key_index("TemperatureHTPAMax")] = str(tlm.reply.temperatureHTPAMax)
            self.data_to_log[self.loggers['data'].key_index("VoltageMCUMin")] = str(tlm.reply.voltageMCUMin)
            self.data_to_log[self.loggers['data'].key_index("VoltageMCUMax")] = str(tlm.reply.voltageMCUMax)
            self.data_to_log[self.loggers['data'].key_index("VoltageHTPAMin")] = str(tlm.reply.voltageHTPAMin)
            self.data_to_log[self.loggers['data'].key_index("VoltageHTPAMax")] = str(tlm.reply.voltageHTPAMax)

        # Software State
        if tlm.tctlm == self.tctlm.SoftwareTimings_t:
            self.ui.text_swtimings_startuptime.setText("%d" % tlm.reply.StartupTime)
            self.ui.text_swtimings_processingtime.setText("%d" % tlm.reply.ProcessingTime)
            self.ui.text_swtimings_maxprocessingtime.setText("%d" % tlm.reply.ProcessingTimeMax)
            self.ui.text_swtimings_looptime.setText("%d" % tlm.reply.LoopTime)
            self.ui.text_swtimings_looptimemax.setText("%d" % tlm.reply.LoopTimeMax)
            self.ui.text_swtimings_imgcapttime.setText("%d" % tlm.reply.ImgCaptureTime)
            self.ui.text_swtimings_lensdistcorrtime.setText("%d" % tlm.reply.LensDistTime)
            self.ui.text_swtimings_estimationtime.setText("%d" % tlm.reply.EstimationTime)
            self.ui.text_swtimings_configrefreshmax.setText("%d" % tlm.reply.ConfigRefreshMax)
            self.ui.text_swtimings_configcheckmemorymax.setText("%d" % tlm.reply.ConfigCheckMemoryMax)
            self.ui.text_swtimings_adcmeasure.setText("%d" % tlm.reply.AdcMeasurement)
            self.ui.text_swtimings_htpaintegration.setText("%d" % tlm.reply.HtpaIntegration)
            self.data_to_log[self.loggers['data'].key_index("SwStartupTime")] = str(tlm.reply.StartupTime)
            self.data_to_log[self.loggers['data'].key_index("SwProcessingTime")] = str(tlm.reply.ProcessingTime)
            self.data_to_log[self.loggers['data'].key_index("SwProcessingTimeMax")] = str(tlm.reply.ProcessingTimeMax)
            self.data_to_log[self.loggers['data'].key_index("SwLoopTime")] = str(tlm.reply.LoopTime)
            self.data_to_log[self.loggers['data'].key_index("SwLoopTimeMax")] = str(tlm.reply.LoopTimeMax)
            self.data_to_log[self.loggers['data'].key_index("SwImgCaptureTime")] = str(tlm.reply.ImgCaptureTime)
            self.data_to_log[self.loggers['data'].key_index("SwLensDistCorrTime")] = str(tlm.reply.LensDistTime)
            self.data_to_log[self.loggers['data'].key_index("SwEstimationTime")] = str(tlm.reply.EstimationTime)
            self.data_to_log[self.loggers['data'].key_index("SwConfigRefreshMaxTime")] = str(tlm.reply.ConfigRefreshMax)
            self.data_to_log[self.loggers['data'].key_index("SwConfigCheckMemoryMaxTime")] = str(tlm.reply.ConfigCheckMemoryMax)
            self.data_to_log[self.loggers['data'].key_index("SwAdcMeasurementTime")] = str(tlm.reply.AdcMeasurement)
            self.data_to_log[self.loggers['data'].key_index("SwHtpaIntegrationTime")] = str(tlm.reply.HtpaIntegration)

        # Memory Test SRAM
        if tlm.tctlm == self.tctlm.MemTestFlash_t:
            self.ui.text_memtest_flash_mode.setText("%s" % tlm.reply.Mode.name)
            self.ui.text_memtest_flash_state.setText("%s" % tlm.reply.State.name)
            self.ui.text_memtest_flash_index.setText("%s" % tlm.reply.Index)
            self.ui.text_memtest_flash_faultpageindex.setText("%s" % tlm.reply.FaultLastIndex)
            self.ui.text_memtest_flash_pagewriteerrors.setText("%s" % tlm.reply.PageWriteErrors)
            self.ui.text_memtest_flash_pageeraseerrors.setText("%s" % tlm.reply.PageEraseErrors)
            self.ui.text_memtest_flash_pagewritetime.setText("%s" % tlm.reply.PageWriteTime)
            self.ui.text_memtest_flash_pageerasetime.setText("%s" % tlm.reply.PageEraseTime)
            self.ui.text_memtest_flash_timetowait.setText("%s" % tlm.reply.TimeToWait)
            self.data_to_log[self.loggers['data'].key_index("MemTestFlashMode")] = "%s" % tlm.reply.Mode.name
            self.data_to_log[self.loggers['data'].key_index("MemTestFlashState")] = "%s" % tlm.reply.State.name
            self.data_to_log[self.loggers['data'].key_index("MemTestFlashIndex")] = "%d" % tlm.reply.Index
            self.data_to_log[self.loggers['data'].key_index("MemTestFlashFaultPageIndex")] = "%d" % tlm.reply.FaultLastIndex
            self.data_to_log[self.loggers['data'].key_index("MemTestFlashPageWriteErrors")] = "%d" % tlm.reply.PageWriteErrors
            self.data_to_log[self.loggers['data'].key_index("MemTestFlashEraseErrors")] = "%d" % tlm.reply.PageEraseErrors
            self.data_to_log[self.loggers['data'].key_index("MemTestFlashPageWriteTime")] = "%d" % tlm.reply.PageWriteTime
            self.data_to_log[self.loggers['data'].key_index("MemTestFlashPageReadTime")] = "%d" % tlm.reply.PageEraseTime
            self.data_to_log[self.loggers['data'].key_index("MemTestFramTimeToWait")] = "%d" % tlm.reply.TimeToWait

        # Memory Test Flash
        if tlm.tctlm == self.tctlm.MemTestSram_t:
            self.ui.text_memtest_sram_mode.setText("%s" % tlm.reply.Mode.name)
            self.ui.text_memtest_sram_faultslastloop.setText("%s" % tlm.reply.FaultsLastLoop)
            self.ui.text_memtest_sram_faultstotal.setText("%s" % tlm.reply.FaultsTotal)
            self.ui.text_memtest_sram_iteratetime.setText("%s" % tlm.reply.IterateTime)
            self.ui.text_memtest_sram_loopnumber.setText("%s" % tlm.reply.LoopNumber)
            self.data_to_log[self.loggers['data'].key_index("MemTestSramMode")] = "%s" % tlm.reply.Mode.name
            self.data_to_log[self.loggers['data'].key_index("MemTestSramFaultsLastLoop")] = "%d" % tlm.reply.FaultsLastLoop
            self.data_to_log[self.loggers['data'].key_index("MemTestSramFaultsTotal")] = "%d" % tlm.reply.FaultsTotal
            self.data_to_log[self.loggers['data'].key_index("MemTestSramLoopNumber")] = "%d" % tlm.reply.LoopNumber
            self.data_to_log[self.loggers['data'].key_index("MemTestSramIterateTime")] = "%d" % tlm.reply.IterateTime

        if tlm.tctlm == self.tctlm.TelecommandAcknowledge_t:
            self.ui.text_tcack_lastid.setText("%d" % tlm.reply.lastTCID)
            self.ui.check_tcack_processed.setChecked(tlm.reply.processed)
            self.ui.text_tcack_errorstatus.setText("%s" % tlm.reply.errorStatus.name)
            self.ui.text_tcack_paramerrindex.setText("%d" % tlm.reply.parameterErrorIndex)
            self.data_to_log[self.loggers['data'].key_index("TcAckLastTCID")] = "%d" % tlm.reply.lastTCID
            self.data_to_log[self.loggers['data'].key_index("TcAckProcessed")] = str(tlm.reply.processed)
            self.data_to_log[self.loggers['data'].key_index("TcAckErrStatus")] = "%s" % tlm.reply.errorStatus.name
            self.data_to_log[self.loggers['data'].key_index("TcAckParamErrIndex")] = "%d" % tlm.reply.parameterErrorIndex

        # Error flags
        if tlm.tctlm == self.tctlm.ErrorFlags_t:
            self.ui.check_dev_flashcorruption.setChecked(tlm.reply.DeviceErrorFlags.FlashCorruption)
            self.ui.check_dev_flashsinglebitflipdetected.setChecked(tlm.reply.DeviceErrorFlags.FlashBitFlipDetected)
            self.ui.check_dev_sram2parityerror.setChecked(tlm.reply.DeviceErrorFlags.SRAM2ParityError)
            self.ui.check_dev_3v3overcurr.setChecked(tlm.reply.DeviceErrorFlags.OverCurrent3V3)
            self.ui.check_dev_htpaovercurr.setChecked(tlm.reply.DeviceErrorFlags.OverCurrentHTPA)
            self.ui.check_dev_mcuovertemp.setChecked(tlm.reply.DeviceErrorFlags.OverTemperatureMCU)
            self.ui.check_dev_mcuundertemp.setChecked(tlm.reply.DeviceErrorFlags.UnderTemperatureMCU)
            self.ui.check_dev_htpaovertemp.setChecked(tlm.reply.DeviceErrorFlags.OverTemperatureHTPA)
            self.ui.check_dev_htpaundertemp.setChecked(tlm.reply.DeviceErrorFlags.UnderTemperatureHTPA)
            self.ui.check_dev_tim1soverrun.setChecked(tlm.reply.DeviceErrorFlags.Timer1sOverRun)
            self.ui.check_error_device.setChecked(True in tlm.reply.DeviceErrorFlags)
            self.ui.check_htpa_notresp.setChecked(tlm.reply.HTPAErrorFlags.NotResponding)
            self.ui.check_htpa_conmis.setChecked(tlm.reply.HTPAErrorFlags.ConfigMismatch)
            self.ui.check_htpa_lookupoutrange.setChecked(tlm.reply.HTPAErrorFlags.CalibLookupOutOfRange)
            self.ui.check_htpa_spitimeout.setChecked(tlm.reply.HTPAErrorFlags.SpiTimeout)
            self.ui.check_htpa_integrationtimeout.setChecked(tlm.reply.HTPAErrorFlags.IntegrationTimeError)
            self.ui.check_error_htpa.setChecked(True in tlm.reply.HTPAErrorFlags)
            self.ui.check_comms_tcparbufffull.setChecked(tlm.reply.CommsErrorFlags.TcParamBuffFull)
            self.ui.check_comms_tcslotsbufffull.setChecked(tlm.reply.CommsErrorFlags.TcSlotsBuffFull)
            self.ui.check_comms_uartmsgincom.setChecked(tlm.reply.CommsErrorFlags.UartMsgIncomplete)
            self.ui.check_comms_uartproterr.setChecked(tlm.reply.CommsErrorFlags.UartProtError)
            self.ui.check_comms_uarttcparamovr.setChecked(tlm.reply.CommsErrorFlags.UartTcParamOverflow)
            self.ui.check_comms_tcinvaldparlen.setChecked(tlm.reply.CommsErrorFlags.CommsTcInvalidParLen)
            self.ui.check_comms_tcinvalidpar.setChecked(tlm.reply.CommsErrorFlags.CommsTcInvalidPar)
            self.ui.check_comms_tcinvalidid.setChecked(tlm.reply.CommsErrorFlags.CommsTcInvalidID)
            self.ui.check_comms_tlmimvalidid.setChecked(tlm.reply.CommsErrorFlags.CommsTlmInvalidID)
            self.ui.check_comms_tctimeout.setChecked(tlm.reply.CommsErrorFlags.CommsTcTimeout)
            self.ui.check_comms_tcparamcrc.setChecked(tlm.reply.CommsErrorFlags.CommsTcCrcError)
            self.ui.check_error_comms.setChecked(True in tlm.reply.CommsErrorFlags)
            self.ui.check_uart_rxbuffovr.setChecked(tlm.reply.UartErrorFlags.RxByteOverrun)
            self.ui.check_uart_rxbyteframe.setChecked(tlm.reply.UartErrorFlags.RxFramingError)
            self.ui.check_uart_rxbyteovr.setChecked(tlm.reply.UartErrorFlags.RxByteOverrun)
            self.ui.check_uart_txreadempty.setChecked(tlm.reply.UartErrorFlags.TxReadingEmptyBuffer)
            self.ui.check_uart_txtimeout.setChecked(tlm.reply.UartErrorFlags.TxTimeout)
            self.ui.check_error_uart.setChecked(True in tlm.reply.UartErrorFlags)
            self.ui.check_fram_devicenotfound.setChecked(tlm.reply.FramErrorFlags.DeviceNotFound)
            self.ui.check_fram_statusnotresponding.setChecked(tlm.reply.FramErrorFlags.StatusNotResponding)
            self.ui.check_fram_spiteimout.setChecked(tlm.reply.FramErrorFlags.SpiTimeout)
            self.ui.check_error_fram.setChecked(True in tlm.reply.FramErrorFlags)
            self.ui.check_config_extmemslotoverwritten.setChecked(tlm.reply.ConfigErrorFlags.ExtMemorySlotOverwritten)
            self.ui.check_config_noconfigfoundinmem.setChecked(tlm.reply.ConfigErrorFlags.NoConfigFoundInMemory)
            self.ui.check_config_novalidslotsinmem.setChecked(tlm.reply.ConfigErrorFlags.NoValidSlotsInMemory)
            self.ui.check_config_rewrittenfrommem.setChecked(tlm.reply.ConfigErrorFlags.RewrittenFromMemory)
            self.ui.check_error_config.setChecked(True in tlm.reply.ConfigErrorFlags)
            self.data_to_log[self.loggers['data'].key_index("SRAM2ParityError")] = str(tlm.reply.DeviceErrorFlags.SRAM2ParityError)
            self.data_to_log[self.loggers['data'].key_index("FlashCorruption")] = str(tlm.reply.DeviceErrorFlags.FlashCorruption)
            self.data_to_log[self.loggers['data'].key_index("3V3OverCurrent")] = str(tlm.reply.DeviceErrorFlags.OverCurrent3V3)
            self.data_to_log[self.loggers['data'].key_index("HTPAOverCurrent")] = str(tlm.reply.DeviceErrorFlags.OverCurrentHTPA)
            self.data_to_log[self.loggers['data'].key_index("MCUOverTemperature")] = str(tlm.reply.DeviceErrorFlags.OverTemperatureMCU)
            self.data_to_log[self.loggers['data'].key_index("MCUUnderTemperature")] = str(tlm.reply.DeviceErrorFlags.UnderTemperatureMCU)
            self.data_to_log[self.loggers['data'].key_index("HTPAOverTemperature")] = str(tlm.reply.DeviceErrorFlags.OverTemperatureHTPA)
            self.data_to_log[self.loggers['data'].key_index("HTPAUnderTemperature")] = str(
                tlm.reply.DeviceErrorFlags.UnderTemperatureHTPA)
            self.data_to_log[self.loggers['data'].key_index("Timer1sOverRun")] = str(tlm.reply.DeviceErrorFlags.Timer1sOverRun)
            self.data_to_log[self.loggers['data'].key_index("HTPANotResponding")] = str(tlm.reply.HTPAErrorFlags.NotResponding)
            self.data_to_log[self.loggers['data'].key_index("HTPAConfigMismatch")] = str(tlm.reply.HTPAErrorFlags.ConfigMismatch)
            self.data_to_log[self.loggers['data'].key_index("HTPAIntegrationTimeError")] = str(tlm.reply.HTPAErrorFlags.IntegrationTimeError)
            self.data_to_log[self.loggers['data'].key_index("HTPACalibLookupOutOfRange")] = str(
                tlm.reply.HTPAErrorFlags.CalibLookupOutOfRange)
            self.data_to_log[self.loggers['data'].key_index("HTPASpiTimeout")] = str(tlm.reply.HTPAErrorFlags.SpiTimeout)
            self.data_to_log[self.loggers['data'].key_index("CommsUartTcParamOverflow")] = str(
                tlm.reply.CommsErrorFlags.UartTcParamOverflow)
            self.data_to_log[self.loggers['data'].key_index("CommsUartProtError")] = str(tlm.reply.CommsErrorFlags.UartProtError)
            self.data_to_log[self.loggers['data'].key_index("CommsUartMsgIncomplete")] = str(tlm.reply.CommsErrorFlags.UartMsgIncomplete)
            self.data_to_log[self.loggers['data'].key_index("CommsTcSlotsBuffFull")] = str(tlm.reply.CommsErrorFlags.TcSlotsBuffFull)
            self.data_to_log[self.loggers['data'].key_index("CommsTcParamBuffFull")] = str(tlm.reply.CommsErrorFlags.TcParamBuffFull)
            self.data_to_log[self.loggers['data'].key_index("CommsTlmInvalidID")] = str(tlm.reply.CommsErrorFlags.CommsTlmInvalidID)
            self.data_to_log[self.loggers['data'].key_index("CommsTcInvalidID")] = str(tlm.reply.CommsErrorFlags.CommsTcInvalidID)
            self.data_to_log[self.loggers['data'].key_index("CommsTcInvalidPar")] = str(tlm.reply.CommsErrorFlags.CommsTcInvalidPar)
            self.data_to_log[self.loggers['data'].key_index("CommsTcInvalidParLen")] = str(
                tlm.reply.CommsErrorFlags.CommsTcInvalidParLen)
            self.data_to_log[self.loggers['data'].key_index("CommsTcTimeout")] = str(tlm.reply.CommsErrorFlags.CommsTcTimeout)
            self.data_to_log[self.loggers['data'].key_index("CommsTcCrcError")] = str(tlm.reply.CommsErrorFlags.CommsTcCrcError)
            self.data_to_log[self.loggers['data'].key_index("UARTRxFramingError")] = str(tlm.reply.UartErrorFlags.RxFramingError)
            self.data_to_log[self.loggers['data'].key_index("UARTTxReadingEmptyBuffer")] = str(
                tlm.reply.UartErrorFlags.TxReadingEmptyBuffer)
            self.data_to_log[self.loggers['data'].key_index("UARTRxByteOverrun")] = str(tlm.reply.UartErrorFlags.RxByteOverrun)
            self.data_to_log[self.loggers['data'].key_index("UARTTxOverflow")] = str(tlm.reply.UartErrorFlags.TxOverflow)
            self.data_to_log[self.loggers['data'].key_index("UartTxTimeout")] = str(tlm.reply.UartErrorFlags.TxTimeout)
            self.data_to_log[self.loggers['data'].key_index("FRAMStatusNotResponding")] = str(tlm.reply.FramErrorFlags.StatusNotResponding)
            self.data_to_log[self.loggers['data'].key_index("FRAMDeviceNotFound")] = str(tlm.reply.FramErrorFlags.DeviceNotFound)
            self.data_to_log[self.loggers['data'].key_index("FRAMSpiTimeout")] = str(tlm.reply.FramErrorFlags.SpiTimeout)

        if tlm.tctlm == self.tctlm.ConfigDeviceName_t:
            self.ui.text_config_devicename_sn.setText(tlm.reply.SerialNumber)
            self.ui.text_config_devicename_i2caddress.setText(str(hex(tlm.reply.I2cAdress)))

        if tlm.tctlm == self.tctlm.ConfigDevice_t:
            self.ui.text_config_device_overcurrent.setText(str(tlm.reply.ResetOnOvercurrent))
            self.ui.check_config_device_enableiwdg.setChecked(tlm.reply.EnableIWDG)
            self.ui.check_config_device_enableewdg.setChecked(tlm.reply.EnableEWDG)
            self.ui.check_config_device_enableresetonbitflip.setChecked(tlm.reply.ResetOnBitFlip)


        # # Get some data from the CubeDock if we are connected to it
        # if self.cubedock.uart.is_open() and len(self.cubedock.tlms) > 0:
        #     self.ui.check_cubedock_pwr3v3_1state.setChecked(self.cubedock.tlms['Pwr_3V3_1'])
        #     self.ui.text_cubedock_pwr3v3_1curr.setText("%.2fmA" % self.cubedock.tlms['Curr_3V3_1'])
        #     data_to_log[self.loggers['data'].key_index("CurrentCubeDock3V3_1")] = str(self.cubedock.tlms['Curr_3V3_1'])
        #     # If want to see afterwards when switch was on/off, go see the event log



    def update_all_fields(self):
        '''Initiated update on GUI and logging system'''
        self.data_to_log = ['-' for i in range(len(self.loggers['data'].keys))]
        if not self.tctlm.is_open():
            return

        self._sig_send_generic_tctlm.emit([
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.Identification_t,
                tlm_fun=self.tctlm.get_identification),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.HardwareState_t,
                tlm_fun=self.tctlm.get_hardware_state),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.HardwareStateMinMax_t,
                tlm_fun=self.tctlm.get_hardware_state_minmax),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.ErrorFlags_t,
                tlm_fun=self.tctlm.get_error_flags),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.SoftwareState_t,
                tlm_fun=self.tctlm.get_software_state),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.SoftwareStateExtended_t,
                tlm_fun=self.tctlm.get_software_state_extended),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.SoftwareTimings_t,
                tlm_fun=self.tctlm.get_software_timings),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.HTPACalibInfo_t,
                tlm_fun=self.tctlm.get_HTPA_Config_Info),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.ADSSettings_t,
                tlm_fun=self.tctlm.get_ADS_Settings),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.HTPAState_t,
                tlm_fun=self.tctlm.get_HTPA_state),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.HTPAImageInfo_t,
                tlm_fun=self.tctlm.get_HTPA_Image_Info),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.HILSettings_t,
                tlm_fun=self.tctlm.get_HIL_settings),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.MemTestFlash_t,
                tlm_fun=self.tctlm.get_memtest_flash),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.MemTestSram_t,
                tlm_fun=self.tctlm.get_memtest_sram),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.ConfigState_t,
                tlm_fun=self.tctlm.get_config_state),
            self.tctlm.Generic_TCTLM_t(
                tctlm=self.tctlm.TelecommandAcknowledge_t,
                tlm_fun=self.tctlm.get_telecommand_acknowledge),
        ])

        # Ping the echo, to know when all telemetry requests are executed
        self._sig_echo.emit(time.time())

    @pyqtSlot(object)
    def slot_echo(self, time_sent):
        '''This function is currently only used for knowing when all telemetries are done'''
        if self.loggers: self.loggers['data'].log_data(self.data_to_log)
        self.ui.label_tlm_time.setText("Update all fields duration: %d ms" % int((time.time()-time_sent)*1000.0))

    def butt_fcn_start(self):
        

    def butt_fcn_config_check_memory(self):
        '''Tells device to check it's internal memory'''
        if self.tctlm.is_open():
            if self.loggers: self.loggers['event'].log_text(
                "Commanded device to check internal configuration memory",
                module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.config_check_memory,
                )])

    def butt_fcn_config_clear(self):
        if self.tctlm.is_open():
            if self.loggers: self.loggers['event'].log_text(
                "Commanded device to clear all stored configuration slots",
                module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.config_clear,
                )])

    def butt_fcn_config_save(self):
        if self.tctlm.is_open():
            if self.loggers: self.loggers['event'].log_text(
                "Commanded device to save the current configuration stored in SRAM into FRAM/Flash",
                module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.config_save,
                )])

    def butt_fcn_config_load(self):
        if self.tctlm.is_open():
            if self.loggers: self.loggers['event'].log_text(
                "Commanded device to load the configuration saved in Flash/FRAM",
                module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.config_load,
                )])

    def butt_fcn_config_loaddefault(self):
        if self.tctlm.is_open():
            if self.loggers: self.loggers['event'].log_text(
                "Commanded device to load the default configuration",
                module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.config_load_default,
                )])

    def butt_fcn_config_get_all(self):
        '''Requests the entire configuration'''
        self.butt_fcn_getconfig_devicename()
        self.butt_fcn_getconfig_device()

    def butt_fcn_config_set_all(self):
        '''Sets the entire configuration'''
        self.butt_fcn_setconfig_devicename()
        self.butt_fcn_setconfig_device()

    def butt_fcn_setconfig_devicename(self):
        '''Sets the internal configuration device name'''
        if self.tctlm.is_open():
            sn = self.ui.text_config_devicename_sn.text()
            i2c = int(self.ui.text_config_devicename_i2caddress.text(), 16)
            if self.loggers: self.loggers['event'].log_text(
                'Setting Configuration name to SN: "%s" and I2C Address to %s.' % (sn, str(hex(i2c))),
                module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.config_set_devicename,
                    params=[sn, i2c]
                )])

    def butt_fcn_getconfig_devicename(self):
        '''Requests the device name configuration'''
        if self.tctlm.is_open():
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tctlm=self.tctlm.ConfigDeviceName_t,
                    tlm_fun=self.tctlm.get_config_devicename)
            ])

    def butt_fcn_setconfig_device(self):
        '''Sets the internal general device configuration'''
        if self.tctlm.is_open():
            oc = int(self.ui.text_config_device_overcurrent.text())
            iwdg = self.ui.check_config_device_enableiwdg.isChecked()
            ewdg = self.ui.check_config_device_enableewdg.isChecked()
            bitflip = self.ui.check_config_device_enableresetonbitflip.isChecked()

            if self.loggers: self.loggers['event'].log_text(
                'Setting General Configuration to: [%d, %r, %r, %r]' % (oc, iwdg, ewdg, bitflip),
                module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.config_set_device,
                    params=[oc, iwdg, ewdg, bitflip]
                )])

    def butt_fcn_getconfig_device(self):
        '''Requests the device configuration'''
        if self.tctlm.is_open():
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tctlm=self.tctlm.ConfigDevice_t,
                    tlm_fun=self.tctlm.get_config_device)
            ])

    def butt_fcn_setconfig_memtest(self):
        '''Sets the internal general device configuration'''
        if self.tctlm.is_open():
            sram_mode = self.tctlm.MemTestMode_e(self.ui.combo_config_memtest_sram.currentIndex()+1)
            flash_mode = self.tctlm.MemTestMode_e(self.ui.combo_config_memtest_flash.currentIndex()+1)
            fram_mode = self.tctlm.MemTestMode_e(self.ui.combo_config_memtest_fram.currentIndex()+1)

            if self.loggers: self.loggers['event'].log_text(
                'Setting MemTest Configuration to: [SRAM: %s, Flash: %s, FRAM: %s]' % (sram_mode.name, flash_mode.name, fram_mode.name),
                module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.config_set_memtest_sram,
                    params=[sram_mode]
                ),
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.config_set_memtest_flash,
                    params=[flash_mode]
                ),
            ])

    def butt_retreive_image(self):
        ''' Starts a image download'''
        if self.tctlm.is_open():
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tctlm=self.tctlm.Image_t,
                    tlm_fun=self.tctlm.download_image,
                    params=[self.tctlm.sig_set_progress_bar, True]
                )])

    def butt_fcn_getptatvdd(self):
        '''Filles the PTAT/Vdd Get/Set boxes'''
        self.ui.text_htpa_ptatgetset.setText(self.ui.text_htpastate_ptat.text())
        self.ui.text_htpa_vddgetset.setText(self.ui.text_htpastate_vdd.text())

    def butt_fcn_setptatvdd(self):
        '''Sets the PTAT/Vdd from the values in the text boxes'''
        if self.tctlm.is_open():
            ptat = int(self.ui.text_htpa_ptatgetset.text())
            vdd = int(self.ui.text_htpa_vddgetset.text())
            if self.loggers: self.loggers['event'].log_text(
                'Setting HTPA internal PTAT to %d and VddDigits to %d.' % (ptat, vdd),
                module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.set_HTPA_state,
                    params=[ptat, vdd, self.tctlm.PowerState_e(3), self.tctlm.PowerState_e(3)]
                )])

    def butt_fcn_force_vdd_compensation(self):
        '''Forces a Vdd calibration on the current image in memory'''
        if self.tctlm.is_open():
            if self.loggers: self.loggers['event'].log_text(
                'Forcing a Vdd Calibration on image in memory.', module="App")
        self._sig_send_generic_tctlm.emit([
            self.tctlm.Generic_TCTLM_t(
                tc_fun=self.tctlm.set_HIL_settings,
                params=[
                    True,   # Enable
                    True,   # Force Sample
                    False,  # No image capture
                    True,   # Exectute Vdd Comp
                    False, False, False, False, False]
            )])
        self._sig_send_generic_tctlm.emit([
            self.tctlm.Generic_TCTLM_t(
                tc_fun=self.tctlm.set_HIL_settings,
                params=[False for i in range(9)]
            )])   # Turn off HIL again

    def butt_fcn_force_tobj_calibration(self):
        '''Forces a Tobj calibration on the current image in memory'''
        if self.tctlm.is_open():
            if self.loggers: self.loggers['event'].log_text(
                'Forcing a Tobj Calibration on image in memory.', module="App")

        self._sig_send_generic_tctlm.emit([
            self.tctlm.Generic_TCTLM_t(
                tc_fun=self.tctlm.set_HIL_settings,
                params=[
                    True,  # Enable
                    True,  # Force Sample
                    False,  # No image capture
                    False,  # Do not execute Vdd Comp
                    True,  # Execute Tobj Calib
                    False, False, False, False]
            )])
        self._sig_send_generic_tctlm.emit([
            self.tctlm.Generic_TCTLM_t(
                tc_fun=self.tctlm.set_HIL_settings,
                params=[False for i in range(9)]
            )])   # Turn off HIL again

    def butt_fcn_clear_error_flags(self):
        '''Clears device's error state and image information.'''
        if self.tctlm.is_open():
            if self.loggers: self.loggers['event'].log_text(
                'Clearing device error flags and latched image information.', module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.clear_error_flags)
                ])

    def butt_fcn_refresh_comm_list(self):
        '''Refreshes the comms list'''
        if not self.tctlm.is_open():
            self.ui.combo_comm_ports.clear()
            self.ui.combo_comm_ports.addItems(self.tctlm.comms.uart.return_list_of_ports())

    def butt_fcn_cubedock_refresh_comm_list(self):
        '''Refreshes the CubeDock comms list'''
        if not self.cubedock.uart.is_open():
            self.ui.combo_cubedock_ports.clear()
            self.ui.combo_cubedock_ports.addItems(self.cubedock.uart.return_list_of_ports())

    def check_fcn_set_comms_logging(self):
        '''Enable or disable comms logging'''
        self.loggers['comms'].enabled = self.ui.check_log_commsenable.isChecked()

    @pyqtSlot(object)
    def slot_device_not_responding(self, problem):
        '''Function that triggers when <tctlm> says that device is NOT responding'''
        if not self.device_not_responding:
            self.ui.label_lost_connection.setText("<font color='red'>Device Not Responding: [%s] </font>" % problem)
            self.device_not_responding = True
            if self.loggers: self.loggers['event'].log_text('Device Not Responding: [%s]' % problem, module="App")

        # Need to reset some variables to ensure program doesn't get stuck
        self.log_imgdwl_counter = 0
        self.dwnlding_img = False

    @pyqtSlot()
    def slot_device_responding(self):
        '''Function that triggers when <tctlm> says that device IS responding'''
        if not self.device_not_responding is False:
            self.ui.label_lost_connection.setText(' ')
            self.device_not_responding = False
            if self.loggers: self.loggers['event'].log_text('Device is Responding', module="App")

    def butt_fcn_htpa_calib_level(self):
        '''Sets the HTPA calib level to what is in the listbox'''
        if self.tctlm.is_open():
            level = self.tctlm.HTPACalibLevel_e(self.ui.combo_htpacaliblevel.currentIndex()+1)
            if self.loggers: self.loggers['event'].log_text(
                'Setting HTPA Calibration Level: %s.' % level.name, module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.set_ADS_Settings,
                    params=[self.tctlm.ADSSampleState_e(4), level]
                )])

    def butt_fcn_htpa_wake(self, wake):
        '''Wake up or sleep the HTPA depending on <wake>'''
        if self.tctlm.is_open():
            if self.loggers: self.loggers['event'].log_text(
                'Setting HTPA Wake State: %s.' % ("Wake Up" if wake else "Sleep"), module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.set_HTPA_state,
                    params=[0, 0, self.tctlm.PowerState_e(3), self.tctlm.PowerState_e(2) if wake else self.tctlm.PowerState_e(1)]
                )])

    def butt_fcn_htpa_pwr(self, pwr):
        '''Turns the HTPA on or off depending on <pwr>'''
        if self.tctlm.is_open():
            if self.loggers: self.loggers['event'].log_text('Setting HTPA Power: %s.' % ("On" if pwr else "Off"),
                                                            module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.set_HTPA_state,
                    params=[0, 0, self.tctlm.PowerState_e(2) if pwr else self.tctlm.PowerState_e(1), self.tctlm.PowerState_e(3)]
                )])

    def butt_fcn_cubedock_pwr_3v3_1(self, pwr):
        '''Turns the HTPA on or off depending on <pwr>'''
        if self.cubedock.uart.is_open():
            if self.loggers: self.loggers['event'].log_text('Setting CubeDock 3V3 Switch 1 Power: %s.' % ("On" if pwr else "Off"),
                                                            module="App")

    def butt_fcn_upload_img(self):
        '''Uploads a image block to the processor'''
        if self.tctlm.is_open():
            options = QFileDialog.Options()
            options |= QFileDialog.DontUseNativeDialog
            filePath, _ = QFileDialog.getOpenFileName(self, "Select Image CSV file to decode", "",
                                                      "CSV Files (*.csv)", options=options)
            if filePath:
                # Found a file and now read it
                img = np.genfromtxt(filePath, delimiter=',', dtype=np.int16).transpose()
                self._sig_send_generic_tctlm.emit([
                    self.tctlm.Generic_TCTLM_t(
                        tc_fun=self.tctlm.upload_image,
                        params=[img, self.tctlm.sig_set_progress_bar]
                    )])

    def receive_image(self, image):
        '''Returns a image downloaded from the device'''
        if self.ui.text_adsstate_caliblevel.text() == 'None':
            for (x, y), pixel in np.ndenumerate(image):
                if image[x][y] < -29000:
                    image[x][y] += 2**16

        self.current_image = image
        self.display_image(self.current_image)

        if self.ui.text_adsstate_caliblevel.text() == 'Tobj':
            if any(pixel < 500 and pixel for (x, y), pixel in np.ndenumerate(image)):
                self.warn('Image block downloaded during onboard calibration process and could be corrupted.')

        # Handle occasional image download
        if self.ui.spin_imgdwnper.value() is not 0 and self.dwnlding_img:
            if self.log_imgdwl_counter >= self.ui.spin_imgdwnper.value():
                if self.ui.check_log_imgcsv.isChecked():
                    self.butt_fcn_imgsavecsv()
                if self.ui.check_log_imgbmp.isChecked():
                    self.butt_fcn_imgsavebmp()
                self.log_imgdwl_counter = 0
                self.dwnlding_img = False
        else:
            self.log_imgdwl_counter = 0

    def display_image(self, image):
        '''Displayes an image (receives numpy array'''
        self.ui.text_imgmin.setText('%.2f' % image.min())
        self.ui.text_imgmax.setText('%.2f' % image.max())
        m = np.mean(image[image != -32768])
        m0 = np.mean(image[image != 0])
        image[image == -32768] = m
        if image.max() > 2000: image[image == 0] = m0
        img = np.flipud(255 - self.array_scale(image, 0, 255).astype(np.uint8)).transpose()
        cm_hot = mpl.cm.get_cmap('jet')
        img = cm_hot(img)
        img = np.uint8(img * 255)
        pilimg = Image.fromarray(img)
        qimage = ImageQt.ImageQt(pilimg)
        pixmap = QtGui.QPixmap.fromImage(qimage)
        size = image.shape
        self.ui.label_image.resize(185, 185 / size[0] * size[1])
        self.ui.label_image.setPixmap(pixmap.scaled(self.ui.label_image.size(), QtCore.Qt.IgnoreAspectRatio))

    def butt_fcn_imgsavebmp(self):
        '''Saves current image as *.bmp'''
        if type(self.current_image) is np.ndarray:
            img = 255 - self.array_scale(self.current_image, 0, 255).astype(np.uint8).transpose()
            cm_hot = mpl.cm.get_cmap('jet')
            img = cm_hot(img)
            img = np.uint8(img * 255)
            img = Image.fromarray(img)
            img.save(self.loggers['path'] + "image" + datetime.now().strftime('_%y%m%d_%H%M%S.bmp'))

    def butt_fcn_imgsavecsv(self):
        '''Saves current image as *.csv'''
        if type(self.current_image) is np.ndarray:
            np.savetxt(self.loggers['path'] + "imagedata_" + datetime.now().strftime('%y%m%d_%H%M%S') + '.csv',
                       self.current_image.transpose(), delimiter=',', fmt='%10.5f')

    @staticmethod
    def array_scale(image, maxz=255, minz=0):
        '''Scales an nparray'''
        if image.max() - image.min() != 0:
            return minz + (maxz - minz) * (image - image.min()) / (image.max() - image.min())
        else:
            return np.zeros(image.shape)

    def butt_fcn_ads_set_HIL(self, enable=True, force_sample=False):
        '''Handle the HIL Module'''
        if self.loggers and not force_sample: self.loggers['event'].log_text(
            'Setting ADS HIL: %s.' % ("Enabled" if enable else "Disabled"), module="App")
        self._sig_send_generic_tctlm.emit([
            self.tctlm.Generic_TCTLM_t(
                tc_fun=self.tctlm.set_HIL_settings,
                params=[enable, force_sample, None, None, None, None]
            )])   # Turn off HIL again

    def butt_fcn_ads_sample_mode(self):
        '''Sets the ADS sample mode'''
        if self.tctlm.is_open():
            mode = self.tctlm.ADSSampleState_e(self.ui.combo_adssampling.currentIndex() + 1)
            if self.loggers: self.loggers['event'].log_text(
                'Setting ADS SampleMode: %s.' % mode.name, module="App")
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.set_ADS_Settings,
                    params=[mode, self.tctlm.HTPACalibLevel_e(4)]
                )])

    def butt_fcn_force_power_cycle(self):
        '''Forces a power cycle on the device'''
        if self.tctlm.is_open():
            if self.loggers: self.loggers['event'].log_text(
                'Forcing device power cycle')
            self._sig_send_generic_tctlm.emit([
                self.tctlm.Generic_TCTLM_t(
                    tc_fun=self.tctlm.device_force_power_cycle
                )])

    def butt_fcn_comm_connect(self):
        '''Connect to the comm port selected'''
        if self.ui.butt_comm_connect.text() == "Connect":

            # Reset some states to not confuse everything
            self.log_imgdwl_counter = 0
            self.dwnlding_img = False

            # Attempt to connect to the com
            port = str(self.ui.combo_comm_ports.currentText())
            if port != "":
                # A valid port is selected
                if self.ui.radio_comms_uart.isChecked():
                    if self.tctlm.comms.uart.connect_to_port(port):
                        self.tctlm.comms.channel = 'UART'
                        self.ui.butt_comm_connect.setStyleSheet('QPushButton {background-color: #58b76b;}')
                        self.ui.butt_comm_connect.setText("Disconnect")
                elif self.ui.radio_comms_i2c.isChecked():
                    if self.tctlm.comms.i2c.connect_to_port(port):
                        self.tctlm.comms.channel = 'I2C'
                        self.ui.butt_comm_connect.setStyleSheet('QPushButton {background-color: #58b76b;}')
                        self.ui.butt_comm_connect.setText("Disconnect")
        else:
            # Attempt to disconnect
            self.tctlm.comms.uart.close()
            self.ui.butt_comm_connect.setStyleSheet('background-color: None')
            self.ui.butt_comm_connect.setText("Connect")
            self.auto_update = False

    def butt_fcn_cubedock_comm_connect(self):
        '''Connect to the comm port selected'''

        if self.ui.butt_cubedock_connect.text() == "Connect":
            # Attempt to connect to the com
            port = str(self.ui.combo_cubedock_ports.currentText())
            if port != "":
                # A valid port is selected
                if self.cubedock.uart.connect_to_port(port):
                    self.ui.butt_cubedock_connect.setStyleSheet('QPushButton {background-color: #58b76b;}')
                    self.ui.butt_cubedock_connect.setText("Disconnect")

        else:
            # Attempt to disconnect
            self.cubedock.uart.close()
            self.ui.butt_cubedock_connect.setStyleSheet('background-color: None')
            self.ui.butt_cubedock_connect.setText("Connect")

    @pyqtSlot(float, float)
    def slot_update_progress_bar(self, val, scale):
        '''Updates the progress bar from a signal'''
        self.ui.progressBar.setValue(val * 100 / scale)

    def butt_fcn_download_blinds2json(self):
        '''Downloads the blind pixels, and saves them to a json file'''
        if self.tctlm.is_open() and not self.handler.isRunning():
            if self.auto_update:
                # Cannot download EEPROM during auto update
                self.butt_fcn_auto_update() # This will turn off the auto updating
            if self.loggers: self.loggers['event'].log_text(
                'Attempting to download Blind Pixels from HTPA into *.json.', module="App")
            self.handler = BLINDS2JSON(tctlm=self.tctlm, json_path=self.loggers['path'])
            self.handler.start()

    def butt_fcn_generate_img_from_csv(self):
        '''Generates .bmp images from .csv files with set minimum/maximum for colour scaling'''
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        filePaths, _ = QFileDialog.getOpenFileNames(self, "Select Image CSV file to decode", "",
                                                  "CSV Files (*.csv)", options=options)

        if filePaths and not self.handler.isRunning():
            self.handler = CSV2BMP(csv_paths=filePaths, bmp_path=filePaths[0],
                                   min_val=float(self.ui.text_misc_genimgcsvmin.text()), max_val=float(self.ui.text_misc_genimgcsvmax.text()))
            self.handler.sig_set_progress_bar.connect(self.slot_update_progress_bar)
            self.handler.start()

    def butt_fcn_graph_clear(self):
        '''Clears all data from the graphs'''
        self.graph_temps_data_mcu.set_xdata([])
        self.graph_temps_data_mcu.set_ydata([])
        self.graph_temps_data_htpa.set_xdata([])
        self.graph_temps_data_htpa.set_ydata([])
        self.graph_currents_data_htpa.set_xdata([])
        self.graph_currents_data_htpa.set_ydata([])
        self.graph_currents_data_3V3.set_xdata([])
        self.graph_currents_data_3V3.set_ydata([])
        self.ui.graph_temps.canvas.draw()

    def butt_fcn_download_eeprom(self):
        '''Downloads the device's eeprom to a *.csv file'''
        if self.tctlm.is_open() and not self.handler.isRunning():
            if self.auto_update:
                # Cannot download EEPROM during auto update
                self.butt_fcn_auto_update() # This will turn off the auto updating
            if self.loggers: self.loggers['event'].log_text(
                'Attempting to download EEPROM from HTPA into *.csv.', module="App")
            self.handler = DeviceEeprom2Csv(path = self.loggers['path'], tctlm=self.tctlm)
            self.handler.start()

    def butt_fcn_decode_eeprom(self):
        '''Decodes a csv raw eeprom file and decodes it into a json file'''
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        filePath, _ = QFileDialog.getOpenFileName(self, "Select EEPROM CSV file to decode", "",
                                                  "CSV Files (*.csv)", options=options)

        if filePath and not self.handler.isRunning():
            self.handler = EepromCsv2Json(csv_path = filePath, json_path=loggers['path'])
            self.handler.sig_set_progress_bar.connect(self.slot_update_progress_bar)
            self.handler.start()

    def butt_fcn_eeprom_c(self):
        '''Build a .c and .h file containing EEPROM data'''
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        filePath, _ = QFileDialog.getOpenFileName(self, "Select EEPROM JSON file to decode", "",
                                                  "JSON Files (*.json)", options=options)

        if filePath and not self.handler.isRunning():
            self.handler = Json2EepromCode(filePath, loggers['path'])
            self.handler.sig_set_progress_bar.connect(self.slot_update_progress_bar)
            self.handler.start()

    def butt_fcn_lookup_c(self):
        '''Build a .c and .h file containing lookup table data'''
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        filePath, _ = QFileDialog.getOpenFileName(self, "Select LOOKUP_TABLE JSON file to build Table.c", "",
                                                  "JSON Files (*.json)", options=options)
        if filePath and not self.handler.isRunning():
            self.handler = Json2LookupCode(filePath, loggers['path'])
            self.handler.sig_set_progress_bar.connect(self.slot_update_progress_bar)
            self.handler.start()

    def butt_fcn_auto_update(self):
        '''Toggles the auto update'''
        if not self.auto_update:
            if self.tctlm.is_open():
                self.ui.butt_auto_update.setStyleSheet('QPushButton {background-color: #58b76b;}')
                self.auto_update = True
        else:
            self.ui.butt_auto_update.setStyleSheet('background-color: None')
            self.auto_update = False

    def timer_handler(self):
        '''Handler for the recurrent timer to update all fields'''
        self.ui.label_time.setText(time.strftime('%H:%M:%S'))
        if self.auto_update and not self.dwnlding_img:  # While downloading image ignore all timer stuff

            # Are we connected to a CubeDock?
            if self.cubedock.uart.is_open():
                self._sig_cubedock_update_all_fields.emit()     # Should happen before CubeIR's

            self.update_all_fields()

            if self.ui.spin_imgdwnper.value() is not 0:
                if self.log_imgdwl_counter >= self.ui.spin_imgdwnper.value() and not self.dwnlding_img:
                    # Time to download a sample
                    self._sig_send_generic_tctlm.emit([
                        self.tctlm.Generic_TCTLM_t(
                            tctlm=self.tctlm.Image_t,
                            tlm_fun=self.tctlm.download_image,
                            params=[self.tctlm.sig_set_progress_bar, True]
                        )])
                    self.dwnlding_img = True
                else:
                    self.log_imgdwl_counter += 1
            else:
                self.log_imgdwl_counter = 0

    def warn(self, text, *args, **kwargs):
        '''Override of the inherent warning system'''
        tb = traceback.extract_stack()
        st = "".join(traceback.format_list(tb)[:-1])
        warning_text = st + '\n' + text

        # log the error here
        if self.loggers: loggers['event'].log_text(warning_text, module="WARNING")

        # Raise the warning
        real_warn(warning_text)

def my_excepthook(exctype, value, tb):

    # Ensure that <Ctr+C> still works
    if issubclass(exctype, KeyboardInterrupt):
        sys.__excepthook__(exctype, value, tb)
        return

    # log the exception here
    if loggers: loggers['event'].log_text(traceback.format_exception(exctype, value, tb), module="EXCEPTION")

    # then call the default handler
    sys.__excepthook__(exctype, value, tb)

if __name__ == "__main__":
    print("Started: app_threaded.py")

    '''Sets up all the loggers and things'''
    log_path = datetime.now().strftime('Logs/Logs_%Y_%m_%d/')
    if not os.path.exists(log_path):
        os.makedirs(log_path)

    logger_comms = Logger(filename="comms_log", path=log_path)
    logger_event = Logger(filename="event_log", path=log_path)
    logger_data = Logger(filename="data_log", ext='.csv', path=log_path)

    loggers = {}
    loggers['comms'] = logger_comms
    loggers['event'] = logger_event
    loggers['data'] = logger_data
    loggers['path'] = log_path

    loggers['event'].log_text('App started running', module='MAIN')
    loggers['data'].keys = ["Runtime",
                                "Current3V3", "Current3V3Min", "Current3V3Max",
                                "CurrentHTPA", "CurrentHTPAMin", "CurrentHTPAMax", "CurrentCubeDock3V3_1",
                                "TemperatureMCU", "TemperatureMCUMin", "TemperatureMCUMax",
                                "TemperatureHTPA", "TemperatureHTPAMin", "TemperatureHTPAMax",
                                "VoltageMCU", "VoltageMCUMin", "VoltageMCUMax",
                                "VoltageHTPA", "VoltageHTPAMin", "VoltageHTPAMax",
                                "ADSSampleMode", "HTPACalibLevel",
                                "HILEnabled", "HILBusy", "HILEnableImgVddComp", "HILEnableImgTobjCalib", "HILEnableEdgeDet", "HILEnableLensDistCorr", "HILEnablePolyFit", "HILEnableAttEst",
                                "HTPADeviceID","HTPATableNumber",
                                "HtpaPowerState", "HtpaWakeState", "HtpaCaptureState", "HtpaPTAT", "HtpaVdd",
                                "HTPAImageLastMin", "HTPAImageLastMean", "HTPAImageLastMax", "HTPAImageAllTimeMin","HTPAImageAllTimeMax",
                                "SRAM2ParityError", "FlashCorruption",
                                "3V3OverCurrent", "HTPAOverCurrent", "Timer1sOverRun",
                                "MCUOverTemperature", "MCUUnderTemperature", "HTPAOverTemperature", "HTPAUnderTemperature","HTPACalibLookupOutOfRange",
                                "HTPAIntegrationTimeError", "HTPANotResponding", "HTPAConfigMismatch", "HTPASpiTimeout", "CommsTcSlotsBuffFull", "CommsTcParamBuffFull",
                                "CommsUartProtError", "CommsUartMsgIncomplete", "CommsUartTcParamOverflow", "UARTTxOverflow",
                                "CommsTcInvalidID", "CommsTcInvalidPar", "CommsTcInvalidParLen", "CommsTcTimeout", "CommsTcCrcError", "CommsTlmInvalidID",
                                "UARTTxReadingEmptyBuffer", "UARTRxByteOverrun", "UARTRxFramingError","UartTxTimeout",
                                "FRAMStatusNotResponding", "FRAMDeviceNotFound", "FRAMSpiTimeout",
                                "TcAckLastTCID","TcAckProcessed", "TcAckErrStatus", "TcAckParamErrIndex",
                                "ConfigRewrittenFromMemory", "ConfigExtMemorySlotOverwritten", "ConfigNoConfigFoundInMemory", "ConfigNumOfValidFramSlots", "ConfigNumOfValidFlashSlots", "ConfigNotSaved", "ConfigDataLoss",
                                "SwStartupTime", "SwProcessingTime", "SwProcessingTimeMax", "SwLoopTime", "SwLoopTimeMax", "SwImgCaptureTime", "SwLensDistCorrTime", "SwEstimationTime",
                                "SwConfigRefreshMaxTime", "SwConfigCheckMemoryMaxTime", "SwAdcMeasurementTime", "SwHtpaIntegrationTime",
                                "DetectedBitFlips", "FlashFaults", "FlashLastFaultAddress",
                                "MemTestSramMode", "MemTestSramFaultsLastLoop", "MemTestSramFaultsTotal", "MemTestSramLoopNumber", "MemTestSramIterateTime",
                                "MemTestFlashMode", "MemTestFlashState", "MemTestFlashIndex", "MemTestFlashFaultPageIndex", "MemTestFlashPageWriteErrors","MemTestFlashEraseErrors","MemTestFlashPageWriteTime", "MemTestFlashPageReadTime", "MemTestFramTimeToWait",
                                "SRAM2ParityCheckEnabled", "InterfaceVersion", "FirmwareVersion", "IRCAM SN"]
    headings = loggers['data'].keys[:]
    headings.insert(0, 'Time')
    loggers['data'].log_data(headings, add_time=False)

    sys.excepthook = my_excepthook      # Show error messages in console

    app = QApplication(sys.argv)
    window = AppThreaded(loggers=loggers)
    window.show()
    sys.exit(app.exec_())
