/*
 * interrupt.h
 *
 * Created: 2015/07/21 04:32:02 PM
 * Author(s): wjordaan/ghjvv
 */ 


#ifndef INTERRUPT_H_
#define INTERRUPT_H_

#include <stdint.h>
#include <stdbool.h>

#define MEASCOUNT        15    // 5ms interrupt
#define CONTROLCOUNT    60    // 20ms interrupt
#define HALLCOUNT        300    // 100ms interrupt

void INTERRUPT_Init(void);
void INTERRUPT_CheckComms(void);

#endif /* INTERRUPT_H_ */