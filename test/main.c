/*
 * RectionWheelFirmware.c
 *
 * Created: 2015/07/21 01:48:06 PM
 * Author(s): wjordaan/ghjvv
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <stdint.h>
#include <stdbool.h>
#include "comms.h"
#include "bldc.h"
#include "interrupt.h"
#include "uart.h"
#include "timer.h"
#include "tctlm.h"
#include "device.h"
#include "can.h"
//Disable the watchdog to prevent infinite reset loop 
void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));
void wdt_init(void)
{
	MCUSR = 0;
	wdt_disable();

	return;
}

// definition of global values
uint16_t g_SerialNumber;
uint8_t g_I2CAddress;
uint16_t g_CANAddress;
bool g_WritevaluesFlag=false;

#define EEPROMSEGMENTS 50


// definition of static variables
static uint8_t ErrorFlags;

int main(void)
{
	_delay_ms(100);
	wdt_enable(WDTO_120MS);
	
	// Initialize variables
	DEVICE_ClearError();
	
	// Extract EEPROM configuration values
	DEVICE_ExtractConfiguration();
	
	// Initialize modules
	BLDC_Init();
	TCTLM_Init();
	COMMS_Init();
	TIMER_Init();
	INTERRUPT_Init();

	
	//sendByte(0x00);
    while(1)
    {
		wdt_reset();//kick the watchdog
		if(BLDC_GetControlFlag()==true)
		{
			// Perform motor controller
			BLDC_WheelControl();
			
			INTERRUPT_CheckComms();
			
			// Measure motor current
			BLDC_MeasureMotorCurrent();
			
			INTERRUPT_CheckComms();
			
			// Clear control flag
			BLDC_SetControlFlag(false);
		}
		
		if(g_WritevaluesFlag==true)
		{
			g_WritevaluesFlag=false;
			EEPROM_writeAllValues();
		}
		
		// Update tlm variable
		g_ErrorFlags = ErrorFlags;
		
		// Process telecommands
        COMMS_processTCMD();
		INTERRUPT_CheckComms();
		ProcessUart();	
		INTERRUPT_CheckComms();
		// Sleep???
    }
}

void DEVICE_SetError(uint8_t error)
{
	ErrorFlags = ErrorFlags | error;
	
	// Update tlm variable
	g_ErrorFlags = ErrorFlags;
}

void DEVICE_ClearError(void)
{
	ErrorFlags = 0;
	g_LoopErrorFlags=0;
}

void DEVICE_MeasureTemperature(void)
{
	// Setting for measuring internal temperature sensor
	ADMUX = 0b11001000;
	uint8_t timeout=0;
	//Start Single conversion
	ADCSRA|=(1<<ADSC);

	//Wait for conversion to complete
	while(!(ADCSRA & (1<<ADIF)))
	{
		if(timeout>200)
		{
			g_LoopErrorFlags=g_LoopErrorFlags|0b00000100;
			break;
		}
		timeout++;
	}

	//Clear ADIF by writing one to it
	ADCSRA|=(1<<ADIF);
	
	g_MCUTemperature = (uint16_t)(TEMPGAIN*ADC+TEMPOFFSET);
	
	
}

void DEVICE_ExtractConfiguration(void)
{
	EEPROM_readAllValues();
	/*
	bool validValues = true;
	int16_t gain;
	uint16_t gainI, gainD;
	uint8_t multI, multD;
	
	// try to read values from EEPROM
	g_SerialNumber = (uint16_t)EEPROM_read(SERIAL_NUMBER_EEPROM_ADDRESS_1)+(((uint16_t)EEPROM_read(SERIAL_NUMBER_EEPROM_ADDRESS_2))<<8);
	g_I2CAddress = EEPROM_read(I2C_ADDRESS_EEPROM_ADDRESS);
	g_CANAddress = EEPROM_read(CAN_ADDRESS_EEPROM_ADDRESS);
	
	if(g_SerialNumber==0 || g_I2CAddress==0 || g_CANAddress==0 || g_SerialNumber==0xFFFF || g_I2CAddress==0xFF || g_CANAddress==0xFF)
	{
		validValues = false;
	}
	
	if(!validValues)
	{
		// load default values
		g_SerialNumber = SERIAL_NUMBER;
		g_I2CAddress = I2C_ADRESS;
		g_CANAddress = CAN_ADDRESS;
		
		DEVICE_SetError(ERROR_CODE_CONFIG);
	}
	else
	{
		// extract gain values for main speed controller gains
		gainI = (uint16_t)EEPROM_read(MAIN_GAIN_I_ADDRESS_EEPROM_ADDRESS_1)+(((uint16_t)EEPROM_read(MAIN_GAIN_I_ADDRESS_EEPROM_ADDRESS_2))<<8);
		multI = (uint8_t)EEPROM_read(MAIN_GAIN_I_MULT_ADDRESS_EEPROM_ADDRESS);
		gainD = (uint16_t)EEPROM_read(MAIN_GAIN_D_ADDRESS_EEPROM_ADDRESS_1)+(((uint16_t)EEPROM_read(MAIN_GAIN_D_ADDRESS_EEPROM_ADDRESS_2))<<8);
		multD = (uint8_t)EEPROM_read(MAIN_GAIN_D_MULT_ADDRESS_EEPROM_ADDRESS);
		
		// basic check for valid values
		if(gainI!=0 && gainI!=0xFF && gainD!=0 && gainD!=0xFF)
		{
			BLDC_SetMainControllerGain(gainI, multI, gainD, multD);
		}
		else
		{
			DEVICE_SetError(ERROR_CODE_CONFIG);
		}
		
		// extract gain values for backup speed controller gains
		gainI = (uint16_t)EEPROM_read(BACK_GAIN_I_ADDRESS_EEPROM_ADDRESS_1)+(((uint16_t)EEPROM_read(BACK_GAIN_I_ADDRESS_EEPROM_ADDRESS_2))<<8);
		multI = (uint8_t)EEPROM_read(BACK_GAIN_I_MULT_ADDRESS_EEPROM_ADDRESS);
		gainD = (uint16_t)EEPROM_read(BACK_GAIN_D_ADDRESS_EEPROM_ADDRESS_1)+(((uint16_t)EEPROM_read(BACK_GAIN_D_ADDRESS_EEPROM_ADDRESS_2))<<8);
		multD = (uint8_t)EEPROM_read(BACK_GAIN_D_MULT_ADDRESS_EEPROM_ADDRESS);
		
		// basic check for valid values
		if(gainI!=0 && gainI!=0xFF && gainD!=0 && gainD!=0xFF)
		{
			BLDC_SetBackupControllerGain(gainI, multI, gainD, multD);
		}
		else
		{
			DEVICE_SetError(ERROR_CODE_CONFIG);
		}
		
		// extract gain values for PWM gain
		gain = (int16_t)EEPROM_read(PWM_GAIN_ADDRESS_EEPROM_ADDRESS_1)+(((int16_t)EEPROM_read(PWM_GAIN_ADDRESS_EEPROM_ADDRESS_2))<<8);
		multI = (uint8_t)EEPROM_read(PWM_GAIN_MULT_ADDRESS_EEPROM_ADDRESS);
		
		// basic check for valid values
		if(gain!=0 && gain!=0xFF)
		{
			BLDC_SetPWMGain(gain, multI);
		}
		else
		{
			DEVICE_SetError(ERROR_CODE_CONFIG);
		}
	}
	*/
}
/*
void DEVICE_WriteSerialEEPROM(uint16_t serialNumber)
{
	uint8_t* num = ((uint8_t*)(&serialNumber));
	EEPROM_write(SERIAL_NUMBER_EEPROM_ADDRESS_1,(uint8_t)num[0]);
	EEPROM_write(SERIAL_NUMBER_EEPROM_ADDRESS_2,(uint8_t)num[1]);
}

void DEVICE_WriteI2CAddressEEPROM(uint8_t i2cAddress)
{
	EEPROM_write(I2C_ADDRESS_EEPROM_ADDRESS,i2cAddress);
}

void DEVICE_WriteCANAddressEEPROM(uint8_t canAddress)
{
	EEPROM_write(CAN_ADDRESS_EEPROM_ADDRESS,canAddress);
}

void DEVICE_WritePWMGainEEPROM(int16_t gain, uint8_t mult)
{
	uint8_t* num = ((uint8_t*)(&gain));
	EEPROM_write(PWM_GAIN_ADDRESS_EEPROM_ADDRESS_1,(uint8_t)num[0]);
	EEPROM_write(PWM_GAIN_ADDRESS_EEPROM_ADDRESS_2,(uint8_t)num[1]);
	
	EEPROM_write(PWM_GAIN_MULT_ADDRESS_EEPROM_ADDRESS,mult);
}

void DEVICE_WriteMainGainEEPROM(uint16_t gainI, uint8_t multI, uint16_t gainD, uint8_t multD)
{
	uint8_t* num = ((uint8_t*)(&gainI));
	EEPROM_write(MAIN_GAIN_I_ADDRESS_EEPROM_ADDRESS_1,(uint8_t)num[0]);
	EEPROM_write(MAIN_GAIN_I_ADDRESS_EEPROM_ADDRESS_2,(uint8_t)num[1]);
	
	EEPROM_write(MAIN_GAIN_I_MULT_ADDRESS_EEPROM_ADDRESS,multI);
	
	num = ((uint8_t*)(&gainD));
	EEPROM_write(MAIN_GAIN_D_ADDRESS_EEPROM_ADDRESS_1,(uint8_t)num[0]);
	EEPROM_write(MAIN_GAIN_D_ADDRESS_EEPROM_ADDRESS_2,(uint8_t)num[1]);
	
	EEPROM_write(MAIN_GAIN_D_MULT_ADDRESS_EEPROM_ADDRESS,multD);
}

void DEVICE_WriteBackupGainEEPROM(uint16_t gainI, uint8_t multI, uint16_t gainD, uint8_t multD)
{
	uint8_t* num = ((uint8_t*)(&gainI));
	EEPROM_write(BACK_GAIN_I_ADDRESS_EEPROM_ADDRESS_1,(uint8_t)num[0]);
	EEPROM_write(BACK_GAIN_I_ADDRESS_EEPROM_ADDRESS_2,(uint8_t)num[1]);
	
	EEPROM_write(BACK_GAIN_I_MULT_ADDRESS_EEPROM_ADDRESS,multI);
	
	num = ((uint8_t*)(&gainD));
	EEPROM_write(BACK_GAIN_D_ADDRESS_EEPROM_ADDRESS_1,(uint8_t)num[0]);
	EEPROM_write(BACK_GAIN_D_ADDRESS_EEPROM_ADDRESS_2,(uint8_t)num[1]);
	
	EEPROM_write(BACK_GAIN_D_MULT_ADDRESS_EEPROM_ADDRESS,multD);
}
*/
void EEPROM_write(unsigned int uiAddress, unsigned char ucData)
{
	uint16_t timeout=0;
	/* Wait for completion of previous write */
	while(EECR & (1<<EEPE))
	{
		if(timeout>5000)
		{
            g_LoopErrorFlags=g_LoopErrorFlags|0b00001000;
			break;
		}
		timeout++;
	}
	/* Set up address and Data Registers */
	EEAR = uiAddress;
	EEDR = ucData;
	/* Write logical one to EEMPE */
	EECR |= (1<<EEMPE);
	/* Start eeprom write by setting EEPE */
	EECR |= (1<<EEPE);
}


unsigned char EEPROM_read(unsigned int uiAddress)
{
	uint16_t timeout=0;
	/* Wait for completion of previous write */
	while(EECR & (1<<EEPE))
	{
		if(timeout>5000)
		{
			g_LoopErrorFlags=g_LoopErrorFlags|0b00010000;
			break;
		}
		timeout++;
	}
	/* Set up address register */
	EEAR = uiAddress;
	/* Start eeprom read by writing EERE */
	EECR |= (1<<EERE);
	/* Return data from Data Register */
	return EEDR;
}
void EEPROM_writeAllValues(void)
{
	//Fill the EEPROM with copies of the global variables and CRC value
	
	
	uint8_t write_buffer[20]={0};
	uint8_t i,j;
	
	//Put all global vars in write buffer
	uint8_t* num = ((uint8_t*)(&g_SerialNumber));
	write_buffer[0]=num[0];
	write_buffer[1]=num[1];
	write_buffer[2]=g_I2CAddress;
	write_buffer[3]=(uint8_t)g_CANAddress;
	num = ((uint8_t*)(&GainI));
	write_buffer[4]=num[0];
	write_buffer[5]=num[1];
	write_buffer[6]=MultI;
	
	num = ((uint8_t*)(&GainP));
	write_buffer[7]=num[0];
	write_buffer[8]=num[1];
	write_buffer[9]=MultP;
	
	num = ((uint8_t*)(&GainIB));
	write_buffer[10]=num[0];
	write_buffer[11]=num[1];
	write_buffer[12]=MultIB;
	
	num = ((uint8_t*)(&GainPB));
	write_buffer[13]=num[0];
	write_buffer[14]=num[1];
	write_buffer[15]=MultPB;
	
	num = ((uint8_t*)(&GainPWM));
	write_buffer[16]=num[0];
	write_buffer[17]=num[1];
	write_buffer[18]=MultPWM;
	
	//calculate CRC-8 (Dallas/Maxim) of variables and store in last spot of write buffer
	for(i=0;i<19;i++)
	{
		write_buffer[19]^=write_buffer[i];
		write_buffer[19]^=(write_buffer[19]<<3)^(write_buffer[19]<<4)^(write_buffer[19]<<6);
		write_buffer[19]^=(write_buffer[19]>>4)^(write_buffer[19]>>5);
	}
	
	for(j=0;j<EEPROMSEGMENTS;j++)//iterate through entire EEPROM and write copies of the variables with their CRC
	{
		for(i=0;i<20;i++)
		{
			EEPROM_write((uint16_t)(j*20+i),write_buffer[i]);
		}
		wdt_reset(); //Writing entire EEPROM takes long time, so kick the watchdog after every couple of write
		COMMS_processTCMD();//service the comms or data might be missed
		INTERRUPT_CheckComms();
		ProcessUart();
	}
	
}


void EEPROM_readAllValues(void)
{
	//Read all copies of data in EEPROM and vote using those with correct CRC values
	//If the most common one with correct CRC does not have the correct range, use default values
	//Correct the spots in EEPROM with corrupted data
	
	uint8_t i,j;
	uint8_t read_buffer[20]={0};
	uint8_t count=1;
	uint8_t most = 0;
	uint8_t elemIndex=EEPROMSEGMENTS+1;
	
	bool validCRC_buffer[EEPROMSEGMENTS]={false};//buffer to indicate if CRCs check out
	
	uint8_t CRC_buffer[EEPROMSEGMENTS]={0};//buffer to store all the read CRC values
	
	for(j=0;j<EEPROMSEGMENTS;j++)
	{
		for(i=0;i<19;i++)//calculate CRC value for currently read copy of data
		{
			CRC_buffer[j]^=EEPROM_read(j*20+i);
			CRC_buffer[j]^=(CRC_buffer[j]<<3)^(CRC_buffer[j]<<4)^(CRC_buffer[j]<<6);
			CRC_buffer[j]^=(CRC_buffer[j]>>4)^(CRC_buffer[j]>>5);
		}
		
		if(CRC_buffer[j]==EEPROM_read(j*20+19)&&CRC_buffer[j]!=0)//verify calculated CRC with the one stored in EEPROM for that copy
		{
			validCRC_buffer[j]=true;
		}
		else
		{
			validCRC_buffer[j]=false;
		}
	}
	
	

	for(i = 0; i < EEPROMSEGMENTS; i++) //find CRC that is the most common one in the array of CRCs
	{
		if(validCRC_buffer[i])//Only look at the CRCs which the data is verified as correct
		{
			count = 1;
			for(j = i + 1; j < EEPROMSEGMENTS; j++)
			{
				if(CRC_buffer[j] == CRC_buffer[i] && validCRC_buffer[j])
				{
					count++;
				}
			}
			if (most < count)
			{
				most = count;
				elemIndex = i;//Remember the index of the CRC that is the most common
			}
		}
	}
	
	if(elemIndex>EEPROMSEGMENTS)
	{
		//ERROR with reading
		//ERROR with range of values
		//Load default
		g_SerialNumber = SERIAL_NUMBER;
		g_I2CAddress = I2C_ADRESS;
		g_CANAddress = CAN_ADDRESS;
		
		DEVICE_SetError(ERROR_CODE_CONFIG);//Indicates to the user a config error
		return;
	}
	
	
	for(j=0;j<5;j++)
	{
		
		for(i=0;i<19;i++)//Read the data of the most common (and correct) CRC
		{
			read_buffer[i]=EEPROM_read(elemIndex*20+i);
			read_buffer[19]^=read_buffer[i];
			read_buffer[19]^=(read_buffer[19]<<3)^(read_buffer[19]<<4)^(read_buffer[19]<<6);
			read_buffer[19]^=(read_buffer[19]>>4)^(read_buffer[19]>>5);
		}
		if(read_buffer[19]==EEPROM_read(elemIndex*20+19) && read_buffer[19]==CRC_buffer[elemIndex])//Ensure that the data was read correctly
		{
			break;
		}else
		{
			read_buffer[19]=0;
		}
	}//retry reading EEPROM if data is not correct
	
	//Check if data is within ranges and also that the reading was successful
	if(
	j >= 4 ||  //read retry count
	(uint16_t)read_buffer[0]+(((uint16_t)read_buffer[1])<<8) <= 0x0708		|| //serial number range
	(uint16_t)read_buffer[0]+(((uint16_t)read_buffer[1])<<8) == 0xFFFF 		||
	read_buffer[2] == 0   													||    //I2C range
	read_buffer[2] == 0xFF													||
	read_buffer[2] % 2 != 0													||
	read_buffer[3] == 0   													||    //CAN range
	read_buffer[3] == 0xFF													||
	(uint16_t)read_buffer[4]+(((uint16_t)read_buffer[5])<<8) 	== 0		||  //GainI range
	(uint16_t)read_buffer[4]+(((uint16_t)read_buffer[5])<<8) 	== 0xFFFF	||
	(uint16_t)read_buffer[7]+(((uint16_t)read_buffer[8])<<8) 	== 0		||  //GainP range
	(uint16_t)read_buffer[7]+(((uint16_t)read_buffer[8])<<8) 	== 0xFFFF	||
	(uint16_t)read_buffer[10]+(((uint16_t)read_buffer[11])<<8) 	== 0		||  //GainIB range
	(uint16_t)read_buffer[10]+(((uint16_t)read_buffer[11])<<8) 	== 0xFFFF	||
	(uint16_t)read_buffer[13]+(((uint16_t)read_buffer[14])<<8) 	== 0		||  //GainPB range
	(uint16_t)read_buffer[13]+(((uint16_t)read_buffer[14])<<8) 	== 0xFFFF	||
	(int16_t)read_buffer[16]+(((int16_t)read_buffer[17])<<8) 	== 0 		||  //GainPWM range
	(int16_t)read_buffer[16]+(((int16_t)read_buffer[17])<<8) 	== 0xFFFF	)
	{
		//ERROR with reading
		//ERROR with range of values
		//Load default
		g_SerialNumber = SERIAL_NUMBER;
		g_I2CAddress = I2C_ADRESS;
		g_CANAddress = CAN_ADDRESS;
		
		DEVICE_SetError(ERROR_CODE_CONFIG);//Indicates to the user a config error
		return;
	}
	else
	{//If everything was read successfully and within correct range, load the data into the global variables
		g_SerialNumber = (uint16_t)read_buffer[0]+(((uint16_t)read_buffer[1])<<8);
		g_I2CAddress = read_buffer[2];
		g_CANAddress = read_buffer[3];
		
		BLDC_SetMainControllerGain((uint16_t)read_buffer[4]+(((uint16_t)read_buffer[5])<<8),
		read_buffer[6],
		(uint16_t)read_buffer[7]+(((uint16_t)read_buffer[8])<<8),
		read_buffer[9]);
		
		BLDC_SetBackupControllerGain((uint16_t)read_buffer[10]+(((uint16_t)read_buffer[11])<<8),
		read_buffer[12],
		(uint16_t)read_buffer[13]+(((uint16_t)read_buffer[14])<<8),
		read_buffer[15]);
		
		BLDC_SetPWMGain((int16_t)read_buffer[16]+(((int16_t)read_buffer[17])<<8),
		read_buffer[18]);

	}
	
	for(j=0;j<EEPROMSEGMENTS;j++)//Overwrite the EEPROM copies that were incorrect with the correct values
	{
		if(validCRC_buffer[j]!=true||CRC_buffer[j]!=read_buffer[19])
		{
			for(i=0;i<20;i++)
			{
				EEPROM_write(j*20+i,read_buffer[i]);
			}
		}
	}
	
	

}