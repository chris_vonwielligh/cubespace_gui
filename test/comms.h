/*
 * comms.h
 *
 * Created: 2015/07/21 01:56:57 PM
 * Author(s): wjordaan/ghjvv
 */ 


#ifndef COMMS_H_
#define COMMS_H_

#include <stdint.h>

#define COMMS_ID_TYPE 0x80
#define COMMS_ID_TLM  0x80
#define COMMS_ID_TCMD 0x00

#define COMMS_ERROR_TCMDBUFOF 	1
#define COMMS_ERROR_UARTTLM		2
#define COMMS_ERROR_I2CTLM  	3

#define COMMS_TCMD_BUFFLEN   	1
#define COMMS_TCMD_PARAMLEN  	6

#define COMMS_TCMDERR_PARAMUF 	1
#define COMMS_TCMDERR_PARAMOF 	2

#define UART_ESCAPECHAR 0x1f
#define UART_SOM        0x7f
#define UART_EOM        0xff

// Struct definitions
typedef struct{
	uint8_t id;
	uint8_t len;
	uint8_t error;
	uint8_t processed;
	uint8_t params[COMMS_TCMD_PARAMLEN];
} COMMS_TCMD_TypeDef;

// Enum definitions
enum I2CcommsState{
	waitForId,
	waitForData
} i2cState;

enum commsState{
	uartIdle,
	uartMsgType,
	uartTcData,
	uartTlmReq
} uartState;

// Function prototypes
void COMMS_Init(void);
void COMMS_processTCMD(void);

void UART_IRQ(void);
void UART_IRQHandler(void);
void UART_FormatAndSend(uint8_t* buffer, int len);
void ProcessUart(void);
void I2C_IRQHandler(void);
void CAN_IRQHandler(void);

#endif /* COMMS_H_ */