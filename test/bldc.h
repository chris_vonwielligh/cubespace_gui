/*
 * bldc.h
 *
 * Created: 2015/07/21 03:40:36 PM
 * Author(s): wjordaan/ghjvv/mkearney
 */ 


#ifndef BLDC_H_
#define BLDC_H_

#include <stdint.h>
#include <stdbool.h>

#define PWM_LIMIT_INT	346
#define PWM_LIMIT_FLOAT 346.0

#define CONTROLMODE_NONE 0
#define CONTROLMODE_NOCONTROL 1
#define CONTROLMODE_DUTY 2
#define CONTROLMODE_SPEED 3

void BLDC_Init(void);
void BLDC_SetControlFlag(uint8_t input);
uint8_t BLDC_GetControlFlag(void);
void BLDC_SetDutyCycle(int16_t duty);
void BLDC_SetMotorSwitch(bool mswitch);
void BLDC_SetEncoderSwitch(bool eswitch);
void BLDC_SetHallSwitch(bool hswitch);
void BLDC_DetermineSpeed(void);
void BLDC_DetermineBackupSpeed(void);
void BLDC_WheelControl(void);
void BLDC_SetWheelSpeed(int16_t duty);
void BLDC_MeasureMotorCurrent(void);
void BLDC_Measure3V3Current(void);
void BLDC_SetPWMGain(int16_t gain, uint8_t mult);
void BLDC_SetMainControllerGain(uint16_t intGain, uint8_t intMult, uint16_t feedGain, uint8_t feedMult);
void BLDC_SetBackupControllerGain(uint16_t intGain, uint8_t intMult, uint16_t feedGain, uint8_t feedMult);
void BLDC_SetControlMode(uint8_t mode);
void BLDC_SetRunMode(bool state);
bool BLDC_GetEncoderSwitch();
void BLDC_SetEncoderWeight(uint8_t intWeight_x2);
void BLDC_GetPWMGain(uint8_t* buffer);
void BLDC_GetMainGain(uint8_t* buffer);
void BLDC_GetBackupGain(uint8_t* buffer);
void BLDC_DetermineBackupDirection(void);

extern uint8_t BRAKEMODE;
extern uint8_t BRAKE_PWM;
extern bool EncoderSwitch;

#endif /* BLDC_H_ */