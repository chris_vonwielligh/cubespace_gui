/*
 * spi.h
 *
 * Created: 2015/07/29 09:38:25 AM
 * Author(s): wjordaan/ghjvv
 */ 


#ifndef SPI_H_
#define SPI_H_

#include <stdint.h>
#include <stdbool.h>

extern uint8_t g_LoopErrorFlags;

void SPI_Encoder_Init(void);
uint16_t SPI_Encoder_Read (void);
void SPI_Encoder_Reset(void);
void SPI_Encoder_ClearError (void);
uint8_t SPI_Transmit(unsigned char output);
void SPI_Encoder_Check (void);

#endif /* SPI_H_ */