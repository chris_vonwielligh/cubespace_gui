/*
 * tctlm.h
 *
 * Created: 2015/07/28 09:56:45 AM
 * Author(s): wjordaan/ghjvv
 */ 


#ifndef TCTLM_H_
#define TCTLM_H_

#include <stdint.h>

// Telemetry variables
extern uint16_t g_RunTimeSecond;
extern uint16_t g_RunTimeMilliSecond;
extern int16_t g_WheelSpeed;
//extern int16_t g_WheelSpeedRaw;
extern int16_t g_WheelReference;
extern uint16_t g_WheelCurrent;
extern uint16_t g_DigitalCurrent;
extern int16_t g_WheelDuty;
extern uint16_t g_MCUTemperature;
extern uint8_t g_ControlMode;
extern uint8_t g_BackupMode;
extern uint8_t g_ErrorFlags;
extern uint8_t g_LoopErrorFlags;
extern uint8_t g_MotorSwitch;
extern uint8_t g_EncoderSwitch;
extern uint8_t g_HallSwitch;
extern int16_t g_HallCount;
extern uint8_t g_Overflow_CNT;
extern uint8_t g_EncoderWeight;

void TCTLM_Init(void);

void TCTLM_TCM_ResetMCU(uint8_t* buffer);
void TCTLM_TCM_WheelSpeed(uint8_t* buffer);
void TCTLM_TCM_WheelDutyCycle(uint8_t* buffer);
void TCTLM_TCM_MotorSwitch(uint8_t* buffer);
void TCTLM_TCM_EncoderSwitch(uint8_t* buffer);
void TCTLM_TCM_HallSwitch(uint8_t* buffer);
void TCTLM_TCM_MotorController(uint8_t* buffer);
void TCTLM_TCM_RunMode(uint8_t* buffer);
void TCTLM_TCM_SetSerial(uint8_t* buffer);
void TCTLM_TCM_SetI2CAddress(uint8_t* buffer);
void TCTLM_TCM_SetCANAddress(uint8_t* buffer);
void TCTLM_TCM_ClearErrors(uint8_t* buffer);
void TCTLM_TCM_SetPWMGain(uint8_t* buffer);
void TCTLM_TCM_SetMainGain(uint8_t* buffer);
void TCTLM_TCM_SetBackupGain(uint8_t* buffer);
void TCTLM_TCM_SetEncoderWeight(uint8_t* buffer);

void TCTLM_GetIdentification(uint8_t* buffer);
void TCTLM_TLM_GetExtendedIdentification(uint8_t* buffer);
void TCTLM_GetWheelStatus(uint8_t* buffer);
void TCTLM_GetWheelSpeed(uint8_t* buffer);
void TCTLM_GetWheelReference(uint8_t* buffer);
void TCTLM_GetWheelCurrent(uint8_t* buffer);
void TCTLM_GetErrorFlags(uint8_t* buffer);
void TCTLM_GetLoopErrorFlags(uint8_t* buffer);
void TCTLM_GetWheelData(uint8_t* buffer);
void TCTLM_TLM_GetWheelDataAdditional(uint8_t* buffer);
void TCTLM_TLM_GetPWMGain(uint8_t* buffer);
void TCTLM_TLM_GetMainGain(uint8_t* buffer);
void TCTLM_TLM_GetBackupGain(uint8_t* buffer);
void TCTLM_TLM_GetEncoderWeight(uint8_t* buffer);

#endif /* TCTLM_H_ */