/*
 * interrupt.c
 *
 * Created: 2015/07/21 04:31:50 PM
 * Author(s): wjordaan/ghjvv
 */ 

#include <avr/interrupt.h>

//#include "spi.h"
//#include <util/delay.h>

#include "interrupt.h"
#include "comms.h"
#include "bldc.h"
#include "timer.h"
#include "uart.h"
#include "spi.h"

static uint8_t ControlCounter = 0;
static uint8_t MeasureCounter = 0;
static uint16_t HallCounter = 0;

uint8_t g_Overflow_CNT = 0;
int16_t g_HallCount = 0;
bool CAN_flag=false;
bool I2C_flag=false;

void INTERRUPT_Init(void)
{
	ControlCounter = 0;
	MeasureCounter = 0;
	
	// Enable the Global Interrupt Enable flag so that interrupts can be processed
	sei(); 
}


void INTERRUPT_CheckComms(void)
{
    UART_IRQHandler();
}


// UART Receive Interrupt
ISR(USART0_RX_vect)
{	
	UART_IRQ();
}

// I2C Receive Interrupt
ISR(TWI0_vect)
{
	I2C_IRQHandler();
}

void manual_UART_ISR(void)
{
	if( UCSR0A & (1<<RXC0) )//check flag
	{
		UART_IRQ();
	}
}

// TIMER2 Counter Interrupt
ISR(TIMER2_COMPA_vect)
{	
	//sei(); 
	// Brake PWM generator
	if (BRAKEMODE)
	{
		if (ControlCounter<BRAKE_PWM)
		{
			STARTSTOP_HIGH;
		}
		else
		{
			STARTSTOP_LOW;
		}
        manual_UART_ISR();
	}
	else
	{
		STARTSTOP_LOW;
	}
	
	if(MeasureCounter >= (MEASCOUNT-1))
	{
		// determine speed from encoder
		BLDC_DetermineSpeed();
        manual_UART_ISR();
		// clear measure flag
		MeasureCounter = 0;
	}
	else
	{		
		//increment MeasureCounter
		MeasureCounter++;		
	}
	
	if(ControlCounter >= (CONTROLCOUNT-1))
	{
		// clear control flag
		ControlCounter = 0;
        manual_UART_ISR();
		// set flag for motor control loop
		BLDC_SetControlFlag(true);
	}
	else
	{
		ControlCounter++;	
	}
	
	if(HallCounter >= (HALLCOUNT-1))
	{
		// determine backup speed from Hall sensors
        manual_UART_ISR();
		BLDC_DetermineBackupSpeed();
		
		
		if (EncoderSwitch)
		{
			SPI_Encoder_Check(); //Check if encoder error occurred	
		}	
		
		TIMER_Increment100Milli();
		
		// clear control flag
		HallCounter = 0;		
	}
	else
	{
		HallCounter++;
	}	
}

ISR(TIMER3_OVF_vect)
{
	// Increment Hall sensor pulse counter overflow
	g_Overflow_CNT = g_Overflow_CNT + 1;
}

ISR(INT0_vect)
{	
	CAN_IRQHandler();
}

// Hall sensor A interrupt (rising edge)
ISR(INT1_vect)
{
	BLDC_DetermineBackupDirection();
}